package clarity_Prepay_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class G_Prepay_MOPPayments extends A_Prepay_Common {
	
Logger logger = Logger.getLogger("G_Prepay_MOPPayments.class");
	
	//MOP Options
	public String MOPTotResPointCost; // Total Reservation Point Cost
	public String MOPointsAvail; //Total Member Points Available
	public String MOPBalPointsNeeded; //Balance of Points Needed
	public String MOPTotalPointCost;// Total Points To Apply / Total Cost
	
	//Rent Points
	public int TotalResPointInt; //Convert MOPTotResPointCost
	public int DBUsemembershipPointsInt; //Multiply with 0.6(60%)
	public String UsemembershipPoints;  //Convert DBUsemembershipPoints to String 
	public String RentPoints; //Rent Points
	public int RentPointsInt; // Convert RentPoints to integer
	public int EstimateRentPointsInt; //Estimate Rent Points
	public String EstimateRentPoints ;//Convert the EstimateRentPointsInt to String 
	public String EstimatedTotPointCost; //Convert EstiTotPointCost to String
	public int EstiTotPointCost; //Estimate the Total points to apply
	public String MOPRentalPointCost; //Cost of Rent points
	
	//Retrieve from Prepay_ResortAvailability.Properties
	public String RAPoints;
	
	//Retrieve from Prepay_Reservation.Properties
	public String CRRespoints;
	
	public void ResProperties() {
		 
		 PropertyConfigurator.configure("Log4j.properties");	
			  try (FileReader reader = new FileReader("Prepay_ResortAvailability.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		RAPoints = properties.getProperty("RAPnts");
		      		logger.info("Points from Resort Availability: " +RAPoints);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			  
			  try (FileReader reader = new FileReader("Prepay_Reservation.Properties")) {
		      		Properties properties = new Properties();
			      		properties.load(reader);
			      		 CRRespoints = properties.getProperty("ClubRespoints");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			   
	 }
	
	//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
	
		public void MethodofPaymentOptions() {
						
				ResProperties(); //Call ResProperties Method
					
				logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
				
			if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
			{
					
				try{

			 			//Total Reservation Point Cost / Compared
			 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
					 		Assert.assertEquals(MOPTotResPointCost, RAPoints);
					 			logger.info("Total Reservation Point Cost Matched with Total Point Cost To Book The Reservation From Resort Availability Page : "+MOPTotResPointCost);
			 			
			 			//Total Member Points Available / Compared
					 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
					 		Assert.assertEquals(CRRespoints, MOPointsAvail);
					 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
					 			
			 			//Balance of Points Needed / Compared
			 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
					 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
					 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
					 	
			  			//----------------------------------------------------------------------------------------------------------------------//
					 	//Rent Points 
				 		TotalResPointInt = Integer.parseInt(MOPTotResPointCost); // Convert the total points cost of resevation to integer
				 		DBUsemembershipPointsInt = (int) (TotalResPointInt * 0.6); //Multiply with 0.6
				 			UsemembershipPoints = String.valueOf(DBUsemembershipPointsInt); //Convert the divided double points to String
					 				logger.info("Use Membership Points: "+UsemembershipPoints);
					 				
		 				//Use Membership points
		 			 	driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).click(); //click on Use membership points text box
		 			 		driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).clear(); //Clear the value	
		 			 			driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).sendKeys(UsemembershipPoints); //Enter the value from RentPointsDB
		 			 				logger.info("Entered Use Membership Points: "+UsemembershipPoints);
		 			 					Thread.sleep(2000);
		 			 					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					 			
				 		driver.findElement(By.xpath(".//*[@id='MOPRENPTS']")).click(); //Click on Rent points text box
			 				RentPoints = driver.findElement(By.xpath(".//*[@id='renpoints']")).getAttribute("value"); //Retrieve the rent points
				 				logger.info("Rental Points: "+RentPoints);
				 					RentPointsInt = Integer.parseInt(RentPoints); //Convert into Integer
				 			
				 			//Estimating RentPoints
				 			EstimateRentPointsInt = TotalResPointInt - DBUsemembershipPointsInt; //Estimating Rent Points
				 			
				 			//Compare Rent Points
				 			Assert.assertEquals(RentPointsInt, EstimateRentPointsInt);
				 				logger.info("Rent Points Matched Estimate Rent Points: Assertion Passed");
				 			
				 				
	 			 		//Total Points To Apply / Total Cost / Compared
				 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
						 		Assert.assertEquals(RAPoints, MOPTotalPointCost);
						 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
						 			
						 			//Total Cost
						 			MOPRentalPointCost = driver.findElement(By.xpath(".//*[@id='pvTotalCost']")).getText();
						 				logger.info("Cost of Rent Points is: "+MOPRentalPointCost);
			
				 			
			 			Properties properties = new Properties();
			 			
			 			properties.setProperty("UsemembershipPoints", UsemembershipPoints);
				 		properties.setProperty("RentPoints",RentPoints);
				 		properties.setProperty("MOPRentalPointCost", MOPRentalPointCost);
				 		
 						File file = new File("Prepay_MOPPayments.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Information from Methods of Payment Options");
						fileOut.close();
					}
				
					catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
									
				 	//Process		
					driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
						logger.info("Clicked on Process button");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			}
	

				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
				
			}

	

}
