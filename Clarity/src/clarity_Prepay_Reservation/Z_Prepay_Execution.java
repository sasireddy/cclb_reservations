package clarity_Prepay_Reservation;

import org.testng.annotations.Test;

public class Z_Prepay_Execution {
	
	B_Prepay_Login Prepay_Login = new B_Prepay_Login();
	C_Prepay_Customer360_LeadInfo Prepay_Leadinfo = new C_Prepay_Customer360_LeadInfo();
	D_Prepay_Reservation Prepay_Reservation = new D_Prepay_Reservation();
	E_Prepay_ClubRes Prepay_ClubRes = new E_Prepay_ClubRes();
	F_Prepay_BookReservation Prepay_BookRes = new F_Prepay_BookReservation();
	G_Prepay_MOPPayments Prepay_MOP = new G_Prepay_MOPPayments();
	H_Prepay_ClubReservations Prepay_Reserve = new H_Prepay_ClubReservations();
	I_Prepay_FinalConfirmation Prepay_ResConfirm = new I_Prepay_FinalConfirmation();
	
	@Test
	public void Prepay_Reservation() 
	{
		Prepay_Login.openBrowser();
		Prepay_Leadinfo.Customer360();
		Prepay_Leadinfo.Lead_Information();
		Prepay_Reservation.Reservations();
		Prepay_ClubRes.ClubRes();
		Prepay_BookRes.BookReservation();
		Prepay_BookRes.Resort_Availability();
		Prepay_BookRes.RoomUpgrade_No();
		Prepay_BookRes.Errata();
		Prepay_BookRes.Discount_No();
		Prepay_MOP.MethodofPaymentOptions();
		Prepay_Reserve.Reservation();
		Prepay_ResConfirm.Reservation_Confirmation();
		
	}

}
