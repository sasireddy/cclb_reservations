package clarity_Prepay_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class I_Prepay_FinalConfirmation extends A_Prepay_Common {
	
	Logger logger = Logger.getLogger("I_Prepay_FinalConfirmation.class");
	
	public int RCBeginningPointBal; //Beginning Point balance + Total Points Used for Reservation
	public String RCBegiPntsBal; //Beginning Point Balance
	public int RCBegiPntsBalInt; //Convert RCBegiPntsBal to integer
	public String RCResCost; //Reservation Cost
	public String RCResCt; //Replace "RCResCost" without ","
	public String RCTotalPntsPurchased; //Total Points Purchased
	public String RCTotalPointsPurchased; //Total Points Purchase without ","
	public String RCTotPntsReservation; //Total Points Used for Reservation
	public String RCTotalPointsReservation; //Replace "RCTotPnts" without ","
	public String RCEndPntBal; //Ending points balance
	public String ConfimationMes; //Confirmation Message with Confirmation Number
	
	//Retrieved from Prepay_Reservation.Properties
	public String ClubRespoints;
	public int ClubReservationPointsInt; //Conver ClubRespoints to Integer
	
	//Retrieved from Prepay_ResortAvailability.Properties
	public String RAPoints;
	
	//Retrieve from Prepay_MOPPayments.Properties
	public String RentPoints;
	public int RentPoints1; //Convert RentPoints to integer
	public String MOPRentalPointCost;
	public String UsemembershipPoints;
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	public void ResProperties()
	 {
		
		 PropertyConfigurator.configure("Log4j.properties");	
		 
		 //Prepay_Reservation
		 try (FileReader reader = new FileReader("Prepay_Reservation.Properties")) {
     		Properties properties = new Properties();
	      		properties.load(reader);
	      		ClubRespoints = properties.getProperty("ClubRespoints");
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		
		 //Prepay_ResortAvailability
		  try (FileReader reader = new FileReader("Prepay_ResortAvailability.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		RAPoints = properties.getProperty("RAPnts");
	      		logger.info("Points from Resort Availability: " +RAPoints);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		  
		//Prepay_MOPPayments.Properties
		  try (FileReader reader = new FileReader("Prepay_MOPPayments.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		RentPoints = properties.getProperty("RentPoints");
		      		MOPRentalPointCost = properties.getProperty("MOPRentalPointCost");
		      		UsemembershipPoints = properties.getProperty("UsemembershipPoints");
		      		
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
	 }
	
	public void Reservation_Confirmation()
	{
		PropertyConfigurator.configure("Log4j.properties");	
		
		ResProperties(); //Call the Method
					
		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					
		logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
								
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
			
			//Beginning Points Balance / Compared
			RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //Present Year points
			RCBegiPntsBalInt = Integer.parseInt(RCBegiPntsBal); // Convert RCBegiPntsBal to integer for comparision
			
			ClubReservationPointsInt = Integer.parseInt(ClubRespoints); //Convert ClubRespoints to Integer for Addition
			RentPoints1 = Integer.parseInt(RentPoints); //Convert RentPoints to Integer for addition
			
			RCBeginningPointBal = ClubReservationPointsInt + RentPoints1; //Sum 
				
			Assert.assertEquals(RCBegiPntsBalInt, RCBeginningPointBal); //Comparision
					logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Estimated Beginning Points Balance: "+RCBeginningPointBal);

					//Reservation Cost
					RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost = RCResCost.length(); // Length of RCResCost
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCResCt = RCResCost.replace(",","");
						Assert.assertEquals(RCResCt, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt);
							
						//Total Points Purchased / Compared
						RCTotalPntsPurchased = driver.findElement(By.xpath(".//*[@id='RentedPoints']")).getText();
							RCTotalPointsPurchased = RCTotalPntsPurchased.replace("," ,"");
								Assert.assertEquals(RCTotalPointsPurchased, RentPoints); //Comparing 
									logger.info("Total Points Purchased Displayed and Matched with Rent Points from MOP Screen: "+RCTotalPointsPurchased);
						
						//Total Points Used for Reservation / Compared 
						RCTotPntsReservation = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							RCTotalPointsReservation = RCTotPntsReservation.replace(",","");
								Assert.assertEquals(RCTotalPointsReservation, UsemembershipPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Use Membership Points from MOP Screen: "+RCTotalPointsReservation);
							
						//Estimating Ending Points
							int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = RCBeginningPointBal - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
						//Ending Points Balance	/ Compared
						String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
							int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
								logger.info("Ending Points Balance: "+RCEndPointsbal);
								
							Assert.assertEquals(RCEndPointsbal, EstEndPnts);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
						
					}
					
					else		
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost);
							
								
						//Total Points Purchased / Compared
						RCTotalPntsPurchased = driver.findElement(By.xpath(".//*[@id='RentedPoints']")).getText();
								Assert.assertEquals(RCTotalPntsPurchased, RentPoints); //Comparing 
									logger.info("Total Points Purchased Displayed and Matched with Rent Points from MOP Screen: "+RCTotalPntsPurchased);
						
						//Total Points Used for Reservation
						RCTotPntsReservation = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							Assert.assertEquals(RCTotPntsReservation, UsemembershipPoints); //Comparing 
								logger.info("Total Points Used for Reservation Displayed and Matched with Use Membership Points from MOP Screen: "+RCTotPntsReservation);
										
						//Estimating Ending Points
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = RCBeginningPointBal - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	
							RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");			
							
					}
					
					//Confirmation Message
					ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
						logger.info(""+ConfimationMes);
						
						//Save to Confirmation.Properties File
						try{
						String ResConNum = ConfimationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
						logger.info("Reservation Confirmation number is: "+ResConNum);
						
						Properties properties = new Properties();
				 		properties.setProperty("ConfirmationNo", ResConNum);
				 		
				 		File file = new File("Prepay_Reservation_Confirmation.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Reservation Confirmation Number");
						fileOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
 			}				
}
