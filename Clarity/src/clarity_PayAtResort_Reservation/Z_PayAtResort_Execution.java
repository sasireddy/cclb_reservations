package clarity_PayAtResort_Reservation;

import java.sql.SQLException;

import org.testng.annotations.Test;

public class Z_PayAtResort_Execution {

	B_PayAtResort_Login PayAtResort_Login = new B_PayAtResort_Login();
	C_PayAtResort_Customer360_LeadInfo PayAtResort_Leadinfo = new C_PayAtResort_Customer360_LeadInfo();
	D_PayAtResort_Reservation PayAtResort_Reservation = new D_PayAtResort_Reservation();
	E_PayAtResort_ClubRes PayAtResort_ClubRes = new E_PayAtResort_ClubRes();
	F_PayAtResort_BookReservation PayAtResort_BookRes = new F_PayAtResort_BookReservation();
	G_PayAtResort_MOPPayments PayAtResort_MOP = new G_PayAtResort_MOPPayments();
	H_PayAtResort_ClubReservations PayAtResort_Reserve = new H_PayAtResort_ClubReservations();
	I_PayAtResort_FinalConfirmation PayAtResort_ResConfirm = new I_PayAtResort_FinalConfirmation();
	J_PayAtResort_DatabaseConfirmation PayAtResort_DbConfirm = new J_PayAtResort_DatabaseConfirmation();
	
	@Test
	public void PayAtResort_Reservation() throws SQLException 
	{
		PayAtResort_Login.openBrowser();
		PayAtResort_Leadinfo.Customer360();
		PayAtResort_Leadinfo.Lead_Information();
		PayAtResort_Reservation.Reservations();
		PayAtResort_ClubRes.ClubRes();
		PayAtResort_BookRes.BookReservation();
		PayAtResort_BookRes.Resort_Availability();
		PayAtResort_BookRes.Errata();
		PayAtResort_MOP.MethodofPaymentOptions();
		PayAtResort_Reserve.Reservation();
		PayAtResort_ResConfirm.Reservation_Confirmation();
		PayAtResort_DbConfirm.DatabaseConfirm();
	}

}
