package clarity_PayAtResort_Reservation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A_PayAtResort_Common {
	
	static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 100);
	
	String Leadid1 = "15162280"; //MGV //Add Points to present year 59-3121941

	//-----------------------------------------------------------------Book New Reservation------------------------------------------------------------//
	//Book New Reservation
	String BookingTypeReservation = "32789"; //Home Standard Reservation Value
	String BookTypeReserName = "Monarch Grand Vacations";
	
	//-----------------------------------------------------------------Search for Availability------------------------------------------------------------//
	//Search for Availability
	String SARegion = "NV"; //Region as Arizona 
	String SAProperty = "CAN"; //Property as Cancun Resort Las Vegas
	String SAArrivalDateMethod = "FlexDays"; //Arrival Date Method as Flexible Days
	String SAArrivalDate = "t"; //Arrival Date 
	String SANights = "3";
	String SAAdults = "2";
	
	
	//-----------------------------------------------------------------Resort Availability - CLUB------------------------------------------------------------//
	//Resort Availability - CLUB
	String ResortType = "1XX"; //Resort Type SubString
	
	//-----------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------//
	//Additional Resort Information (Errata)
	String ErrataHeader = "Additional Resort Information (Errata)"; //
	
	//-----------------------------------------------------------------Discount------------------------------------------------------------//
	//Discount
	String DiscountHeader = "Discount";
	String DisMesAvail = "Member has sufficient points to book this reservation.";
	String DisMesNotAvail = "Member does not have sufficient points to book this reservation.";
	String DiscountPercentage = "25";
	String DiscountYesMessage = "Selected Yes For Discount";
	
	//-----------------------------------------------------------------Method of Payments------------------------------------------------------------//
	//Method of Payments
	String MOPBalPointsNd = "0"; //Balance of Points needed
	
	//-----------------------------------------------------------------Club Reservations------------------------------------------------------------//
	
	//Reservation and Misc Fees
	String CRResAndMiscFees = "$ 0.00";
	String CRReserMiscFeesHeader = "Reservation and Misc Fees"; //Reservation and Misc Fees Flag Header
		String CRPayInfoHeader = "Payment Information";
			String CreditCardMainHeader = "Credit Card Maintenance";
				String CCNumber = "4012001037141112";
					String CCType = "Visa";
						String CCName = "Diamond Tester";
							String ExpMonth = "01";
								String ExpYr = "20";

	//-----------------------------------------------------------------Reservation Confirmation------------------------------------------------------------//
								
	String Message = "Thank you for your payment. Your payment request was sent.";
	String ResConfirmationHeader = "Reservation Confirmation"; //Reservation Confirmation Header
	String RCPnSumHeader = "Points Summary";
	String RCConfirmLetterHeader = "Confirmation Letter";
	
}

