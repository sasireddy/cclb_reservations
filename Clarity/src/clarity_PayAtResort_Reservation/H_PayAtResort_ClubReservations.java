package clarity_PayAtResort_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class H_PayAtResort_ClubReservations extends A_PayAtResort_Common {

	Logger logger = Logger.getLogger("H_PayAtResort_ClubReservations.class");

	//-------------------------------Reservation---------------------------//
		public String CRReservationHeader; //Reservation Header
		public String CRBookFunc; //Booking Function
		public String CRMarkeyCode; //Marketing Key Code
		public String CRMarKeyCodeFlagHeader; //Marketing Key Code Flag Header
		public String CResType; //Reservation Type
		public String CResStatus; //Reservation Status
		public String CRProperty; //Property
		public String CRPropFlagHeader; //Property Flag Header
		public String CRArrDate; //Arrival Date
		public String CRNights; //Nights
		public String CRNigFlagHeader; //Nights Flag Headr (Inventory)
		public String CRDepDate; //Departure Date
		public String CRGuaCode; //Guarantee Code
		public String CRUsagePeriod; //Usage Period
		public String CRMemshipNum; //Membership Number
		public String NoofGues; //Number of Guests
		public String CRRmType; //RoomType
		public String CRRoomType; //Substring for CRRmType
		public String CRRoomTypeFlagHeader; //RoomType Flag Header
		
		//Financial Information
		public String strNightOne; //Night
		public String strNightOnePoints; //Points for 1st
		public String strNightTwo; //Night
		public String strNightTwoPoints; //Points for 2nd
		public String strNightThree; //Night
		public String strNightThreePoints; //Points for 3rd
		public String TotalPointsNights; //Total Points for 3 nights
		public String NightRateOne; //Total Rate for 1st night
		public String NightRateTwo; //Total Rate for 2nd night
		public String NightRateThree; //Total Rate for 3rd night
		public String Zero = "0.00"; //Night rate information 
		
		//Retrieve from PayAtResort_ResortAvailability.Properties
		public String RAResortName; //ResortName
		public String RAArrDate; //Arrival Date
		public String RADepDate; //Departure Date
		public String RARoomType; //Room Type
		
		//Retrieve from PayAtResort_Reservation.Properties
		public String MembershipNumber;
		
		//Retrieve from PayAtResort_Res_Leadid.Properties
		public String Lead_Id;
		
		//Retrieve from PayAtResort_MOPPayments.Properties
		public String UsemembershipPoints;
		public String MOPRentalPointCost;
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
		
		public void ResProperties() {
			 
			 PropertyConfigurator.configure("Log4j.properties");	
			
			 	//PayAtResort_ResortAvailability.Properties
				  try (FileReader reader = new FileReader("PayAtResort_ResortAvailability.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		RAResortName = properties.getProperty("RAResortName");
			      			RAArrDate = properties.getProperty("RAArrDate");
			      				RADepDate = properties.getProperty("RADepDate");
			      					RARoomType = properties.getProperty("RARoomType");
			      				
			      				
		      			logger.info("Resort Name from Resort Availability: " +RAResortName);
		      				logger.info("Arrival Date from Resort Availability: " +RAArrDate);
		      					logger.info("Departure Date from Resort Availability: " +RADepDate);
		      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				  
			  //PayAtResort_Reservation.Properties
				  try (FileReader reader = new FileReader("PayAtResort_Reservation.Properties")) {
						 Properties properties = new Properties();
				      		properties.load(reader);
				      		MembershipNumber = properties.getProperty("MembershipNumber");	
			      			logger.info("Membership Number: " +MembershipNumber);
			      			
				      	} catch (IOException e) {
				      		e.printStackTrace();
				      	}
				  
			//PayAtResort_Res_Leadid.Properties
			  try (FileReader reader = new FileReader("PayAtResort_Res_Leadid.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		Lead_Id = properties.getProperty("LILeadid");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			  
			//PayAtResort_MOPPayments.Properties
			  try (FileReader reader = new FileReader("PayAtResort_MOPPayments.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		UsemembershipPoints = properties.getProperty("UsemembershipPoints");
			      		MOPRentalPointCost = properties.getProperty("MOPRentalPointCost");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
		}	  
		
		
		public void Reservation() {
			
		ResProperties(); //Calling Method
			
		 PropertyConfigurator.configure("Log4j.properties");
				
		    logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
			
			//-----------------------------------------------------------------------Reservation:------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Reservation------------------------------------------------------------");
			
					
			//Booking Function / Compared
			CRBookFunc = driver.findElement(By.xpath(".//*[@id='ResSum_BookingPurpose']")).getText();
				Assert.assertEquals(CRBookFunc, BookTypeReserName);
					logger.info("Booking Fuction Displayed and Passed Assertion: "+CRBookFunc);
					
			//Marketing KeyCode / Compared
			CRMarkeyCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
				logger.info("Marketing Key Code: "+CRMarkeyCode);
					
					
			//ReservationType
			CResType = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[4]/td[2]")).getText();
				logger.info("Reservation Type is: "+CResType);
				
			//Reservation Status
			CResStatus = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]")).getText();
				logger.info("Reservation Status: "+CResStatus);
				
			//Property / Compared
			CRProperty = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText();
				Assert.assertEquals(CRProperty, RAResortName);
					logger.info("Property displayed and Matched From Resort Availabilty screen: "+CRProperty);
					
					//Property Flag / Compared
					driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[6]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on The flag image
						logger.info("Clicked on Property Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the Close button is present 
								CRPropFlagHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
									Assert.assertEquals(CRPropFlagHeader, ErrataHeader);
										logger.info("Property Flag Header displayed and Matched: "+CRPropFlagHeader);
											driver.findElement(By.id("REWbtnClose")).click(); //Click on Close button

			//ArrivalDate / Compared
			CRArrDate = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[7]/td[2]")).getText();
				Assert.assertEquals(CRArrDate, RAArrDate);
					logger.info("Arrival Date displayed and Matched From Resort Availabilty screen: "+CRArrDate);
			
			//Nights / Compared
			CRNights = driver.findElement(By.xpath(".//*[@id='ResSum_pnNights']")).getAttribute("value");
				Assert.assertEquals(SANights, CRNights);
					logger.info("Number of Nights Displayed and Matched Assertion: "+CRNights);
													
			//Departure Date / Compared
			CRDepDate = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value");
				Assert.assertEquals(CRDepDate, RADepDate);
					logger.info("Departure Date displayed and Matched from Resort Availability Screen: "+CRDepDate);		
					
			//Guarantee Code
			CRGuaCode = driver.findElement(By.xpath(".//*[@id='ResSum_pvGuarantee_input']")).getAttribute("value");
				logger.info("Guarantee Code: "+CRGuaCode);
				
		/*	//Usage Period
			CRUsagePeriod = driver.findElement(By.xpath(".//*[@id='ResSum_UsagePeriod']")).getText();
				logger.info("Usage Period: "+CRUsagePeriod);
			
			//Membership Number / Compared
			CRMemshipNum = driver.findElement(By.xpath(".//*[@id='ResSum_MemberContrID']")).getText();
				logger.info("MemberShip Number: "+CRMemshipNum);
					Assert.assertEquals(CRMemshipNum, MembershipNumber);
						logger.info("Membership Number Matched from ClubRes (Membership Detail for:): "+CRMemshipNum);		
			
			//Number of Guests / Compared
			NoofGues = driver.findElement(By.xpath(".//*[@id='ResSum_Adults']")).getAttribute("value");	
				Assert.assertEquals(SAAdults, NoofGues);
					logger.info("Number of Adults Displayed and Matched in Search of Availability Screen: "+NoofGues);
			
			//Roomtype / Compared
			CRRmType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
				CRRoomType = CRRmType.substring(0, 3);
					logger.info("Room Type: " +CRRoomType);
						Assert.assertEquals(CRRoomType, RARoomType);
							logger.info("Room Type Displayed and Matched from RoomUpgrade Screen: "+CRRoomType);
			
			
				//Room Type Flag 
				driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Roomtype Flag
					logger.info("Clicked on Room Type Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the Close button is present
							CRRoomTypeFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
								logger.info(""+CRRoomTypeFlagHeader);
									driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();*/
									

		//----------------------------------------------------------------------Financial Information-----------------------------------------------------------------------------------------//
		
				logger.info("-------------------------------------------------------------------Financial Information------------------------------------------------------------");	
				
				//Rate Amount(Curr/Pts):
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[3]/td[2]/div/span[2]/img")));
					driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[3]/td[2]/div/span[2]/img")).click();
						logger.info("Clicked on Rate Amount Flag");
				
						
						try{
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]"))); //wait till close button is clickable
						
								//Number of Nights and Points
								strNightOne = driver.findElement(By.xpath(".//*[@id='RATE1']/td[1]")).getText(); //Date
												logger.info("Night One: "+strNightOne);
												
								strNightOnePoints = driver.findElement(By.xpath(".//*[@id='RATE1']/td[2]")).getText();
														logger.info("Points: "+strNightOnePoints); //Points
														
														
									strNightTwo = driver.findElement(By.xpath(".//*[@id='RATE2']/td[1]")).getText(); //Date
													logger.info("Night Two: "+strNightTwo);
									strNightTwoPoints = driver.findElement(By.xpath(".//*[@id='RATE2']/td[2]")).getText();
															logger.info("Points: "+strNightTwoPoints); //Points
										
											strNightThree = driver.findElement(By.xpath(".//*[@id='RATE3']/td[1]")).getText(); //Date
															logger.info("Night Three: "+strNightThree);
															
											strNightThreePoints = driver.findElement(By.xpath(".//*[@id='RATE3']/td[2]")).getText();
																	logger.info("Points: "+strNightThreePoints); //Points
																	
								//Total Points
									TotalPointsNights = driver.findElement(By.xpath(".//*[@id='RATEPOINTTOTAL']")).getText();
										logger.info("Total Points for 3 nights: " +TotalPointsNights);
										
										Assert.assertEquals(TotalPointsNights, UsemembershipPoints);
											logger.info("MemeberPoints Used from MOP Screen Matched with Night Rate Information");
										
								//Rate
									NightRateOne = driver.findElement(By.xpath(".//*[@id='RATE1']/td[3]")).getText();
										logger.info("Rate for 1st night with out tax: "+NightRateOne);
											

									NightRateTwo = driver.findElement(By.xpath(".//*[@id='RATE2']/td[3]")).getText();
										logger.info("Rate for 2nd night with out tax: "+NightRateTwo);
										
									
									NightRateThree = driver.findElement(By.xpath(".//*[@id='RATE3']/td[3]")).getText();
										logger.info("Rate for 3rd nights with out tax: "+NightRateThree);
										
									Properties properties = new Properties();
							 			properties.setProperty("strNightOnePoints", strNightOnePoints);
							 				properties.setProperty("strNightTwoPoints", strNightTwoPoints);
							 					properties.setProperty("strNightThreePoints", strNightThreePoints);
							 						properties.setProperty("NightRateOne", NightRateOne);
							 							properties.setProperty("NightRateTwo", NightRateTwo);
							 								properties.setProperty("NightRateThree", NightRateThree);
										
							 					driver.findElement(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]")).click(); //Click on Close Button
										
										
								
						
					 		
					 		File file = new File("PayAtResort_NightInformation.Properties");
							FileOutputStream fileOut = new FileOutputStream(file);
							properties.store(fileOut, "Reservation Confirmation Number");
							fileOut.close();
										
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
			
				//Click on SAVE Button
				driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
						logger.info("Clicked on SAVE BUTTON");
		}			
						
}

