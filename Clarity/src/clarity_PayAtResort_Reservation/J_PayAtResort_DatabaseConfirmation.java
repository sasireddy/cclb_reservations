package clarity_PayAtResort_Reservation;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class J_PayAtResort_DatabaseConfirmation {
	
	Logger logger = Logger.getLogger("J_PayAtResort_DatabaseConfirmation.class");
	
	public Connection connection = null;
	public  PreparedStatement prepared = null;
	
	//PayAtResort_NightInformation
	public String strNightOnePoints; //Points for 1st
	public String strNightTwoPoints; //Points for 2nd
	public String strNightThreePoints; //Points for 3rd
	public String NightRateOne; //Total Rate for 1st night
	public String NightRateTwo; //Total Rate for 2nd night
	public String NightRateThree; //Total Rate for 3rd night
	public String strZero = "0";
	

	//PayAtResort_Reservation_Confirmation
	public String ConfirmationNo; //Reservation Confirmation Number
	
	 @Test
	public void DatabaseConfirm() throws SQLException {
		 
		 PropertyConfigurator.configure("Log4j.properties");

		//PayAtResort_NightInformation
		  try (FileReader reader = new FileReader("PayAtResort_NightInformation.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		strNightOnePoints = properties.getProperty("strNightOnePoints");
	      		strNightTwoPoints = properties.getProperty("strNightTwoPoints");
	      		strNightThreePoints = properties.getProperty("strNightThreePoints");
	      		NightRateOne = properties.getProperty("NightRateOne");
	      		NightRateTwo = properties.getProperty("NightRateTwo");
	      		NightRateThree = properties.getProperty("NightRateThree");
	      		
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}

		//PayAtResort_Reservation_Confirmation
		  try (FileReader reader = new FileReader("PayAtResort_Reservation_Confirmation.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		ConfirmationNo = properties.getProperty("ConfirmationNo");
	      		
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		  
	 //Initialize
		try {
	
			Class.forName("oracle.jdbc.driver.OracleDriver");
	
		} catch (ClassNotFoundException e) {
	
			logger.info("Oracle JDBC server not found");
			e.printStackTrace();
			return;
		}
		
		logger.info("Oracle JDBC Driver Registered!");
	
		//Connect to Database
		try {
			
			connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1550))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "sreddy","bhushan22");
	
			connection = DriverManager.getConnection("jdbc:oracle:thin:@dc1-ora-ddba26.global.ldap.wan:1541:STAGE1", "sreddy","bhushan22"); //STage
			
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@10.127.103.155:1541:TEST1", "sreddy","bhushan22"); //Test
	
		} catch (SQLException e) {
	
			logger.info("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	
		//Retrive data
		if (connection != null) {
			logger.info("Logged into STAGE DB Successfully");
			
			//Query
			prepared = connection.prepareStatement("select PRU.RESV_num, PRU.RESV_EFF_DATE , PRU.RESV_END_DATE , PRU.RATE_CODE, PRU.RATE_AMOUNT, PRU.PTS_AMT, PRU.OVERRIDE_AMT, PRU.OVERRIDE_PTS, PRU.PM_UNIT_NUM, PRU.PM_UNIT_TYPE_ID,PRU.CTRL_ACCESS_ID, PPUT.PM_UNIT_TYPE_DESC, PPUT.LOCK_OFF, PR.UPGRADE_COUNT from premier.p_resv_unit pru inner join p_pm_unit_type pput on PRU.PM_UNIT_TYPE_ID = PPUT.PM_UNIT_TYPE_ID INNER JOIN P_RESERVATION PR ON PRU.RESV_NUM = PR.RESV_NUM where PRU.RESV_NUM = ? order by PRU.RESV_NUM, PRU.RESV_EFF_DATE");
			//Enter Confirmation Number in Query
			prepared.setString(1, ConfirmationNo);
			//Execute the query
			prepared.executeUpdate();

			System.out.printf("%10s%23s%20s%18s%n", "-------------", "-------", "------------", "------------");
			System.out.printf("%10s%23s%20s%18s%n", "RESV_EFF_DATE", "PTS_AMT", "OVERRIDE_AMT", "OVERRIDE_PTS");
			System.out.printf("%10s%23s%20s%18s", "-------------", "-------", "------------", "------------");
			
			//Add to Array
			ArrayList<String> Points = new ArrayList<String>();
			Points.add(strNightOnePoints);
			Points.add(strNightTwoPoints);
			Points.add(strNightThreePoints);
			
			//Add to Array
			ArrayList<String> Rate = new ArrayList<String>();
			Rate.add(NightRateOne);
			Rate.add(NightRateTwo);
			
			Rate.add(NightRateThree);
			

			Iterator<String> NgtPoints = Points.iterator();
			Iterator<String> NgtRate = Rate.iterator();
		
			while(NgtPoints.hasNext()) {
				
				while(NgtRate.hasNext()) {
					
					while (prepared.getResultSet().next()) {
						
						DecimalFormat decimal = new DecimalFormat("0.00"); //Decimal Format.  Decimal Format is used to match the retrieved values from databse to match with values retrieved from clarity.																			 //Example: Clarity value = 0.00, Oracle value is 0. To match both values the oracle value will be changed to 0.00.
						
						String Date = prepared.getResultSet().getString("RESV_EFF_DATE"); //Get RESV_EFF_DATE
		
						String PTS_AMT = prepared.getResultSet().getString("PTS_AMT"); //Get PTS_AMT
													
						String OVERRIDE_AMT =  prepared.getResultSet().getString("OVERRIDE_AMT"); //Get OVERRIDE_AMT
						double OVERIDE_AMNT = Double.valueOf(OVERRIDE_AMT);
						String OVERRIDE_AMOUNT = decimal.format(OVERIDE_AMNT);
				
							
						String OVERRIDE_PTS =  prepared.getResultSet().getString("OVERRIDE_PTS"); //Get OVERRIDE_PTS
						
						System.out.printf("%n%10s%15s%15s%20s%n",Date,PTS_AMT,OVERRIDE_AMOUNT,OVERRIDE_PTS);
						
						System.out.println();
						
						if(OVERRIDE_AMT.equals(strZero)) //If the Overide Amount is equal to 0
						{
							String PntAmnt = NgtPoints.next(); //Iterate next 
							
							Assert.assertEquals(PTS_AMT, PntAmnt); //Compare Points Amount with Iterate next 
							
								logger.info("Points Amount: "+PntAmnt); //Print the Iterated Value
								
								logger.info("Passed the Points Amount: "+Date+ "----"+PntAmnt); //Print Points amount value with date and iterated value
								
									String RteAmnt = NgtRate.next(); //Iterate the Rate Amount as well so that if, ELSE occurs in the next loop the value will be iterated to next value.
									
										logger.info("Rates: "+RteAmnt);
						}
						else
						{
							String PntAmnt = NgtPoints.next(); //Iterate
							
							String RteAmnt = NgtRate.next(); //Iterate

							Assert.assertEquals(OVERRIDE_PTS, PntAmnt); //Compare the OVERRIDE_AMT 

							logger.info("Passed the OVERRIDE_PTS Amount: "+Date+ "----"+PntAmnt);
							
							Assert.assertEquals(OVERRIDE_AMOUNT, RteAmnt); //Compare the OVERRIDE _PNTS
							
							logger.info("Passed the OVERRIDE_AMOUNT Amount: "+Date+ "----"+RteAmnt);
						
						}
				
					} 
				}
			}
		}
	
		else {
		logger.info("Failed to make connection!");
		}
	}

}

