package clarity_Modify_Reservation;

import org.testng.annotations.Test;


public class Z_Modify_Execution {
	
	B_Modify_Login Modify_Login = new B_Modify_Login();
	C_Modify_Customer360_LeadInfo Modify_Leadinfo = new C_Modify_Customer360_LeadInfo();
	D_Modify_Reservation Modify_Reservation = new D_Modify_Reservation();
	E_Modify_ClubRes Modify_ClubRes = new E_Modify_ClubRes();
	F_Modify_BookReservation Modify_BookRes = new F_Modify_BookReservation();
	G_Modify_MOPPayments Modify_MOP = new G_Modify_MOPPayments();
	H_Modify_ClubReservations Modify_Reserve = new H_Modify_ClubReservations();
	I_Modify_FinalConfirmation Modify_ResConfirm = new I_Modify_FinalConfirmation();
	J_Modify_ModifyReservation Modify = new J_Modify_ModifyReservation();
	
	
	@Test
	public void ModifyReservation()
	{
		Modify_Login.openBrowser();
		Modify_Leadinfo.Customer360();
		Modify_Leadinfo.Lead_Information();
		Modify_Reservation.Reservations();
		Modify_ClubRes.ClubRes();
		Modify_BookRes.BookReservation();
		Modify_BookRes.Resort_Availability();
		Modify_BookRes.RoomUpgrade_No();
		Modify_BookRes.Errata();
		Modify_BookRes.Discount_No();
		Modify_MOP.MethodofPaymentOptions();
		Modify_Reserve.Reservation();
		Modify_ResConfirm.Reservation_Confirmation();
		Modify.Modify_Reservation();
	}
			
}


