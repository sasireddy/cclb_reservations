package clarity_Modify_Reservation;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class G_Modify_MOPPayments extends A_Modify_Common {
	
Logger logger = Logger.getLogger("G_Modify_MOPPayments.class");
	
	//MOP Options
	public String MOPTotResPointCost; // Total Reservation Point Cost
	public String MOPointsAvail; //Total Member Points Available
	public String MOPBalPointsNeeded; //Balance of Points Needed
	public String MOPTotalPointCost;// Total Points To Apply / Total Cost
	
	//Retrieve from Modify_ResortAvailability.Properties
	public String RAPoints;
	
	//Retrieve from Modfiy_Reservation.Properties
	public String CRRespoints;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	
	 public void ResProperties() {
		 
		 PropertyConfigurator.configure("Log4j.properties");	
			  try (FileReader reader = new FileReader("Modify_ResortAvailability.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		RAPoints = properties.getProperty("RAPnts");
		      		logger.info("Points from Resort Availability: " +RAPoints);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			  
			  try (FileReader reader = new FileReader("Modify_Reservation.Properties")) {
		      		Properties properties = new Properties();
			      		properties.load(reader);
			      		 CRRespoints = properties.getProperty("ClubRespoints");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			   
	 }
	
	//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
	
		public void MethodofPaymentOptions() {
						
				ResProperties(); //Call ResProperties Method
					
				logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
				
				if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
				{
					
			 			//Total Reservation Point Cost / Compared
			 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
					 		Assert.assertEquals(MOPTotResPointCost, RAPoints);
					 			logger.info("Total Reservation Point Cost Matched with Total Point Cost To Book The Reservation From Resort Availability Page : "+MOPTotResPointCost);
			 			
			 			//Total Member Points Available / Compared
					 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
					 		Assert.assertEquals(MOPointsAvail, CRRespoints);
					 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
					 			
			 			//Balance of Points Needed / Compared
			 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
					 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
					 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
				
						//Total Points To Apply / Total Cost / Compared
			 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
					 		Assert.assertEquals(MOPTotalPointCost, RAPoints);
					 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
					 			
					 	//Process		
						driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
							logger.info("Clicked on Process button");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
				}

				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
				
			}

}