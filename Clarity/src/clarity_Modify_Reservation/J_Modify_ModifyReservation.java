package clarity_Modify_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class J_Modify_ModifyReservation extends A_Modify_Common {
	
	Logger logger = Logger.getLogger("J_Modify_ModifyReservation.class");
	
	public String winHandleBefore; //Store Current Window Handle
	public String RAPoints; //Retrive Cost of Reservation from Modify_ResortAvailability.Properites
	public String ClubRespoints; //Retrive Points Available for this year from Modify_Reservation.Properties
	public String ResLeadId; //Reservation LeadId
	public String ResConNum; //Retrieve Reservation Confirmation Number
	public String ModResAlert; //Modify Reservation Alert Header
	public String MOPTotResPointCost; // Total Reservation Point Cost
	public String MOPointsAvail; //Total Member Points Available
	public String MOPBalPointsNeeded; //Balance of Points Needed
	public String MOPTotalPointCost;// Total Points To Apply / Total Cost
	
	public String RCBegiPntsBal; //Beginning Point Balance
	public String RCResCost; //Reservation Cost
	public String RCResCt; //Replace "RCResCost" without ","
	public String RCTotPnts; //Total Points Used for Reservation
	public String RCTotalPoints; //Replace "RCTotPnts" without ","
	public String RCEndPntBal; //Ending points balance
	public String ConfimationMes; //Confirmation Message with Confirmation Number
	public String NewResConfirmNum; //New Confirmation Number
	
	public void Modify_Reservation() {
		
			//Wait for 3 Seconds
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
			//Get the Window Handle 
			try (FileReader reader = new FileReader("Modify_WindowHandle.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		
      		winHandleBefore	 = properties.getProperty("winHandleBefore");
	      		logger.info(winHandleBefore);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}

		
			driver.switchTo().window(winHandleBefore);
			
			logger.info("-----------------------------------------Modify Reservation-----------------------------------------------------");
					
			 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
		 		logger.info("clicked on Reservations Tab");
		 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	 			
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//				 			
			//Retrieve Confirmation Number From Modify_Reservation_Confirmation Properties File
			try (FileReader reader = new FileReader("Modify_Reservation_Confirmation.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		ResConNum = properties.getProperty("ResConNum");
		      			logger.info(ResConNum);
		      			
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			
			//Retrieve Cost of Reservation Points From Modify_ResortAvailability.Properites
			try (FileReader reader = new FileReader("Modify_ResortAvailability.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		RAPoints = properties.getProperty("RAPnts");
		      			logger.info("Cost of Reservation is: "+RAPoints+" Points");
		      			
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			
			//Retrieve Cost of Reservation Points From Modify_Reservation.Properites
			try (FileReader reader = new FileReader("Modify_Reservation.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		ClubRespoints = properties.getProperty("ClubRespoints");
		      			logger.info("Points Available to Book Reservation This Year: "+ClubRespoints);
		      			
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}	
	      	
			
			WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody")); //Table Tbody
			List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
				//logger.info("No.of rows: " +columns.size());
					int rowsnum = columns.size();
							String xpath =null;
								String ResNum;
									
										for(int i = 1;i<=rowsnum;i++)
										{
											xpath= ".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
												ResNum = driver.findElement(By.xpath(xpath)).getText();
														logger.info("Substring is: "+ResNum);
													
												driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
												
												if(ResConNum.equals(ResNum))
												{
													logger.info("Confirmation Number Found and Matcehd from the Reservation Confirmation Screen");
													
														driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
															logger.info("Clicked on the Flag image of the Confirmation Number");
															break;
												}
										}
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RMS_btnSave']"))); // Wait till the Save Res button is Clickable
			 
			//------------------------------------------------Modify Reservation--------------------------------------------------------------------------------//
			
			logger.info("-----------------------------------------Modify Reservation---------------------------------------------");
			
			//Select Property
			driver.findElement(By.xpath(".//*[@id='ResSum_pvProperty_input']")).click(); //Click on Property
				Select Property = new Select(driver.findElement(By.id("ResSum_pvProperty_select")));
					Property.selectByValue(ResSummaryProperty); //Select by Value
						logger.info("Selected Property: "+ResSummaryProperty);
							
						//Click on Nights Flag and Close
						driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[8]/td[2]/table/tbody/tr/td[2]/img")).click();
							driver.findElement(By.xpath(".//*[@id='RNWbtnClose']")).click();
							
			//Select RoomType
			driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).click(); //Click on Roomtype
			
				//Wait for 2 seconds
				try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			
			driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_select']/option[5]")).click(); //Select RoomType	
				logger.info("RoomType Selected");
			
			//Decline the Purchase of ARPP
			driver.findElement(By.xpath(".//*[@id='declineRPP']")).click();
				logger.info("Declined ARPP");
			
			//Save Reservation
			driver.findElement(By.xpath(".//*[@id='RMS_btnSave']")).click(); 
				logger.info("Clicked on Save");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ContCancelRebookBtn']"))); //Wait till the continue button displays and clickable
				
					ModResAlert = driver.findElement(By.xpath(".//*[@id='CancelPenaltyDiv']/table/tbody/tr/td")).getText();
						Assert.assertEquals(ModResAlert, ModifyResAlertHeader);
						logger.info("Header: "+ModResAlert+" Matched and passed the Assertion");
						
			//Override Points Penalty Amount To:
			driver.findElement(By.xpath(".//*[@id='tdOverridePenalty']")).sendKeys(OverridePenaltyAmnt);
				logger.info("Override Points Penalty Amount To: " +OverridePenaltyAmnt);
						
			//Click on Continue
			driver.findElement(By.xpath(".//*[@id='ContCancelRebookBtn']")).click();

							
			//Discount
			driver.findElement(By.xpath(".//*[@id='confirmDscNoBtn']")).click();
				logger.info("Clicked on No for Discount");
				
			//-------------------------------------------------------------------------------------------------------------------------------------------------------//	
			//Estimation
			int EstRntPnts = Integer.parseInt(RAPoints); //Cost of Reservation Points Convert to Integer
			
				int EstClubResPoints = Integer.parseInt(ClubRespoints); // Present Year Points convert to integer 
		
					int EstMemPointsAvail = EstClubResPoints + EstRntPnts;
				
						String EstMemberPointsAvail = String.valueOf(EstMemPointsAvail); //Convert to String 
				
			//Method of Payment Options
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
								
			 			//Total Reservation Point Cost / Compared
			 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
				 			logger.info("Total Reservation Point Cost: "+MOPTotResPointCost);
			 			
			 			//Total Member Points Available / Compared
					 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
					 		Assert.assertEquals(MOPointsAvail, EstMemberPointsAvail);
					 			logger.info("Total Member Points Available Matched with Estimated Total Member Points Available: "+MOPointsAvail);
					 				
			 			//Balance of Points Needed / Compared
			 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
					 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
					 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
							 			
						//Total Points To Apply / Total Cost / Compared
			 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
			 				Assert.assertEquals(MOPTotalPointCost, MOPTotResPointCost);
					 			logger.info("Total Points To Apply / Total Cost Matched with Total Reservation Point Cost: "+MOPTotalPointCost);
					 			
					 	//Process		
						driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
							logger.info("Clicked on Process button");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								
			//-------------------------------------------------------------------------------------------------------------------------------------------------------//					
			//Reservation Confirmation
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
				int OverRidePay = Integer.parseInt(OverridePenaltyAmnt);
				int EstBeginPntBal = EstClubResPoints - OverRidePay;
					
					String EstBegPointBalance = String.valueOf(EstBeginPntBal); //Convert to String 
			
						logger.info("Estimated Beginning Point Balance: "+EstBeginPntBal);
				
				//Beginning Points Balance / Compared
				RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //Available points
					Assert.assertEquals(RCBegiPntsBal, EstBegPointBalance);
						logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Estimated Beginning Point Balance: "+EstBegPointBalance);

						//Reservation Cost
						RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
						int LengthofRCResCost = RCResCost.length(); // Length of RCResCost
						String S = "1,000";
						int S1 = S.length(); // Length of S String
						
						if(LengthofRCResCost == S1) //If Size is the same then take out "," and follows the rest of the process
						{
							//Reservation Cost / Compared 
							RCResCt = RCResCost.replace(",","");
							Assert.assertEquals(RCResCt, MOPTotResPointCost); //Comparing 
								logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Method of Payment Options Screen: "+RCResCt);
										
							//Total Points Used for Reservation / Compared 
							RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
								RCTotalPoints = RCTotPnts.replace(",","");
									Assert.assertEquals(RCTotalPoints, MOPTotResPointCost); //Comparing 
										logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Method of Payment Options Screen: "+RCTotalPoints);
								
							//Estimating Ending Points
							int ResPnts = Integer.parseInt(RCResCt); //Conver String to Int
								int EstEndPnts = EstBeginPntBal - ResPnts;
									logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
											
								//Ending Points Balance	/ Compared
								RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
									int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
										logger.info("Ending Points Balance: "+RCEndPointsbal);
										
									Assert.assertEquals(RCEndPointsbal, EstEndPnts);
										logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
								
						}
						
						else		
						{
							Assert.assertEquals(RCResCost, MOPTotResPointCost); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Method of Payment Options Screen: "+RCResCost);
									
							//Total Points Used for Reservation / Compared
							RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
								Assert.assertEquals(RCTotPnts, MOPTotResPointCost); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Method of Payment Options Screen: "+RCTotPnts);
											
							//Estimating Ending Points
							int ResPnts = Integer.parseInt(RCResCost); //Conver String to Int
								int EstEndPnts = EstBeginPntBal - ResPnts;
									logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
													
							//Ending Points Balance	/ Compared
							RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
												
							Assert.assertEquals(RCEndPointsbal, EstEndPnts);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");	
				
						}
						
				//Confirmation Message
				ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
					logger.info(""+ConfimationMes);	
					
					String ResConNumber = ConfimationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
						String ResConfirmNumb = ResConNumber.substring(0, 9);					
							Assert.assertEquals(ResConfirmNumb, ResConNum);
								logger.info("Previous Reservation Number Matched and Cancelled: "+ResConfirmNumb);
						
					NewResConfirmNum = 	ResConNumber.substring(9, 18);
						logger.info("New Reservation Number is: "+NewResConfirmNum);

					//Save to Modify_Reservation_Confirmation.Properties File
					try{
					
					Properties properties = new Properties();
			 		properties.setProperty("NewResConfirmNum", NewResConfirmNum);
			 		
			 		File file = new File("Modify_Reservation_Confirmation.Properties");
					FileOutputStream fileOut = new FileOutputStream(file);
					properties.store(fileOut, "Reservation Confirmation Number");
					fileOut.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
								
			}				
}
