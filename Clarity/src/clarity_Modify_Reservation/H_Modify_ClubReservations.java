package clarity_Modify_Reservation;


import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class H_Modify_ClubReservations extends A_Modify_Common {
	
Logger logger = Logger.getLogger("H_Modify_ClubReservations.class");
	
	//-------------------------------Reservation---------------------------//
		public String CRBookFunc; //Booking Function
		public String CRMarkeyCode; //Marketing Key Code
		public String CRMarKeyCodeFlagHeader; //Marketing Key Code Flag Header
		public String CResType; //Reservation Type
		public String CResStatus; //Reservation Status
		public String CRProperty; //Property
		public String CRPropFlagHeader; //Property Flag Header
		public String CRArrDate; //Arrival Date
		public String CRNights; //Nights
		public String CRNigFlagHeader; //Nights Flag Headr (Inventory)
		public String CRDepDate; //Departure Date
		public String CRGuaCode; //Guarantee Code
		public String CRUsagePeriod; //Usage Period
		public String CRMemshipNum; //Membership Number
		public String NoofGues; //Number of Guests
		public String CRRmType; //RoomType
		public String CRRoomType; //Substring for CRRmType
		public String CRRoomTypeFlagHeader; //RoomType Flag Header
				
		//-------------------------------Financial Summary--------------------------//
		public String CPResAdMsc; //Reservation and Misc Fees
		public String CPResAdMsc1; //Take off the $ or � or � sign of CPResAdMsc
		public String CPResAdMsc2; //Trim the String CPResAdMsc1
		public String AccUpgradeFee; //Loyalty Accommodation Upgrade Fee
		public String TotalAmountDue; //Total Amount due
		
		//Retrieve from Modify_ResortAvailability.Properties
		public String RAResortName; //ResortName
		public String RAArrDate; //Arrival Date
		public String RADepDate; //Departure Date
		public String RARoomType; //Room Type
		
		//Retrieve from Modify_Reservation.Properties
		public String MembershipNumber;

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
		
		public void ResProperties() {
			 
			 PropertyConfigurator.configure("Log4j.properties");	
			
			 	//Modify_ResortAvailability.Properties
				  try (FileReader reader = new FileReader("Modify_ResortAvailability.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		RAResortName = properties.getProperty("RAResortName");
			      			RAArrDate = properties.getProperty("RAArrDate");
			      				RADepDate = properties.getProperty("RADepDate");
			      					RARoomType = properties.getProperty("RARoomType");
			      				
			      				
		      			logger.info("Resort Name from Resort Availability: " +RAResortName);
		      				logger.info("Arrival Date from Resort Availability: " +RAArrDate);
		      					logger.info("Departure Date from Resort Availability: " +RADepDate);
		      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				  
			  //Modify_Reservation.Properties
				  try (FileReader reader = new FileReader("Modify_Reservation.Properties")) {
						 Properties properties = new Properties();
				      		properties.load(reader);
				      		MembershipNumber = properties.getProperty("MembershipNumber");	
			      			logger.info("Membership Number: " +MembershipNumber);
			      			
				      	} catch (IOException e) {
				      		e.printStackTrace();
				      	}
		}
		
		public void Reservation() {
			
		ResProperties(); //Calling Method
			
		 PropertyConfigurator.configure("Log4j.properties");
				
		    logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
			
			//-----------------------------------------------------------------------Reservation:------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Reservation------------------------------------------------------------");
			
					
			//Booking Function / Compared
			CRBookFunc = driver.findElement(By.xpath(".//*[@id='ResSum_BookingPurpose']")).getText();
				Assert.assertEquals(CRBookFunc, BookTypeReserName);
					logger.info("Booking Fuction Displayed and Passed Assertion: "+CRBookFunc);
					
			//Marketing KeyCode / Compared
			CRMarkeyCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
				logger.info("Marketing Key Code: "+CRMarkeyCode);
					
					
			//ReservationType
			CResType = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[4]/td[2]")).getText();
				logger.info("Reservation Type is: "+CResType);
				
			//Reservation Status
			CResStatus = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]")).getText();
				logger.info("Reservation Status: "+CResStatus);
				
			//Property / Compared
			CRProperty = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText();
				Assert.assertEquals(CRProperty, RAResortName);
					logger.info("Property displayed and Matched From Resort Availabilty screen: "+CRProperty);
					
					//Property Flag / Compared
					driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[6]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on The flag image
						logger.info("Clicked on Property Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the Close button is present 
								CRPropFlagHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
									Assert.assertEquals(CRPropFlagHeader, ErrataHeader);
										logger.info("Property Flag Header displayed and Matched: "+CRPropFlagHeader);
											driver.findElement(By.id("REWbtnClose")).click(); //Click on Close button

			//ArrivalDate / Compared
			CRArrDate = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[7]/td[2]")).getText();
				Assert.assertEquals(CRArrDate, RAArrDate);
					logger.info("Arrival Date displayed and Matched From Resort Availabilty screen: "+CRArrDate);
			
			//Nights / Compared
			CRNights = driver.findElement(By.xpath(".//*[@id='ResSum_pnNights']")).getAttribute("value");
				Assert.assertEquals(SANights, CRNights);
					logger.info("Number of Nights Displayed and Matched Assertion: "+CRNights);
						
										
			//Departure Date / Compared
			CRDepDate = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value");
				Assert.assertEquals(CRDepDate, RADepDate);
					logger.info("Departure Date displayed and Matched from Resort Availability Screen: "+CRDepDate);		
					
			//Guarantee Code
			CRGuaCode = driver.findElement(By.xpath(".//*[@id='ResSum_pvGuarantee_input']")).getAttribute("value");
				logger.info("Guarantee Code: "+CRGuaCode);
				
			//Usage Period
			CRUsagePeriod = driver.findElement(By.xpath(".//*[@id='ResSum_UsagePeriod']")).getText();
				logger.info("Usage Period: "+CRUsagePeriod);
			
			//Membership Number / Compared
			CRMemshipNum = driver.findElement(By.xpath(".//*[@id='ResSum_MemberContrID']")).getText();
				logger.info("MemberShip Number: "+CRMemshipNum);
					Assert.assertEquals(CRMemshipNum, MembershipNumber);
						logger.info("Membership Number Matched from ClubRes (Membership Detail for:): "+CRMemshipNum);		
			
			//Number of Guests / Compared
			NoofGues = driver.findElement(By.xpath(".//*[@id='ResSum_Adults']")).getAttribute("value");	
				Assert.assertEquals(SAAdults, NoofGues);
					logger.info("Number of Adults Displayed and Matched in Search of Availability Screen: "+NoofGues);
			
			//Roomtype / Compared
			CRRmType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
				CRRoomType = CRRmType.substring(0, 3);
					logger.info("Room Type: " +CRRoomType);
						Assert.assertEquals(CRRoomType, RARoomType);
							logger.info("Room Type Displayed and Matched from RoomUpgrade Screen: "+CRRoomType);
				
			//Room Type Flag / Compared
			driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Roomtype Flag
				logger.info("Clicked on Room Type Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the Close button is present
						CRRoomTypeFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
							logger.info(""+CRRoomTypeFlagHeader);
								driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
									
		//-----------------------------------------------------------------------Travel Protection------------------------------------------------------------------------------------------//
		
			logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");			
				
			//Decline Rpp
			driver.findElement(By.xpath(".//*[@id='declineRPP']")).click();
				logger.info("Declined RPP");
						
		//----------------------------------------------------------------------Financial Summary------------------------------------------------------------------------------------------//				
				logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
										
				//Reservation And Misc Fees / Compared
				CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText();
					logger.info("Reservation And Misc Fees: "+CPResAdMsc);
					
					CPResAdMsc1 = CPResAdMsc.replace("$", ""); //Take of Dollar Sign
					 CPResAdMsc2 = CPResAdMsc1.trim();// Trim the string to compress blank space 
					 
				//Reservation And Misc Fees Flag 
				driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
					logger.info("Clicked on Reservation And Misc Fees Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
						
							//Total Amount Due	/ Compared	
								TotalAmountDue = driver.findElement(By.xpath(".//*[@id='FRR_TOTAL_AMOUNT']")).getText();
									logger.info("Total Amount due is: "+TotalAmountDue);
										Assert.assertEquals(TotalAmountDue, CPResAdMsc2);
											logger.info("Total Amount due matched Reservation And Misc Fees");

									driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
															
								//Click on SAVE Button
								driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
										logger.info("Clicked on SAVE BUTTON");
										
			}
}	

