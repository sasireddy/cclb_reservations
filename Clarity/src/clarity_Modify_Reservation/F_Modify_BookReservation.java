package clarity_Modify_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class F_Modify_BookReservation extends A_Modify_Common {
	
Logger logger = Logger.getLogger("F_Modify_BookReservation.class");
	
	//Resort Availability - CLUB
	public String RAResort; //Resort Name
	public String RAErrataHeader; //Errata Header
	public String RARmType; //Room Type
	public String RARoomType; //Substring of Room Type
	public String RAFlagHeader; //Flag
	public String RAArrivalDate; //Arrival Date
	public String RADepartureDate; //Departure Date
	public String RAPoints; //Poins To Book The Reservation
	
	//Errata
	public String ADRIErrataHeader; //Additional Resort Information (Errata)	
	
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	
	public void BookReservation()
	{
		
		PropertyConfigurator.configure("Log4j.properties");
		
			//-----------------------------------------------------------------------Book Reservation------------------------------------------------------------------------------------------//
			logger.info("-------------------------------------------------------------------Book Reservation------------------------------------------------------------");
			
			//Book Reservation																						
			driver.findElement(By.xpath(".//*[@id='cr_buttonBOK']")).click();  //Clicked on Book Reservation
				logger.info("Clicked on Book Reservation");
					driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
								
			Select BookingType = new Select(driver.findElement(By.xpath(".//*[@id='rtmBookType_select']"))); 
				BookingType.selectByValue(BookingTypeReservation);// Select Booking Type	
			
			logger.info("Clicked and selected the booking type");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
			driver.findElement(By.xpath(".//*[@id='rtm_continueBtn']")).click();  //Click Continue
				logger.info("Clicked Continue");
				
		//-----------------------------------------------------------------------Search For Availability------------------------------------------------------------------------------------------//
			logger.info("-------------------------------------------------------------------Search For Availability------------------------------------------------------------");
					
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ren_buttonSearch']"))); // Wait till the element is present to click
					
			//Select Region
				driver.findElement(By.xpath(".//*[@id='caw_region_input']")).click(); //Click on Region
					Select Region = new Select(driver.findElement(By.id("caw_region_select"))); 
						Region.selectByValue(SARegion); //Select by Value 
							logger.info("Selected Region: "+SARegion);
								driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
			//Select Property
				driver.findElement(By.xpath(".//*[@id='caw_property_input']")).click(); //Click on Property
					Select Property = new Select(driver.findElement(By.id("caw_property_select")));
						Property.selectByValue(SAProperty); //Select by Value
							logger.info("Selected Property: "+SAProperty);
								driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
								
			//Select Arrival Method
				driver.findElement(By.xpath(".//*[@id='caw_arr_meth_input']")).click(); //Click on Arrival Date Method	
					Select Arrival = new Select(driver.findElement(By.id("caw_arr_meth_select")));
						Arrival.selectByValue(SAArrivalDateMethod); //Select by Value
							logger.info("Selected Arrival Method as Flexible days: "+SAArrivalDateMethod);
								driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
			//Arrival Date
				driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).click(); 
					driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).sendKeys(SAArrivalDate);
		
			//No.of Nights
				driver.findElement(By.xpath(".//*[@id='caw_nights']")).sendKeys(SANights);
					logger.info("Selected No.of Nights as: "+SANights);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					
			//No.of Adults
				driver.findElement(By.xpath(".//*[@id='caw_num_adults']")).sendKeys(SAAdults);
					logger.info("Entered Adults: "+SAAdults);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
			//Click on Search		
			driver.findElement(By.xpath(".//*[@id='ren_buttonSearch']")).click(); //Click on Search button
				logger.info("Clicked on Search");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ResAvailResults']/tbody/tr[3]/td[5]/img"))); // Wait till the element is present to click
		}

	//-------------------------------------------------------------------------Resort Availability-----------------------------------------------------------------------------//

		public void Resort_Availability() {
		
			logger.info("-------------------------------------------------------------------Resort Availabilty------------------------------------------------------------");

			try{
			
			//Resort Availability - CLUB
					
				//Reservation
				WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody"));
				List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
					//logger.info("No.of rows: " +columns.size());
						int rowsnum = columns.size();
								String xpath =null;
									String cellval;
										String cellval1;
									for(int i = 3;i<=rowsnum;i++)
									{
										xpath= ".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[4]";
											cellval = driver.findElement(By.xpath(xpath)).getText();
												cellval1 = cellval.substring(0,3);
													logger.info("Substring is: "+cellval1);
												
											driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											
											if(ResortType.equals(cellval1))
											{
													driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[1]/input")).click();
													logger.info("Clicked on " +ResortType+ " Available Resort");
													
													//ResortName
													RAResort = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[3]")).getText();
														logger.info("Clicked on the Resort" +RAResort);
														
													//RoomType/Max Occ
													RARmType = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[4]")).getText();
														logger.info("RoomType: "+RARmType);
													RARoomType = RARmType.substring(0, 3);
														
													//Flag
													driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[5]/img")).click();
														wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the element is present to click
															RAFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
																logger.info("Flag Header is: "+RAFlagHeader);
																	driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
																		logger.info("Clicked on Close Button (Room Amenitiesx Flag)");
															
													//Arrival Date
													RAArrivalDate = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[8]")).getText();;
														logger.info("Arrival Date: "+RAArrivalDate);
														
														
													//Departure Date
													RADepartureDate = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[9]")).getText();//Departure Date;
														logger.info("Departure Date: "+RADepartureDate);
													
													//Points
													RAPoints = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[10]")).getText(); //Number of points;
														logger.info("Points: "+RAPoints);
															driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
															
															Properties properties = new Properties();
															
													 		properties.setProperty("RAPnts",RAPoints);
											 					properties.setProperty("RAResortName", RAResort);
											 						properties.setProperty("RARoomType",RARoomType);
											 							properties.setProperty("RAArrDate", RAArrivalDate);
											 								properties.setProperty("RADepDate", RADepartureDate);
															
															File file = new File("Modify_ResortAvailability.Properties");
															FileOutputStream fileOut = new FileOutputStream(file);
															properties.store(fileOut, "Information from Resort Availability");
															fileOut.close();
															
															//Click on Continue
															driver.findElement(By.xpath(".//*[@id='caw_ASWBtnProcess']")).click();								
																logger.info("Clicked on Continue");
																	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
															break;
											}
									}
				}
											 
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		
		//------------------------------------------------------------------------------------RoomUpgrade-------------------------------------------------------------------------------------//
		public void RoomUpgrade_No(){
					
			logger.info("-------------------------------------------------------------------RoomUpgrade------------------------------------------------------------");
					
			if(driver.findElements(By.xpath(".//*[@id='upgrdRoomNo']")).size()!=0)
			{
			
				driver.findElement(By.xpath(".//*[@id='upgrdRoomNo']")).click();
					logger.info("Clicked No for Room Upgrade");	
			}
				
			else
			{
				logger.info("Room Upgrade is not available");
				
			}
		}
	
		//------------------------------------------------------------------------------------Errata-------------------------------------------------------------------------------------//
		public void Errata(){
			
			logger.info("-------------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------");
			
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till Close Button is present to click
			
				 if(driver.findElements(By.xpath(".//*[@id='REWHeader']")).size() !=0) 
				 	{
					 
					 //Errata Header
					  ADRIErrataHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText(); // //Header of Additional Resort Information (Errata)
					 	Assert.assertEquals(ADRIErrataHeader, ErrataHeader);
					 		logger.info("Header Displayed and Passed Assertion: "+ADRIErrataHeader);
					 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					 	
					 	//Click on Close Button
						driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();
							logger.info("Clicked on close button");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
					}
					
					else
					{
						logger.info("Errata window not found");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					}
		} 
		
		//------------------------------------------------------------------------------------Discount-------------------------------------------------------------------------------------//
			 
			 public void Discount_No() { 
				 
				  logger.info("-------------------------------------------------------------------Discount------------------------------------------------------------");
													
					if(driver.findElements(By.xpath(".//*[@id='cmnConfirmCntDiv']")).size() !=0) 
					{

						driver.findElement(By.xpath(".//*[@id='confirmDscNoBtn']")).click();
							logger.info("Clicked on No for discount");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		 			}
					 			
					 			
					else
					{
						logger.info("Discount Window not found");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					}
					
					
				}
			 
}

