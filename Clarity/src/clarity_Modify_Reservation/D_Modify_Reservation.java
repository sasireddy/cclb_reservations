package clarity_Modify_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.testng.Assert;

public class D_Modify_Reservation extends A_Modify_Common {
	
Logger logger = Logger.getLogger("D_Modify_Reservation.class");
	
	//Reservations Method
	public String winHandleBefore; //Store Current Window Handle
	public String Lead_Id; //Used inside try method to retrieve LILeadid from Modify_Res_Leadid.Properties
	
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
		public void Reservations() {
			
			PropertyConfigurator.configure("Log4j.properties");
		
			 try (FileReader reader = new FileReader("Modify_Res_Leadid.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		Lead_Id = properties.getProperty("LILeadid");
		      		logger.info(Lead_Id);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			 
			 try {
			 		
				 winHandleBefore = driver.getWindowHandle(); // get the current window handle
				 	Properties properties = new Properties();
				 		properties.setProperty("winHandleBefore", winHandleBefore);
			
					File file = new File("Modify_WindowHandle.Properties");
					FileOutputStream fileOut = new FileOutputStream(file);
					properties.store(fileOut, "Store WindowHandle");
					fileOut.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				

				 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 						
							//Capture Leadid
								String ResLeadId = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
															logger.info("Lead Number in Reservations Tab: "+ResLeadId);
																logger.info("From Lead Information TAB "+Lead_Id);
																	Assert.assertEquals(ResLeadId, Lead_Id);
																		logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");

							//Opens New Window
							driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
						 		logger.info("clicked on Club Res");
					 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

							// Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}
			}
	
}


