package claritydb;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import javax.swing.JOptionPane; 

public class InsertData extends Common {
	
	Logger logger = Logger.getLogger("Login.class");
	Random rand = new Random();
    int num1 = 510;
    DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
  

	
	@Test(priority = 1)
	public void openBrowser() throws InterruptedException   {
			  
		  PropertyConfigurator.configure("Log4j.properties");
			  driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL
			  //driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL
				driver.manage().window().maximize();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='pvUsername']"))); // Wait till the element is present to click
				Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan23");// Entering Password
									logger.info("Entered Password");
										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
													
	}
	
	@Test(priority = 2)
	public void Customer360() 
	{
		PropertyConfigurator.configure("Log4j.properties");
		
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div/div/table/tbody/tr[2]/td[3]/div"))); // Wait till the element is present to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 			/*	driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 					
		String LeadArea = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/select/option[10]")).getText(); // Get DBL Lead Area	
							driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/select/option[10]")).click(); // Click on the DBL Lead Area 
								logger.info("DBL Lead Area Selected as: "+LeadArea);*/
								
								
			String Leadid = JOptionPane.showInputDialog("Please Enter LEAD ID ");
			driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span[2]/input")).sendKeys(Leadid); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+Leadid);	 
					driver.findElement(By.xpath("/html/body/div/div[2]/div/table[2]/tbody/tr/td/table/tbody/tr/td[3]/div")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a Member");
															break Search;				
										}
										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										
					 }
				 }
				
				if(driver.findElements(By.xpath(".//*[@id='alert']/div")).size()!=0) 
				{
					logger.info("Member has limited mobility");
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
					
				}
				
				else
				{
					logger.info("Member has limited mobility------No Found for this lead");
				}
				
				if(driver.findElements(By.xpath(".//*[@id='alert']/div")).size()!=0) 
				{
					logger.info("Member has limited mobility");
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
					
				}
				
				else
				{
					logger.info("Member has limited mobility------No Found for this lead");
				}
		}

	@Test(priority = 3)
	public void DataInsert() throws InterruptedException
	{

		if(driver.findElements(By.xpath(".//*[@id='alert']/div")).size()!=0) 
		{
			logger.info("There is currently no phone number for this member. Please add one if available.");
				driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
			
		}
		
		else
		{
			logger.info("There is currently no phone number for this member. Please add one if available--------------Window not found for this lead");
		}

	
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='homephone_country_primary_input']")));
				
				
		//---------------------------------------------------------------------------------------------------------------------------------------------//
				
		//HomePhone Primary Lead
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//*[@id='homephone_country_primary_input']")).click();	
				driver.findElement(By.xpath(".//*[@id='homephone_country_primary_select']/option[2]")).click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						driver.findElement(By.xpath(".//*[@id='homephone_formatted_primary_input']")).clear();
							driver.findElement(By.xpath(".//*[@id='homephone_formatted_primary_input']")).sendKeys(HomePhone);
								logger.info("Entered PrimaryLead Home Phone");
								
								
		//WorkPhone	Primary Lead				
		driver.findElement(By.xpath(".//*[@id='workphone_country_primary_input']")).click();	
			driver.findElement(By.xpath(".//*[@id='workphone_country_primary_select']/option[2]")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					driver.findElement(By.xpath(".//*[@id='workphone_formatted_primary_input']")).clear();
						driver.findElement(By.xpath(".//*[@id='workphone_formatted_primary_input']")).sendKeys(WorkPhone);
							logger.info("Entered PrimaryLead Work Phone");
						
		//Mobile Phone Primary Lead
		driver.findElement(By.xpath(".//*[@id='mobilephone_country_primary_input']")).click();	
			driver.findElement(By.xpath(".//*[@id='mobilephone_country_primary_select']/option[2]")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					driver.findElement(By.xpath(".//*[@id='mobilephone_primary_input']")).clear();
						driver.findElement(By.xpath(".//*[@id='mobilephone_primary_input']")).sendKeys(MobilePhone);
							logger.info("Entered PrimaryLead Mobile Phone");
		
		//Fax Number Primary Lead
		driver.findElement(By.xpath(".//*[@id='fax_number_country_primary_input']")).click();	
			driver.findElement(By.xpath(".//*[@id='fax_number_country_primary_select']/option[2]")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					driver.findElement(By.xpath(".//*[@id='fax_number_formatted_primary_input']")).clear();
						driver.findElement(By.xpath(".//*[@id='fax_number_formatted_primary_input']")).sendKeys(FaxNumber);
							logger.info("Entered PrimaryLead Fax Number");
					
		//---------------------------------------------------------------------------------------------------------------------------------------------//
					
		//HomePhone Secondary Lead
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.findElement(By.xpath(".//*[@id='homephone_country_secondary_input']")).click();	
				driver.findElement(By.xpath(".//*[@id='homephone_country_secondary_select']/option[2]")).click();
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						driver.findElement(By.xpath(".//*[@id='homephone_formatted_secondary_input']")).clear();
							driver.findElement(By.xpath(".//*[@id='homephone_formatted_secondary_input']")).sendKeys(HomePhone1);
								logger.info("Entered SecondaryLead Home Phone");
							
		//WorkPhone	Secondary Lead				
		driver.findElement(By.xpath(".//*[@id='workphone_country_secondary_input']")).click();	
			driver.findElement(By.xpath(".//*[@id='workphone_country_secondary_select']/option[2]")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					driver.findElement(By.xpath(".//*[@id='workphone_formatted_secondary_input']")).clear();
						driver.findElement(By.xpath(".//*[@id='workphone_formatted_secondary_input']")).sendKeys(WorkPhone1);
							logger.info("Entered SecondaryLead Work Phone");
		
		//Mobile Phone Secondary Lead
		driver.findElement(By.xpath(".//*[@id='mobilephone_country_secondary_input']")).click();	
			driver.findElement(By.xpath(".//*[@id='mobilephone_country_secondary_select']/option[2]")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					driver.findElement(By.xpath(".//*[@id='mobilephone_secondary_input']")).clear();
						driver.findElement(By.xpath(".//*[@id='mobilephone_secondary_input']")).sendKeys(MobilePhone1);	
							logger.info("Entered SecondaryLead Mobile Phone");
		
		//Fax Number Secondary Lead
		driver.findElement(By.xpath(".//*[@id='fax_number_country_secondary_input']")).click();	
			driver.findElement(By.xpath(".//*[@id='fax_number_country_secondary_select']/option[2]")).click();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					driver.findElement(By.xpath(".//*[@id='fax_number_formatted_secondary_input']")).clear();	
						driver.findElement(By.xpath(".//*[@id='fax_number_formatted_secondary_input']")).sendKeys(FaxNumber1);	
							logger.info("Entered SecondaryLead Fax Number");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					
		//---------------------------------------------------------------------------------------------------------------------------------------------//
					
		//Primary Email Address
		driver.findElement(By.xpath(".//*[@id='emailaddress_primary_input']")).clear();
			driver.findElement(By.xpath(".//*[@id='emailaddress_primary_input']")).sendKeys(PriEmail);
				logger.info("Entered Primary Email Address");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				
		//Secondary Email Address
		driver.findElement(By.xpath(".//*[@id='emailaddress_secondary_input']")).clear();
			driver.findElement(By.xpath(".//*[@id='emailaddress_secondary_input']")).sendKeys(SecEmail);
				logger.info("Entered Secondary Email Address");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					
		//Enter Address
		driver.findElement(By.xpath(".//*[@id='addressLine1Input']")).clear();
			driver.findElement(By.xpath(".//*[@id='addressLine1Input']")).sendKeys(Address);
				logger.info("Entered Address");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
				
		//Enter Postal Code.
		driver.findElement(By.xpath(".//*[@id='pvguest_postal_code']")).clear();
			driver.findElement(By.xpath(".//*[@id='pvguest_postal_code']")).sendKeys(Postal);
				logger.info("Entered Postal Code");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						driver.findElement(By.xpath(".//*[@id='pvguest_city']")).click(); 
				
		Thread.sleep(2000);
		
		//Click on Save //
		driver.findElement(By.xpath(".//*[@id='leadSaveButton']")).click();
			logger.info("Clicked on Save Button");
				
						//Verify Your Address Details
						if(driver.findElements(By.xpath(".//*[@id='address_closebtn']")).size()!=0) 
						{
							logger.info("Verify Your Address Details");
								driver.findElement(By.xpath(".//*[@id='address_closebtn']")).click();
							
						}
						
						else
						{
							logger.info("Verify Your Address Details--------------Window not found for this lead");
						}
		WebElement Updated = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the element is present to click
			driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
				logger.info("Lead Updated Successfully" +Updated);
		
			
	}
	
}
