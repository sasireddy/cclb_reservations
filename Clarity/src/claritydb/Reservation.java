package claritydb;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;



 public class Reservation extends Common  {
	
	Logger logger = Logger.getLogger("Reservation.class");
	LeadInformation Leadid = new LeadInformation();
	
	public String winHandleBefore;
	public String ConfirmationNum; 
	
		public void window()
		{
			// Store the current window handle
			 winHandleBefore = driver.getWindowHandle();
		
		}
			
		public void Reservations() {
		
			Leadid.Lead_ID(); //calling the method from LeadInformation Class
			
				 PropertyConfigurator.configure("Log4j.properties");
				 
				 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 			
				 			//Customer Name
				 			String LICustomerRes = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[2]/span")).getText();
				 										logger.info("Customer Name: "+LICustomerRes);
				 			//Reservation History							
							String LIResHistory = driver.findElement(By.xpath(".//*[@id='tvl_resvHistDiv']/table/tbody/tr/td")).getText();
													logger.info("Header: "+LIResHistory);
														Assert.assertEquals(ResHistHeader, LIResHistory);
															logger.info("Reservations History Header Displayed and Assertion Passed");
																driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
							//Capture Leadid
								String ReservationsLN = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
															logger.info("Lead Number in Reservations Tab: "+ReservationsLN);
															logger.info("From Lead Information TAB "+Leadid.LEAD_ID);
																Assert.assertEquals(Leadid.LEAD_ID, ReservationsLN);
																	logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");

							
								
							//Opens New Window
							driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
						 		logger.info("clicked on Club Res");
					 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

							// Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}
			}
			public void ReservationNum()
			{
			
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 			
				//Retrieve Confirmation Number From Confirmation Properties File
				try (FileReader reader = new FileReader("Confirmation.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		
			      		ConfirmationNum = properties.getProperty("ConfirmationNo");
			      			logger.info(ConfirmationNum);
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				 
				WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody")); //Table Tbody
				List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
					//logger.info("No.of rows: " +columns.size());
						int rowsnum = columns.size();
								String xpath =null;
									String ResNum;
										
											for(int i = 1;i<=rowsnum;i++)
											{
												xpath= ".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
													ResNum = driver.findElement(By.xpath(xpath)).getText();
															logger.info("Substring is: "+ResNum);
														
													driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
													
													if(ConfirmationNum.equals(ResNum))
													{
														logger.info("Confirmation Number Found and Matcehd from the Reservation Confirmation Screen");
														
															driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
																logger.info("Clicked on the Flag image of the Confirmation Number");
																break;
													}
											}
				
			}
					
	 }
	 	

  

