package claritydb;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class MOP_Payments extends Common {
	
	Logger logger = Logger.getLogger("MOP_Payments.class");
	
	 public String RAPoints;
	 public String CRRes2015points;
	 public String CostOfPntsBrwd;
	 public String ReservationPossible;
	 public String DiscountYes;
	 public String FinalResCost;
	 
	 public int MOPEstBorrow;
	 
	 public int CRReser2015points;
	 public int RAvailPoints;
		
	 public void ResProperties() {
		 
		 PropertyConfigurator.configure("Log4j.properties");	
			  try (FileReader reader = new FileReader("ResortAvailability.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		RAPoints = properties.getProperty("RAPnts");
		      		logger.info(RAPoints);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			  
			  try (FileReader reader = new FileReader("Reservation.Properties")) {
		      		Properties properties = new Properties();
			      		properties.load(reader);
			      		 CRRes2015points = properties.getProperty("CRRes2015pnts");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			  
			  try (FileReader reader = new FileReader("Discount.Properties")) {
		      		Properties properties = new Properties();
			      		properties.load(reader);
			      		 DiscountYes = properties.getProperty("DiscountYes");
			      		FinalResCost = properties.getProperty("FinalResCost");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			   
	 }
	
	//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
	
		public void MethodofPaymentOptions() {
						
				ResProperties(); //Call ResProperties Method
					
				logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
				
				if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
				{
					
						//---------------------------------------------------------------DISCOUNT = YES----------------------------------------------//
							//If Discount is YES
						if(DiscountYesMessage.equals(DiscountYes))
							{
								//MOP options
								 String MPPaymentOpt = driver.findElement(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).getText(); // Methods of Payment Options header
								 	Assert.assertEquals(MPPaymentOpt, MethodofPayments);
								 		logger.info("Method of Payment options Header Displayed and Assertion Passed: "+MPPaymentOpt);
								 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								 			
								 //Estimate and Confirm Reservation Points 
								 	
								 	//Total Point Cost
								 	String MOPPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
								 		Assert.assertEquals(FinalResCost, MOPPointCost);
								 			logger.info("Total Reservation Point Cost Matched The Points in Resort Availability: "+MOPPointCost);
								 			
								 	//Total Member Points Available 
								 	String MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
								 		Assert.assertEquals(CRRes2015points, MOPointsAvail);
								 			logger.info("Total Member Points Available Matched The Available for Reservations in Club Reservations: "+MOPointsAvail);
								 			
								 			//Borrow from Next Year if the Available Points are less than the reservation cost
								 			CRReser2015points = Integer.parseInt(CRRes2015points);//Convert to integer for comparision
								 				logger.info(CRReser2015points);
							 				RAvailPoints = Integer.parseInt(FinalResCost);//Convert to integer for comparision
							 					logger.info(RAvailPoints);
											
												if(RAvailPoints > CRReser2015points) //Compare 
									 			{
													
													 MOPEstBorrow = RAvailPoints - CRReser2015points;
													 
													 logger.info("There are No Available Points To Book The Reservation.  Need to Borrow");
													 
													 //Points need to borrow( ESTIMATION)
													 logger.info("Estimating the points needed to borrow is: "+MOPEstBorrow);
													 
													 //Balance of Points Needed
													 String MOPPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']/b")).getText(); 
													 	logger.info("Balance of Points Needed: "+MOPPointsNeeded);
													 	
													 	int MOPBalPointsNeeded = Integer.parseInt(MOPPointsNeeded); //Convert to integer for comparision
													 	
													 	Assert.assertEquals(MOPBalPointsNeeded, MOPEstBorrow); //Compare the Estimation with actual
													 		logger.info("Balance of Points Needed Matched with the Estimated points to borrow: ASSERTION PASSED");
													 	
													 	//Use Membership Points
													 		String MOPUseMemPoints = driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).getAttribute("value");
													 			logger.info("Use Membership Points in Method of Payments Section: "+MOPUseMemPoints);
													 				logger.info("Available points: "+CRRes2015points);
													 					Assert.assertEquals(MOPUseMemPoints, CRRes2015points);
													 						logger.info("Use Membership Points Matched the Available points: ASSERTION PASSED");
													 						
													 						
													 						
													 	//Borrow From Next Year
													 		String MOPBorrowfrm = driver.findElement(By.xpath(".//*[@id='nBorPoints']")).getAttribute("value");	
													 					int MOPBorrowfrmNxYr = Integer.parseInt(MOPBorrowfrm);
													 						logger.info("Borrow Points from Next Year in Method of Payments Section:" +MOPBorrowfrmNxYr);
													 							logger.info("Estimated Points to Borrow: "+MOPEstBorrow);
													 							
													 								Assert.assertEquals(MOPBorrowfrmNxYr, MOPEstBorrow);
													 									logger.info("Borrow from Next Year Matched the Estimation Points to Borrow: ASSERTION PASSED");
													 									
													 									//Cost of the points borrowed
													 									try {
													 										
													 								 		 CostOfPntsBrwd = driver.findElement(By.xpath(".//*[@id='nPrePmtAmount']")).getAttribute("value");
													 											logger.info("Cost for borrowed points: "+CostOfPntsBrwd);
													 											
													 											ReservationPossible = "The Points Are Not Available To Book The Reservation";
													 												logger.info(ReservationPossible);
													 											
													 									 	
													 											Properties properties = new Properties();
													 									 			properties.setProperty("CostOfPntsBrwd", CostOfPntsBrwd);
													 									 				properties.setProperty("ReservationPossible", ReservationPossible);
													 									 				properties.setProperty("PointsBorrowed", MOPBorrowfrm);
													 									 				
													 										File file = new File("Prepayment.Properties");
													 										FileOutputStream fileOut = new FileOutputStream(file);
													 										properties.store(fileOut, "Cost of Borrowed Points");
													 										fileOut.close();
													 									} catch (FileNotFoundException e) {
													 										e.printStackTrace();
													 									}catch (IOException e) {
													 										e.printStackTrace();
													 									}
													 									
					 									//Total Points To Apply / Total Cost
					 								 	String MOPTotalCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
					 								 		Assert.assertEquals(FinalResCost, MOPTotalCost);
					 								 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalCost);
					 								 			
					 								 	//Process		
					 									driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
					 									logger.info("Clicked on Process button");
					 										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					 										
				 										//Wait till the confirmation message displays	
					 										wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the element is present 
					 										
					 										String PointsBorrowed = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText(); //Get Confirmation Text
					 											logger.info(""+PointsBorrowed);
					 											
					 										driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
									 			}
											
												else
												{
													ReservationPossible = "The Points Are Available To Book The Reservation";
														logger.info(ReservationPossible);
													
													//Total Points To Apply / Total Cost
												 	String MOPTotalCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
												 		Assert.assertEquals(FinalResCost, MOPTotalCost);
												 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalCost);
												 			
												 	//Process		
													driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
														logger.info("Clicked on Process button");
															driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
															
															try {
						 											Properties properties = new Properties();
						 									 			properties.setProperty("ReservationPossible", ReservationPossible);
						 										File file = new File("Prepayment.Properties");
						 										FileOutputStream fileOut = new FileOutputStream(file);
						 										properties.store(fileOut, "Cost of Borrowed Points");
						 										fileOut.close();
						 									} catch (FileNotFoundException e) {
						 										e.printStackTrace();
						 									}catch (IOException e) {
						 										e.printStackTrace();
						 									}
												}
								}
					
							else
								{
								//---------------------------------------------------------------DISCOUNT = NO----------------------------------------------------------//	
								//MOP options
								 String MPPaymentOpt = driver.findElement(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).getText(); // Methods of Payment Options header
								 	Assert.assertEquals(MPPaymentOpt, MethodofPayments);
								 		logger.info("Method of Payment options Header Displayed and Assertion Passed: "+MPPaymentOpt);
								 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								 			
								 //Estimate and Confirm Reservation Points 
								 	
								 	//Total Point Cost
								 	String MOPPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
								 		Assert.assertEquals(RAPoints, MOPPointCost);
								 			logger.info("Total Reservation Point Cost Matched The Points in Resort Availability: "+MOPPointCost);
								 			
								 	//Total Member Points Available 
								 	String MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
								 		Assert.assertEquals(CRRes2015points, MOPointsAvail);
								 			logger.info("Total Member Points Available Matched The Available for Reservations in Club Reservations: "+MOPointsAvail);
								 			
								 			//Borrow from Next Year if the Available Points are less than the reservation cost
								 			CRReser2015points = Integer.parseInt(CRRes2015points);//Convert to integer for comparision
								 				logger.info(CRReser2015points);
							 				RAvailPoints = Integer.parseInt(RAPoints);//Convert to integer for comparision
							 					logger.info(RAvailPoints);
											
												if(RAvailPoints > CRReser2015points) //Compare 
									 			{
													
													 MOPEstBorrow = RAvailPoints - CRReser2015points;
													 
													 logger.info("There are No Available Points To Book The Reservation.  Need to Borrow");
													 
													 //Points need to borrow( ESTIMATION)
													 logger.info("Estimating the points needed to borrow is: "+MOPEstBorrow);
													 
													 //Balance of Points Needed
													 String MOPPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']/b")).getText(); 
													 	logger.info("Balance of Points Needed: "+MOPPointsNeeded);
													 	
													 	int MOPBalPointsNeeded = Integer.parseInt(MOPPointsNeeded); //Convert to integer for comparision
													 	
													 	Assert.assertEquals(MOPBalPointsNeeded, MOPEstBorrow); //Compare the Estimation with actual
													 		logger.info("Balance of Points Needed Matched with the Estimated points to borrow: ASSERTION PASSED");
													 	
													 	//Use Membership Points
													 		String MOPUseMemPoints = driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).getAttribute("value");
													 			logger.info("USe Membership Points in Method of Payments Section: "+MOPUseMemPoints);
													 				logger.info("Available points: "+CRRes2015points);
													 					Assert.assertEquals(MOPUseMemPoints, CRRes2015points);
													 						logger.info("Use Membership Points Matched the Available points: ASSERTION PASSED");
													 						
													 						
													 						
													 	//Borrow From Next Year
													 		String MOPBorrowfrm = driver.findElement(By.xpath(".//*[@id='nBorPoints']")).getAttribute("value");	
													 					int MOPBorrowfrmNxYr = Integer.parseInt(MOPBorrowfrm);
													 						logger.info("Borrow Points from Next Year in Method of Payments Section:" +MOPBorrowfrmNxYr);
													 							logger.info("Estimated Points to Borrow: "+MOPEstBorrow);
													 							
													 								Assert.assertEquals(MOPBorrowfrmNxYr, MOPEstBorrow);
													 									logger.info("Borrow from Next Year Matched the Estimation Points to Borrow: ASSERTION PASSED");
													 									
													 									//Cost of the points borrowed
													 									try {
													 										
													 								 		 CostOfPntsBrwd = driver.findElement(By.xpath(".//*[@id='nPrePmtAmount']")).getAttribute("value");
													 											logger.info("Cost for borrowed points: "+CostOfPntsBrwd);
													 											
													 											ReservationPossible = "The Points Are Not Available To Book The Reservation";
													 												logger.info(ReservationPossible);
													 											
													 									 	
													 											Properties properties = new Properties();
													 									 			properties.setProperty("CostOfPntsBrwd", CostOfPntsBrwd);
													 									 				properties.setProperty("ReservationPossible", ReservationPossible);
													 									 				properties.setProperty("PointsBorrowed", MOPBorrowfrm);
													 									 				
													 										File file = new File("Prepayment.Properties");
													 										FileOutputStream fileOut = new FileOutputStream(file);
													 										properties.store(fileOut, "Cost of Borrowed Points");
													 										fileOut.close();
													 									} catch (FileNotFoundException e) {
													 										e.printStackTrace();
													 									}catch (IOException e) {
													 										e.printStackTrace();
													 									}
													 									
					 									//Total Points To Apply / Total Cost
					 								 	String MOPTotalCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
					 								 		Assert.assertEquals(RAPoints, MOPTotalCost);
					 								 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalCost);
					 								 			
					 								 	//Process		
					 									driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
					 									logger.info("Clicked on Process button");
					 										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					 										
				 										//Wait till the confirmation message displays	
					 										wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the element is present 
					 										
					 										String PointsBorrowed = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText(); //Get Confirmation Text
					 											logger.info(""+PointsBorrowed);
					 											
					 										driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
									 			}
											
												else
												{
													ReservationPossible = "The Points Are Available To Book The Reservation";
														logger.info(ReservationPossible);
													
													//Total Points To Apply / Total Cost
												 	String MOPTotalCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
												 		Assert.assertEquals(RAPoints, MOPTotalCost);
												 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalCost);
												 			
												 	//Process		
													driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
														logger.info("Clicked on Process button");
															driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
															
															try {
						 											Properties properties = new Properties();
						 									 			properties.setProperty("ReservationPossible", ReservationPossible);
						 										File file = new File("Prepayment.Properties");
						 										FileOutputStream fileOut = new FileOutputStream(file);
						 										properties.store(fileOut, "Cost of Borrowed Points");
						 										fileOut.close();
						 									} catch (FileNotFoundException e) {
						 										e.printStackTrace();
						 									}catch (IOException e) {
						 										e.printStackTrace();
						 									}
									 				
												}
								}
					
				}
				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
				
			}

}
