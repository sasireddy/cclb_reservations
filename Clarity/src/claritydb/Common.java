package claritydb;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Common {
  
	static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 60);
	
	
	String Leadid1= "1866329";
	
	String PLH = "Primary Lead:";
	String SLH = "Secondary Lead:";
	String LeadFlag = "Owner Snapshot";
	String LeadEmailFlag = "Email Type";
	String LeadSocialNetworks = "Social Network Account Information";
	String LeadIDLookup = "ID Lookup";
	String LeadCC = "Credit Card Maintenance";
	String AddressHeader = "Address Information:";
	String DemographHeader = "Demographics";
	String Com_AlertsHeader = "Comments and Alerts:";
	
	String HomePhone = "2022022222";
	String WorkPhone = "3033033333";
	String MobilePhone = "4044044444";
	String FaxNumber = "5055055555";
	
	String HomePhone1 = "6066066666";
	String WorkPhone1 = "7077077777";
	String MobilePhone1 = "8088088888";
	String FaxNumber1 = "9099099999";
	
	String PriEmail = "Primary@dri.com";
	String SecEmail = "Secondary@dri.com";
	String Address = "10175 W Charleston Blvd";
	String Country = "USA - United States";
	String Postal = "89135-1260";
	String City = "Las Vegas";
	String State = "NV - Nevada";
	
	String ResHistHeader = "Reservations History";
	String CRLeadFlag = "Lead Flags";
	String CRCusProfile = "Customer Profile";
	String CRFullLeadInfo = "Lead Information";
	String CRAssocMembers = "Associate Member";
	
	String CRMemBen = "Member Benefits";
	String CRPoiSavDdLines = "Point Saving Deadlines";
	
	String CRFinancialInformation = "Financial Information";
	
	String CRContractDetails = "Contract Details";
	
	String CRBookNewREs = "Book New Reservation";
	String BookingTypeReservation = "10194"; //Home Standard Reservation Value
	String BookTypeReserName = "HOME-STANDARD RESERVATION";
	
	String SASearchforAva = "Search for Availability";
	String SADestination = "Choose a Destination";
	String SATravelDates = "Choose Travel Dates:";
	String SASearPara = "Additional Search Parameters:";
	String SANights = "2";
	String SAAdults = "2";
	
	String ResortAvaCLUB = "Resort Availability - CLUB";
	String ResortType = "SXX";
	String RAErrataCLUB = "Additional Resort Information (Errata)";
	
	String RoomUpgrade1 = "Room Upgrade";
	
	String Discount1 = "Discount";
	String DisMesAvail = "Member has sufficient points to book this reservation.";
	String DisMesNotAvail = "Member does not have sufficient points to book this reservation.";
	String DiscountPercentage = "25";
	String DiscountYesMessage = "Selected Yes For Discount";
	
	String MethodofPayments = "Methods of Payment Options";
	
	String CRReserHeader = "Reservation:";
	String CRMrKyFlag = "Marketing Information";
	String CRSpecReq = "Special Requests";
	String CRDisAccReq = "Disability and Access Requirements";
	String CRGue = "Guests:";
	String CRCanPol = "Cancellation Policy";
	String CRTravelProtection = "Travel Protection";
	String CRTraProtecFlag = "Travel Protection Information";
	String CRAddProductsOff = "Additional Products Offer";
	String CRFinSumm = "Financial Summary:";
	String CRNgtRateInform = "Night Rate Information:"; //Rate Amount Flag Header
	String CRExtrResChar = "Extra Resort Charges"; //Extra Resort Charges Flag Header
	String CRAdvDeposits = "Advance Deposits"; //Reservation Advance Deposit Flag Header
	String CRReserMiscFees = "Reservation and Misc Fees"; //Reservation and Misc Fees Flag Header
	String CreditCardMain = "Credit Card Maintenance";
	String CCNumber = "4321000000001119";
	String CCType = "Visa";
	String CCName = "Diamond Tester";
	String ExpMonth = "01";
	String ExpYr = "20";
	
	
	String RConfirm = "Reservation Confirmation"; //Reservation Confirmation Header
	String RCPnSum = "Points Summary";
	String RCConLet = "Confirmation Letter";
}
