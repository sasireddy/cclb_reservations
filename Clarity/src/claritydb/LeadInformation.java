package claritydb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class LeadInformation extends Common {
	
	Logger logger = Logger.getLogger("LeadInformation.class");
	
	public String LEAD_ID;
	public void Customer360() 
	{
		
		PropertyConfigurator.configure("Log4j.properties");
		
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 					
		/*String Leadid = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/select/option[10]")).getText(); // Get DBL Lead Area	
							driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/select/option[10]")).click(); // Click on the DBL Lead Area 
								logger.info("DBL Lead Area Selected as: "+Leadid);*/
								
								
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(Leadid1); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+Leadid1);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a Member");
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					 }
				 }
		}
		
		public void Lead_ID()
		{
					
			try {
		 		
				 LEAD_ID = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[1]/td[2]")).getText();
				
			 	Properties properties = new Properties();
			 		properties.setProperty("Leadid", LEAD_ID);
		
				File file = new File("Leadid.Properties");
				FileOutputStream fileOut = new FileOutputStream(file);
				properties.store(fileOut, "Lead Id");
				fileOut.close();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		public void Lead_Area_Id() {
			
			Lead_ID();
			
			 logger.info("--------------------------------------------------------------------------Lead Information---------------------------------------------------------------------------------");
			 
			 logger.info("---------------------------------------------Primary Lead and Secondary Lead Information--------------------------------------------------------");
			//Primary Lead Header
			String PrimaryLeadHeader = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[1]")).getText();
				logger.info("Primary Lead Header Displayed: " +PrimaryLeadHeader);
					Assert.assertEquals(PLH, PrimaryLeadHeader);
					
			//Secondary Lead Header		
			String SecondaryLeadHeader = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[4]")).getText();
				logger.info("Secondary Lead Header Displayed: " +SecondaryLeadHeader);
					Assert.assertEquals(SLH, SecondaryLeadHeader);
				
			//Primary Lead Name
			String PrimaryLead1 = driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div/div/div/table/tbody/tr[1]/td[2]/span[1]")).getText();
				logger.info("Secondary Lead Header Displayed: " +PrimaryLead1);
			
			//Secondary Lead Name		
			String SecondaryLead1 = driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div/div/div/table/tbody/tr[1]/td[5]/span")).getText();
				logger.info("Secondary Lead Header Displayed: " +SecondaryLead1);
			
			//---------------------------------------------------------------------------------------------------------------------------------------------------//
			//Lead Area-Lead Id	
			String Lead_Area_Id = LEAD_ID;
				logger.info("Lead Area-Lead Id: " +Lead_Area_Id);
				
			String Lead_Type = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[2]/td[2]/table/tbody/tr/td[1]")).getText();
				String Sub_Type = driver.findElement(By.xpath(".//*[@id='lead_subtype_input']")).getAttribute("value");
					logger.info("Lead Type/Subtype: "+ Lead_Type +" "+Sub_Type);
			
					//Owner Snapshot
					driver.findElement(By.xpath(".//*[@id='ownerSnapshotImg']")).click();
						logger.info("Clicked on Lead Type/SubType Flag icon");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ownerSnapshotCloseButton']"))); // Wait till the element is present to click
								String Flag1 = driver.findElement(By.xpath(".//*[@id='leadFlagsHeaderTable']/tbody/tr/td")).getText();
									logger.info("Owner Snapshot displayed:"+Flag1);
										Assert.assertEquals(Flag1, LeadFlag);
											logger.info("Owner Snapshot header Passed (Flag)");
												driver.findElement(By.xpath(".//*[@id='ownerSnapshotCloseButton']")).click();	
													logger.info("Clicked on Close Button (Flag)");
					
			//---------------------------------------------------------------------------------------------------------------------------------------------------//		
			//Primary FirstName		
			String LIFirstName = driver.findElement(By.xpath(".//*[@id='firstname_primary_input']")).getAttribute("value");
				String LISec_FirstName = driver.findElement(By.xpath(".//*[@id='firstname_secondary_input']")).getAttribute("value");
					logger.info("Primary Lead FirstName: "+ LIFirstName);
						logger.info("Secondary Lead FirstName:"+ LISec_FirstName);
			
			//Primary LastName
			String LILastName = driver.findElement(By.xpath(".//*[@id='lastname_primary_input']")).getAttribute("value");
				logger.info("LastName: "+ LILastName);
				
			//Primary HomePhone
			String LIHomePhone = driver.findElement(By.xpath(".//*[@id='homephone_formatted_primary_input']")).getAttribute("value");
				logger.info("HomePhone: "+LIHomePhone);
				
			//Primary WorkPhone
			String LIWorkPhone = driver.findElement(By.xpath(".//*[@id='workphone_formatted_primary_input']")).getAttribute("value");
				logger.info("Work Phone: "+LIWorkPhone);
				
			//Primary MobilePhone
			String LIMobilePhone = 	driver.findElement(By.xpath(".//*[@id='mobilephone_primary_input']")).getAttribute("value");
				logger.info("Mobile Phone: "+LIMobilePhone);	
			
			//Primary FaxNumber
			String LIFaxNumber = 	driver.findElement(By.xpath(".//*[@id='fax_number_formatted_primary_input']")).getAttribute("value");
				logger.info("Fax Number: "+LIFaxNumber);	
			
			//---------------------------------------------------------------------------------------------------------------------------------------------------//		
			//Primary Email Address
			String LIEmail = driver.findElement(By.xpath(".//*[@id='emailaddress_primary_input']")).getAttribute("value");
				logger.info("Email Address: "+LIEmail);
				
			driver.findElement(By.xpath(".//*[@id='emailAddressImg']")).click();
				logger.info("Clicked on Email Address Flag icon");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='emailAddressCloseButton']"))); // Wait till the element is present to click
						String Flag2 = driver.findElement(By.xpath(".//*[@id='emailAddressInnerDiv']/div/table/tbody/tr/td[1]")).getText();
							logger.info("Email Type Header displayed: "+ Flag2);
								Assert.assertEquals(Flag2, LeadEmailFlag);
									logger.info("Email Type header Passed (Flag)");
										driver.findElement(By.xpath(".//*[@id='emailAddressCloseButton']")).click();	
											logger.info("Clicked on Close Button (Email Address Flag)");
											
			//---------------------------------------------------------------------------------------------------------------------------------------------------//	
			//Social Networks								
			driver.findElement(By.xpath(".//*[@id='SocNetAcctImg']")).click();
				logger.info("Clicked on Social Networks Flag icon");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='SocialNetworkCloseButton']"))); // Wait till the element is present to click
						String Flag3 = driver.findElement(By.xpath(".//*[@id='SocInfoInnerDiv']/div[1]/table/tbody/tr/td")).getText();
							logger.info("Social Network Account Information Header displayed: " +Flag3);
								Assert.assertEquals(Flag3,LeadSocialNetworks);
									logger.info("Social Network Account Information Header displayed header Passed (Flag)");
										driver.findElement(By.xpath(".//*[@id='SocialNetworkCloseButton']")).click();	
											logger.info("Clicked on Close Button (Social Networks Flag)");	
											
			//---------------------------------------------------------------------------------------------------------------------------------------------------//	
			//ID LookUp								
			driver.findElement(By.xpath(".//*[@id='idLookupImg']")).click();
				logger.info("Clicked on ID LookUp Flag icon");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='IDLookupCloseButton']"))); // Wait till the element is present to click
						String Flag4 = driver.findElement(By.xpath(".//*[@id='idInfoInnerDiv']/div/table/tbody/tr/td")).getText();
							logger.info("ID LookUp Header displayed: " +Flag4);
								Assert.assertEquals(Flag4, LeadIDLookup);
									logger.info("ID LookUp header Passed (Flag)");
										driver.findElement(By.xpath(".//*[@id='IDLookupCloseButton']")).click();	
											logger.info("Clicked on Close Button (ID Lookup Flag)");	
			//---------------------------------------------------------------------------------------------------------------------------------------------------//	
			//Credit Cards							
			driver.findElement(By.xpath(".//*[@id='creditCardImg']")).click();
				logger.info("Clicked on Credit Cards Flag icon");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[2]/tbody/tr/td[4]/span/font"))); // Wait till the element is present to click
						String Flag5 = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[1]/tbody/tr/td/b")).getText();
							logger.info("Credit Card Maintenance Header displayed: " +Flag5);
								Assert.assertEquals(Flag5, LeadCC);
									logger.info("Credit Card Maintenance header Passed (Flag)");
										
										String CCLead = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[2]/tbody/tr/td[4]/span/font")).getText();	//Lead id
											logger.info("Lead id in Credit Card Maintenance Flag: "+CCLead); //Print Lead ID
												
												//Compare Lead id
												if(CCLead.equals(Lead_Area_Id))
												{
													logger.info("Lead Id Matched in Credit Card Maintenance Flag");
												}
												else
												{
													logger.info("Lead Id Didnt Match in Credit Card Maintenance Flag");
													driver.quit();
												}
											
										driver.findElement(By.xpath(".//*[@id='CCCancelBtn']")).click();	
											logger.info("Clicked on Cancel Button (Credit Card Flag)");	
			//---------------------------------------------------------------------------------------------------------------------------------------------------//	
				
				logger.info("---------------------------------------------Address Information & Demographics & Comments and Alerts--------------------------------------------------------");
				
				//Address Header							
				String LIAddress = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[2]/div[1]/div/table[1]/tbody/tr[1]/td")).getText();
										logger.info(""+LIAddress);
											Assert.assertEquals(AddressHeader, LIAddress);
												logger.info("Address Information Header Matched");
				
				
				//Demographics							
				String LIDemograph = driver.findElement(By.xpath(".//*[@id='lead_demoOuter']/div/table[1]/tbody/tr/td")).getText();
										logger.info(""+LIDemograph);
											Assert.assertEquals(DemographHeader, LIDemograph);	
												logger.info("Demographics Header Matched");
												
				//Comments and Alerts							
				String LICom_Alerts = driver.findElement(By.xpath(".//*[@id='commentfilter_tab']/tbody/tr[1]/td/div")).getText();
										logger.info(""+LICom_Alerts);
											Assert.assertEquals(Com_AlertsHeader, LICom_Alerts);	
												logger.info("Comments and Alerts Header Matched");
		}
	}
