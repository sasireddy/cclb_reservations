package claritydb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.testng.Assert;


public class Discount extends Common {

	 Logger logger = Logger.getLogger("Discount.class");
	 
	 public String CRRes2015points;
	 public String RAPoints;
	 
	 public int CRReser2015points;
	 public int RAvailPoints;
	 

	 
	 public void Convert()
	 {
		 PropertyConfigurator.configure("Log4j.properties");	
		 
		  //Get the values from the property file and compare the available points to the reservation point cost for discount
		  try (FileReader reader = new FileReader("Reservation.Properties")) {
	    		Properties properties = new Properties();
		      		properties.load(reader);
		      			CRRes2015points = properties.getProperty("CRRes2015pnts"); //Get the Value
		      		
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			 
			 try (FileReader reader = new FileReader("ResortAvailability.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		 RAPoints = properties.getProperty("RAPnts");//Get the value
		      			
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}

				CRReser2015points = Integer.parseInt(CRRes2015points);//Convert to integer for comparision
				RAvailPoints = Integer.parseInt(RAPoints);//Convert to integer for comparision
								
	 }
	 
	 //For Discount = NO
	  public void Discount_No() { 
		  
		  Convert();
		  
	  PropertyConfigurator.configure("Log4j.properties");	
		
	  logger.info("-------------------------------------------------------------------Discount------------------------------------------------------------");
										
		if(driver.findElements(By.xpath(".//*[@id='cmnConfirmCntDiv']")).size() !=0) 
		{
			
		//Discount Header
		 String Discount = driver.findElement(By.xpath(".//*[@id='cmnConfirmDiv']/div[1]/div")).getText(); // //Header of Discount
		 	Assert.assertEquals(Discount, Discount1);
		 		logger.info("Discount Header Displayed and Passed Assertion: "+Discount);
		 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 			
		 			
		 			if(CRReser2015points > RAvailPoints) //Compare 
		 			{
		 			String DisMessage = driver.findElement(By.xpath(".//*[@id='cmnConfirmCntDiv']")).getText(); //Message from Discount
		 						String DisMessage1 = DisMessage.substring(0, 54);//Print only 54 chars to compare with DisMess String declared in Common.
		 							Assert.assertEquals(DisMessage1, DisMesAvail);
		 								logger.info("Member has sufficient points to book the reservation Message displayed: ASSERTION PASSED");
		 									driver.findElement(By.xpath(".//*[@id='confirmDscNoBtn']")).click();
		 										logger.info("Clicked on No for discount");
		 											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		 			}
		 			
		 			else
		 			{
		 				String DisMessage = driver.findElement(By.xpath(".//*[@id='cmnConfirmCntDiv']")).getText();
		 					String DisMessage1 = DisMessage.substring(0, 64);//Print only 64 chars to compare with DisMess String declared in Common.
		 						logger.info("Member has no sufficient points to book the reservation:");
		 							logger.info("" +DisMessage1);
		 								Assert.assertEquals(DisMessage1, DisMesNotAvail);
		 									logger.info("Member does not have sufficient points to book the reservation Message displayed: ASSERTION PASSED");
		 										driver.findElement(By.xpath(".//*[@id='confirmDscNoBtn']")).click();
		 											logger.info("Clicked on No for discount");
		 												driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		 			}
		 			
		}
		
		else
		{
			logger.info("Discount Window not found");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}
		
		
	}
  
	  //Discount = YES
  public void Discount_Yes()  {
	  
	  Convert();
	  
	  PropertyConfigurator.configure("Log4j.properties");	
		
	  logger.info("-------------------------------------------------------------------Discount------------------------------------------------------------");
										
		if(driver.findElements(By.xpath(".//*[@id='cmnConfirmCntDiv']")).size() !=0) 
		{
			
		//Discount Header
		 String Discount = driver.findElement(By.xpath(".//*[@id='cmnConfirmDiv']/div[1]/div")).getText(); // //Header of Discount
		 	Assert.assertEquals(Discount, Discount1);
		 		logger.info("Discount Header Displayed and Passed Assertion: "+Discount);
		 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 			
		 			
		 			if(CRReser2015points > RAvailPoints) //Compare 
		 			{
		 			String DisMessage = driver.findElement(By.xpath(".//*[@id='cmnConfirmCntDiv']")).getText(); //Message from Discount
		 						String DisMessage1 = DisMessage.substring(0, 54);//Print only 54 chars to compare with DisMess String declared in Common.
		 							Assert.assertEquals(DisMessage1, DisMesAvail);
		 								logger.info("Member has sufficient points to book the reservation Message displayed: ASSERTION PASSED");
		 									driver.findElement(By.xpath(".//*[@id='confirmDscYesBtn']")).click();
		 										logger.info("Clicked on YES for discount");
		 											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		 			}
		 			
		 			else
		 			{
		 				String DisMessage = driver.findElement(By.xpath(".//*[@id='cmnConfirmCntDiv']")).getText();
		 					String DisMessage1 = DisMessage.substring(0, 64);//Print only 64 chars to compare with DisMess String declared in Common.
		 						logger.info("Member has no sufficient points to book the reservation:");
		 							logger.info("" +DisMessage1);
		 								Assert.assertEquals(DisMessage1, DisMesNotAvail);
		 									logger.info("Member does not have sufficient points to book the reservation Message displayed: ASSERTION PASSED");
		 										driver.findElement(By.xpath(".//*[@id='confirmDscYesBtn']")).click();
		 											logger.info("Clicked on YES for discount");
		 												driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		 			}
		 			
		 		//Discount Percentage(%)
		 			driver.findElement(By.xpath(".//*[@id='discPct']")).click(); //Click on Discount Percentage
		 			
		 				driver.findElement(By.xpath(".//*[@id='discPct']")).sendKeys(DiscountPercentage); //Enter the Percentage Amount
		 				
		 					driver.findElement(By.xpath(".//*[@id='ttlCost']")).click();
		 					
		 				
		 				try  
		 				{
		 					double Percentage = 0.25;
		 						double EstimatedDiscntAmount = Math.round(RAvailPoints * Percentage);
		 							
		 								logger.info("Discount Points Amount: "+EstimatedDiscntAmount);
		 								
		 								DecimalFormat decimal = new DecimalFormat("###.#");
		 								
							String EstimatedDiscountAmount = decimal.format(EstimatedDiscntAmount); // Convert Long to String
		 					
		 					String DiscountYes = "Selected Yes For Discount";
		
 							String FinalResCost = driver.findElement(By.xpath(".//*[@id='ttlCost']")).getAttribute("value"); //Get the Final Reservation Cost 
 								logger.info("Final Reservation Cost: "+FinalResCost);
 								
							
 								
		 					 		Properties properties = new Properties();
		 					 			properties.setProperty("EstimatedDiscountAmount", EstimatedDiscountAmount);
		 					 				properties.setProperty("FinalResCost", FinalResCost);
		 					 					properties.setProperty("DiscountYes", DiscountYes);
		 					 		
		 						File file = new File("Discount.Properties");
		 						FileOutputStream fileOut = new FileOutputStream(file);
		 						properties.store(fileOut, "Discounted Points Amount");
		 						fileOut.close();
		 					} catch (FileNotFoundException e) {
		 						e.printStackTrace();
		 					} catch (IOException e) {
		 						e.printStackTrace();
		 			      	}
	 			
		 				driver.findElement(By.xpath(".//*[@id='confirmDscPrcYesBtn']")).click(); //Click on Process Button
		}
		
		else
		{
			logger.info("Discount Window not found");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}
		
  }
}

