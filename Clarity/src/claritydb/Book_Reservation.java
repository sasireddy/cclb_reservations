package claritydb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class Book_Reservation extends Common{
	
	 Logger logger = Logger.getLogger("Book_Reservation.class");
	 
	 public String CRRes2015pnt;
	 public String CRRes2015pnts;
	 public String CRRes2016pnt;
	 public String CRRes2016pnts;
	
	/* public String RAPoints;
	 public String RAResort;
	 public String RARoomType;
	 public String RAArrivalDate;
	 public String RADepartureDate;*/
	 
	 public String RAAPoints;
	 public String RAAResort;
	 public String RAARoomType;
	 public String RAAArrivalDate;
	 public String RAADepartureDate;
	 
	 public String RoomupgAvailabilityComp = "Room Upgrade Option is Available";
	 public String RoomupgAvailability;
	 public String RoomType;
	 
	 public void CReservations15()
	 {

		 	try {
		 		
		 		 CRRes2015pnt = driver.findElement(By.xpath(".//*[@id='mbrRemBan']")).getText();
					CRRes2015pnts = CRRes2015pnt.replace(",","");
				 CRRes2016pnt = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[3]/td[3]")).getText();
					CRRes2016pnts = CRRes2016pnt.replace(",","");
				
			 	Properties properties = new Properties();
			 		properties.setProperty("CRRes2015pnt", CRRes2015pnt);
			 			properties.setProperty("CRRes2015pnts",CRRes2015pnts);
			 				properties.setProperty("CRRes2016pnt", CRRes2016pnt);
			 					properties.setProperty("CRRes2016pnts", CRRes2016pnts);
				

				File file = new File("Reservation.Properties");
				FileOutputStream fileOut = new FileOutputStream(file);
				properties.store(fileOut, "Available Points");
				fileOut.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 
	 }
	 
	public void BookReservation() {
		
		CReservations15();
		
		PropertyConfigurator.configure("Log4j.properties");
		
	//-----------------------------------------------------------------------Usage Information------------------------------------------------------------------------------------------//
		logger.info("-------------------------------------------------------------------Usage Information------------------------------------------------------------");
		
		//Recognition Level
		String CRRecogLev = driver.findElement(By.xpath(".//*[@id='pvMbrRecLev']")).getText();
			logger.info("Recognition Level: "+CRRecogLev);
				
			//Level Effective Date
			String CRLevEffDate = driver.findElement(By.xpath(".//*[@id='pvMbrEFD']")).getText();
				logger.info("Level Effective Date: "+CRLevEffDate);
				
				//Club Select
				String CRClubSelect = driver.findElement(By.xpath(".//*[@id='pvMbrClub']")).getText();
					logger.info("Club Select: "+CRClubSelect);
					
					//Club Combination
					String CRClubCom = driver.findElement(By.xpath(".//*[@id='pvMbrClubCombo']")).getText();
						logger.info("Club Combination: "+CRClubCom);
						
						//# of Intervals
						String CRIntevals = driver.findElement(By.xpath(".//*[@id='pnMbrInterval']")).getText();
							logger.info("# of Intervals: "+CRIntevals);
							
							//Annual Points
							String CRAnnualPoints = driver.findElement(By.xpath(".//*[@id='pnMbrPccp']")).getText();
								logger.info("Annual Points: "+CRAnnualPoints);
								
							//2015 points
							  String CRAllotment15 = driver.findElement(By.xpath(".//*[@id='pv1MbrAlt']")).getText();
								logger.info("Allotment for 2015: "+CRAllotment15);
								
								//2015 Avail for Reservations
									String CRReservations15 = CRRes2015pnt;
										logger.info("2015 Available for Reservations: "+CRReservations15);
									
									//2015 Avail for Member Benefits
									String CRMemBen15 = driver.findElement(By.xpath(".//*[@id='mbrAvaBenefit']")).getText();
										logger.info("2015 Available for Member Benefits: "+CRMemBen15);
								
									//Available for Member Benefits Flag
									driver.findElement(By.xpath(".//*[@id='mbrbenefitimg']")).click();
										logger.info("Clicked on Available for Member Benefits Flag icon");
											wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='MBTCloseBtn']"))); // Wait till the element is present to click
												String CRMemBenfFlag = driver.findElement(By.xpath(".//*[@id='MBTTitle']")).getText();
													Assert.assertEquals(CRMemBenfFlag, CRMemBen);
														logger.info("Member Benefits Header display and  Passed Assertion (Flag): "+CRMemBenfFlag);
															driver.findElement(By.xpath(".//*[@id='MBTCloseBtn']")).click();	
																logger.info("Clicked on Close Button (Flag)");		
										
										//2015 Avail to save to following yr 
										String CRSave15 = driver.findElement(By.xpath(".//*[@id='mbrAvaSave']")).getText();
											logger.info("2015 Available to Save to Following yr: "+CRSave15);
											
										//Available to Save to following yr Flag
										driver.findElement(By.xpath(".//*[@id='mbrsavepointimg']")).click();
											logger.info("Available to Save to following yr Flag icon");
												wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='MSPICloseBtn']"))); // Wait till the element is present to click
													String CRPtSgDs = driver.findElement(By.xpath(".//*[@id='MSPITitle']")).getText();
														Assert.assertEquals(CRPtSgDs, CRPoiSavDdLines);
															logger.info("Point Saving Deadlines Header display and  Passed Assertion (Flag): "+CRPtSgDs);
																driver.findElement(By.xpath(".//*[@id='MSPICloseBtn']")).click();	
																	logger.info("Clicked on Close Button (Flag)");		
										
											//2016 points
											String CRAllotment16 = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[2]/td[3]")).getText();
												logger.info("Allotment for 2016: "+CRAllotment16);
												
												//2016 Avail for Reservations
													String CRReservations16 = CRRes2016pnt;
														logger.info("2016 Available for Reservations: "+CRReservations16);
													
													//2016 Avail for Member Benefits
													String CRMemBen16 = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[4]/td[3]")).getText();
														logger.info("2016 Available for Member Benefits: "+CRMemBen16);
														
														//2016 Avail to save to following yr 
														String CRSave16 = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[5]/td[3]")).getText();
															logger.info("2016 Available to Save to Following yr: "+CRSave16);
																	
																
	//-----------------------------------------------------------------------------------Financial Information----------------------------------------------------------------//
						
					logger.info("-------------------------------------------------------------------Financial Information------------------------------------------------------------");
					
					//Financial Information Header
					String CRFinancialInfo = driver.findElement(By.xpath(".//*[@id='membershipAcctTitle']")).getText();
						Assert.assertEquals(CRFinancialInfo, CRFinancialInformation);
							logger.info("Financial Information Header displayed and Passed Assertion- "+CRFinancialInfo);
							
	//-----------------------------------------------------------------------------------Contract Details----------------------------------------------------------------//
	
					logger.info("-------------------------------------------------------------------Contract Details------------------------------------------------------------");
					
					//Contract Details Header
					String CRContDet = driver.findElement(By.xpath(".//*[@id='tblContractInfo']/thead/tr[1]/td")).getText();
					Assert.assertEquals(CRContDet, CRContractDetails);
						logger.info("Contract Details Header displayed and Passed Assertion- "+CRContDet);		
			
	//-----------------------------------------------------------------------Book Reservation------------------------------------------------------------------------------------------//
					logger.info("-------------------------------------------------------------------Book Reservation------------------------------------------------------------");
					
					//Book Reservation																						
					driver.findElement(By.xpath(".//*[@id='cr_buttonBOK']")).click();  //Clicked on Book Reservation
						logger.info("Clicked on Book Reservation");
							driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
					
					Select BookingType = new Select(driver.findElement(By.xpath(".//*[@id='rtmBookType_select']"))); 
						BookingType.selectByValue(BookingTypeReservation);// Select Booking Type	
					
					logger.info("Clicked and selected the booking type");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							
					//Book Reservation Header
					String BookNewReservation = driver.findElement(By.xpath(".//*[@id='ResvType_FRM_DragBar']")).getText();		
						logger.info("Book New Reservation Header: "+BookNewReservation);
							Assert.assertEquals(BookNewReservation, CRBookNewREs);
								logger.info("Book New Reservation Matched and Assertion passed");
				
					driver.findElement(By.xpath(".//*[@id='rtm_continueBtn']")).click();  //Click Continue
						logger.info("Clicked Continue");
						
	//-----------------------------------------------------------------------Search For Availability------------------------------------------------------------------------------------------//
					logger.info("-------------------------------------------------------------------Search For Availability------------------------------------------------------------");
							
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ren_buttonSearch']"))); // Wait till the element is present to click
					
					//Search for Availability Header
					String SearchforAvail = driver.findElement(By.xpath(".//*[@id='caw_searchAvailHeaderTable']/tbody/tr/td")).getText();
						Assert.assertEquals(SearchforAvail, SASearchforAva);
							logger.info("Search for Availability Header displayed and Passed Assertion" +SearchforAvail);
							
					//Choose a Destination Header
					String ChseDestination = driver.findElement(By.xpath(".//*[@id='caw_StayParams']/table[1]/tbody/tr[1]/td")).getText();
						Assert.assertEquals(ChseDestination, SADestination);
							logger.info("Choose a Destination Header displayed and Passed Assertion: " +ChseDestination);
					
					//Choose Travel Dates Header
					String ChseTravDates = driver.findElement(By.xpath(".//*[@id='caw_StayParams']/table[1]/tbody/tr[8]/td")).getText();
						Assert.assertEquals(ChseTravDates, SATravelDates);
							logger.info("Choose Travel Dates Header displayed and Passed Assertion: " +ChseTravDates);
							
					//Additional Search Parameters Header
					String AddSearPara = driver.findElement(By.xpath(".//*[@id='caw_add_search_params']/tbody/tr[1]/td")).getText();
						Assert.assertEquals(AddSearPara, SASearPara);
							logger.info("Additional Search Parameterss Header displayed and Passed Assertion: " +AddSearPara);
							
					//Select Region
						driver.findElement(By.xpath(".//*[@id='caw_region_input']")).click();
							Select Region = new Select(driver.findElement(By.id("caw_region_select")));
								Region.selectByValue("AZ");
									logger.info("Selected Region as Arizona");
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
					//Select Property
						driver.findElement(By.xpath(".//*[@id='caw_property_input']")).click();
							Select Property = new Select(driver.findElement(By.id("caw_property_select")));
								Property.selectByValue("BRI");
									logger.info("Selected Property as Bell Rock Inn");
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					//Select Arrival Method
						driver.findElement(By.xpath(".//*[@id='caw_arr_meth_input']")).click();
							Select Arrival = new Select(driver.findElement(By.id("caw_arr_meth_select")));
								Arrival.selectByValue("FlexDays");
									logger.info("Selected Arrival Method as Flexible days");
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
					//Arrival Date
						driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).click();
							driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).sendKeys("t+90");
			
					//No.of Nights
						driver.findElement(By.xpath(".//*[@id='caw_nights']")).sendKeys(SANights);
							logger.info("Selected No.of Nights as  - 2");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							
					//No.of Adults
						driver.findElement(By.xpath(".//*[@id='caw_num_adults']")).sendKeys(SAAdults);
							logger.info("Entered Adults - 2");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					
					//Click on Search		
					driver.findElement(By.xpath(".//*[@id='ren_buttonSearch']")).click(); //Click on Search button
						logger.info("Clicked on Search");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ResAvailResults']/tbody/tr[3]/td[5]/img"))); // Wait till the element is present to click
			}
	
			
	//---------------------------------------------------------------------Resort Availability------------------------------------------------------------//
			
		public void Resort_Availability(){
			
			logger.info("-------------------------------------------------------------------Resort Availabilty------------------------------------------------------------");
			
			try{
			
			//Resort Availability - CLUB
			String ResAvaCLUB = driver.findElement(By.xpath(".//*[@id='caw_ASWTitle']")).getText();
				Assert.assertEquals(ResAvaCLUB, ResortAvaCLUB);
					logger.info("Resort Availability-CLUB header displayed and Passed Assertion: "+ResAvaCLUB);
					
					//To get the Room Upgrade Screen
					WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody"));
					List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
						//logger.info("No.of rows: " +columns.size());
							int rowsnum = columns.size();
									String xpath =null;
										String cellval;
											String cellval1;
												for(int i = 3;i<=rowsnum;i++)
												{
													xpath= ".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[4]";
														cellval = driver.findElement(By.xpath(xpath)).getText();
															cellval1 = cellval.substring(0,3);
																logger.info("Substring is: "+cellval1);
															
														driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
														
														if(ResortType.equals(cellval1))
														{
																driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[1]/input")).click();
																logger.info("Clicked on SXX Available Resort");
																
																//Errata
																driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[2]/img")).click();
																	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the element is present to click
																		String RAErrata = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
																			Assert.assertEquals(RAErrata, RAErrataCLUB);
																				logger.info("Additional Resort Information (Errata) header displayed and Passed Assertion: "+RAErrata);
																					driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();	
																						logger.info("Clicked on Close Button (Additional Resort Information Flag)");
																						
																						 
																//ResortName
																String RAResort = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[3]")).getText();
																	logger.info("Clicked on the Resort" +RAResort);
																	
																//RoomType/Max Occ
																String RARmType = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[4]")).getText();
																	logger.info("RoomType: "+RARmType);
																String RARoomType = RARmType.substring(0, 3);
																	
																//Flag
																driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[5]/img")).click();
																	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the element is present to click
																		String RAFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
																			logger.info("Flag Header is: "+RAFlagHeader);
																				driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
																					logger.info("Clicked on Close Button (Room Amenitiesx Flag)");
																		
																//Arrival Date
																String RAArrivalDate = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[8]")).getText();;
																	logger.info("Arrival Date: "+RAArrivalDate);
																	
																	
																//Departure Date
																String RADepartureDate = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[9]")).getText();//Departure Date;
																	logger.info("Departure Date: "+RADepartureDate);
																
																//Points
																String RAPoints = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[10]")).getText(); //Number of points;
																	logger.info("Points: "+RAPoints);
																		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
																		
																		Properties properties = new Properties();
																		
																 		properties.setProperty("RAPnts",RAPoints);
														 					properties.setProperty("RAResortName", RAResort);
														 						properties.setProperty("RARoomType",RARoomType);
														 							properties.setProperty("RAArrDate", RAArrivalDate);
														 								properties.setProperty("RADepDate", RADepartureDate);
																		
																		File file = new File("ResortAvailability.Properties");
																		FileOutputStream fileOut = new FileOutputStream(file);
																		properties.store(fileOut, "Information from Resort Availability");
																		fileOut.close();
																		
																		//Click on Continue
																		driver.findElement(By.xpath(".//*[@id='caw_ASWBtnProcess']")).click();								
																			logger.info("Clicked on Continue");
																				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
																		break;
														}
												}
				}
														 catch (FileNotFoundException e) {
															e.printStackTrace();
														} catch (IOException e) {
															e.printStackTrace();
														}
			
			}
		
			
		//---------------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------//
			
			public void Errata(){
			 
			logger.info("-------------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------");
			
			 if(driver.findElements(By.xpath(".//*[@id='REWHeader']")).size() !=0) 
				{
				 
				 //Errata Header
				 String ADRIErrata = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText(); // //Header of Additional Resort Information (Errata)
				 	//Assert.assertEquals(ADRIErrata, RAErrataCLUB);
				 		logger.info("Header Displayed and Passed Assertion: "+ADRIErrata);
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 	
				 	//Click on Close Button
					driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();
						logger.info("Clicked on close button");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					
				}
				
				else
				{
					logger.info("Errata window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
			 
			}
			
				
			public void ResProperties()
			 {
				  try (FileReader reader = new FileReader("ResortAvailability.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		
			      		RAAPoints = properties.getProperty("RAPnts");
			      		RAARoomType = properties.getProperty("RARoomType");
			      		RAAResort = properties.getProperty("RAResortName");
			      		RAAArrivalDate = properties.getProperty("RAArrDate");
			      		RAADepartureDate = properties.getProperty("RADepDate");
			      		
			      		logger.info(RAAPoints);
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				  
				  try (FileReader reader = new FileReader("RoomUpgrade.Properties")) {
						 Properties properties = new Properties();
				      		properties.load(reader);
				      		
				      		RoomupgAvailability = properties.getProperty("RoomupgAvailability");
				      		RoomType = properties.getProperty("RuRoomType");
				      	} catch (IOException e) {
				      		e.printStackTrace();
				      	}
				 
			 }
			
		//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
			
			public void Validation() {
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
				
				ResProperties(); //Call ResProperties Method
				
				logger.info("-------------------------------------------------------------------Reservation------------------------------------------------------------");
				//Reservation Header
				String CRReservation = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[1]/td")).getText();
					Assert.assertEquals(CRReservation, CRReserHeader);
						logger.info("Reservation Header Displayed and Passed Assertion: "+CRReservation);
						
				//Booking Function
				String CRBookFunc = driver.findElement(By.xpath(".//*[@id='ResSum_BookingPurpose']")).getText();
					Assert.assertEquals(CRBookFunc, BookTypeReserName);
						logger.info("Booking Fuction Name Printed and Matched the Assertion: "+CRBookFunc);
						
				//Marketing KeyCode
				String CRMarkeyCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
					logger.info("Marketing Key Code: "+CRMarkeyCode);
						
						/*//Marketing KeyCode Flag
						driver.findElement(By.xpath(".//*[@id='imgMktInfo']")).click(); //Click on Marketing Keycode Img Flag
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mktSaveButton']"))); // Wait till the SAVE button is present 
								String CRMarKeyCodeFlag = driver.findElement(By.xpath(".//*[@id='RMIWHeader']")).getText();
									Assert.assertEquals(CRMarKeyCodeFlag, CRMrKyFlag);
										logger.info("Marketing Information (In Flag) Header Displayed and Passed Assertion: "+CRMarKeyCodeFlag);
											driver.findElement(By.xpath(".//*[@id='RMIWbtnClose']")).click(); //Click on Close button
*/				
				//ReservationType
				String CResType = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[4]/td[2]")).getText();
					logger.info("Reservation Type is: "+CResType);
					
				//Reservation Status
				String CResStatus = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]")).getText();
					logger.info("Reservation Status: "+CResStatus);
					
				//Property
				String CRProperty = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText();
					Assert.assertEquals(CRProperty, RAAResort);
						logger.info("Property displayed and Matched From Resort Availabilty screen: "+CRProperty);
						
						//Property Flag
						driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[6]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on The flag image
							logger.info("Clicked on Property Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the Close button is present 
								String CRPropFlag = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
									Assert.assertEquals(RAErrataCLUB, CRPropFlag);
										logger.info("Property Flag Header displayed and Matched: "+CRPropFlag);
											driver.findElement(By.id("REWbtnClose")).click(); //Click on Close button

				//ArrivalDate
				String CRArrDate = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[7]/td[2]")).getText();
					Assert.assertEquals(RAAArrivalDate, CRArrDate);
						logger.info("Arrival Date displayed and Matched From Resort Availabilty screen: "+CRArrDate);
				
				//Nights
				String CRNights = driver.findElement(By.xpath(".//*[@id='ResSum_pnNights']")).getAttribute("value");
					Assert.assertEquals(SANights, CRNights);
						logger.info("Number of Nights Displayed and Matched Assertion: "+CRNights);
							
							//Nights Flag
							driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[8]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Nights Flag
							logger.info("Clicked on Nights Flag");
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RNWbtnClose']"))); // Wait till the Close button is present 
									String CRNigFlag = driver.findElement(By.xpath(".//*[@id='RNWHeader']")).getText();
										logger.info("Nights Flag Header Displayed: "+CRNigFlag);
											driver.findElement(By.xpath(".//*[@id='RNWbtnClose']")).click();
											
				//Departure Date
				String CRDepDate = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value");
					Assert.assertEquals(RAADepartureDate, CRDepDate);
						logger.info("Departure Date displayed and Matched from Resort Availability Screen: "+CRDepDate);		
						
				//Guarantee Code
				String CRGuaCode = driver.findElement(By.xpath(".//*[@id='ResSum_pvGuarantee_input']")).getAttribute("value");
					logger.info("Guarantee Code: "+CRGuaCode);
					
				//Usage Period
				String CRUsaPeriod = driver.findElement(By.xpath(".//*[@id='ResSum_UsagePeriod']")).getText();
					logger.info("Usage Period: "+CRUsaPeriod);
				
				//Membership Number
				String CRMemshipNum = driver.findElement(By.xpath(".//*[@id='ResSum_MemberContrID']")).getText();
					logger.info("MemberShip Number: "+CRMemshipNum);
				
				//Number of Guests
				String NoofGues = driver.findElement(By.xpath(".//*[@id='ResSum_Adults']")).getAttribute("value");	
					Assert.assertEquals(SAAdults, NoofGues);
						logger.info("Number of Adults Displayed and Matched in Search of Availability Screen: "+NoofGues);
				
				//Roomtype
				if(RoomupgAvailabilityComp.equals(RoomupgAvailability))	
				{
					String CRRoomType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
					String CRRmType = CRRoomType.substring(0, 3);
						logger.info(CRRmType);
							Assert.assertEquals(RoomType, CRRmType);
								logger.info("Room Type Displayed and Matched from Reservation Availability Screen: "+CRRoomType);
				}
				
				else
				{
				String CRRoomType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
				String CRRmType = CRRoomType.substring(0, 3);
					logger.info(CRRmType);
						Assert.assertEquals(RAARoomType, CRRmType);
							logger.info("Room Type Displayed and Matched from Reservation Availability Screen: "+CRRoomType);
				}
				
				//Room Type Flag
				driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Roomtype Flag
					logger.info("Clicked on Room Type Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the Close button is present
							String CRTFlag = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
								logger.info(""+CRTFlag);
									driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
	
				logger.info("-------------------------------------------------------------------Special Requests------------------------------------------------------------");
				
				//Special Request Header
				String CRSpecReq = driver.findElement(By.xpath(".//*[@id='specialRequests']/table/tbody/tr[1]/td")).getText();
					Assert.assertEquals(CRSpecReq, CRSpecReq);
						logger.info("Special Requests Header displayed and Passed Assertion: "+CRSpecReq);
				
				logger.info("-------------------------------------------------------------------Disability and Access Requirements------------------------------------------------------------");
				
				//Disability and Access Requirements
				String CRDisAccRequirements = driver.findElement(By.xpath(".//*[@id='ADAwidget']/table[1]/tbody/tr[1]/td")).getText();
					Assert.assertEquals(CRDisAccReq, CRDisAccRequirements);
						logger.info("Disability and Access Requirement Header displayed and Passed Assertion: "+CRDisAccRequirements);	
						
				logger.info("-------------------------------------------------------------------Guests------------------------------------------------------------");		
				
				//Guests
				String CRGuests = driver.findElement(By.xpath(".//*[@id='addGuestDiv']/form/table[1]/tbody/tr/td")).getText();
					Assert.assertEquals(CRGue, CRGuests);
						logger.info("Guests Header displayed and Passed Assertion: "+CRGuests);	
				
				logger.info("-------------------------------------------------------------------Cancellation Policy------------------------------------------------------------");
				
				//Cancellation Policy
				String CRCancelPolicy = driver.findElement(By.xpath(".//*[@id='cancelation_header']/tbody/tr[1]/td")).getText();
					Assert.assertEquals(CRCanPol, CRCancelPolicy);
						logger.info("Cancellation Policy Header displayed and Passed Assertion: "+CRCancelPolicy);	
						
				logger.info("-------------------------------------------------------------------Additional Products Offer------------------------------------------------------------");
				
				//Additional Products Offer
				String CRAddProOff = driver.findElement(By.xpath(".//*[@id='OFFRDATATABLE']/tbody/tr[1]/td")).getText();
					Assert.assertEquals(CRAddProOff, CRAddProductsOff);
						logger.info("Additional Products Offer Header Displayed and Passed Assertion: "+CRAddProOff);
										
			}
				 
					
	}
