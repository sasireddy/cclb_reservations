package claritydb;


import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;


public class ClubRes extends Common {
	 Logger logger = Logger.getLogger("ClubRes.class");
			 
	
	 public void Clubres() {
		 
		 PropertyConfigurator.configure("Log4j.properties");
			
		 logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
			if(driver.findElements(By.xpath("/html/body/div[15]/div/div/div[3]")).size()!=0) 
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath("/html/body/div[15]/div/div/div[3]")).click();
				
			}
			
			else
			{
				logger.info("---Notice---Notice---Notice---Window not found");
			}

			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
	//-----------------------------------------------------------------------GUEST------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Guest------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='guestypeimg']"))); // Wait till the element LEAD TYPE FLAG is present to click
			
			//GuestName and Header
			String CRGuestHeader = driver.findElement(By.xpath(".//*[@id='GUESTTABLE']/tbody/tr[1]/td[1]/span[1]")).getText();
				logger.info("Guest Header Displayed: "+CRGuestHeader);
			String CRGuestName = driver.findElement(By.xpath(".//*[@id='pv1guest_name']")).getText();
				logger.info("Guest Name: "+CRGuestName);
			
				
				//Lead Number
				String CRLead_Num = driver.findElement(By.xpath(".//*[@id='pv1guest_lead']/a")).getText();
					logger.info("Lead Area-Lead Id: " +CRLead_Num);
				
					//Lead Type
					String CRLead_Type = driver.findElement(By.xpath(".//*[@id='pv1GuestType']/b")).getText();
							logger.info("Lead Type/Subtype: "+ CRLead_Type);
					
					//LTFlag
					driver.findElement(By.xpath(".//*[@id='guestypeimg']")).click();
						logger.info("Clicked on Lead Type Flag icon");	
							Actions action = new Actions(driver);
								WebElement LTFlag = driver.findElement(By.xpath(".//*[@id='Closeguesttooltip']/font"));
									action.moveToElement(LTFlag).perform();
											      
			//Membership Number
			String CRMemNum = driver.findElement(By.xpath(".//*[@id='pv1IIMemberNum']")).getText();
				logger.info("Membership Number: "+CRMemNum);
										
				//HomePhone
				String CRHomePhone = driver.findElement(By.xpath(".//*[@id='pv1HomePhNum']")).getAttribute("value");
			        	logger.info("Club Reservations Homephone: " +CRHomePhone);
			        		Assert.assertEquals(HomePhone, CRHomePhone);
			        			logger.info("HomePhone Number in Club Reservations Matched the HomePhone Number in LeadInformation Page");

		    		//WorkPhone
					String CRWorkPhone = driver.findElement(By.xpath(".//*[@id='pv1WorkPhNum']")).getAttribute("value");
				        	logger.info("Club Reservations WorkPhone: " +CRWorkPhone);
				        		Assert.assertEquals(WorkPhone, WorkPhone);
				        			logger.info("WorkPhone Number in Club Reservations Matched the WorkPhone Number in LeadInformation Page");
						
						//MobilePhone
						String CRMobilePhone = driver.findElement(By.xpath(".//*[@id='pv1MobilePhNum']")).getAttribute("value");
					        	logger.info("Club Reservations MobilePhone: " +CRWorkPhone);
					        		Assert.assertEquals(MobilePhone, CRMobilePhone);
					        			logger.info("MobilePhone Number in Club Reservations Matched the MobilePhone Number in LeadInformation Page");

			//FaxNumber
			String CRFaxNumber = driver.findElement(By.xpath(".//*[@id='pv1FaxNum']")).getAttribute("value");
		        	logger.info("Club Reservations FaxNumber: " +CRFaxNumber);
		        		Assert.assertEquals(FaxNumber, CRFaxNumber);
		        			logger.info("FaxNumber in Club Reservations Matched the FaxNumber in LeadInformation Page");

				//Email Address
			    String CREmail = driver.findElement(By.xpath(".//*[@id='pv1Email']")).getAttribute("value");
			    	logger.info("Club Reservations Email Address: "+CREmail);
			    		Assert.assertEquals(PriEmail, CREmail);
			    			logger.info("Email Address in Club Reservations Matched the Email Address in LeadInformation Page");

					//Address
				    String CRAddress = driver.findElement(By.xpath(".//*[@id='pv1Addr']")).getAttribute("value");
				    	logger.info("Club Reservations Address: "+CRAddress);
				    		Assert.assertEquals(Address, CRAddress);
				    			logger.info("Address in Club Reservations Matched the Address in LeadInformation Page");
				    			
						//Postal
					    String CRPostal = driver.findElement(By.xpath(".//*[@id='pv1Postal']")).getAttribute("value");
					    	logger.info("Club Reservations Postal Code: "+CRPostal);
					    		Assert.assertEquals(Postal, CRPostal);
					    			logger.info("Postal Code in Club Reservations Matched the Postal Code in LeadInformation Page");
						
												    					
			//LeadFlags
				driver.findElement(By.xpath(".//*[@id='1leadflagimage']")).click();
					logger.info("Clicked on Lead Flags icon");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='addLeadCancelButton']"))); // Wait till the element is present to click
							String LeadFlags = driver.findElement(By.xpath(".//*[@id='leadFlagsHeaderTable']/tbody/tr/td")).getText();
								logger.info("LeadFlags displayed:"+LeadFlags);
									Assert.assertEquals(LeadFlags, CRLeadFlag);
										logger.info("LeadFlags header Passed (Flag)");
											driver.findElement(By.xpath(".//*[@id='addLeadCancelButton']")).click();	
												logger.info("Clicked on Close Button (Flag)");
										
				//Customer Profile Flag
				driver.findElement(By.xpath(".//*[@id='1custProfileImage']")).click();
					logger.info("Clicked on CustomerProfile Flag icon");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='CPWDCloseBtn']"))); // Wait till the element is present to click
							String CusProFlag = driver.findElement(By.xpath(".//*[@id='CPWDTitle']")).getText();
								logger.info("Customer Profile Flag displayed: "+CusProFlag);
									Assert.assertEquals(CusProFlag, CRCusProfile);
										logger.info("Customer Profile header Passed (Flag)");
											driver.findElement(By.xpath(".//*[@id='CPWDCloseBtn']")).click();	
												logger.info("Clicked on Close Button (Flag)");
									
					//Full Lead Information Flag
					driver.findElement(By.xpath(".//*[@id='pv1FullLeadInfoIcon']")).click();
						logger.info("Clicked on Full Lead Information Flag icon");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='LEAD_FRM_CLOSE_BUTTON']"))); // Wait till the element is present to click
								String LeadInfoFlag = driver.findElement(By.xpath(".//*[@id='Lead_FRM_DragBar']")).getText();
									logger.info("Full Lead Information Flag displayed: "+LeadInfoFlag);
										Assert.assertEquals(LeadInfoFlag, CRFullLeadInfo);
											logger.info("Lead Information header Passed (Flag)");
												driver.findElement(By.xpath(".//*[@id='LEAD_FRM_CLOSE_BUTTON']")).click();	
													logger.info("Clicked on Close Button (Flag)");								

		//-----------------------------------------------------------------------Membership Information------------------------------------------------------------------------------------------//
			logger.info("-------------------------------------------------------------------Membership Information------------------------------------------------------------");		
			
				//Membership Information Header
				String CRMembershipInfo = driver.findElement(By.xpath(".//*[@id='divMemberInfo']/table[1]/tbody/tr/td")).getText();
					logger.info("Membership Information Header Displayed: " +CRMembershipInfo);
				
					//Lead Designation
					String CRLeadDesig = driver.findElement(By.xpath(".//*[@id='tdMbrLeadDesgn']")).getText();
						logger.info("Lead Designation: "+CRLeadDesig);
					
						//Membership Type
						String CRMemType = driver.findElement(By.xpath(".//*[@id='tdMbrtype']")).getText();
							logger.info("Membership Type: "+CRMemType);
						
							//Membership Status
							String CRMemStatus = driver.findElement(By.xpath(".//*[@id='mbrStatus']")).getText();
								logger.info("Membership Status: "+CRMemStatus);
				
								//Membership Date
								String CRMemDate = driver.findElement(By.xpath(".//*[@id='tdMbrDate']")).getText();
									logger.info("Membership Date: "+CRMemDate);
						
									//Membership Effective
									String CRMemEffec = driver.findElement(By.xpath(".//*[@id='tdMbrEff']")).getText();
										logger.info("Membership Effective: "+CRMemEffec);
						
										//Exchange Affiliation
										String CRExcAff = driver.findElement(By.xpath(".//*[@id='tdMbrXchAff']")).getText();
											logger.info("Exchange Affiliation: "+CRExcAff);
											
											/*	// Store the current window handle
											String winClubRes = driver.getWindowHandle();
												
											//Associate Members Flag
											driver.findElement(By.xpath(".//*[@id='AtlasMembInfoIcon']")).click();
												logger.info("Clicked on Associate Members Flag icon");
									 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

											// Switch to new window opened
											//for(String winAssociate : driver.getWindowHandles()){
											   // driver.switchTo().window(winAssociate);
											
	  							driver.switchTo().frame(driver.findElement(By.id("folioFrameSet")));
	  							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/form/table[2]/tbody/tr/td"))); // Wait till the element is present to click
	  									String AssocMembers = driver.findElement(By.xpath("html/body/form/table[2]/tbody/tr/td")).getText();
	  										logger.info("Associate Member Header displayed: "+AssocMembers);
	  											Assert.assertEquals(AssocMembers, CRAssocMembers);
	  												logger.info("Associate Members header Passed (Flag)");
	  													driver.findElement(By.xpath("html/body/form/div[1]/table/tbody/tr/td[1]/a/img")).click();	
	  														logger.info("Clicked on Back Button (Flag)");	
	  															driver.switchTo().window(winClubRes);//Change to Parent Window
*/	  															
		//-----------------------------------------------------------------------Membership Detail------------------------------------------------------------------------------------------//
											
			logger.info("-------------------------------------------------------------------Membership Detail------------------------------------------------------------");
			
			String CRMemDetail = driver.findElement(By.xpath(".//*[@id='tdMembership']")).getText(); //Membership Detail For:
				logger.info("Membership Details for: "+CRMemDetail);
					
	
}
}
