package claritydb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;


public class Financial_Summary extends Common {
	
	 Logger logger = Logger.getLogger("Financial_Summary.class");
	 
	public String LEADID;
	public String RoomUpgradeCost;
	public String PrepaymentFee;
	
	public String ReservationPossible;
	public String RoomupgAvailability;
	
	public String ReservationPossibleComp = "The Points Are Not Available To Book The Reservation";
	public String RoomupgAvailabilityComp = "Room Upgrade Option is Available";
	
	public String RmUpgNotAvail;
	public String RoomUpgNotAvailComp = "Room Upgrade is not available, so loyalty accomodation upgrade fee is not available: PASSED";
	
	public String Zero = "0.00";
	
	 

	
	//With decline rpp
	public void FinancialSummary() {
	
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
		
		//Financial Summary Header
		String CRFinanSumm = driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[1]/td[1]")).getText();
			Assert.assertEquals(CRFinanSumm, CRFinSumm);
				logger.info("Financial Summary Header displayed and Passed Assertion: "+CRFinanSumm);
					
		//Reservation Rate Code
		String CRResRateCode = driver.findElement(By.xpath(".//*[@id='tdfolio_rate']/b")).getText();
			logger.info("Reservation Rate Code: "+CRResRateCode);
				
		//Rate Amount (Curr/pts)
		String CRRateAmount = driver.findElement(By.xpath(".//*[@id='pvfolio_rateamt']")).getText();
			logger.info("Rate Amount (Curr/pts): "+CRRateAmount);
			
				//Rate Amount Flag
				driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[3]/td[2]/div/span[2]/img")).click(); //Click on Flag icon	
					logger.info("Clicked on Rate Amount Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]"))); // Wait till the Close Button is present 
							String CRNigRatInfo = driver.findElement(By.xpath(".//*[@id='RATEINFO']/tbody/tr[1]/td")).getText();
								Assert.assertEquals(CRNigRatInfo, CRNgtRateInform);
									logger.info("Night Rate Information Flag Header Displayed and Passed Assertion: "+CRNigRatInfo);
										driver.findElement(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]")).click(); //Click on Close button
		//Stay Total
		String CRStaTot = driver.findElement(By.xpath(".//*[@id='pvfolio_staytot']")).getText();
			logger.info("Stay Total: "+CRStaTot);
			
		//Extra Resort Charges
		String CRResCharges = driver.findElement(By.xpath(".//*[@id='pvfolio_excharge']")).getText();
			logger.info("Extra Resort Charges: "+CRResCharges);	
				
				//Extra Resort Charges Flag
				driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[8]/td[2]/div/span[2]/img")).click(); //Click on Flag icon	
					logger.info("Extra Resort Charges Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='exchdiv']/div/table/tbody/tr/td[1]"))); // Wait till the Close Button is present 
							String CRExtra = driver.findElement(By.xpath(".//*[@id='EXTRACHARGESINFO']/tbody/tr[1]/td")).getText();
								Assert.assertEquals(CRExtra, CRExtrResChar);
									logger.info("Extra Resort Charges Flag Header Displayed and Passed Assertion: "+CRExtra);
										driver.findElement(By.xpath(".//*[@id='exchdiv']/div/table/tbody/tr/td[1]")).click(); //Click on Close button
		//Credit Line:
		String CRCrdLine = driver.findElement(By.xpath(".//*[@id='pvfolio_creditline']")).getText();
			logger.info("Credit Line: "+CRCrdLine);
				
		//Reservation Advance Deposit
		String CPResAdDep = driver.findElement(By.xpath(".//*[@id='advDepSpan']")).getText();
			logger.info("Reservation Advance Deposit: "+CPResAdDep);
	
				//Reservation Advance Deposit Flag
				driver.findElement(By.xpath(".//*[@id='folioImg']")).click();
					logger.info("Clicked on Reservation Advance Deposit Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='BackBtnAdvDep']"))); // Wait till the Close Button is present 
							String CRAdvanceDep = driver.findElement(By.xpath(".//*[@id='ADVDEPOSITSINFO1']/tbody/tr[1]/td")).getText();
								Assert.assertEquals(CRAdvanceDep, CRAdvDeposits);
									logger.info("Advance Deposits Flag Header Displayed and Passed Assertion: "+CRAdvanceDep);
										driver.findElement(By.xpath(".//*[@id='BackBtnAdvDep']")).click(); //Click on Close button	
															
		//Reservation And Misc Fees
		String CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText();
			logger.info("Reservation And Misc Fees: "+CPResAdMsc);
	
				//Reservation And Misc Fees Flag
				driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
					logger.info("Clicked on Reservation And Misc Fees Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
							String CRResMisFees = driver.findElement(By.xpath(".//*[@id='RFIFTitle']")).getText();
								Assert.assertEquals(CRResMisFees, CRReserMiscFees);
									logger.info("Reservation And Misc Fees Flag Header Displayed and Passed Assertion: "+CRAdvanceDep);
										driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
										
			//Click on SAVE Button
				driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
					logger.info("Clicked on SAVE BUTTON");
					
	}
	
					
	//With RPP Purchase or Decline RPP
	public void FinancialSummary1() {
		
		try (FileReader reader = new FileReader("Leadid.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		
	      		LEADID = properties.getProperty("Leadid");
	      		logger.info(""+LEADID);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		
		try (FileReader reader = new FileReader("Prepayment.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		
	      		PrepaymentFee = properties.getProperty("CostOfPntsBrwd");
	      			ReservationPossible = properties.getProperty("ReservationPossible");
	      				logger.info(""+PrepaymentFee);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		
		try (FileReader reader = new FileReader("RoomUpgrade.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		
	      		RoomUpgradeCost = properties.getProperty("RoomUpgCost");
	      		RoomupgAvailability = properties.getProperty("RoomupgAvailability");
	      				logger.info(""+RoomUpgradeCost);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		
		
		logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
		
		//Financial Summary Header
		String CRFinanSumm = driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[1]/td[1]")).getText();
			Assert.assertEquals(CRFinanSumm, CRFinSumm);
				logger.info("Financial Summary Header displayed and Passed Assertion: "+CRFinanSumm);
					
		//Reservation Rate Code
		String CRResRateCode = driver.findElement(By.xpath(".//*[@id='tdfolio_rate']/b")).getText();
			logger.info("Reservation Rate Code: "+CRResRateCode);
				
		//Rate Amount (Curr/pts)
		String CRRateAmount = driver.findElement(By.xpath(".//*[@id='pvfolio_rateamt']")).getText();
			logger.info("Rate Amount (Curr/pts): "+CRRateAmount);
			
				//Rate Amount Flag
				driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[3]/td[2]/div/span[2]/img")).click(); //Click on Flag icon	
					logger.info("Clicked on Rate Amount Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]"))); // Wait till the Close Button is present 
							String CRNigRatInfo = driver.findElement(By.xpath(".//*[@id='RATEINFO']/tbody/tr[1]/td")).getText();
								Assert.assertEquals(CRNigRatInfo, CRNgtRateInform);
									logger.info("Night Rate Information Flag Header Displayed and Passed Assertion: "+CRNigRatInfo);
										driver.findElement(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]")).click(); //Click on Close button
		//Stay Total
		String CRStaTot = driver.findElement(By.xpath(".//*[@id='pvfolio_staytot']")).getText();
			logger.info("Stay Total: "+CRStaTot);
			
		//Extra Resort Charges
		String CRResCharges = driver.findElement(By.xpath(".//*[@id='pvfolio_excharge']")).getText();
			logger.info("Extra Resort Charges: "+CRResCharges);	
				
				//Extra Resort Charges Flag
				driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[8]/td[2]/div/span[2]/img")).click(); //Click on Flag icon	
					logger.info("Extra Resort Charges Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='exchdiv']/div/table/tbody/tr/td[1]"))); // Wait till the Close Button is present 
							String CRExtra = driver.findElement(By.xpath(".//*[@id='EXTRACHARGESINFO']/tbody/tr[1]/td")).getText();
								Assert.assertEquals(CRExtra, CRExtrResChar);
									logger.info("Extra Resort Charges Flag Header Displayed and Passed Assertion: "+CRExtra);
										driver.findElement(By.xpath(".//*[@id='exchdiv']/div/table/tbody/tr/td[1]")).click(); //Click on Close button
		//Credit Line:
		String CRCrdLine = driver.findElement(By.xpath(".//*[@id='pvfolio_creditline']")).getText();
			logger.info("Credit Line: "+CRCrdLine);
				
		//Reservation Advance Deposit
		String CPResAdDep = driver.findElement(By.xpath(".//*[@id='advDepSpan']")).getText();
			logger.info("Reservation Advance Deposit: "+CPResAdDep);
	
				//Reservation Advance Deposit Flag
				driver.findElement(By.xpath(".//*[@id='folioImg']")).click();
					logger.info("Clicked on Reservation Advance Deposit Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='BackBtnAdvDep']"))); // Wait till the Close Button is present 
							String CRAdvanceDep = driver.findElement(By.xpath(".//*[@id='ADVDEPOSITSINFO1']/tbody/tr[1]/td")).getText();
								Assert.assertEquals(CRAdvanceDep, CRAdvDeposits);
									logger.info("Advance Deposits Flag Header Displayed and Passed Assertion: "+CRAdvanceDep);
										driver.findElement(By.xpath(".//*[@id='BackBtnAdvDep']")).click(); //Click on Close button	
															
		//Reservation And Misc Fees
		String CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText();
			logger.info("Reservation And Misc Fees: "+CPResAdMsc);
			
			String CPResAdMsc1 = CPResAdMsc.replace("$", ""); //Take of $dollar Sign
			String CPResAdMsc2 = CPResAdMsc1.trim();// Trim the string to compress blank space 
			
			//Reservation And Misc Fees Flag
			logger.info("---------------------------------------------Reservation And Misc Fees Flag------------------------------------------------------------------------------------");
			driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
				logger.info("Clicked on Reservation And Misc Fees Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
					
					//Header Comparision
					String CRResMisFees = driver.findElement(By.xpath(".//*[@id='RFIFTitle']")).getText();
					Assert.assertEquals(CRResMisFees, CRReserMiscFees);
						logger.info("Reservation And Misc Fees Flag Header Displayed and Passed Assertion: "+CRAdvanceDep);
						
					//Loyalty Accomodation Upgrade Fee Compare
					if(RoomupgAvailabilityComp.equals(RoomupgAvailability))
					{
						logger.info("-------------------------------Loyalty Accomodation Upgrade Fee------------------------------------------------------------");
						String AccUpgradeFee = driver.findElement(By.xpath(".//*[@id='FRRTRN2AMOUNT']")).getText();
						String AccountUpgradeFee = AccUpgradeFee.substring(0,2);
							logger.info("Loyality Accomodation Upgrade Fee: "+AccountUpgradeFee);
								Assert.assertEquals(AccountUpgradeFee, RoomUpgradeCost);
									logger.info("Loyalty Accomodation Upgrade Fee Matched with RoomUpgrade Cost: ASSERTION PASSED");
					}
					else
					{
						try
						{
							String RoomUpgNotAvail = "Room Upgrade is not available, so loyalty accomodation upgrade fee is not available: PASSED";
							logger.info(RoomUpgNotAvail);
						
						Properties properties = new Properties();
				 			properties.setProperty("RoomUpgNotAvail", RoomUpgNotAvail);
				 			File file = new File("Reservation&MiscFees.Properties");
	 						FileOutputStream fileOut = new FileOutputStream(file);
	 						properties.store(fileOut, "Discounted Points Amount");
	 						fileOut.close();
	 					} catch (FileNotFoundException e) {
	 						e.printStackTrace();
	 					} catch (IOException e) {
	 						e.printStackTrace();
	 			      	}
						
					}
						
					//Club Prepayment Fee
					if(ReservationPossibleComp.equals(ReservationPossible))
					{
							try (FileReader reader = new FileReader("Reservation&MiscFees.Properties")) {
					      		Properties properties = new Properties();
						      		properties.load(reader);
						      		RmUpgNotAvail  = properties.getProperty("RoomUpgNotAvail");
						   
						      	} catch (IOException e) {
						      		e.printStackTrace();
						      	}
							
							if(RoomUpgNotAvailComp.equals(RmUpgNotAvail))
							{
								logger.info("---------------------------------------Club Prepayment Fee------------------------------------------------------------");
								String PrepaymentFee = driver.findElement(By.xpath(".//*[@id='FRRTRN2AMOUNT']")).getText();
									logger.info("Club Prepayment Fee: "+PrepaymentFee);
										Assert.assertEquals(PrepaymentFee, PrepaymentFee);
											logger.info("Club Prepayment Fee Matched with Cost of Points Borrowed from next year: ASSERTION PASSED");	
							}
							else
							{
								logger.info("---------------------------------------Club Prepayment Fee------------------------------------------------------------");
								String PrepaymentFee = driver.findElement(By.xpath(".//*[@id='FRRTRN3AMOUNT']")).getText();
									logger.info("Club Prepayment Fee: "+PrepaymentFee);
										Assert.assertEquals(PrepaymentFee, PrepaymentFee);
											logger.info("Club Prepayment Fee Matched with Cost of Points Borrowed from next year: ASSERTION PASSED");	
							}
						
					}
					else
					{
						logger.info("Points are available to book the reservation, so club prepayment fee is not shown: PASSED");
					}
								
								
						//Total Amount Due
						String FRRTotal = driver.findElement(By.xpath(".//*[@id='FRR_TOTAL_AMOUNT']")).getText();
							logger.info("Total Amount due is: "+FRRTotal);
								Assert.assertEquals(FRRTotal, CPResAdMsc2);
									logger.info("Total Amount due matched the Reservation and Misc Fees");
									
						//Add Credit Card
						driver.findElement(By.xpath(".//*[@id='pvpaycc_input']")).click();
							Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='pvpaycc_select']"))); 
								Payment.selectByValue("0");// Select Booking Type
								
								//Credit Card Maintenance Header and Section
								String CCMain = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[1]/tbody/tr/td/b")).getText();
									logger.info("Header is: "+CCMain);
										Assert.assertEquals(CCMain, CreditCardMain);
											logger.info("Credit Card Maintenance Header Matched");
											
										//Leadid
										String Leadid = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[2]/tbody/tr/td[4]/span/font")).getText();
											logger.info("Leadid in Credit Card Maintenance :"+Leadid);
												Assert.assertEquals(Leadid, LEADID);
													logger.info("Leadid from Credit Card Maintenance section matched");
														driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
													
										//-------Add Credit Card---------//
													
										//Credit Card Number
										driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
											driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
												driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											
										/*//Credit Card Type
										String CCType1 = driver.findElement(By.xpath(".//*[@id='pvCCType_input']")).getAttribute("value");//Get Credit Card Type Value 
											logger.info(""+CCType1);
											*/
										//Card Holder Name
											driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
										driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
										
										//Exp mnth & Year
										driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
											Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
												ExpMth.selectByValue(ExpMonth);// Exp Mnth
										
										driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
											Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
												ExpYear.selectByValue(ExpYr);// Exp Year
											
										//Active
										driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
											Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
												Active.selectByValue("Y");// Active
													driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
										
										//DRI Credit Card Y/N
										driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
											Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
												Dricc.selectByValue("N");// DRI Credit Card	
										
										//Click on Save Button
										driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
											logger.info("Clicked on Save Button");
											
											wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok Button is present 
											
											String Success = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
												logger.info(""+Success); //Success MEssage
												
											driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Click on OK
							
											driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
									
				//Click on SAVE Button
					driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
						logger.info("Clicked on SAVE BUTTON");
				
				if(CPResAdMsc2.equals(Zero))
				{
					logger.info("Continue to Next Step");
				}
				else
				{
					//Payment confirmation
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Close Button appears
						String PayConfirm = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
							logger.info(""+PayConfirm);
				}
						
	}
	
	
}
