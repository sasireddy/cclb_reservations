package claritydb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
/*import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;*/

public class Final_Confirmation extends Common {
	 
	 Logger logger = Logger.getLogger("Final_Confirmation.class");
	 
	 Reservation res = new Reservation();
	 
	public String CRRes2015points;
	public String CRRes2016points;
	public int CRRes2016pointsINT;
	
	public int RCEndingBalEstimation;
	public String RC16EndingBal;
	public int RC16EndingBalINT;
	
	public String RAPoints;
	
	public String PointsBorrowed;
	public int PointsBorrowedINT;
	
	public String PointsBorrFrmNxtYr;
	public String PaymentMessage;

	public String PointsBorrFrmNxtYrComp = "The Points Are Not Available To Book The Reservation";
	
	public String Discount;
	public String DiscountMessage = "Selected Yes For Discount";
	public String EstimatedDiscountAmount;
	
	 public void ResProperties()
	 {
		 try (FileReader reader = new FileReader("Reservation.Properties")) {
      		Properties properties = new Properties();
	      		properties.load(reader);
	      		 CRRes2015points = properties.getProperty("CRRes2015pnts");
	      		 	CRRes2016points = properties.getProperty("CRRes2016pnts");
	      		 		logger.info(CRRes2015points);
	      		 		logger.info(CRRes2016points);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		 try (FileReader reader = new FileReader("ResortAvailability.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		RAPoints = properties.getProperty("RAPnts");
	      		logger.info(RAPoints);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		 try (FileReader reader = new FileReader("Prepayment.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		PointsBorrowed = properties.getProperty("PointsBorrowed");
	      		PointsBorrFrmNxtYr = properties.getProperty("ReservationPossible");
	      		logger.info(PointsBorrowed);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		 try (FileReader reader = new FileReader("Discount.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		Discount = properties.getProperty("DiscountYes");
	      		EstimatedDiscountAmount = properties.getProperty("EstimatedDiscountAmount");
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
	 }
	
	 public void Reservation_Confirmation()  {
		 
		 ResProperties(); //Call the method of properties
				
		 PropertyConfigurator.configure("Log4j.properties");
		
		
		 //If Points are borrowed from next year if loops executes
		if(PointsBorrFrmNxtYrComp.equals(PointsBorrFrmNxtYr))	
		{
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); //Wait till the element is present
			 PaymentMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
			 	logger.info(""+PaymentMessage);
			 		driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Clicked on closebutton  
			 		
			 		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
					//Reservation Confimation Header
					String RCHeader = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[1]/td")).getText();
						Assert.assertEquals(RCHeader, RConfirm);
							logger.info("Reservation Confirmation Header Displayed and Passed Assertion: "+RCHeader);
					
					//----------------------------------------------------------------------Points Summary------------------------------------------------------------------------------------//
					logger.info("---------------------------------------------------------Points Summary------------------------------------------------------------");
					
					//2016 Ending Balance
					CRRes2016pointsINT = Integer.parseInt(CRRes2016points); // Convert 2016 points to integer
					PointsBorrowedINT = Integer.parseInt(PointsBorrowed); //Convert Borrowed points to integer
					
					
					RCEndingBalEstimation = CRRes2016pointsINT-PointsBorrowedINT; //Do the difference 
					logger.info("Ending Point of 2016 should be: "+RCEndingBalEstimation);
					
					RC16EndingBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal2']")).getText();
						logger.info("Ending Balance of 2016 is: "+RC16EndingBal);
							RC16EndingBalINT = Integer.parseInt(RC16EndingBal);
								Assert.assertEquals(RC16EndingBalINT, RCEndingBalEstimation);
									logger.info("2016 Ending Balance Points Matched with the Estimated Ending Point Balance: ASSERTIN PASSED");
					
					//Points Summary Header
					String RCPointsSum = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[3]/th")).getText();
						Assert.assertEquals(RCPnSum, RCPointsSum);
							logger.info("Points Summary Header Displayed and Passed Assertion: "+RCHeader); 	
					
					//Beginning Points Balance
					String RCBegiPntsBal2015 = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //2015 points
						Assert.assertEquals(RCBegiPntsBal2015, RAPoints);
							logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Club Reservation Points For Reservations 2015: "+RCBegiPntsBal2015);
								logger.info("RES CONFIR " +RCBegiPntsBal2015);
									logger.info("Total Reservation Points "+RAPoints);
									
					//Reservation Cost
					String RCResCost2015 = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost2015 = RCResCost2015.length(); // Length of RCResCost2015
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost2015 == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						String RCResCt2015 = RCResCost2015.replace(",","");
						Assert.assertEquals(RCResCt2015, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt2015);
						
						//Discount Points
							if(DiscountMessage.equals(Discount))
							{
								String RCDiscnt = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
									String RCDiscount = RCDiscnt.replace(",","");
										Assert.assertEquals(RCDiscount, EstimatedDiscountAmount); //Comparing 
											logger.info("Discount Points in Reservation Confirmation Matched with Discount points from Method of Payments: "+EstimatedDiscountAmount);
							}
							
							else
							{
								logger.info("Haven't Used Discount");
							}
									
						//Borrowed Points
							String RCBorrowedPoints = driver.findElement(By.xpath(".//*[@id='BorrowPoints']")).getText();
								String RCBorrowedPnts = RCBorrowedPoints.replace(",","");
									Assert.assertEquals(RCBorrowedPnts, PointsBorrowed); //Comparing 
										logger.info("Total Points Borrowed Matched with Borrowed points from Method of Payments: "+RCBorrowedPnts);
								
						//Total Points Used for Reservation
						String RcTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							String RCTotalPnts = RcTotPnts.replace(",","");
								Assert.assertEquals(RCTotalPnts, RAPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RcTotPnts);
										
						//Estimating Ending Points
							//int AvailPoints2015 = Integer.parseInt(CRRes2015points);//Convert Int to String
								//int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = 0;
										logger.info("Estimation of Ending Point Balance for 2015 should be: "+EstEndPnts);
										
							//Ending Points Balance	
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							
					}
					
					else		
					{
						Assert.assertEquals(RCResCost2015, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost2015);
							logger.info("Reservation Confirmation Section - Reservation Cost : "+RCResCost2015);
								logger.info("From Resort Availability- Reservation Cost: "+RAPoints);
					
								
							//Discount Points
							if(DiscountMessage.equals(Discount))
							{
								String RCDiscnt = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
										Assert.assertEquals(RCDiscnt, EstimatedDiscountAmount); //Comparing 
											logger.info("Discount Points in Reservation Confirmation Matched with Discount points from Method of Payments: "+EstimatedDiscountAmount);
							}
							
							else
							{
								logger.info("Haven't Used Discount");
							}
							
							//Borrowed Points
							String RCBorrowedPoints = driver.findElement(By.xpath(".//*[@id='BorrowPoints']")).getText();
									Assert.assertEquals(RCBorrowedPoints, PointsBorrowed); //Comparing 
										logger.info("Reservation Confirmation Section - Borrowed : "+RCResCost2015);
											logger.info("From Method of Payments - Borrowed Points: "+PointsBorrowed);
												logger.info("Total Points Borrowed Matched with Borrowed points from Method of Payments: "+RCBorrowedPoints);
										
							//Total Points Used for Reservation
							String RcTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
								Assert.assertEquals(RcTotPnts, RAPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RcTotPnts);
										//logger.info("RCCCC: "+RcTotPnts);
											//logger.info("From Resort AVAIl: "+RAPoints);
											
							//Estimating Ending Points
								//int AvailPoints2015 = Integer.parseInt(CRRes2015points);//Convert Int to String
									//int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
										int EstEndPnts = 0;
											logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
											
								//Ending Points Balance	
								String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
									int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
										logger.info("Ending Points Balance: "+RCEndPointsbal);
										
									Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");			
								
							}
					
		}
		
		else
		{
		
				//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
					//Reservation Confimation Header
					String RCHeader = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[1]/td")).getText();
						Assert.assertEquals(RCHeader, RConfirm);
							logger.info("Reservation Confirmation Header Displayed and Passed Assertion: "+RCHeader);
					
					//----------------------------------------------------------------------Points Summary------------------------------------------------------------------------------------//
					logger.info("---------------------------------------------------------Points Summary------------------------------------------------------------");
				
					//Points Summary Header
					String RCPointsSum = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[3]/th")).getText();
						Assert.assertEquals(RCPnSum, RCPointsSum);
							logger.info("Points Summary Header Displayed and Passed Assertion: "+RCHeader); 	
					
					//Beginning Points Balance
					String RCBegiPntsBal2015 = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //2015 points
						Assert.assertEquals(CRRes2015points, RCBegiPntsBal2015);
							logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Club Res Available Points For Reservations 2015: "+RCBegiPntsBal2015);
								logger.info("RES CONFIR " +RCBegiPntsBal2015);
									logger.info("Club Res "+CRRes2015points);
									
					//Reservation Cost
					String RCResCost2015 = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost2015 = RCResCost2015.length(); // Length of RCResCost2015
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost2015 == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						String RCResCt2015 = RCResCost2015.replace(",","");
						Assert.assertEquals(RCResCt2015, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt2015);
								//logger.info("RCCCC: "+RCResCt2015);
									//logger.info("From Resort Availability: "+RAPoints);
									
						//Total Points Used for Reservation
						String RcTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							String RCTotalPnts = RcTotPnts.replace(",","");
								Assert.assertEquals(RCTotalPnts, RAPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RcTotPnts);
										//logger.info("RCCCC: "+RcTotPnts);
										//logger.info("From Resort AVAIl: "+RAPoints);
						
						//Discount Points
						if(DiscountMessage.equals(Discount))
						{
							String RCDiscnt = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
									Assert.assertEquals(RCDiscnt, EstimatedDiscountAmount); //Comparing 
										logger.info("Discount Points in Reservation Confirmation Matched with Discount points from Method of Payments: "+EstimatedDiscountAmount);
						}
						
						else
						{
							logger.info("Haven't Used Discount");
						}
									
						//Estimating Ending Points
							int AvailPoints2015 = Integer.parseInt(CRRes2015points);//Convert Int to String
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = AvailPoints2015 - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							
					}
					
					else		
					{
						Assert.assertEquals(RCResCost2015, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost2015);
							logger.info("RCCCC: "+RCResCost2015);
								logger.info("From Resort AVAIl: "+RAPoints);
								
						//Total Points Used for Reservation
						String RcTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							Assert.assertEquals(RcTotPnts, RAPoints); //Comparing 
								logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RcTotPnts);
									//logger.info("RCCCC: "+RcTotPnts);
										//logger.info("From Resort AVAIl: "+RAPoints);
								
						//Discount Points
						if(DiscountMessage.equals(Discount))
						{
							String RCDiscnt = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
									Assert.assertEquals(RCDiscnt, EstimatedDiscountAmount); //Comparing 
										logger.info("Discount Points in Reservation Confirmation Matched with Discount points from Method of Payments: "+EstimatedDiscountAmount);
						}
						
						else
						{
							logger.info("Haven't Used Discount");
						}
										
						//Estimating Ending Points
							int AvailPoints2015 = Integer.parseInt(CRRes2015points);//Convert Int to String
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = AvailPoints2015 - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");			
							
					}
					
					
		}	
				
				//Confirmation Message
				String ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
					logger.info(""+ConfimationMes);
					logger.info("lenght of the string is:" +ConfimationMes.length());
					
					//Save to Confirmation.Properties File
					try{
					String ResConNum = ConfimationMes.replaceAll("[^0-9]","");
					logger.info("Reservation Confirmation number is: "+ResConNum);
					
					Properties properties = new Properties();
			 		properties.setProperty("ConfirmationNo", ResConNum);
			 		
			 		File file = new File("Confirmation.Properties");
					FileOutputStream fileOut = new FileOutputStream(file);
					properties.store(fileOut, "Reservation Confirmation Number");
					fileOut.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					
					
					e.printStackTrace();
				}
					
				/*	//Close
					driver.findElement(By.xpath(".//*[@id='ClbResvConf_close']")).click();
						logger.info("Clicked on Close button");	
					
					//To Close All broswers	
					Robot robot =new Robot();
					robot.keyPress(KeyEvent.VK_ALT);
					robot.keyPress(KeyEvent.VK_SPACE);
					robot.keyPress(KeyEvent.VK_C);*/
						
			 }				
	}