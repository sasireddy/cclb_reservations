package claritydb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.testng.Assert;


public class RoomUpgrade extends Common{
	
	Logger logger = Logger.getLogger("RoomUpgrade.class");
	
	String RoomUpgCost;
	String RoomupgAvailability;

	
	//---------------------------------------------------------------------Room Upgrade------------------------------------------------------------//	
	
	//Room Upgrade = NO
	public void Room_Upgrade_No() {
		
		PropertyConfigurator.configure("Log4j.properties");
		logger.info("-------------------------------------------------------------------Room Upgrade------------------------------------------------------------");
		
		if(driver.findElements(By.xpath(".//*[@id='upgrdRoomNo']")).size()!=0)
		{
		//Room Upgrade Header
		String RoomUpgrade = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[1]/tbody/tr[1]/td")).getText();
			Assert.assertEquals(RoomUpgrade, RoomUpgrade1);
				logger.info("RoomUpgrade Header displayed and Passed Assertion: "+RoomUpgrade);
		
		driver.findElement(By.xpath(".//*[@id='upgrdRoomNo']")).click();
			logger.info("Clicked NO for Room Upgrade");	
		}
		
		else
		{
			logger.info("Room Upgrade Option is not available");
		}
		
	}
	
	//Room Upgrade = YES
		public void Room_Upgrade_Yes() {
			
			logger.info("-------------------------------------------------------------------Room Upgrade------------------------------------------------------------");
				
			try {
					
				if(driver.findElements(By.xpath(".//*[@id='upgrdRoomNo']")).size()!=0)
				{
				
					//Room Upgrade Header
					String RoomUpgrade = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[1]/tbody/tr[1]/td")).getText();
						Assert.assertEquals(RoomUpgrade, RoomUpgrade1);
							logger.info("RoomUpgrade Header displayed and Passed Assertion: "+RoomUpgrade);
							
					//Click on RoomType for Upgrade
					driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[2]/tbody/tr[1]/td[1]/input")).click();	
						String RURoomType = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[2]/tbody/tr[1]/td[2]")).getText();
						String RURoomType1 = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[2]/tbody/tr[1]/td[3]")).getText();	
							logger.info("Room Type: "+RURoomType+ " "+RURoomType1);
						
						logger.info("Clicked on Room Upgrade");
							
						RoomUpgCost = driver.findElement(By.xpath(".//*[@id='cst_upg']")).getText();
								logger.info("Room Upgrade Cost: "+RoomUpgCost);
								
								RoomupgAvailability =  "Room Upgrade Option is Available";
									logger.info("Room Upgrade Cost: "+RoomupgAvailability);
								
								Properties properties = new Properties();
										properties.setProperty("RoomUpgCost", RoomUpgCost);
											properties.setProperty("RoomupgAvailability",RoomupgAvailability);
												properties.setProperty("RuRoomType", RURoomType);
										
										File file = new File("RoomUpgrade.Properties");
								 			FileOutputStream fileOut = new FileOutputStream(file);
								 				properties.store(fileOut, "RoomUpgrade Cost");
								 					fileOut.close();
				
					driver.findElement(By.xpath(".//*[@id='upgrdRoomYes']")).click();
						logger.info("Clicked Yes for Room Upgrade");	
				}
					
				else
				{
					RoomupgAvailability =  "Room Upgrade Option is not Available";
						logger.info(RoomupgAvailability);
							Properties properties = new Properties();
								properties.setProperty("RoomupgAvailability",RoomupgAvailability);
								
								File file = new File("RoomUpgrade.Properties");
					 				FileOutputStream fileOut = new FileOutputStream(file);
					 					properties.store(fileOut, "RoomUpgrade Cost");
					 						fileOut.close();
				}
		 
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
				
			}
}
