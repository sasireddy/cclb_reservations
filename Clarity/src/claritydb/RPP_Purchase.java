package claritydb;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class RPP_Purchase extends Common{
 
	Logger logger = Logger.getLogger("RPP_Purchase.class");
	
	//RPP Decline
	public void DeclineRPP()
	{
		PropertyConfigurator.configure("Log4j.properties");
		logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
		
		//Travel Policy
		String CRTraPolicy = driver.findElement(By.xpath(".//*[@id='travelProtection']/tbody/tr[1]/td")).getText();
			Assert.assertEquals(CRTraPolicy, CRTravelProtection);
				logger.info("Travel Policy Header displayed and Passed Assertion: "+CRTraPolicy);
				
				//Travel Policy Flag	
				driver.findElement(By.xpath(".//*[@id='trvlprotImage']")).click();
					logger.info("Clicked on Travel Policy Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='trvlproinfCancelButton']"))); // Wait till the Close Button is present 
							String CRTravProInfoFlag = driver.findElement(By.xpath(".//*[@id='travelProtectionInf_header']/tbody/tr/td")).getText();
								Assert.assertEquals(CRTravProInfoFlag, CRTraProtecFlag);
									logger.info("Travel Protection Information Flag Header Displayed and Passed Assertion: "+CRTravProInfoFlag);
										driver.findElement(By.xpath(".//*[@id='trvlproinfCancelButton']")).click();	
			
		
					driver.findElement(By.xpath(".//*[@id='declineRPP']")).click();
						logger.info("Declined RPP");
			}
	
	
	//Purchase RPP
	public void PurchaseRPP()
		{
			logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
			
			//Travel Policy
			String CRTraPolicy = driver.findElement(By.xpath(".//*[@id='travelProtection']/tbody/tr[1]/td")).getText();
				Assert.assertEquals(CRTravelProtection, CRTraPolicy);
					logger.info("Travel Policy Header displayed and Passed Assertion: "+CRTraPolicy);
					
					//Travel Policy Flag	
					driver.findElement(By.xpath(".//*[@id='trvlprotImage']")).click();
						logger.info("Clicked on Travel Policy Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='trvlproinfCancelButton']"))); // Wait till the Close Button is present 
								String CRTravProInfoFlag = driver.findElement(By.xpath(".//*[@id='travelProtectionInf_header']/tbody/tr/td")).getText();
									Assert.assertEquals(CRTravProInfoFlag, CRTraProtecFlag);
										logger.info("Travel Protection Information Flag Header Displayed and Passed Assertion: "+CRTravProInfoFlag);
											driver.findElement(By.xpath(".//*[@id='trvlproinfCancelButton']")).click();	
				
				
					//Purchase RPP
					driver.findElement(By.xpath(".//*[@id='purchaseRPP']")).click();
						logger.info("Purchase RPP");
					
					
		}
}
