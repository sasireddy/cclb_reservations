package clarity_EU_Reservation;

import org.testng.annotations.Test;


public class Z_EU_Execution {

	B_EU_Login EU_Login = new B_EU_Login();
	C_EU_Customer360_LeadInfo EU_Leadinfo = new C_EU_Customer360_LeadInfo();
	D_EU_Reservation EU_Reservation = new D_EU_Reservation();
	
	E_EU_ClubRes EU_ClubRes = new E_EU_ClubRes();
	F_EU_BookReservation EU_BookRes = new F_EU_BookReservation();
	G_EU_MOPPayments EU_MOP = new G_EU_MOPPayments();
	H_EU_ClubReservations EU_Reserve = new H_EU_ClubReservations();
	I_EU_FinalConfirmation EU_ResConfirm = new I_EU_FinalConfirmation();
	
	
	@Test
	public void EU_Reservation()
	{
		EU_Login.openBrowser();
		EU_Leadinfo.Customer360();
		EU_Leadinfo.Lead_Information();
		EU_Reservation.Reservations();
		EU_ClubRes.ClubRes();
		EU_BookRes.BookReservation();
		EU_BookRes.Resort_Availability();
		EU_BookRes.RoomUpgrade_No();
		EU_BookRes.Errata();
		EU_BookRes.Discount_No();
		EU_MOP.MethodofPaymentOptions();
		EU_Reserve.Reservation();
		EU_ResConfirm.Reservation_Confirmation();
	}
			
}
