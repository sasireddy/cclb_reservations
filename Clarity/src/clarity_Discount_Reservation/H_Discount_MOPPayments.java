package clarity_Discount_Reservation;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class H_Discount_MOPPayments extends A_Discount_Common {
	
	Logger logger = Logger.getLogger(H_Discount_MOPPayments.class);
	
	//MOP Options
	public String MOPTotResPointCost; // Total Reservation Point Cost
	public String MOPointsAvail; //Total Member Points Available
	public String MOPBalPointsNeeded; //Balance of Points Needed
	public String MOPTotalPointCost;// Total Points To Apply / Total Cost
	
	//Retrieve from Discount_Reservation.Properties
	public String CRRespoints;
	
	//Retrieve from Discount.Properties
	public String FinalResCost;
	public String FinalReservationCost;
	public String Message = "Value has a comma";
	public String DiscountMessage;
	
	//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
	
		public void MethodofPaymentOptions() {
		
			PropertyConfigurator.configure("Log4j.properties");	
			 
			 //Retrieve from Discount.Properties
			 try (FileReader reader = new FileReader("Discount.Properties")) {
		      		Properties properties = new Properties();
			      		properties.load(reader);
			      		FinalResCost = properties.getProperty("FinalResCost");
			      		FinalReservationCost = properties.getProperty("FinalReservationCost");
			      		DiscountMessage = properties.getProperty("DiscountMessage");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			 
			 //Retrieve from  Discount_Reservation.Properties
			 try (FileReader reader = new FileReader("Discount_Reservation.Properties")) {
		      		Properties properties = new Properties();
			      		properties.load(reader);
			      		 CRRespoints = properties.getProperty("ClubRespoints");
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
			 
			logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
					
					if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
					{ 
						
						if(Message.equals(DiscountMessage))
						{
							//Total Reservation Point Cost / Compared
				 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
						 		Assert.assertEquals(MOPTotResPointCost, FinalReservationCost);
						 			logger.info("Total Reservation Point Cost Matched with Discount(Final Reservation Cost): "+MOPTotResPointCost);
						 				logger.info("Final Reservation Cost From Discount Page: "+FinalResCost);
						 					logger.info("Final Reservation Cost From Discount Page: "+FinalReservationCost);
						 				
			 				//Total Member Points Available / Compared
						 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
						 		Assert.assertEquals(CRRespoints, MOPointsAvail);
						 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
						 			
				 			//Balance of Points Needed / Compared
				 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
						 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
						 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
					
							//Total Points To Apply / Total Cost / Compared
				 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
						 		Assert.assertEquals(MOPTotalPointCost, FinalReservationCost);
						 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
						 			
						 	//Process		
							driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
								logger.info("Clicked on Process button");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						}
						
						else
						{
				 			//Total Reservation Point Cost / Compared
				 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
						 		Assert.assertEquals(MOPTotResPointCost, FinalResCost);
						 			logger.info("Total Reservation Point Cost Matched with Discount(Final Reservation Cost): "+MOPTotResPointCost);
						 				logger.info("Final Reservation Cost From Discount Page: "+FinalResCost);
						 				
				 			//Total Member Points Available / Compared
						 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
						 		Assert.assertEquals(CRRespoints, MOPointsAvail);
						 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
						 			
				 			//Balance of Points Needed / Compared
				 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
						 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
						 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
					
							//Total Points To Apply / Total Cost / Compared
				 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
						 		Assert.assertEquals(MOPTotalPointCost, FinalResCost);
						 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
						 			
						 	//Process		
							driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
								logger.info("Clicked on Process button");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
											
							}
		
					}
					
					else
					{
						logger.info("Methods of Payemnt Options Window not found");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
					}
					
					
				}


}
