package clarity_Discount_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class J_Discount_FinalConfirmation extends A_Discount_Common {

Logger logger = Logger.getLogger("J_Discount_FinalConfirmation.class");
	
	public String RCBegiPntsBal; //Beginning Point Balance
	public String RCResCost; //Reservation Cost
	public String RCResCt; //Replace "RCResCost" without ","
	public String RCDiscountedPoints; //Discounted Points
	public String RCDisPoints; //Replace "RCDiscountedPoints" without ","
	public String RCTotPnts; //Total Points Used for Reservation
	public String RCTotalPoints; //Replace "RCTotPnts" without ","
	public String RCEndPntBal; //Ending points balance
	public String ConfimationMes; //Confirmation Message with Confirmation Number
	
	
	//Retrieved from Discount_Reservation.Properties
	public String ClubRespoints;
	
	
	//Retrieved from Discount_ResortAvailability.Properties
	public String RAPoints;
	
	//Retrieved from Discount.Properties
	public String EstimatedDiscountAmount;
	public String FinalResCost;
	public String FinalReservationCost;
	public String DiscountMessage;
	public String Message = "Value has a comma";
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	public void ResProperties()
	 {
		 try (FileReader reader = new FileReader("Discount_Reservation.Properties")) {
     		Properties properties = new Properties();
	      		properties.load(reader);
	      		ClubRespoints = properties.getProperty("ClubRespoints");
	      		 		logger.info(ClubRespoints);
	      		 	
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		  try (FileReader reader = new FileReader("Discount_ResortAvailability.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		RAPoints = properties.getProperty("RAPnts");
	      		logger.info("Points from Resort Availability: " +RAPoints);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		  
		  try (FileReader reader = new FileReader("Discount.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		EstimatedDiscountAmount = properties.getProperty("EstimatedDiscountAmount");
		      		FinalResCost = properties.getProperty("FinalResCost");
		      		FinalReservationCost = properties.getProperty("FinalReservationCost");
		      		DiscountMessage = properties.getProperty("DiscountMessage");
		      		logger.info("Estimated Discounted Points from Discount Screen: " +EstimatedDiscountAmount);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
	 }
	
	public void Reservation_Confirmation()
	{
		PropertyConfigurator.configure("Log4j.properties");	
		
		ResProperties(); //Call the Method
					
		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					
		logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
								
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
			
			//Beginning Points Balance / Compared
			RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //Present Year points
				Assert.assertEquals(RCBegiPntsBal, ClubRespoints);
					logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Club Res Available Points For Reservations Present Year: "+RCBegiPntsBal);
					
					//Reservation Cost
					RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost = RCResCost.length(); // Length
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCResCt = RCResCost.replace(",","");
						Assert.assertEquals(RCResCt, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt);
									
							//Total Points Used for Reservation / Compared
								if(Message.equals(DiscountMessage))
								{
									RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
									RCTotalPoints = RCTotPnts.replace(",","");
										Assert.assertEquals(RCTotalPoints, FinalReservationCost); //Comparing 
											logger.info("Total Points Used for Reservation Displayed and Matched with Final Reservation Cost From Discount Page: "+RCTotalPoints);	
								}
								else
								{
									RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
									RCTotalPoints = RCTotPnts.replace(",","");
										Assert.assertEquals(RCTotalPoints, FinalResCost); //Comparing 
											logger.info("Total Points Used for Reservation Displayed and Matched with with Final Reservation Cost From Discount Page: "+RCTotalPoints);	
								}
							

				
						//Discounted Points / Compared
						RCDiscountedPoints = driver.findElement(By.xpath(".//*[@id='DiscPoints']")).getText();
							RCDisPoints = RCDiscountedPoints.replace(",","");
							Assert.assertEquals(RCDisPoints, EstimatedDiscountAmount); //Comparing 
							logger.info("Discounted Points in Reservation Confirmation Matched with Estimated Discounted Points: Assertion Passed: "+RCDiscountedPoints);
									
						//Estimating Ending Points
							
							if(Message.equals(DiscountMessage))
							{
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(FinalReservationCost); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	/ Compared
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							}
							else
							{
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(FinalResCost); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	/ Compared
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							}
								
							
					}
					
					else		
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost);
							logger.info("Reservation Confirmation: "+RCResCost);
								logger.info("Club Reservations: "+RAPoints);
								
						//Total Points Used for Reservation / Compared 
								if(Message.equals(DiscountMessage))
								{
									RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
									RCTotalPoints = RCTotPnts.replace(",","");
										Assert.assertEquals(RCTotalPoints, FinalReservationCost); //Comparing 
											logger.info("Total Points Used for Reservation Displayed and Matched with Final Reservation Cost From Discount Page: "+RCTotalPoints);	
								}
								else
								{
									RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
									RCTotalPoints = RCTotPnts.replace(",","");
										Assert.assertEquals(RCTotalPoints, FinalResCost); //Comparing 
											logger.info("Total Points Used for Reservation Displayed and Matched with with Final Reservation Cost From Discount Page: "+RCTotalPoints);	
								}
							
								
						//Discounted Points / Compared
						RCDiscountedPoints = driver.findElement(By.xpath(".//*[@id='DiscPoints']")).getText();
							Assert.assertEquals(RCDiscountedPoints, EstimatedDiscountAmount); //Comparing 
							logger.info("Discounted Points in Reservation Confirmation Matched with Estimated Discounted Points: Assertion Passed: "+RCDiscountedPoints);
										
						//Estimating Ending Points
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(FinalResCost); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
						//Ending Points Balance	
						RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
							int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
								logger.info("Ending Points Balance: "+RCEndPointsbal);
								
							Assert.assertEquals(RCEndPointsbal, EstEndPnts);
							logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");			
							
					}
					
					
				
				
					//Confirmation Message
					ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
						logger.info(""+ConfimationMes);
						
						//Save to Confirmation.Properties File
						try{
						String ResConNum = ConfimationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
						logger.info("Reservation Confirmation number is: "+ResConNum);
						
						Properties properties = new Properties();
				 		properties.setProperty("ConfirmationNo", ResConNum);
				 		
				 		File file = new File("Discount_Reservation_Confirmation.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Reservation Confirmation Number");
						fileOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {	
						e.printStackTrace();
					}
						
 			}				
		
	}
	


