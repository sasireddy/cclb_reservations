package clarity_Discount_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;


public class G_Discount_Discount extends A_Discount_Common {
	
	Logger logger = Logger.getLogger(G_Discount_Discount.class);
	
	//Retrieve from Discount_ResortAvialability.Properties
	public String RAPoints; //Cost of Reservation Points
	public int RAvailPoints; //Convert RAPoints to integer for comparision purpose
	
	
	public void Discount_Yes()  {
		    
		  PropertyConfigurator.configure("Log4j.properties");	
			
		  logger.info("-------------------------------------------------------------------Discount------------------------------------------------------------");
		  
		  //Retrieve from Discount_ResortAvailability.Properties
		  try (FileReader reader = new FileReader("Discount_ResortAvailability.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		 RAPoints = properties.getProperty("RAPnts");//Get the value
		      			
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}

				RAvailPoints = Integer.parseInt(RAPoints);//Convert to integer for comparision
											
				if(driver.findElements(By.xpath(".//*[@id='cmnConfirmCntDiv']")).size() !=0) 
				{
					driver.findElement(By.xpath(".//*[@id='confirmDscYesBtn']")).click();
						logger.info("Clicked on YES for discount");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
				 		//Discount Percentage(%)
				 			driver.findElement(By.xpath(".//*[@id='discPct']")).click(); //Click on Discount Percentage
				 				driver.findElement(By.xpath(".//*[@id='discPct']")).sendKeys(DiscountPercentage); //Enter the Percentage Amount
				 					driver.findElement(By.xpath(".//*[@id='ttlCost']")).click();
				 				
				 					double Percentage = 0.50; 	//Changing this value might cause error.  Please change the DiscountPercentage Value in Common when you change this value.				
				 						double EstimatedDiscntAmount = Math.round(RAvailPoints * Percentage); //Estimated Discount Amount
			 								logger.info("Discount Points Amount: "+EstimatedDiscntAmount);
				 								
	 								DecimalFormat decimal = new DecimalFormat("###.#"); //Decimal Format
				 								
									String EstimatedDiscountAmount = decimal.format(EstimatedDiscntAmount); // Convert Long to String
				 					
		 							String FinalResCost = driver.findElement(By.xpath(".//*[@id='ttlCost']")).getAttribute("value"); //Get the Final Reservation Cost 
		 								logger.info("Final Reservation Cost: "+FinalResCost);
		 									int LenofFinalResCost = FinalResCost.length(); // Length of FinalResCost
		 										String S = "1,000";
		 											int S1 = S.length(); // Length of S String
		 								
		 								if(LenofFinalResCost == S1) //If Size is the same then take out "," and follows the rest of the process
		 								{
		 									try{
		 											String DiscountMessage = "Value has a comma";
			 										String FinalReservationCost = FinalResCost.replace(",","");
			 											logger.info("Final Reservation Cost: "+FinalReservationCost);
			 										
		 											Properties properties = new Properties();
		 											
			 											properties.setProperty("EstimatedDiscountAmount", EstimatedDiscountAmount);
			 												properties.setProperty("FinalReservationCost", FinalReservationCost);	
			 													properties.setProperty("DiscountMessage", DiscountMessage);
			 												
			 										File file = new File("Discount.Properties");
							 						FileOutputStream fileOut = new FileOutputStream(file);
							 						properties.store(fileOut, "Discounted Points Amount");
							 						fileOut.close();
							 						
							 					} catch (FileNotFoundException e) {
							 						e.printStackTrace();
							 					} catch (IOException e) {
							 						e.printStackTrace();
							 			      	}
						 			
			 										
		 								}
		 								
		 								else
		 								{
		 									try{
						 					 		Properties properties = new Properties();
						 					 			properties.setProperty("EstimatedDiscountAmount", EstimatedDiscountAmount);
						 					 				properties.setProperty("FinalResCost", FinalResCost);
						 					 					
						 					 				
						 						File file = new File("Discount.Properties");
						 						FileOutputStream fileOut = new FileOutputStream(file);
						 						properties.store(fileOut, "Discounted Points Amount");
						 						fileOut.close();
						 					} catch (FileNotFoundException e) {
						 						e.printStackTrace();
						 					} catch (IOException e) {
						 						e.printStackTrace();
						 			      	}
		 								}
		 								
		 								driver.findElement(By.xpath(".//*[@id='confirmDscPrcYesBtn']")).click(); //Click on Process Button
		 									
				}						
				
				else
				{
					logger.info("Discount Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
	  	}
}
	


