package clarity_Discount_Reservation;

import org.testng.annotations.Test;



public class Z_Discount_Execution {
	
	B_Discount_Login Login = new B_Discount_Login();
	C_Discount_Customer360_LeadInfo Leadinfo = new C_Discount_Customer360_LeadInfo();
	D_Discount_Reservation Reservation = new D_Discount_Reservation();
	E_Discount_ClubRes ClubRes = new E_Discount_ClubRes();
	F_Discount_BookReservation BookRes = new F_Discount_BookReservation();
	G_Discount_Discount Discount = new G_Discount_Discount();
	H_Discount_MOPPayments MOP = new H_Discount_MOPPayments();
	I_Discount_ClubReservations Reserve = new I_Discount_ClubReservations();
	J_Discount_FinalConfirmation ResConfirm = new J_Discount_FinalConfirmation();
	
	@Test
	public void Discount_Reservation() 
	{
		Login.openBrowser();
		Leadinfo.Customer360();
		Leadinfo.Lead_Information();
		Reservation.Reservations();
		ClubRes.ClubRes();
		BookRes.BookReservation();
		BookRes.Resort_Availability();
		BookRes.RoomUpgrade_No();
		BookRes.Errata();
		Discount.Discount_Yes();
		MOP.MethodofPaymentOptions();
		Reserve.Reservation();
		ResConfirm.Reservation_Confirmation();
	
	}
	
	
			
}



