package clarity_Refund_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class H_Refund_ClubReservations extends A_Refund_Common {
	
Logger logger = Logger.getLogger("H_Refund_ClubReservations.class");
	
	//-------------------------------Reservation---------------------------//
		public String CRBookFunc; //Booking Function
		public String CRMarkeyCode; //Marketing Key Code
		public String CRMarKeyCodeFlagHeader; //Marketing Key Code Flag Header
		public String CResType; //Reservation Type
		public String CResStatus; //Reservation Status
		public String CRProperty; //Property
		public String CRPropFlagHeader; //Property Flag Header
		public String CRArrDate; //Arrival Date
		public String CRNights; //Nights
		public String CRNigFlagHeader; //Nights Flag Headr (Inventory)
		public String CRDepDate; //Departure Date
		public String CRGuaCode; //Guarantee Code
		public String CRUsagePeriod; //Usage Period
		public String CRMemshipNum; //Membership Number
		public String NoofGues; //Number of Guests
		public String CRRmType; //RoomType
		public String CRRoomType; //Substring for CRRmType
		public String CRRoomTypeFlagHeader; //RoomType Flag Header
				
		//-------------------------------Financial Summary--------------------------//
		public String CPResAdMsc; //Reservation and Misc Fees
		public String CPResAdMsc1; //Take off the $ or � sign of CPResAdMsc
		public String CPResAdMsc2; //Trim the String CPResAdMsc1
		public String AccUpgradeFee; //Loyalty Accommodation Upgrade Fee
		public String TotalAmountDue; //Total Amount due
		
		//Retrieve from Refund_ResortAvailability.Properties
		public String RAResortName; //ResortName
		public String RAArrDate; //Arrival Date
		public String RADepDate; //Departure Date
		public String RARoomType; //Room Type
		
		//Retrieve from Refund_Reservation.Properties
		public String MembershipNumber;
		
		//Retrieve from Refund_RoomUpgrade.Properties
		public String RoomUpgCost; //RoomUpgrade Cost
		public String RURoomType;// RoomType

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
		
		public void ResProperties() {
			 
			 PropertyConfigurator.configure("Log4j.properties");	
			
			 	//Refund_ResortAvailability.Properties
				  try (FileReader reader = new FileReader("Refund_ResortAvailability.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		RAResortName = properties.getProperty("RAResortName");
			      			RAArrDate = properties.getProperty("RAArrDate");
			      				RADepDate = properties.getProperty("RADepDate");
			      					RARoomType = properties.getProperty("RARoomType");
			      				
			      				
		      			logger.info("Resort Name from Resort Availability: " +RAResortName);
		      				logger.info("Arrival Date from Resort Availability: " +RAArrDate);
		      					logger.info("Departure Date from Resort Availability: " +RADepDate);
		      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				  
			  //Refund_Reservation.Properties
				  try (FileReader reader = new FileReader("Refund_Reservation.Properties")) {
						 Properties properties = new Properties();
				      		properties.load(reader);
				      		MembershipNumber = properties.getProperty("MembershipNumber");	
			      			logger.info("Membership Number: " +MembershipNumber);
			      			
				      	} catch (IOException e) {
				      		e.printStackTrace();
				      	}
				  
			  //Refund_RoomUpgrade.Properties
			  try (FileReader reader = new FileReader("Refund_RoomUpgrade.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		RoomUpgCost = properties.getProperty("RoomUpgCost");	
			      		RURoomType = properties.getProperty("RURoomType");			
		 	
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
		}	  
		
		public void Reservation() {
			
		ResProperties(); //Calling Method
			
		 PropertyConfigurator.configure("Log4j.properties");
				
		    logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
			
			//-----------------------------------------------------------------------Reservation:------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Reservation------------------------------------------------------------");
			
					
			//Booking Function / Compared
			CRBookFunc = driver.findElement(By.xpath(".//*[@id='ResSum_BookingPurpose']")).getText();
				Assert.assertEquals(CRBookFunc, BookTypeReserName);
					logger.info("Booking Fuction Displayed and Passed Assertion: "+CRBookFunc);
					
			//Marketing KeyCode / Compared
			CRMarkeyCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
				logger.info("Marketing Key Code: "+CRMarkeyCode);
					
					
			//ReservationType
			CResType = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[4]/td[2]")).getText();
				logger.info("Reservation Type is: "+CResType);
				
			//Reservation Status
			CResStatus = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]")).getText();
				logger.info("Reservation Status: "+CResStatus);
				
			//Property / Compared
			CRProperty = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText();
				Assert.assertEquals(CRProperty, RAResortName);
					logger.info("Property displayed and Matched From Resort Availabilty screen: "+CRProperty);
					
					//Property Flag / Compared
					driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[6]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on The flag image
						logger.info("Clicked on Property Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the Close button is present 
								CRPropFlagHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
									Assert.assertEquals(CRPropFlagHeader, ErrataHeader);
										logger.info("Property Flag Header displayed and Matched: "+CRPropFlagHeader);
											driver.findElement(By.id("REWbtnClose")).click(); //Click on Close button

			//ArrivalDate / Compared
			CRArrDate = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[7]/td[2]")).getText();
				Assert.assertEquals(CRArrDate, RAArrDate);
					logger.info("Arrival Date displayed and Matched From Resort Availabilty screen: "+CRArrDate);
			
			//Nights / Compared
			CRNights = driver.findElement(By.xpath(".//*[@id='ResSum_pnNights']")).getAttribute("value");
				Assert.assertEquals(SANights, CRNights);
					logger.info("Number of Nights Displayed and Matched Assertion: "+CRNights);
						
										
			//Departure Date / Compared
			CRDepDate = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value");
				Assert.assertEquals(CRDepDate, RADepDate);
					logger.info("Departure Date displayed and Matched from Resort Availability Screen: "+CRDepDate);		
					
			//Guarantee Code
			CRGuaCode = driver.findElement(By.xpath(".//*[@id='ResSum_pvGuarantee_input']")).getAttribute("value");
				logger.info("Guarantee Code: "+CRGuaCode);
				
			//Usage Period
			CRUsagePeriod = driver.findElement(By.xpath(".//*[@id='ResSum_UsagePeriod']")).getText();
				logger.info("Usage Period: "+CRUsagePeriod);
			
			//Membership Number / Compared
			CRMemshipNum = driver.findElement(By.xpath(".//*[@id='ResSum_MemberContrID']")).getText();
				logger.info("MemberShip Number: "+CRMemshipNum);
					Assert.assertEquals(CRMemshipNum, MembershipNumber);
						logger.info("Membership Number Matched from ClubRes (Membership Detail for:): "+CRMemshipNum);		
			
			//Number of Guests / Compared
			NoofGues = driver.findElement(By.xpath(".//*[@id='ResSum_Adults']")).getAttribute("value");	
				Assert.assertEquals(SAAdults, NoofGues);
					logger.info("Number of Adults Displayed and Matched in Search of Availability Screen: "+NoofGues);
			
			//Roomtype / Compared
			CRRmType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
				CRRoomType = CRRmType.substring(0, 3);
					logger.info("Room Type: " +CRRoomType);
						Assert.assertEquals(CRRoomType, RURoomType);
							logger.info("Room Type Displayed and Matched from RoomUpgrade Screen: "+CRRoomType);
				
			//Room Type Flag / Compared
			driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Roomtype Flag
				logger.info("Clicked on Room Type Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the Close button is present
						CRRoomTypeFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
							logger.info(""+CRRoomTypeFlagHeader);
								driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
									
		//-----------------------------------------------------------------------Travel Protection------------------------------------------------------------------------------------------//
		
			logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");			
				
			//Purchase Rpp
			driver.findElement(By.xpath(".//*[@id='purchaseARP']")).click();
				logger.info("Purchased Annual RPP");
						
		//----------------------------------------------------------------------Financial Summary------------------------------------------------------------------------------------------//				
				logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
										
				//Reservation And Misc Fees / Compared
				CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText();
					logger.info("Reservation And Misc Fees: "+CPResAdMsc);
					
					CPResAdMsc1 = CPResAdMsc.replace("$", ""); //Take of dollar Sign
					 CPResAdMsc2 = CPResAdMsc1.trim();// Trim the string to compress blank space 
					 
				//Reservation And Misc Fees Flag 
				driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
					logger.info("Clicked on Reservation And Misc Fees Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
						
				//Loyalty Accomodation Upgrade Fee Compare / Compareed
				logger.info("-------------------------------Loyalty Accomodation Upgrade Fee------------------------------------------------------------");
					AccUpgradeFee = driver.findElement(By.xpath(".//*[@id='FRRTRN2AMOUNT']")).getText();
						String AccountUpgradeFee = AccUpgradeFee.substring(0,2);
							logger.info("Loyality Accomodation Upgrade Fee: "+AccountUpgradeFee);
								Assert.assertEquals(AccountUpgradeFee, RoomUpgCost);
									logger.info("Loyalty Accomodation Upgrade Fee Matched with RoomUpgrade Cost: ASSERTION PASSED");
						
							//Total Amount Due	/ Compared	
							try {
								TotalAmountDue = driver.findElement(By.xpath(".//*[@id='FRR_TOTAL_AMOUNT']")).getText();
									logger.info("Total Amount due is: "+TotalAmountDue);
										Assert.assertEquals(TotalAmountDue, CPResAdMsc2);
											logger.info("Total Amount due matched Reservation And Misc Fees");
											
										 	Properties properties = new Properties();
										 		properties.setProperty("TotalAmountDue", TotalAmountDue);
										 		properties.setProperty("AccUpgradeFee", AccUpgradeFee);

											File file = new File("Refund_AnnualRPP.Properties");
											FileOutputStream fileOut = new FileOutputStream(file);
											properties.store(fileOut, "Annual Reservation Protetion Plan");
											fileOut.close();
											
										} catch (FileNotFoundException e) {
											e.printStackTrace();
										} catch (IOException e) {
											e.printStackTrace();
										}
							
							//Add Credit Card
							driver.findElement(By.xpath(".//*[@id='pvpaycc_input']")).click();
								Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='pvpaycc_select']"))); 
									Payment.selectByValue("0");// Select Booking Type

											
								//-------Add Credit Card---------//
											
								//Credit Card Number
								driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
									driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
										driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						
								//Card Holder Name
									driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
								driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
								
								//Exp mnth & Year
								driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
									Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
										ExpMth.selectByValue(ExpMonth);// Exp Mnth
								
								driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
									Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
										ExpYear.selectByValue(ExpYr);// Exp Year
									
								//Active
								driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
									Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
										Active.selectByValue("Y");// Active
											driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								
								//DRI Credit Card Y/N
								driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
									Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
										Dricc.selectByValue("N");// DRI Credit Card	
								
								//Click on Save Button
								driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
									logger.info("Clicked on Save Button");
									
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok Button is present 
									
									String Success = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
										logger.info(""+Success); //Success MEssage
										
									driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Click on OK
					
									driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
															
								//Click on SAVE Button
								driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
										logger.info("Clicked on SAVE BUTTON");
										
								//Payment Mes
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Close Button is present 
								String PaymentMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
									logger.info(""+PaymentMessage);
										driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Close											
	
			}
}	


