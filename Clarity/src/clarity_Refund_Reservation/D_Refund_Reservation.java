package clarity_Refund_Reservation;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;


public class D_Refund_Reservation extends A_Refund_Common {
	
Logger logger = Logger.getLogger("D_Refund_Reservation.class");

	
	//Reservations Method
	public String winHandleBefore; //Store Current Window Handle
	public String Lead_Id; //Used inside try method to retrieve LILeadid from Refund_Res_Leadid.Properties
	public String ResLeadId; //Reservation LeadId
	
	//Refund Method
	public String ResConNum; //Retrieve Reservation Confirmation Number
	public String RAPnts; //Retrive Cost of Reservation from Refund_ResortAvailability.Properites
	public String ClubRespoints; //Retrive Points Available for this year from Refund_Reservation.Properties
	public String TotalAmountDue; //Retrieve Cost of Annual RPP from Refund_AnnualRPP.Properties
	public String AccUpgradeFee; //Loyalty Accomodation  Upgrade Fee from Refund_AnnualRPP.Properties
	public String RoomUpgCost;  //RoomUpgrade Cost from Refund_RoomUpgrade.Properties
	public String LoyaltyAccUpgFee; //Loyalty Accomodation Upgrade Fee
	
	public String CancelResHeader; //Cancel Reservation Header
	public String TotalResPoints; //Total Reservation Points
	public String NewPointsBalance; //New PointsBalance

	public String CanResMiscFeesHeader; //Cancel Reservation: Reservation and Misc Fees Header
	public String CostofARPP; //Cost of ARPP 
	
	
	
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
		public void Reservations() {
			
			PropertyConfigurator.configure("Log4j.properties");
		
			 try (FileReader reader = new FileReader("Refund_Res_Leadid.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		Lead_Id = properties.getProperty("LILeadid");
		      		logger.info(Lead_Id);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			 
			 try {
			 		
				 winHandleBefore = driver.getWindowHandle(); // get the current window handle
				 	Properties properties = new Properties();
				 		properties.setProperty("winHandleBefore", winHandleBefore);
			
					File file = new File("Refund_WindowHandle.Properties");
					FileOutputStream fileOut = new FileOutputStream(file);
					properties.store(fileOut, "Store WindowHandle");
					fileOut.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				

				 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 						
							//Capture Leadid
								 ResLeadId = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
															logger.info("Lead Number in Reservations Tab: "+ResLeadId);
																logger.info("From Lead Information TAB "+Lead_Id);
																	Assert.assertEquals(ResLeadId, Lead_Id);
																		logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");

							//Opens New Window
							driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
						 		logger.info("clicked on Club Res");
					 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

							// Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}
			}
		
		
		
		public void Cancel_Reservation_Refund() {
			
			//Wait for 3 Seconds
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//Get the Window Handle 
			try (FileReader reader = new FileReader("Refund_WindowHandle.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
	      		winHandleBefore	 = properties.getProperty("winHandleBefore");
		      		logger.info(winHandleBefore);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}

			
			
			driver.switchTo().window(winHandleBefore);
			
			logger.info("-----------------------------------------Cancel Reservation With Refund-----------------------------------------------------");
					
			 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
		 		logger.info("clicked on Reservations Tab");
		 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 			
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//				 			
				//Retrieve Confirmation Number From Refund_Reservation_Confirmation Properties File
				try (FileReader reader = new FileReader("Refund_Reservation_Confirmation.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		
			      		ResConNum = properties.getProperty("ResConNum");
			      			logger.info(ResConNum);
			      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				 
		      //Retrieve Cost of Reservation Points From Refund_ResortAvailability.Properites
				try (FileReader reader = new FileReader("Refund_ResortAvailability.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		
			      		RAPnts = properties.getProperty("RAPnts");
			      			logger.info("Cost of Reservation is: "+RAPnts+" Points");
			      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				
				//Retrieve Cost of Reservation Points From Refund_Reservation.Properites
				try (FileReader reader = new FileReader("Refund_Reservation.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		
			      		ClubRespoints = properties.getProperty("ClubRespoints");
			      			logger.info("Points Available to Book Reservation This Year: "+ClubRespoints);
			      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}	
		      	
			    //Retrive Cost of Annual RPP from  Refund_AnnualRPP.Properties
					try (FileReader reader = new FileReader("Refund_AnnualRPP.Properties")) {
						 Properties properties = new Properties();
				      		properties.load(reader);
				      		TotalAmountDue = properties.getProperty("TotalAmountDue");
				      		AccUpgradeFee = properties.getProperty("AccUpgradeFee");
				      			logger.info("Total Amont: "+TotalAmountDue);
				      			
				      			
				      	} catch (IOException e) {
				      		e.printStackTrace();
				      	}
					
				//RoomUpgrade Cost from Refund_RoomUpgrade.Properties
				try (FileReader reader = new FileReader("Refund_RoomUpgrade.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		
			      		RoomUpgCost = properties.getProperty("RoomUpgCost");	      			
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//					
				WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody")); //Table Tbody
						List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
							//logger.info("No.of rows: " +columns.size());
								int rowsnum = columns.size();
										String xpath =null;
											String ResNum;
												
													for(int i = 1;i<=rowsnum;i++)
													{
														xpath= ".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
															ResNum = driver.findElement(By.xpath(xpath)).getText();
																	logger.info("Substring is: "+ResNum);
																
															driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
															
															if(ResConNum.equals(ResNum))
															{
																logger.info("Confirmation Number Found and Matcehd from the Reservation Confirmation Screen");
																
																	driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
																		logger.info("Clicked on the Flag image of the Confirmation Number");
																		break;
															}
													}
													
				if(driver.findElements(By.xpath(".//*[@id='genericWidgetInnerDiv']/div/div[3]")).size()!=0) 
				{
					logger.info("Clicked on Notice Notice");
						driver.findElement(By.xpath(".//*[@id='genericWidgetInnerDiv']/div/div[3]")).click();	
				}
				
				else
				{
					logger.info("Window not found");
				}
														
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RMS_ButtonBar']/table/tbody/tr/td[2]/div"))); // Wait till the Cancel Res button is Clickable
				
				driver.findElement(By.xpath(".//*[@id='RMS_ButtonBar']/table/tbody/tr/td[2]/div")).click(); //Click on Cancel Res button
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RCWbtnSave']"))); // Wait till the Continue Button in Cancel Reservation window is Clickable
				
				//Cancel Reservation Header / Compared
				CancelResHeader = driver.findElement(By.xpath(".//*[@id='RCWHeader']")).getText();
					Assert.assertEquals(CancelResHeader, CancelReservationHeader);
						logger.info("Cancel Reservation Header Displayed and Passed the Assertion");
						
				//Choose a cancellation Reason
				driver.findElement(By.xpath(".//*[@id='RCF_pvCancelCode_input']")).click();
				
				Select AnnualRPP = new Select(driver.findElement(By.id("RCF_pvCancelCode_select")));
					AnnualRPP.selectByValue("PPR"); //Select PPR
				
				//Cancellation Comments
				driver.findElement(By.xpath(".//*[@id='RCF_CNX_REMARK']")).click();
				driver.findElement(By.xpath(".//*[@id='RCF_CNX_REMARK']")).sendKeys("RPP Purchase");
				logger.info("Cancellation Reason - RPP Purchase");
				
				//-------Points-------------//
				//Total Res Points / Compared
				TotalResPoints = driver.findElement(By.xpath(".//*[@id='ClubResvPointsTable']/table/tbody/tr[2]/td[2]")).getText();
					Assert.assertEquals(TotalResPoints, RAPnts);
						logger.info("Total Res Points Matched with Cost of Reservation Points from Resort Availability Screen");
				
				/*//New Points Balance / Compared
				NewPointsBalance = driver.findElement(By.xpath(".//*[@id='ClubResvPointsTable']/table/tbody/tr[6]/td[2]")).getText();
					Assert.assertEquals(NewPointsBalance, ClubRespoints);
						logger.info("New Points Balance Matched with Available Points to Book Reservation This Year from Club Res: "+NewPointsBalance);*/
						
				driver.findElement(By.xpath(".//*[@id='RCWbtnSave']")).click(); // Click on Continue button 
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClubFeeRefundSaveBtn']"))); // Wait till the Process Button in Cancel Reservation: Reservtion & Misc Fees window is Clickable
				
				//---------------------------------------------------------------//Cancel Reservation: Reservation and Misc Fees-------------------------------------------------//
				
				//Cancel Reservation: Reservation and Misc Fees Header / Compared
				CanResMiscFeesHeader = driver.findElement(By.xpath(".//*[@id='ClubFeeRefundFormTitle']")).getText();
					Assert.assertEquals(CanResMiscFeesHeader, CRResMisFeesHeader);
						logger.info("Cancel Reservation: Reservation and Misc Fees Header Displayed and Passed Assertion");
						
				//Loyalty Accommodation Upgrade Fee //Compared
				LoyaltyAccUpgFee = driver.findElement(By.xpath(".//*[@id='ClubFeeRefundTable']/tr[2]/td[3]")).getText();
					Assert.assertEquals(LoyaltyAccUpgFee, RoomUpgCost);
						logger.info("Cost of Loyality Accomodation Upgrade Fee Matched the Roomupgrade Fee:  "+LoyaltyAccUpgFee);		
						
				//Annual Reservation Protection Plan // Compared
				double AnnualResPP = Double.parseDouble(TotalAmountDue); //Convert String to Double 
					int ARPP = (int) AnnualResPP; //Convert double to integer to elimate decimals
						int RoomUpgradeCost = Integer.parseInt(RoomUpgCost); //convert to int	
							 int ARPPlan = ARPP - RoomUpgradeCost; //Cost of ARPP
							  	logger.info("Cost of ARPP: "+ARPPlan);
							  		String AnnualReservationPP = String.valueOf(ARPPlan); //convert int to string for comparision
							  		
				CostofARPP = driver.findElement(By.xpath(".//*[@id='ClubFeeRefundTable']/tr[3]/td[3]")).getText();
					Assert.assertEquals(CostofARPP, AnnualReservationPP);
						logger.info("Cost of Annual Reservation Protection Matched the ARPP with the Cost of ARPP in Travel Protection: "+CostofARPP);
						
				driver.findElement(By.xpath(".//*[@id='ClubFeeRefundTable']/tr[2]/td[1]/input")).click(); //Click on the check box of Annual Reservation Protecton
				
				driver.findElement(By.xpath(".//*[@id='ClubFeeRefundSaveBtn']")).click();
						
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok button is Clickable
				
				String PaymentMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
					logger.info(""+PaymentMessage); //Refund Message
					driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Click on Ok button
					
				String CancelResConfirmation = driver.findElement(By.xpath(".//*[@id='GRMBContent']")).getText();
					logger.info(""+CancelResConfirmation); //Cancel Reservation Confirmation
					
				driver.findElement(By.xpath(".//*[@id='GRMBCloseBtn']")).click(); //click on Close button
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]/table/tbody/tr/td[2]/img"))); // Wait till the Reservation Status Flag is clickable
				
				String CXL_Cancelled = driver.findElement(By.xpath(".//*[@id='ResSum_ResvStatusDesc']")).getText();
					logger.info("Reservation Status: "+CXL_Cancelled);
	
				
				}
					
		

}

