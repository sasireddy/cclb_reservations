package clarity_Refund_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class I_Refund_FinalConfirmation extends A_Refund_Common {

	Logger logger = Logger.getLogger("I_Refund_FinalConfirmation.class");

	public String RCBegiPntsBal; //Beginning Point Balance
	public String RCResCost; //Reservation Cost
	public String RCResCt; //Replace "RCResCost" without ","
	public String RCTotPnts; //Total Points Used for Reservation
	public String RCTotalPoints; //Replace "RCTotPnts" without ","
	public String RCEndPntBal; //Ending points balance
	public String ConfimationMes; //Confirmation Message with Confirmation Number
	
	
	//Retrieved from Refund_Reservation.Properties
	public String ClubRespoints;
	
	//Retrieved from Refund_ResortAvailability.Properties
	public String RAPoints;
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	public void ResProperties()
	 {
		 try (FileReader reader = new FileReader("Refund_Reservation.Properties")) {
     		Properties properties = new Properties();
	      		properties.load(reader);
	      		ClubRespoints = properties.getProperty("ClubRespoints");
	      		 		logger.info(ClubRespoints);
	      		 	
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		 PropertyConfigurator.configure("Log4j.properties");	
		  try (FileReader reader = new FileReader("Refund_ResortAvailability.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		RAPoints = properties.getProperty("RAPnts");
	      		logger.info("Points from Resort Availability: " +RAPoints);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
	 }
	
	public void Reservation_Confirmation()
	{
		PropertyConfigurator.configure("Log4j.properties");	
		
		ResProperties(); //Call the Method
					
		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					
		logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
								
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
			//Beginning Points Balance / Compared
			RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //Present Year points
				Assert.assertEquals(RCBegiPntsBal, ClubRespoints);
					logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Club Res Available Points For Reservations Present Year: "+RCBegiPntsBal);
					
					//Reservation Cost
					RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost = RCResCost.length(); // Length of RCResCost
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCResCt = RCResCost.replace(",","");
						Assert.assertEquals(RCResCt, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt);
									
						//Total Points Used for Reservation / Compared 
						RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							RCTotalPoints = RCTotPnts.replace(",","");
								Assert.assertEquals(RCTotalPoints, RAPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCTotalPoints);
				
									
						//Estimating Ending Points
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	/ Compared
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							
					}
					
					else		
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost);
							
								
						//Total Points Used for Reservation
						RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							Assert.assertEquals(RCTotPnts, RAPoints); //Comparing 
								logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCTotPnts);
										
						//Estimating Ending Points
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	
							RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");			
							
					}
					
				
					//Confirmation Message
					ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
						logger.info(""+ConfimationMes);
						
						//Save to Refund_Reservation_Confirmation.Properties File
						try{
						String ResConNum = ConfimationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
						logger.info("Reservation Confirmation number is: "+ResConNum);
						
						Properties properties = new Properties();
				 		properties.setProperty("ResConNum", ResConNum);
				 		
				 		File file = new File("Refund_Reservation_Confirmation.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Reservation Confirmation Number");
						fileOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				driver.findElement(By.xpath(".//*[@id='ClbResvConf_close']")).click(); //Close Button in Reservation Confirmation Screen
					
 			}				
		
}
	
