package clarity_Refund_Reservation;

import org.testng.annotations.Test;

public class Z_Refund_Execution {

	B_Refund_Login Refund_Login = new B_Refund_Login();
	C_Refund_Customer360_LeadInfo Refund_Leadinfo = new C_Refund_Customer360_LeadInfo();
	D_Refund_Reservation Refund_Reservation = new D_Refund_Reservation();
	E_Refund_ClubRes Refund_ClubRes = new E_Refund_ClubRes();
	F_Refund_BookReservation Refund_BookRes = new F_Refund_BookReservation();
	G_Refund_MOPPayments Refund_MOP = new G_Refund_MOPPayments();
	H_Refund_ClubReservations Refund_Reserve = new H_Refund_ClubReservations();
	I_Refund_FinalConfirmation Refund_ResConfirm = new I_Refund_FinalConfirmation();
	
	@Test
	public void Refund_Reservation()
	{
		Refund_Login.openBrowser();
		Refund_Leadinfo.Customer360();
		Refund_Leadinfo.Lead_Information();
		Refund_Reservation.Reservations();
		Refund_ClubRes.ClubRes();
		Refund_BookRes.BookReservation();
		Refund_BookRes.Resort_Availability();
		Refund_BookRes.Room_Upgrade_Yes();
		Refund_BookRes.Errata();
		Refund_BookRes.Discount_No();
		Refund_MOP.MethodofPaymentOptions();
		Refund_Reserve.Reservation();
		Refund_ResConfirm.Reservation_Confirmation();
		Refund_Reservation.Cancel_Reservation_Refund();
		
		
		
		
	}

}

