package Testing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ClubReservation extends A_Standard_Common {
	
		
	Logger logger = Logger.getLogger("B_RoomUpgrade_Login.class");
	int k = 1;
	public void openBrowser() {
			  
		  PropertyConfigurator.configure("Log4j.properties");
			  driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL
			  //driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL
				driver.manage().window().maximize();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan20");// Entering Password
									logger.info("Entered Password");
										driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}				
		 
	public void Customer(){
		 				
 				driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).click(); //Click on the drop down
 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 					
			try {
				
				FileInputStream fileInputStream = new FileInputStream("H://LeadInfo.xlsx");
				//@SuppressWarnings("resource")
				XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
				XSSFSheet worksheet = workbook.getSheet("Lead Info");
				int rowCount = worksheet.getLastRowNum()- worksheet.getFirstRowNum();
				logger.info("Row count is" +rowCount);
				
			
				if(k <= rowCount) {	

					Row row = worksheet.getRow(k);
				        String Leadid = new DataFormatter().formatCellValue(row.getCell(0));
		        		logger.info(Leadid);
   		
		        		driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(Leadid); //Enter DBL LEAD Id
		        			logger.info("Lead ID is: "+Leadid);	 
		        				driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a Member");
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					 }
				 }
				
			k++; }
				
		}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
		}
	}
	
	public void Lead_Information() {
			
			 logger.info("--------------------------------------------------------------------------Lead Information---------------------------------------------------------------------------------");
			 
			 logger.info("---------------------------------------------Primary Lead and Secondary Lead Information--------------------------------------------------------");
		
			//---------------------------------------------------------------------------------------------------------------------------------------------------//
			//Lead Area-Lead Id	
			String Lead_Area_Id = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[1]/td[2]")).getText();
				logger.info("Lead Area-Lead Id: " +Lead_Area_Id);
				
				//******************************************************//
				//Add the Leadid into Cookie
				Cookie Leadid = new Cookie("Lead_Area_Id",Lead_Area_Id);
					driver.manage().addCookie(Leadid);
				//******************************************************//
					
			String Lead_Type = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[2]/td[2]/table/tbody/tr/td[1]")).getText();
				String Sub_Type = driver.findElement(By.xpath(".//*[@id='lead_subtype_input']")).getAttribute("value");
					logger.info("Lead Type/Subtype: "+ Lead_Type +" "+Sub_Type);
					
			 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
					
			 String  winHandleBefore = driver.getWindowHandle();
			 Cookie cookie = new Cookie("winHandleBefore", winHandleBefore);
			 driver.manage().addCookie(cookie);
			 
			 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
			 		logger.info("clicked on Reservations Tab");
			 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			 						
						//Capture Leadid
							String ResLeadId = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
														logger.info("Lead Number in Reservations Tab: "+ResLeadId);
															logger.info("From Lead Information TAB "+Lead_Area_Id);
																Assert.assertEquals(ResLeadId, Lead_Area_Id);
																	logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");

						//Opens New Window
						driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
					 		logger.info("clicked on Club Res");
				 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

						// Switch to new window opened
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
		}
	
			public void ReservationsTab() {
				String winHandleBefore = driver.manage().getCookieNamed("winHandleBefore").getValue();
				
				driver.switchTo().window(winHandleBefore);
				
				driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]")).click();
				
				 WebDriverWait wait = new WebDriverWait(driver, 30);
				 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
				 		customer360.click(); // Click on Customer 360
				 			logger.info("Clicked on Customer 360");
				 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				
				
			}
	
				
	}
	


