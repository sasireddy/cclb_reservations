package cclb_Reservations;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A_Common {
	
	protected static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 100);

	//-----------------------------------------------------------------Book New Reservation------------------------------------------------------------//
		//Book New Reservation
		String BookingTypeReservation = "10194"; //Home Standard Reservation Value
		String BookTypeReserName = "HOME-STANDARD RESERVATION";
		
		//-----------------------------------------------------------------Search for Availability------------------------------------------------------------//
		//Search for Availability
		String SAArrivalDateMethod = "FlexDays"; //Arrival Date Method as Flexible Days
		String SAArrivalDate = "t+30"; //Arrival Date
		String SAAdults = "2";
		String SANights = "2";
		String BorrowSANights = "10";
		
		//-----------------------------------------------------------------Availability Search Request----------------------------------------------------------//
		String AvailabilityResultsHeader = "Availability Results";
		String AvailabilitySearchReqHeader = "Availability Search Request";
		String EarliestArrivalDate = "t+60"; //Availability Search Request Earilest Arrival Date
		String LatestArrivalDate = "t+70"; //Availability Search Request Latest Arrival Date
		String AvailSeaReqChoicesAvail = "Availability Search Request - Choices Available"; //Availability Search Request - Choices Available Header
		String CurSeaReqHeaader = "Current Search Requests";
		String PastSeaReqHeader = "Past Search Requests";
		
		//-----------------------------------------------------------------Resort Availability - CLUB------------------------------------------------------------//
		//Resort Availability - CLUB
		String ResortType = "EXX"; //Resort Type SubString
		
		//-----------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------//
		//Additional Resort Information (Errata)
		String ErrataHeader = "Additional Resort Information (Errata)"; //
		
		//-----------------------------------------------------------------Room Upgrade------------------------------------------------------------//
		//Room Upgrade
		String RoomUpgrade1 = "Room Upgrade";
		
		//-----------------------------------------------------------------Discount------------------------------------------------------------//
		
		//Discount
		String DiscountHeader = "Discount";
		String DisMesAvail = "Member has sufficient points to book this reservation.";
		String DisMesNotAvail = "Member does not have sufficient points to book this reservation.";
		String DiscountPercentage = "50";
		String DiscountYesMessage = "Selected Yes For Discount";
		
		//-----------------------------------------------------------------Method of Payments------------------------------------------------------------//
		
		//Method of Payments
		String MOPBalPointsNd = "0"; //Balance of Points needed
		
		//-----------------------------------------------------------------Club Reservations------------------------------------------------------------//
		
		//Reservation and Misc Fees
		String CRResAndMiscFees = "$ 0.00";
		String CRReserMiscFeesHeader = "Reservation and Misc Fees"; //Reservation and Misc Fees Flag Header
			String CRPayInfoHeader = "Payment Information";
				String CreditCardMainHeader = "Credit Card Maintenance";
					String CCNumber = "4321000000001119";
						String CCType = "Visa";
							String CCName = "Diamond Tester";
								String ExpMonth = "01";
									String ExpYr = "20";

		//-----------------------------------------------------------------Reservation Confirmation------------------------------------------------------------//
			
		String ResConfirmationHeader = "Reservation Confirmation"; //Reservation Confirmation Header
		String RCPnSumHeader = "Points Summary";
		String RCConfirmLetterHeader = "Confirmation Letter";
		
		//-----------------------------------------------------------------Cancel Reservation------------------------------------------------------------//
		
		//Cancel Reservation
		String CancelReservationHeader = "Cancel Reservation";
		String CRResMisFeesHeader = "Cancel Reservation: Reservation and Misc Fees";
}
