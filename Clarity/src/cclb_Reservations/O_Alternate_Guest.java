package cclb_Reservations;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class O_Alternate_Guest extends A_Common {
	
	Logger logger = Logger.getLogger("O_Alternate_Guest.class");
	
	String AlternateGuestMessage; //Alterante Message 
	String AlternateGuest = "ALT";
	
	@Test
	@Parameters("Alternate_Guest_Leadid")
	public void Alternate_Guest(String Alternate_Guest_Leadid ) {
		
		//-----------------------------------------------------------------------Guests------------------------------------------------------------------------------------------//
		
		logger.info("-------------------------------------------------------------------Alternate Guests------------------------------------------------------------");	
			
			driver.findElement(By.xpath(".//*[@id='pvaddguest_type_input']")).click(); //Click on Select
				Select Property = new Select(driver.findElement(By.id("pvaddguest_type_select")));
					Property.selectByValue(AlternateGuest); //Select by Value ALT (Alternate Guest)
						logger.info("Selected: "+AlternateGuest);

			//Handle Window			
			String parentHandle = driver.getWindowHandle(); // get the current window handle
				driver.findElement(By.xpath(".//*[@id='addguest_lead_display']")).click(); // click on select lead

			for (String winHandle : driver.getWindowHandles())
			{
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			/*Select LeadArea = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_input']")));
			LeadArea.selectByValue(AlternateLeadArea);*/
			
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(Alternate_Guest_Leadid); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+Alternate_Guest_Leadid);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
	
			String LeadType = "OWN";
	
			Search:
			 for (int i=1; i<=10;i++)
			 {
				 for(int j=2;j<=2;j++)
				 {
					 String LeadType1 = null;
						String xpath  = null;
							xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
								LeadType1 = driver.findElement(By.xpath(xpath)).getText();
										driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								
									if(LeadType.equals(LeadType1))
									{
												driver.findElement(By.xpath(xpath)).click();
													logger.info("Lead is a Member");
														break Search;				
									}	
				 }
			 }

			driver.switchTo().window(parentHandle); // switch back to the original window
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='AddGuestSaveBtn']"))); // Wait till the Close Button is present 
				driver.findElement(By.xpath(".//*[@id='AddGuestSaveBtn']")).click(); //Click on Save
					driver.findElement(By.xpath(".//*[@id='divYesBtn']")).click(); //Click on Yes
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Close Button is present
			AlternateGuestMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
				logger.info(""+AlternateGuestMessage);
					driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); // Click on Ok Button
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
		
		
		
		
	}
}
