package cclb_Reservations;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Q_Errata extends A_Common {
	
	Logger logger = Logger.getLogger("Q_Errata.class");
	
	//Errata
	 String ADRIErrataHeader; //Additional Resort Information (Errata)	
	
	@Test
	public void Errata() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("-------------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till Close Button is present to click
		
		 if(driver.findElements(By.xpath(".//*[@id='REWHeader']")).size() !=0) 
			{
			 
			 //Errata Header
			  ADRIErrataHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText(); // //Header of Additional Resort Information (Errata)
			 	Assert.assertEquals(ADRIErrataHeader, ErrataHeader);
			 		logger.info("Header Displayed and Passed Assertion: "+ADRIErrataHeader);
			 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			 	
			 	//Click on Close Button
				driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();
					logger.info("Clicked on close button");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				
			}
			
			else
			{
				logger.info("Errata window not found");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}
		 
	}
	

}
