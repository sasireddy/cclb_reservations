package cclb_Reservations;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class F_ClubMembership extends A_Common {

	Logger logger = Logger.getLogger("F_ClubMembership.class");
	
	//-----------------------------------Guest----------------------------//
	 String ClubResLeadNum;//Lead Number
	 String Lead_Area_Id; //Use to retrieve Lead id  from cookie
	
	//-----------------------------------Membership Detail for:----------------------------//
	 String ClubResMemDetail; //Membership Detail for: details
	 String ClubResMemDetailfor; //Substring of ClubResMemDetail
	
	//-----------------------------------Usage Information----------------------------//
	 String ClubRespoints; //Replace "," with "" from Present Year Points(Points available for Reservations)
	 String ClubResAllotment; // Present Year Allotment 
	 String ClubResMemBen; //Present Year Available for MemberBenefits
	 String ClubResSave; //Present Year Available to save to follwoing yr
	 String ClubRespointsNextYear; //Points available for reservation for next year
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	@Test
	public void ClubRes() {
		 
		 PropertyConfigurator.configure("Log4j.properties");
			
		 logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		 
	//-----------------------------------------------------------------------GUEST------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Guest------------------------------------------------------------");
			
			WebDriverWait wait = new WebDriverWait(driver, 120);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='guestypeimg']"))); // Wait till the element LEAD TYPE FLAG is present to click
			

		 	if(driver.findElements(By.xpath(".//*[@id='genericWidgetInnerDiv']/div/div[3]")).size()!=0) 
			{
				logger.info("Cliked on Notice---Notice--Notice--- Member has limited Mobility-- Close Button");
					driver.findElement(By.xpath(".//*[@id='genericWidgetInnerDiv']/div/div[3]")).click();
				
			}
			
			else
			{
				logger.info("---Notice---Notice---Notice---Window not found");
			}
			
			
			//***************************************************************************//
			//Retrieve Value from Cookie
			Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
			logger.info("Value Retrieved from cookie is "+Lead_Area_Id);
			//***************************************************************************//
			
			//Lead Number / Compared
			ClubResLeadNum = driver.findElement(By.xpath(".//*[@id='pv1guest_lead']/a")).getText();
				logger.info("Lead Area-Lead Id: " +ClubResLeadNum);
					Assert.assertEquals(ClubResLeadNum, Lead_Area_Id);
						logger.info("Lead Number in Lead Information Tab Matched with Club Reservations");
				
		//-----------------------------------------------------------------------Membership Detail------------------------------------------------------------------------------------------//
											
			logger.info("-------------------------------------------------------------------Membership Detail------------------------------------------------------------");
			
						
			//Details			
			ClubResMemDetail = driver.findElement(By.xpath(".//*[@id='tdMembership']")).getText(); //Membership Detail For:
				logger.info("Membership Details for: "+ClubResMemDetail);
			
				ClubResMemDetailfor = ClubResMemDetail.substring(0,6); //Membership Detail for subString
				logger.info("Membership Number: "+ClubResMemDetailfor);
				
		//-----------------------------------------------------------------------Usage Information------------------------------------------------------------------------------------------//
			
				logger.info("-------------------------------------------------------------------Usage Information------------------------------------------------------------");
					
				//Present Year points
				ClubResAllotment = driver.findElement(By.xpath(".//*[@id='pv1MbrAlt']")).getText();
					logger.info("Allotment for Present Year: "+ClubResAllotment);
					
						//Present Year Avail for Reservations
					ClubRespoints = driver.findElement(By.xpath(".//*[@id='mbrRemBan']")).getText().replace(",", ""); //Get Present Year Points and replace , with null;
							logger.info("Present Year Available for Reservations: "+ClubRespoints);
							
							ClubRespointsNextYear = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[3]/td[3]")).getText().replace(",", ""); //Get Next Year Points 
								logger.info("Points Available for Reservation Next Year: "+ClubRespointsNextYear);
							
							//Present Year Avail for Member Benefits
							ClubResMemBen = driver.findElement(By.xpath(".//*[@id='mbrAvaBenefit']")).getText();
								logger.info("Present Year Available for Member Benefits: "+ClubResMemBen);
											
								//Present Year Avail to save to following yr 
								ClubResSave = driver.findElement(By.xpath(".//*[@id='mbrAvaSave']")).getText();
									logger.info("Present Year Available to Save to Following yr: "+ClubResSave);
								
									//***************************************************************************//
									//Add Cookie
									Cookie ClubRes1 = new Cookie("ClubRespoints", ClubRespoints);
									Cookie ClubRes2 = new Cookie("MembershipNumber", ClubResMemDetailfor);
									Cookie ClubRes3 = new Cookie("ClubRespointsNextYear",ClubRespointsNextYear);

									driver.manage().addCookie(ClubRes1);
									driver.manage().addCookie(ClubRes2);
									driver.manage().addCookie(ClubRes3);
									//***************************************************************************//
									
									
								//Room Upgrades Used/ Remaining:
								String CRRoomUpgrade = driver.findElement(By.xpath(".//*[@id='mbrRoomUpd']")).getText();
									logger.info("Room Upgrades: "+CRRoomUpgrade);
										String RoomUpgrade = CRRoomUpgrade.substring(2);
											String RmUpgrade = CRRoomUpgrade.substring(0,1); 
											
											int CRRoomUpgrade1 = Integer.parseInt(RoomUpgrade);
												int CRRoomUpgrade2 = Integer.parseInt(RmUpgrade);
												
												if(CRRoomUpgrade2 < CRRoomUpgrade1)
												{
													logger.info("Room Upgrade is Available");
												}
												
												else
												{
													logger.info("Room Upgrade is not available");
													//driver.quit();
												}
												
							
	 }
}
