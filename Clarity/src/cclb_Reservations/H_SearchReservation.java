package cclb_Reservations;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class H_SearchReservation extends A_Common {
	
	Logger logger = Logger.getLogger("H_SearchReservation.class");
	
	//Availability Search Request - Choices Available
	public String AvailabilityResultsHeaderComp; //Availability Results Header
	public String AvailabilitySearchReqHeaderComp; //Availability Search Request Header
	public String Avail_Search_Request_ChoAvailHeader; //Availability Search Request - Choices Available Header
	public String EarliestArrivalDate ; //Earliest Arrival Date (Retrieve Value)
	public String LatestArrivalDate; //Latest Arrival Date (Retrieve Value)
	
	@Test
	public void Availability_Search_Request_ChoicesAvailable() {
		
		//----------------------------------------------------------------Availability Search Request---------------------------------------------------------------------------------//	
		
		//Availability Results Header / Compared
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='srAvailabiltyResultAlertYesBtn']"))); // Wait till the Availability Results Window appears (Yes Button)
			AvailabilityResultsHeaderComp = driver.findElement(By.xpath(".//*[@id='srAvailabiltyResultAlertTitle']")).getText();
				Assert.assertEquals(AvailabilityResultsHeaderComp, AvailabilityResultsHeader);
					logger.info("Availability Results Header Matched and passed the assertion");
					
					driver.findElement(By.xpath(".//*[@id='srAvailabiltyResultAlertYesBtn']")).click(); //Clicked on Yes
		
		//Availability Search Request Header / Compared			
		AvailabilitySearchReqHeaderComp = driver.findElement(By.xpath(".//*[@id='CASRTitle']/table/tbody/tr[1]/td")).getText();
			Assert.assertEquals(AvailabilitySearchReqHeaderComp, AvailabilitySearchReqHeader);
				logger.info("Availability Search Request Header Displayed and Matched");
		
		//Earliest Arrival Date
		EarliestArrivalDate = driver.findElement(By.xpath(".//*[@id='pvEarlyArrDate']")).getAttribute("value");
			logger.info("Earliest Arrival Date: "+EarliestArrivalDate);
		
		//Latest Arrival Date	
		LatestArrivalDate = driver.findElement(By.xpath(".//*[@id='pvLateArrDate']")).getAttribute("value");
			logger.info("Earliest Arrival Date: "+LatestArrivalDate);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		//Desired Arrival Day:
		driver.findElement(By.xpath(".//*[@id='SearchRequestWidgetDiv']/form/div[2]/table[2]/tbody/tr[3]/td[2]/input[8]")).click();
			logger.info("Selected Monday as Desired Arrival Day");
			
		//Length of Stay:
		driver.findElement(By.xpath(".//*[@id='SearchRequestWidgetDiv']/form/div[2]/table[3]/tbody/tr[3]/td[1]/input")).click();
			logger.info("Selected Flexible");
			
		driver.findElement(By.xpath(".//*[@id='pvLengthStay']")).sendKeys(SANights); //Nights 
			logger.info("No of nights: "+SANights);
			
		//Unit Size:
		driver.findElement(By.xpath(".//*[@id='unitsizedivNo']/table/tbody/tr[1]/td/input")).click();
				logger.info("Selected Unit Type as Studio Bedroom");
			
		//Notifications:
		driver.findElement(By.xpath(".//*[@id='pvPPay']")).click();  //CLick the checkbox in Notifications
			logger.info("Clicked on Check box - Member been advised that a prepayment may be necessary to confirm availability?");

		//Process
		driver.findElement(By.xpath(".//*[@id='CASRProcessBtn']")).click();
			logger.info("Clicked on Process Button");
		
		logger.info("-------------------------------------------------------------------Availability Search Request - Choices Available------------------------------------------------------------");

		
		//Availability Search Request - Choices Available Header / Compared
			Avail_Search_Request_ChoAvailHeader = driver.findElement(By.xpath(".//*[@id='srSRListTitle']")).getText();
				Assert.assertEquals(Avail_Search_Request_ChoAvailHeader, AvailSeaReqChoicesAvail);
					logger.info("Availability Search Request - Choices Available header displayed and passed Assertion");
			
			//Continue Search Request
				driver.findElement(By.xpath(".//*[@id='srSRListReqBtn']")).click();	
					logger.info("Clicked on Continue Search Request Button");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok button is available to be clickable.
							String Message = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
								logger.info(""+Message);
									driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
										logger.info("Clicked on Ok Button");
										
										
										
							WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='srSRPastCurrent']/div[1]/table/tbody"));
							List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
								//logger.info("No.of rows: " +columns.size());
									int rowsnum = columns.size();
											String xpath =null;
												String cellval;
													
								Search:
													
								for(int i = 1;i<=rowsnum;i++)
								{
									xpath= ".//*[@id='srSRPastCurrent']/div[1]/table/tbody/tr["+ String.valueOf(i) +"]/td[4]";
										cellval = driver.findElement(By.xpath(xpath)).getText();
												logger.info("Arrival From: "+cellval);
											
										driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
										
										if(EarliestArrivalDate.equals(cellval)) // If Arrival From date Matches
										{
											//Request Id
											String RequestId = driver.findElement(By.xpath(".//*[@id='srSRPastCurrent']/div[1]/table/tbody/tr["+ String.valueOf(i) +"]/td[1]")).getText();
												logger.info("Request Id: "+RequestId);
											
											//Property
											String Property = driver.findElement(By.xpath(".//*[@id='srSRPastCurrent']/div[1]/table/tbody/tr["+ String.valueOf(i) +"]/td[3]")).getText();
												logger.info("Property: "+Property);
												
											//ArrivalFrom
											String ArrivalFrom = driver.findElement(By.xpath(".//*[@id='srSRPastCurrent']/div[1]/table/tbody/tr["+ String.valueOf(i) +"]/td[4]")).getText();
												logger.info("Property: "+ArrivalFrom);
											
											//ArrivalTo
											String ArrivalTo = driver.findElement(By.xpath(".//*[@id='srSRPastCurrent']/div[1]/table/tbody/tr["+ String.valueOf(i) +"]/td[5]")).getText();
												logger.info("Property: "+ArrivalTo);
													Assert.assertEquals(ArrivalTo, LatestArrivalDate);
														logger.info("ArrivalTo Date Matched with Latest Arrival Date");
												
											//Duration
											String DurationType = driver.findElement(By.xpath(".//*[@id='srSRPastCurrent']/div[1]/table/tbody/tr["+ String.valueOf(i) +"]/td[7]")).getText();
												logger.info("Property: "+DurationType);
												
												break Search;
										}
								}			
												
					}

}
