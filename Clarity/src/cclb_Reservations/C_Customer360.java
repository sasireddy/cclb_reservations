package cclb_Reservations;


import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class C_Customer360 extends A_Common {
	
	Logger logger = Logger.getLogger("C_Customer360.class");

@Test
@Parameters("Lead_id")
public void Customer360(String Lead_id) throws InterruptedException{
	
	PropertyConfigurator.configure("Log4j.properties");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='pvSearchLead_id']"))); //Wait till the element is Clickable
		
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).click(); //Click on the drop down
			
 					driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(Lead_id); //Enter DBL LEAD Id
 						logger.info("Lead ID is: "+Lead_id);	 
 							driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
 				
 						String LeadType = "MBR";
 				
 						Search:
 						 for (int i=1; i<=10;i++) 
 						 {
 							 for(int j=2;j<=2;j++) 
 							 {
 								 String LeadType1 = null;
 									String xpath  = null;
 										xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
 											LeadType1 = driver.findElement(By.xpath(xpath)).getText(); 
 													driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
 											
 												if(LeadType.equals(LeadType1))
 												{
 															driver.findElement(By.xpath(xpath)).click();
 																logger.info("Lead is a Member");
 																if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) //no phone number	
 																{
 																	logger.info("Cliked on Notice---Notice--Notice--- Member Has limited mobility Close Button");
 																		driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
 																}
 																
 																else
 																{
 																	logger.info("Member Has limited mobility screen not present");
 																}
 																	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
 																		String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
 																			logger.info("LeadName: " +LeadName);
 																				break Search;				
 												}
 																
 							 }
 						 }
 				}

}
