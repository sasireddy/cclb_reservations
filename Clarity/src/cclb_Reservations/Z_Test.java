package cclb_Reservations;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Z_Test extends A_Common {
	
Logger logger = Logger.getLogger("Z_FinalConfirmation.class");
	
	 String ResConfirmHeader; //Reservation Confirmation Header
	 String RCPointsSumHeader; //Points Summary
	 String RCBegiPntsBal; //Beginning Point Balance
	 String RCResCost; //Reservation Cost
	 String RCResCt; //Replace "RCResCost" without ","
	 String RCBorrowedPnts; //Borrowed Points
	 String RCBorrowedPoints; //Replace Borrowed Points with ","
	 String RCDiscountedPoints; //Discounted Points
	 String RCDisPoints; //Replace "RCDiscountedPoints" without ","
	 String RCTotPnts; //Total Points Used for Reservation
	 String RCTotalPoints; //Replace "RCTotPnts" without ","
	 String RCEndPntBal; //Ending points balance
	 String ConfirmationMes; //Confirmation Message with Confirmation Number
	 String Message = "Value has a comma"; //Discount
	
	 int PointLen; //Lenght of the Points
	
	//Get Values From Cookie
	 String ClubRespoints; //Beginning Point Balance
	 String RAPoints; // Reservation Cost
	 String EstimatedDiscountAmount; //Estimated Discount Amount
	 String DiscountFinalResCost; //Final Reservation Cost from Discount
	 String DiscountMessage; //Message 
	 String MOPBorrow;//Borrowed Points
	 String ClubRespointsNextYear; //Points available for reservation for next year
		
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	@Test(priority = 1)
	public void Reservation_Confirmation()
	{
		PropertyConfigurator.configure("Log4j.properties");	
		
		//******************************************************************************//
		//Get Values from Cookies
		RAPoints = driver.manage().getCookieNamed("RAPoints").getValue();
			logger.info("Reservation Points from Resort Availability: "+RAPoints);
		
		ClubRespoints =	driver.manage().getCookieNamed("ClubRespoints").getValue();
			logger.info("Reservation available from Club Reservations: "+ClubRespoints);
		//******************************************************************************//
		
		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					
		logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
								
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
			//Reservation Confimation Header / Compared
			ResConfirmHeader = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[1]/td")).getText();
				Assert.assertEquals(ResConfirmHeader, ResConfirmationHeader);
					logger.info("Reservation Confirmation Header Displayed and Passed Assertion: "+ResConfirmHeader);
			
			//Points Summary Header / Compared
			RCPointsSumHeader = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[3]/th")).getText();
				Assert.assertEquals(RCPointsSumHeader, RCPnSumHeader);
					logger.info("Points Summary Header Displayed and Passed Assertion: "+RCPointsSumHeader); 
					
	}
	
	@Test(priority = 2)
	public void Beginning_Points_Balance() {
		
		//Beginning Points Balance / Compared
		RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //Present Year points
			Assert.assertEquals(RCBegiPntsBal, ClubRespoints);
				logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Club Res Available Points For Reservations Present Year: "+RCBegiPntsBal);
	}
		
	@Test(priority = 3)
	public void Length() {
		
		String Points = "1,000";
		 PointLen = Points.length(); // Length of Points String
	}
	
	
	@Test(priority = 4)
	public void Reservation_Cost() {		
		//Reservation Cost
		RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
			int LengthofRCResCost = RCResCost.length(); // Length of RCResCost
					if(LengthofRCResCost == PointLen) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCResCt = RCResCost.replace(",","");
							Assert.assertEquals(RCResCt, RAPoints); //Comparing 
								logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt);		
					}
					
					else
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost);
					}
	}
	
	
	@Test(priority = 5)
	public void Discount() {
	//******************************************************************************//
		//Get Values from Cookies
		 EstimatedDiscountAmount = driver.manage().getCookieNamed("EstimatedDiscountAmount").getValue();
		 	logger.info("Estimated Discount Amount: "+EstimatedDiscountAmount);
	 	DiscountFinalResCost = driver.manage().getCookieNamed("DiscountFinalResCost").getValue();
		 	logger.info("Final Cost After Discount: "+DiscountFinalResCost);
		 DiscountMessage = driver.manage().getCookieNamed("DiscountMessage").getValue();
	 		logger.info(DiscountMessage);
	//******************************************************************************//
		 	
	 	RCDiscountedPoints = driver.findElement(By.xpath(".//*[@id='DiscPoints']")).getText();
	 		logger.info("Discounted Points :"+RCDiscountedPoints);
	 		
	 		int LengthofRCDiscountedPoints = RCDiscountedPoints.length(); // Length of RCResCost
			
	 		if(LengthofRCDiscountedPoints == PointLen) //If Size is the same then take out "," and follows the rest of the process
			{
				RCDisPoints = RCDiscountedPoints.replace(",","");
					Assert.assertEquals(RCDisPoints, EstimatedDiscountAmount); //Comparing 
						logger.info("Discounted Points in Reservation Confirmation Matched with Estimated Discounted Points: Assertion Passed: "+RCDiscountedPoints);
			}
			
			else
			{
				Assert.assertEquals(RCDiscountedPoints, EstimatedDiscountAmount); //Comparing 
					logger.info("Discounted Points in Reservation Confirmation Matched with Estimated Discounted Points: Assertion Passed: "+RCDiscountedPoints);
			}
			
	}
	
	@Test(priority = 6)
	public void Borrowed_Points() {		
		
		//******************************************************************************//
			//Get Values from Cookies
			MOPBorrow = driver.manage().getCookieNamed("MOPBorrow").getValue();
				logger.info("Points Borrowed from MOP Screen: "+MOPBorrow);
		//******************************************************************************//
		
		//Reservation Cost
		RCBorrowedPnts = driver.findElement(By.xpath(".//*[@id='BorrowPoints']")).getText();
			logger.info("Borrowed Points: "+RCBorrowedPnts);
			
			int LengthofRCBorrowedPnts = RCBorrowedPnts.length(); // Length of RCBorrowedPnts
					if(LengthofRCBorrowedPnts == PointLen) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCBorrowedPoints = RCBorrowedPnts.replace(",","");
							Assert.assertEquals(RCBorrowedPoints, MOPBorrow); //Comparing 
								logger.info("Borrowed Points Displayed and Matched with Borrow Points from MOP Payments: "+RCBorrowedPnts);
					}
					
					else
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
							logger.info("Borrowed Points Displayed and Matched with Borrow Points from MOP Payments: "+RCBorrowedPnts);
					}
					
	}
	
	
	@Test(priority = 7)
	public void Total_Points_Used_for_Reservation() {				
		
		//Total Points Used for Reservation / Compared 
			RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
				int LengthofRcTotPnts = RCTotPnts.length();
		
				if(LengthofRcTotPnts == PointLen)
				{
					if(Message.equals(DiscountMessage)) { //With Discount
						//Total Points Used for Reservation
						RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							RCTotalPoints = RCTotPnts.replace(",","");
								Assert.assertEquals(RCTotalPoints, DiscountFinalResCost); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Final Reservation Cost From Discount Page: "+RCTotalPoints);
					}
					else
					{
					//Total Points Used for Reservation
					RCTotalPoints = RCTotPnts.replace(",","");
						Assert.assertEquals(RCTotalPoints, RAPoints); //Comparing 
					}		logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCTotalPoints);
				}
				
				else 
				{	
					if(Message.equals(DiscountMessage))  { //With Discount
						RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							Assert.assertEquals(RCTotPnts, DiscountFinalResCost); //Comparing 
								logger.info("Total Points Used for Reservation Displayed and Matched with with Final Reservation Cost From Discount Page: "+RCTotPnts);
					}
					else
					{
					//Total Points Used for Reservation
					RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
						Assert.assertEquals(RCTotPnts, RAPoints); //Comparing 
							logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCTotPnts);
					}
				}
			
	}
	
	@Test(priority = 8)
	public void Ending_Points_Balance() {		
		
		if(Message.equals(DiscountMessage)) //With Discount
		{
			//Estimating Ending Points
			int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
				int ResPnts = Integer.parseInt(DiscountFinalResCost); //Conver String to Int
					int EstEndPnts = AvailPoints - ResPnts;
						logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
						
						//Ending Points Balance	/ Compared
						 RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
							String EstimatedPoints = String.valueOf(EstEndPnts);
								logger.info("Ending Points Balance: "+RCEndPntBal);
								
							Assert.assertEquals(RCEndPntBal, EstimatedPoints);
							logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
		}
		
		else
		{
			//Estimating Ending Points
				int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
					int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
						int EstEndPnts = AvailPoints - ResPnts;
							logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
							
				//Ending Points Balance	/ Compared
				 RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
					String EstimatedPoints = String.valueOf(EstEndPnts);
						logger.info("Ending Points Balance: "+RCEndPntBal);
						
					Assert.assertEquals(RCEndPntBal, EstimatedPoints);
						logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
		}	
	}
	
	@Test(priority = 9)
	public void NextYearEnding_Points_Balance() {
		
		//******************************************************************************//
		//Get Values from Cookies
		MOPBorrow = driver.manage().getCookieNamed("MOPBorrow").getValue();
			logger.info("Points Borrowed from MOP Screen: "+MOPBorrow);
			
		ClubRespointsNextYear = driver.manage().getCookieNamed("ClubRespointsNextYear").getValue();
			logger.info("Points Available for Reservation Next Year: "+ClubRespointsNextYear);
		//******************************************************************************//
		
		//Convert to INTEGER
		int ClubRespointsNextYearINT = Integer.parseInt(ClubRespointsNextYear);
			int MOPBorrowINT = Integer.parseInt(MOPBorrow);
		
		int EndingPointsBalance_NextYearINT = ClubRespointsNextYearINT - MOPBorrowINT;
			
		//Convert to String for Comparision
		String ESTEndingPointsBalance_NextYear = String.valueOf(EndingPointsBalance_NextYearINT);
		
		//Compare
		String EndingPointsBalance_NextYear = driver.findElement(By.xpath(".//*[@id='EndPointsBal2']")).getText();
			logger.info("Ending Points Balance of Next Year is: "+EndingPointsBalance_NextYear);
				Assert.assertEquals(EndingPointsBalance_NextYear, ESTEndingPointsBalance_NextYear);
					logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS OF NEXT YEAR");	
	}
					
	
	@Test(priority = 10)
	public void Confirmation_Message() {
		
	//Confirmation Message
	ConfirmationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
		logger.info(""+ConfirmationMes);			
		
		//Save to Cookie
		String ResConNum = ConfirmationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
			logger.info("Reservation Confirmation number is: "+ResConNum);
		
			Cookie FinalConfirmation = new Cookie("ResConNum",ResConNum);
				driver.manage().addCookie(FinalConfirmation);
						
 			}	
		
	}
	





