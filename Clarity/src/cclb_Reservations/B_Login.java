package cclb_Reservations;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class B_Login extends A_Common {
	
	Logger logger = Logger.getLogger("B_Login.class");
	
	@Test
	public void openBrowser() {  
		  PropertyConfigurator.configure("Log4j.properties");
		//	 driver.get("https://clarityactive.diamondresorts.com/pls/clarityactive"); // URL ACTIVE ACTIVE
			driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL
			// driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL
				driver.manage().window().maximize();
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								//driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Golive20");// Entering Password
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan23");// Entering Password
									logger.info("Password Entered");
										driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
												
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
	}				

}
