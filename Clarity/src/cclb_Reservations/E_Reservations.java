package cclb_Reservations;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.Test;

public class E_Reservations extends A_Common {

	Logger logger = Logger.getLogger("E_Reservations.class");
	String Lead_Area_Id;
	
	@Test	
	public void Reservations()  {
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[6]/div[1]/div")).click(); // Click on reservations tab
 			logger.info("clicked on Reservations Tab");
 			
			logger.info("---------------------------------------------Reservations Tab---------------------------------------------------------------------------------");
			
			
			//*********************************************************************//
			//Retrieve Values from cookies
			 Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
			 logger.info("Value Saved in Cookie - Membership Number: "+Lead_Area_Id);
			//*********************************************************************//
			 
						//Capture Leadid
							String ResLeadId = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
														logger.info("Lead Number in Reservations Tab: "+ResLeadId);
															logger.info("From Lead Information TAB "+Lead_Area_Id);
																Assert.assertEquals(ResLeadId, Lead_Area_Id);
																	logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");
																	
						String winHandleBefore = driver.getWindowHandle(); 
						 //*********************************************************************//
						 //Add WindowHandle to Cookie
						 Cookie cookie = new Cookie("winHandleBefore", winHandleBefore);
						 driver.manage().addCookie(cookie);
						//*********************************************************************//

						//Opens New Window
						driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
					 		logger.info("clicked on Club Res");
				 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

						// Switch to new window opened
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
		}
}
