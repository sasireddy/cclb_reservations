package cclb_Reservations;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class M_Refund extends A_Common { 
	
	Logger logger = Logger.getLogger("M_Refund.class");
	
	String winHandleBefore; //Screen No:1. Reservations Tab, Retrieve from Cookie
	String RAPoints; //Cost of Resort, Retrieve from Cookie
	String ResConNum; //Reservation Confirmation Number, Retrieve from Cookie
	String ClubRespoints; // Available Points, Retrieve from Cookie
	String TotalAmountDue; //Financial Summary Total Amount, Retrieve from Cookie
	String AccUpgradeFee; //Loyalty Upgrade Fee, Retrieve from Cookie
	String RoomUpgCost; //Roomupgrade cost, Retreive from Cookie
	
	 String CancelResHeader; //Cancel Reservation Header
	 String TotalResPoints; //Total Reservation Points
	 String NewPointsBalance; //New PointsBalance

	 String CanResMiscFeesHeader; //Cancel Reservation: Reservation and Misc Fees Header
	 String CostofARPP; //Cost of ARPP 
	 String LoyaltyAccUpgFee; //Loyalty upgrade Fee
	
	
	 @Test
	public void Refund() throws InterruptedException {

		//******************************************************************************//
			//Get Values from Cookies
		logger.info("---------------------------Values From Cookies-----------------------------------");
			winHandleBefore = driver.manage().getCookieNamed("winHandleBefore").getValue();
				logger.info("Window handle: "+winHandleBefore);
				
			RoomUpgCost = driver.manage().getCookieNamed("RoomUpgCost").getValue();
				logger.info("Roomupgrade Cost: "+RoomUpgCost);
				
			RAPoints = driver.manage().getCookieNamed("RAPoints").getValue();
				logger.info("Reservation Points from Resort Availability: "+RAPoints);
	
			ResConNum = driver.manage().getCookieNamed("ResConNum").getValue();
				logger.info("Reservation Points from Resort Availability: "+RAPoints);
			
			ClubRespoints =	driver.manage().getCookieNamed("ClubRespoints").getValue();
				logger.info("Reservation available from Club Reservations: "+ClubRespoints);
				
			TotalAmountDue = driver.manage().getCookieNamed("TotalAmountDue").getValue();
				logger.info("Financial Summary Total Amount: "+TotalAmountDue);
				
			AccUpgradeFee = driver.manage().getCookieNamed("AccUpgradeFee").getValue();
				logger.info("Room Upgrade Fee: "+AccUpgradeFee);
				
		//******************************************************************************//
			logger.info("------------------------------------------------------------------------------");
			
			driver.findElement(By.xpath(".//*[@id='ClbResvConf_close']")).click();
				logger.info("Clicked on Close in Reservation Confirmation Screen");
				
			
				driver.switchTo().window(winHandleBefore);
				
				logger.info("-----------------------------------------Cancel Reservation With Refund-----------------------------------------------------");
						
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
			 		logger.info("clicked on Reservations Tab");
			 		
			 		Thread.sleep(3000);
			 						 					
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//					
					WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody")); //Table Tbody
							List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
									int rowsnum = columns.size();
											String xpath =null;
												String ResNum;
														for(int i = 1;i<=rowsnum;i++)
														{
															xpath= ".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
																ResNum = driver.findElement(By.xpath(xpath)).getText();
																		logger.info("Substring is: "+ResNum);
																	
																driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
																
																if(ResConNum.equals(ResNum))
																{
																	logger.info("Confirmation Number Found and Matcehd from the Reservation Confirmation Screen");
																	
																		driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
																			logger.info("Clicked on the Flag image of the Confirmation Number");
																			break;
																}
														}
					
					Thread.sleep(3000);
					if(driver.findElements(By.xpath(".//*[@id='genericWidgetInnerDiv']/div/div[3]")).size()!=0) 
					{
						logger.info("Clicked on Notice Notice");
							driver.findElement(By.xpath(".//*[@id='genericWidgetInnerDiv']/div/div[3]")).click();	
					}
					
					else
					{
						logger.info("Window not found");
					}
					
															
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RMS_ButtonBar']/table/tbody/tr/td[2]/div"))); // Wait till the Cancel Res button is Clickable
					
					driver.findElement(By.xpath(".//*[@id='RMS_ButtonBar']/table/tbody/tr/td[2]/div")).click(); //Click on Cancel Res button
						logger.info("Clicked on Cancel Reservation");
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RCWbtnSave']"))); // Wait till the Continue Button in Cancel Reservation window is Clickable
					
					//Cancel Reservation Header / Compared
					CancelResHeader = driver.findElement(By.xpath(".//*[@id='RCWHeader']")).getText();
						Assert.assertEquals(CancelResHeader, CancelReservationHeader);
							logger.info("Cancel Reservation Header Displayed and Passed the Assertion");
							
					//Choose a cancellation Reason
					driver.findElement(By.xpath(".//*[@id='RCF_pvCancelCode_input']")).click();
					
					Select AnnualRPP = new Select(driver.findElement(By.id("RCF_pvCancelCode_select")));
						AnnualRPP.selectByValue("PPR"); //Select PPR
							logger.info("Chose Cancellation Reason");
					
					//Cancellation Comments
					driver.findElement(By.xpath(".//*[@id='RCF_CNX_REMARK']")).click();
						driver.findElement(By.xpath(".//*[@id='RCF_CNX_REMARK']")).sendKeys("RPP Purchase");
							logger.info("Cancellation Reason - RPP Purchase");
					
					//-------Points-------------//
					//Total Res Points / Compared
					TotalResPoints = driver.findElement(By.xpath(".//*[@id='ClubResvPointsTable']/table/tbody/tr[2]/td[2]")).getText();
						Assert.assertEquals(TotalResPoints, RAPoints);
							logger.info("Total Res Points Matched with Cost of Reservation Points from Resort Availability Screen");
							
					driver.findElement(By.xpath(".//*[@id='RCWbtnSave']")).click(); // Click on Continue button 
						logger.info("Clicked on Continue");
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClubFeeRefundSaveBtn']"))); // Wait till the Process Button in Cancel Reservation: Reservtion & Misc Fees window is Clickable
					
					//---------------------------------------------------------------//Cancel Reservation: Reservation and Misc Fees-------------------------------------------------//
					
					//Cancel Reservation: Reservation and Misc Fees Header / Compared
					CanResMiscFeesHeader = driver.findElement(By.xpath(".//*[@id='ClubFeeRefundFormTitle']")).getText();
						Assert.assertEquals(CanResMiscFeesHeader, CRResMisFeesHeader);
							logger.info("Cancel Reservation: Reservation and Misc Fees Header Displayed and Passed Assertion");
							
					//Loyalty Accommodation Upgrade Fee //Compared
					LoyaltyAccUpgFee = driver.findElement(By.xpath(".//*[@id='ClubFeeRefundTable']/tr[2]/td[3]")).getText();
						Assert.assertEquals(LoyaltyAccUpgFee, RoomUpgCost);
							logger.info("Cost of Loyality Accomodation Upgrade Fee Matched the Roomupgrade Fee:  "+LoyaltyAccUpgFee);		
							
					//Annual Reservation Protection Plan // Compared
					double AnnualResPP = Double.parseDouble(TotalAmountDue); //Convert String to Double 
						int ARPP = (int) AnnualResPP; //Convert double to integer to elimate decimals
							int RoomUpgradeCost = Integer.parseInt(RoomUpgCost); //convert to int	
								 int ARPPlan = ARPP - RoomUpgradeCost; //Cost of ARPP
								  	logger.info("Cost of ARPP: "+ARPPlan);
								  		String AnnualReservationPP = String.valueOf(ARPPlan); //convert int to string for comparision
								  		
					CostofARPP = driver.findElement(By.xpath(".//*[@id='ClubFeeRefundTable']/tr[3]/td[3]")).getText();
						Assert.assertEquals(CostofARPP, AnnualReservationPP);
							logger.info("Cost of Annual Reservation Protection Matched the ARPP with the Cost of ARPP in Travel Protection: "+CostofARPP);
							
					driver.findElement(By.xpath(".//*[@id='ClubFeeRefundTable']/tr[2]/td[1]/input")).click(); //Click on the check box of Loyalty Accommodation Upgrade Fee
						logger.info("Clicked on Loyalty Accommodation Upgrade fee");
					
					driver.findElement(By.xpath(".//*[@id='ClubFeeRefundSaveBtn']")).click();
						logger.info("Clicked on Save Button");
							
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok button is Clickable
					
					String PaymentMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
						logger.info(""+PaymentMessage); //Refund Message
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Click on Ok button
						
					String CancelResConfirmation = driver.findElement(By.xpath(".//*[@id='GRMBContent']")).getText();
						logger.info(""+CancelResConfirmation); //Cancel Reservation Confirmation
						
					driver.findElement(By.xpath(".//*[@id='GRMBCloseBtn']")).click(); //click on Close button
						logger.info("Clicked on Close Button");
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]/table/tbody/tr/td[2]/img"))); // Wait till the Reservation Status Flag is clickable
					
					String CXL_Cancelled = driver.findElement(By.xpath(".//*[@id='ResSum_ResvStatusDesc']")).getText();
						logger.info("Reservation Status: "+CXL_Cancelled);
		
					
					}				
			
	}

