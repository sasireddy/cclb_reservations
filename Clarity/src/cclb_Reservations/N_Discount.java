package cclb_Reservations;


import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.testng.annotations.Test;

public class N_Discount extends A_Common {
	
	Logger logger = Logger.getLogger("N_Discount.class");
	
	String RAPoints; //Cost of Resort Points
	int RAPointsInt; //Convert RAPoints to int for comparision purpose
	String DiscountFinalResCost; //Final Reservation Cost
	String DiscountMessage = "Value has a comma"; 
	String DiscountHder; //Disocunt Header
	String DiscountMessageSub; //SubString of DiscontMessage
	
	@Test
	public void Discount_Yes() {
		
		 PropertyConfigurator.configure("Log4j.properties");	
			
		  logger.info("-------------------------------------------------------------------Discount------------------------------------------------------------");
		  //******************************************************************************//
			//Get Values from Cookies
			RAPoints = driver.manage().getCookieNamed("RAPoints").getValue();
			logger.info("Reservation Points from Resort Availability: "+RAPoints);
			
		 //******************************************************************************//
			
				RAPointsInt = Integer.parseInt(RAPoints);//Convert to integer for comparision
					logger.info("Convereted to integer for comparision");
											
				if(driver.findElements(By.xpath(".//*[@id='cmnConfirmCntDiv']")).size() !=0) 
				{
					driver.findElement(By.xpath(".//*[@id='confirmDscYesBtn']")).click();
						logger.info("Clicked on YES for discount");
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
				 		//Discount Percentage(%)
				 			driver.findElement(By.xpath(".//*[@id='discPct']")).click(); //Click on Discount Percentage
				 				driver.findElement(By.xpath(".//*[@id='discPct']")).sendKeys(DiscountPercentage); //Enter the Percentage Amount
				 					driver.findElement(By.xpath(".//*[@id='ttlCost']")).click();
				 				
				 					double Percentage = 0.50; 	//Changing this value might cause error.  Please change the DiscountPercentage Value in Common when you change this value.				
				 						double EstimatedDiscntAmount = Math.round(RAPointsInt * Percentage); //Estimated Discount Amount
			 								logger.info("Discount Points Amount: "+EstimatedDiscntAmount);
				 								
	 								DecimalFormat decimal = new DecimalFormat("###.#"); //Decimal Format
				 								
									String EstimatedDiscountAmount = decimal.format(EstimatedDiscntAmount); // Convert Long to String
										logger.info("Estimated Discount Amount: "+EstimatedDiscountAmount);
				 					
		 							String FinalResCost = driver.findElement(By.xpath(".//*[@id='ttlCost']")).getAttribute("value"); //Get the Final Reservation Cost 
		 								logger.info("Final Reservation Cost: "+FinalResCost);
		 									int LenofFinalResCost = FinalResCost.length(); // Length of FinalResCost
		 									String Points = "1,000";
		 										int PointLen = Points.length(); // Length of Points String
		 								
		 								if(LenofFinalResCost == PointLen) //If Size is the same then take out "," and follows the rest of the process
		 								{
		 									
			 										String FinalReservationCost = FinalResCost.replace(",","");
			 											logger.info("Final Reservation Cost: "+FinalReservationCost);	
			 											DiscountFinalResCost = FinalReservationCost;
			 											
			 											//******************************************************//
			 											//Add the Leadid into Cookie
			 											Cookie Discount1 = new Cookie("DiscountMessage",DiscountMessage);
			 											Cookie Discount2 = new Cookie("DiscountFinalResCost",DiscountFinalResCost);
			 											Cookie Discount3 = new Cookie("EstimatedDiscountAmount",EstimatedDiscountAmount);
			 											
		 												driver.manage().addCookie(Discount1);
		 												driver.manage().addCookie(Discount2);
		 												driver.manage().addCookie(Discount3);
			 												
			 											//******************************************************//
		 								}
		 								
		 								else
		 								{
		 									DiscountFinalResCost = FinalResCost;
		 									//******************************************************//
 											//Add the Leadid into Cookie
		 									Cookie Discount4 = new Cookie("DiscountMessage",DiscountMessage);
 											Cookie Discount5 = new Cookie("EstimatedDiscountAmount",EstimatedDiscountAmount);
 											Cookie Discount6 = new Cookie("DiscountFinalResCost",DiscountFinalResCost);
 										
											driver.manage().addCookie(Discount4);
											driver.manage().addCookie(Discount5);
											driver.manage().addCookie(Discount6);
											
 											//******************************************************//
		 								}
		 								
		 								driver.findElement(By.xpath(".//*[@id='confirmDscPrcYesBtn']")).click(); //Click on Process Button
		 									
				}						
				
				else
				{
					logger.info("Discount Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
	  	}
	
	//For Discount = NO
	  @Test
	  public void Discount_No() { 
			 
		  logger.info("-------------------------------------------------------------------Discount------------------------------------------------------------");
											
			if(driver.findElements(By.xpath(".//*[@id='cmnConfirmCntDiv']")).size() !=0) 
			{

				driver.findElement(By.xpath(".//*[@id='confirmDscNoBtn']")).click();
					logger.info("Clicked on No for discount");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
			}
			 			
			 			
			else
			{
				logger.info("Discount Window not found");
					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			}
			
			
		}
		
		
	}


