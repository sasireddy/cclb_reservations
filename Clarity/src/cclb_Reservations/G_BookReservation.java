package cclb_Reservations;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class G_BookReservation extends A_Common {
	
Logger logger = Logger.getLogger("G_BookReservation.class");
	
	//Available Points
	 String ClubRespoints; //To retrieve from RoomUpgrade_Reservation.Properties File
	 int ClubReservationPoints; //To Convert ClubResPoints to Integer
	
	//Resort Availability - CLUB
	 String RAResortName; //Resort Name
	 String RAErrataHeader; //Errata Header
	 String RARmType; //Room Type
	 String RARoomType; //Substring of Room Type
	 String RAFlagHeader; //Flag
	 String RAArrivalDate; //Arrival Date
	 String RADepartureDate; //Departure Date
	 String RAPoints; //Poins To Book The Reservation
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	
	@Test
	@Parameters({"SARegion","SAProperty"})
	public void BookReservation(String SARegion, String SAProperty)
	{
		
		PropertyConfigurator.configure("Log4j.properties");
		
			//-----------------------------------------------------------------------Book Reservation------------------------------------------------------------------------------------------//
			logger.info("-------------------------------------------------------------------Book Reservation------------------------------------------------------------");
			
			//Book Reservation																						
			driver.findElement(By.xpath(".//*[@id='cr_buttonBOK']")).click();  //Clicked on Book Reservation
				logger.info("Clicked on Book Reservation");
								
			Select BookingType = new Select(driver.findElement(By.xpath(".//*[@id='rtmBookType_select']"))); 
				BookingType.selectByValue(BookingTypeReservation);// Select Booking Type	
					logger.info("Clicked and selected the booking type");
	
			driver.findElement(By.xpath(".//*[@id='rtm_continueBtn']")).click();  //Click Continue
				logger.info("Clicked Continue");
				
		//-----------------------------------------------------------------------Search For Availability------------------------------------------------------------------------------------------//
			logger.info("-------------------------------------------------------------------Search For Availability------------------------------------------------------------");
					
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ren_buttonSearch']"))); // Wait till the element is present to click
					
			//Select Region
				driver.findElement(By.xpath(".//*[@id='caw_region_input']")).click(); //Click on Region
					Select Region = new Select(driver.findElement(By.id("caw_region_select"))); 
						Region.selectByValue(SARegion); //Select by Value 
							logger.info("Selected Region: "+SARegion);
								
		
			//Select Property
				driver.findElement(By.xpath(".//*[@id='caw_property_input']")).click(); //Click on Property
					Select Property = new Select(driver.findElement(By.id("caw_property_select")));
						Property.selectByValue(SAProperty); //Select by Value
							logger.info("Selected Property: "+SAProperty);
								
								
			//Select Arrival Method
				driver.findElement(By.xpath(".//*[@id='caw_arr_meth_input']")).click(); //Click on Arrival Date Method	
					Select Arrival = new Select(driver.findElement(By.id("caw_arr_meth_select")));
						Arrival.selectByValue(SAArrivalDateMethod); //Select by Value
							logger.info("Selected Arrival Method as Flexible days: "+SAArrivalDateMethod);
								
		
			//Arrival Date
				driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).click(); 
					driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).sendKeys(SAArrivalDate);
		
			//No.of Nights: (Borrow Points From Next Year To Book Reservation: Increasing No.of nights incresae the cost of reservation points) 
			//Borrow Points From Next Year To Book Reservation uses different Resort Type.  When the SAProperty parameter differs the else loop works.
				if(SAProperty.equals("BRI"))
				{
					driver.findElement(By.xpath(".//*[@id='caw_nights']")).sendKeys(SANights);
						logger.info("Selected No.of Nights as: "+SANights);
				}
				else
				{
					driver.findElement(By.xpath(".//*[@id='caw_nights']")).sendKeys(BorrowSANights);
					logger.info("Selected No.of Nights as: "+BorrowSANights);
				}
						
					
			//No.of Adults
				driver.findElement(By.xpath(".//*[@id='caw_num_adults']")).sendKeys(SAAdults);
					logger.info("Entered Adults: "+SAAdults);
					
					/*try {
						Thread.sleep(8000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	*/
			
			//Click on Search		
			driver.findElement(By.xpath(".//*[@id='ren_buttonSearch']")).click(); //Click on Search button
				logger.info("Clicked on Search");
				
		}
	//-------------------------------------------------------------------------Resort Availability-----------------------------------------------------------------------------//

		@Test
		@Parameters("ResortType")
		public void Resort_Availability(String ResortType) {
		
			logger.info("-------------------------------------------------------------------Resort Availabilty------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ResAvailResults']/tbody/tr[3]/td[5]/img"))); // Wait till the element is present to click
			//Resort Availability - CLUB
					
				//To get the Room Upgrade Screen
				WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody"));
				List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
					//logger.info("No.of rows: " +columns.size());
						int rowsnum = columns.size();
								String xpath =null;
										String TypeofResort;
										
									Search:
										
									for(int i = 3;i<=rowsnum;i++)
									{
										xpath= ".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[4]";
											TypeofResort = driver.findElement(By.xpath(xpath)).getText().substring(0, 3);
												logger.info("Substring is: "+TypeofResort);
												
											driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											
											if(ResortType.equals(TypeofResort))
											{
													driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[1]/input")).click();
													logger.info("Clicked on " +ResortType+ " Available Resort");
													
													//Errata
													if(driver.findElements(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[2]/img")).size() !=0) 
													{
														driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[2]/img")).click();
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the element is present to click
																RAErrataHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
																	Assert.assertEquals(RAErrataHeader, ErrataHeader);
																		logger.info("Additional Resort Information (Errata) header displayed and Passed Assertion: "+RAErrataHeader);
																			driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();	
																				logger.info("Clicked on Close Button (Additional Resort Information Flag)");
													}					
																			 
													//ResortName
													RAResortName = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[3]")).getText();
														logger.info("Clicked on the Resort" +RAResortName);
														
													//RoomType/Max Occ
													RARmType = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[4]")).getText();
														logger.info("RoomType: "+RARmType);
													RARoomType = RARmType.substring(0, 3);
														
													//Flag
													driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[5]/img")).click();
														wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the element is present to click
															RAFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
																logger.info("Flag Header is: "+RAFlagHeader);
																	driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
																		logger.info("Clicked on Close Button (Room Amenitiesx Flag)");
															
													//Arrival Date
													RAArrivalDate = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[8]")).getText();;
														logger.info("Arrival Date: "+RAArrivalDate);
														
														
													//Departure Date
													RADepartureDate = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[9]")).getText();//Departure Date;
														logger.info("Departure Date: "+RADepartureDate);
													
													//Points
													RAPoints = driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody/tr["+ String.valueOf(i) +"]/td[10]")).getText(); //Number of points;
														logger.info("Points: "+RAPoints);
														

													//******************************************************************//
					 								//Add the values to Cookies
													Cookie BookRes1 = new Cookie("RAPoints", RAPoints);
													Cookie BookRes2 = new Cookie("RAResortName", RAResortName);
													Cookie BookRes3 = new Cookie("RARoomType", RARoomType);
													Cookie BookRes4 = new Cookie("RAArrivalDate", RAArrivalDate);
													Cookie BookRes5 = new Cookie("RADepartureDate", RADepartureDate);
													
													driver.manage().addCookie(BookRes1);
													driver.manage().addCookie(BookRes2);
													driver.manage().addCookie(BookRes3);
													driver.manage().addCookie(BookRes4);
													driver.manage().addCookie(BookRes5);
													//******************************************************************//

								
													//Click on Continue
													driver.findElement(By.xpath(".//*[@id='caw_ASWBtnProcess']")).click();								
														logger.info("Clicked on Continue");
															driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
															
															try {
																Thread.sleep(4000);
															} catch (InterruptedException e) {
																// TODO Auto-generated catch block
																e.printStackTrace();
															}
															break Search;								
											}
									}				
		}
}
