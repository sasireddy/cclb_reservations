package cclb_Reservations;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class K_FinancialSummary extends A_Common {
	
	Logger logger = Logger.getLogger("K_FinancialSummary.class");
	
	//-------------------------------Financial Summary--------------------------//
		public String CPResAdMsc; //Reservation and Misc Fees
		public String CRResMisFeesHeader; //Reservation and Misc Fees Flag Header
			public String CRPaymentInforHeader; //Payment Information Header
			public String AccUpgradeFee; //Account Upgrade Fee
			public String TotalAmountDue; //Total Reservation and Misc Fees Total in Flag
			public String CCMainHeader; //Credit Card Maintenance Header	
			public String Leadid; //Leadid in Credit Card Maintenance Section
			
	//-------------------------------Financial Summary--------------------------//
		public String PaymentMessage;  //Payment Message 
		
	//Retrieve Values From Cookies for room upgrade
	public String RURoomType; //From RoomUpgrade Class
	public String RoomUpgCost;
	
	//---------------------
	public String Lead_Area_Id;

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	
	@Test
	public void Reservation_RoomUpgrade() {
	 PropertyConfigurator.configure("Log4j.properties");
	 
	 //*******************************************************************************************//
		 RURoomType =  driver.manage().getCookieNamed("RURoomType").getValue();
		 logger.info("Value Saved in Cookie - RoomType: "+RURoomType);
		 
		 RoomUpgCost = driver.manage().getCookieNamed("RoomUpgCost").getValue();
		 logger.info("Value Saved in Cookie - Room Upgrade Cost: "+RoomUpgCost);
		 
	//*******************************************************************************************//
	}
	
	@Test
	public void Travel_Protection_No() {
		logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");			
			
		//Decline Rpp
		driver.findElement(By.xpath(".//*[@id='declineRPP']")).click();
			logger.info("Declined RPP");
	}
	
	@Test
	public void Travel_Protection_Yes() {
		
		//-----------------------------------------------------------------------Travel Protection------------------------------------------------------------------------------------------//
		
		logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");			
			
		//Purchase Rpp
		driver.findElement(By.xpath(".//*[@id='purchaseARP']")).click();
			logger.info("Purchased Annual RPP");
	}
	

	@Test
	public void Financial_Summary_Pound() { 
		
		//Reservation And Misc Fees / Compared
		CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText().replace("�", "").trim();
			logger.info("Reservation And Misc Fees: "+CPResAdMsc);
		
	}
	
	@Test
	public void Financial_Summary_Dollar() {
		
		//Reservation And Misc Fees / Compared
		CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText().replace("$", "").trim();
			logger.info("Reservation And Misc Fees: "+CPResAdMsc);
		
	}
	
	@Test
	public void Financial_Summary_Euro() {
		
		//Reservation And Misc Fees / Compared
		CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText().replace("�", "").trim();
			logger.info("Reservation And Misc Fees: "+CPResAdMsc);
		
	}


	@Test
	public void Financial_Summary() { 
		
		//*********************************************************************//
		//Retrieve Values from cookies
		 Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
		 logger.info("Value Saved in Cookie - Membership Number: "+Lead_Area_Id);
		//*********************************************************************//
		 
	//----------------------------------------------------------------------Financial Summary------------------------------------------------------------------------------------------//				
			logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
					//Reservation And Misc Fees Flag Header / Compared
					driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
						logger.info("Clicked on Reservation And Misc Fees Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
								
								//Reservation and Misc Fees Header / Compared
								CRResMisFeesHeader = driver.findElement(By.xpath(".//*[@id='RFIFTitle']")).getText();
									Assert.assertEquals(CRResMisFeesHeader, CRReserMiscFeesHeader);
										logger.info("Reservation And Misc Fees Flag Header Displayed and Passed Assertion: "+CRResMisFeesHeader);
												
									//Total Amount Due	/ Compared		
									TotalAmountDue = driver.findElement(By.xpath(".//*[@id='FRR_TOTAL_AMOUNT']")).getText();
										logger.info("Total Amount due is: "+TotalAmountDue);
											Assert.assertEquals(TotalAmountDue, CPResAdMsc);
												logger.info("Total Amount due matched the Reservation and Misc Fees");
													String PaymentAmount = "0.00";
												
												/* If Payment Amount is 0.00 close the Reservation and Misc Fees, Else Call the method Add_CreditCard */
												if(TotalAmountDue.equals(PaymentAmount))
												{
													driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
														logger.info("Clicked on Close Button");
												}
												else
												{
													//*********************************************************************//
													//Retrieve Values from cookies
													 Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
													 logger.info("Value Saved in Cookie - Membership Number: "+Lead_Area_Id);
													//*********************************************************************//
													Add_Creditcard();	
												}
	}
		
	@Test
	public void Financial_Summary_Fee() {  //Only for Room Upgrade
		//*********************************************************************//
			//Retrieve Values from cookies
			 Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
			 logger.info("Value Saved in Cookie - Membership Number: "+Lead_Area_Id);
			//*********************************************************************//
			 
	//----------------------------------------------------------------------Financial Summary------------------------------------------------------------------------------------------//				
			logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
					//Reservation And Misc Fees Flag Header / Compared
					driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
						logger.info("Clicked on Reservation And Misc Fees Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
								
								//Reservation and Misc Fees Header / Compared
								CRResMisFeesHeader = driver.findElement(By.xpath(".//*[@id='RFIFTitle']")).getText();
									Assert.assertEquals(CRResMisFeesHeader, CRReserMiscFeesHeader);
										logger.info("Reservation And Misc Fees Flag Header Displayed and Passed Assertion: "+CRResMisFeesHeader);
										
								//Loyalty Accomodation Upgrade Fee Compare / Compareed
								logger.info("-------------------------------Loyalty Accomodation Upgrade Fee------------------------------------------------------------");
									AccUpgradeFee = driver.findElement(By.xpath(".//*[@id='FRRTRN2AMOUNT']")).getText().substring(0, 2);
											logger.info("Loyality Accomodation Upgrade Fee: "+AccUpgradeFee);
												Assert.assertEquals(AccUpgradeFee, RoomUpgCost);
													logger.info("Loyalty Accomodation Upgrade Fee Matched with RoomUpgrade Cost: ASSERTION PASSED");
									
														
									//Total Amount Due	/ Compared		
									TotalAmountDue = driver.findElement(By.xpath(".//*[@id='FRR_TOTAL_AMOUNT']")).getText();
										logger.info("Total Amount due is: "+TotalAmountDue);
											Assert.assertEquals(TotalAmountDue, CPResAdMsc);
												logger.info("Total Amount due matched the Reservation and Misc Fees");
												
												//*********************************************************************//
												Cookie AccountUpgrade = new Cookie("AccUpgradeFee",AccUpgradeFee);
												Cookie TotalAmount = new Cookie("TotalAmountDue",TotalAmountDue);
												
												driver.manage().addCookie(AccountUpgrade);
												driver.manage().addCookie(TotalAmount);
												//*********************************************************************//
	}
	
	@Test
	public void Add_Creditcard() {
		
				//Add Credit Card
				driver.findElement(By.xpath(".//*[@id='pvpaycc_input']")).click();
					Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='pvpaycc_select']"))); 
						Payment.selectByValue("0");// Select Booking Type
						
						//Credit Card Maintenance Header and Section / Compared
						CCMainHeader = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[1]/tbody/tr/td/b")).getText();
							logger.info("Header is: "+CCMainHeader);
								Assert.assertEquals(CCMainHeader, CreditCardMainHeader);
									logger.info("Credit Card Maintenance Header Matched");
									
								//Lead ID / Compared
								Leadid = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[2]/tbody/tr/td[4]/span/font")).getText();
									logger.info("Leadid in Credit Card Maintenance :"+Leadid);
										Assert.assertEquals(Leadid, Lead_Area_Id); 
											logger.info("Leadid from Credit Card Maintenance section matched");
												driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											
								//-------Add Credit Card---------//
											
								//Credit Card Number
								driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
									driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
										driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
									
								/*//Credit Card Type
								String CCType1 = driver.findElement(By.xpath(".//*[@id='pvCCType_input']")).getAttribute("value");//Get Credit Card Type Value 
									logger.info(""+CCType1);
									*/
								//Card Holder Name
									driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
								driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
								
								//Exp mnth & Year
								driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
									Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
										ExpMth.selectByValue(ExpMonth);// Exp Mnth
								
								driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
									Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
										ExpYear.selectByValue(ExpYr);// Exp Year
									
								//Active
								driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
									Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
										Active.selectByValue("Y");// Active
											driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								
								//DRI Credit Card Y/N
								driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
									Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
										Dricc.selectByValue("N");// DRI Credit Card	
								
								//Click on Save Button
								driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
									logger.info("Clicked on Save Button");
									
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok Button is present 
									
									String Success = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
										logger.info(""+Success); //Success MEssage
										
									driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Click on OK
					
									driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
										logger.info("Clicked on Close button");
	}
	
	@Test
	public void Save_Reservation()
	{
		//Click on SAVE Button
		driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
				logger.info("Clicked on SAVE BUTTON");
				
	}
	
	@Test
	public void Save_Reservation_PaidFee()
	{
		//Click on SAVE Button
		driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
				logger.info("Clicked on SAVE BUTTON");
				
				//Payment Mes
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Close Button is present 
				
				PaymentMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
					logger.info(""+PaymentMessage);
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Close						
	}
	
	
	
}
