package cclb_Reservations;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class I_MOPPayments extends A_Common {
	
Logger logger = Logger.getLogger("I_MOPPayments.class");

	//MOP Options
	 String MOPTotResPointCost; // Total Reservation Point Cost
	 String MOPointsAvail; //Total Member Points Available
	 String MOPBalPointsNeeded; //Balance of Points Needed
	 String MOPTotalPointCost;// Total Points To Apply / Total Cost
	 String EstimationPointsNeeded; //Used to convert "EstiBalanceOfPointsNeeded" to String
	 String MOPUseMemPoints; //Use Membership Points
	 String MOPBorrow; //Borrow from Next Year
	 String CostofPointsBorrowed; //Total Cost for Borrowing Points
	 String PointsBorrowed;// Points Borrowed Message
	 String BorrowedMessage;// Points Borrowed defined Message
	
	//Retrieve from Cookies
	 String RAPoints; //Reservation Cost
	 String ClubRespoints; //Beginning Point Balance
	 String DiscountFinalResCost; //Final Cost after Discount
	 String ClubRespointsNextYear; //Points available for reservation for next year

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
		@Test
		public void MethodofPaymentOptions() {

				PropertyConfigurator.configure("Log4j.properties");	
				
				//********************************************************************************//
				//Get Values from Cookies
				RAPoints = driver.manage().getCookieNamed("RAPoints").getValue();
					logger.info("Reservation Points from Resort Availability: "+RAPoints);
				
				ClubRespoints =	driver.manage().getCookieNamed("ClubRespoints").getValue();
					logger.info("Reservation available from Club Reservations: "+ClubRespoints);
				//********************************************************************************//
				
				logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
				
				if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
				{
						 
			 			//Total Reservation Point Cost / Compared
			 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
					 		Assert.assertEquals(MOPTotResPointCost, RAPoints);
					 			logger.info("Total Reservation Point Cost Matched with Total Point Cost To Book The Reservation From Resort Availability Page : "+MOPTotResPointCost);
			 			
			 			//Total Member Points Available / Compared
					 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
					 		Assert.assertEquals(ClubRespoints, MOPointsAvail);
					 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
					 			
			 			//Balance of Points Needed / Compared
			 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
					 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
					 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
				
						//Total Points To Apply / Total Cost / Compared
			 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
					 		Assert.assertEquals(RAPoints, MOPTotalPointCost);
					 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
					 			
					 	//Process		
						driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
							logger.info("Clicked on Process button");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
				}
	
				
				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
				
			}
		
		@Test
		public void MethodofPaymentOptions_Discount() {

				PropertyConfigurator.configure("Log4j.properties");	
				
				//********************************************************************************//
				//Get Values from Cookies
				DiscountFinalResCost = driver.manage().getCookieNamed("DiscountFinalResCost").getValue();
					logger.info("Final Reservation Cost After Discount: "+DiscountFinalResCost);
				
				ClubRespoints =	driver.manage().getCookieNamed("ClubRespoints").getValue();
					logger.info("Reservation available from Club Reservations: "+ClubRespoints);
				//********************************************************************************//
				
				logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
				
				if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
				{
						 
			 			//Total Reservation Point Cost / Compared
			 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
					 		Assert.assertEquals(MOPTotResPointCost, DiscountFinalResCost);
					 			logger.info("Total Reservation Point Cost Matched Discount(Final Reservation Cost): "+MOPTotResPointCost);
			 			
			 			//Total Member Points Available / Compared
					 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
					 		Assert.assertEquals(ClubRespoints, MOPointsAvail);
					 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
					 			
			 			//Balance of Points Needed / Compared
			 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
					 		Assert.assertEquals(MOPBalPointsNeeded, MOPBalPointsNd);
					 			logger.info("Balance of Points Needed Matched and Assertion Passed: "+MOPBalPointsNeeded);
				
						//Total Points To Apply / Total Cost / Compared
			 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
					 		Assert.assertEquals(DiscountFinalResCost, MOPTotalPointCost);
					 			logger.info("Total Points To Apply / Total Cost Matched with Discount(Final Reservation Cost): "+MOPTotalPointCost);
					 			
					 	//Process		
						driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
							logger.info("Clicked on Process button");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
				}
	
				
				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
				
			}

		@Test
		public void MethodofPaymentOptions_Borrow() {
			
			/*---------------------------------------------------------------
			 * 
			 * Turn on Prepay option from Atlas and Change The Percentage.
			----------------------------------------------------------------- */

				PropertyConfigurator.configure("Log4j.properties");	
				
				//********************************************************************************//
				//Get Values from Cookies
				RAPoints = driver.manage().getCookieNamed("RAPoints").getValue();
					logger.info("Reservation Points from Resort Availability: "+RAPoints);
				
				ClubRespoints =	driver.manage().getCookieNamed("ClubRespoints").getValue();
					logger.info("Reservation available from Club Reservations: "+ClubRespoints);
				//********************************************************************************//
				
				logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
				
				if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
				{
						 
			 			//Total Reservation Point Cost / Compared
			 			MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
					 		Assert.assertEquals(MOPTotResPointCost, RAPoints);
					 			logger.info("Total Reservation Point Cost Matched with Total Point Cost To Book The Reservation From Resort Availability Page : "+MOPTotResPointCost);
					 				int RAPointsINT = Integer.parseInt(RAPoints); //Convert Reservation point cost to integer
					 				
			 			//Total Member Points Available / Compared
					 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
					 		Assert.assertEquals(ClubRespoints, MOPointsAvail);
					 			logger.info("Total Member Points Available Matched with Available for Reservations From Club Reservations Page: "+MOPointsAvail);
				 					int ClubRespointsINT = Integer.parseInt(ClubRespoints); //Convert Available points for Reservations to integer 
				 					
				 		//Estimating Balance of points needed
				 			int EstiBalanceOfPointsNeededINT = Math.abs(RAPointsINT - ClubRespointsINT); 
				 				logger.info("Estimating the Balance of points needed should be: "+EstiBalanceOfPointsNeededINT);
				 		
				 		//Convert "EstiBalanceOfPointsNeeded" to String for Comparision
			 				EstimationPointsNeeded = String.valueOf(EstiBalanceOfPointsNeededINT);
					 			
			 			//Balance of Points Needed / Compared
			 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
					 		Assert.assertEquals(MOPBalPointsNeeded, EstimationPointsNeeded);
					 			logger.info("Balance of Points Needed Matched the Estimation Point Balance Neede and Assertion Passed: "+MOPBalPointsNeeded);
					 			
						 			//Use Membership Points / Compared
							 		MOPUseMemPoints = driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).getAttribute("value");
							 			logger.info("Use Membership Points in MOP: "+MOPUseMemPoints);
							 				logger.info("Available points: "+ClubRespoints);
							 					Assert.assertEquals(MOPUseMemPoints, ClubRespoints);
							 						logger.info("Use Membership Points Matched the Available points from USAGE INFORMATION: ASSERTION PASSED");
						
								 	//Borrow From Next Year / Compared
			 						MOPBorrow = driver.findElement(By.xpath(".//*[@id='nBorPoints']")).getAttribute("value");	
				 						int MOPBorrowINT = Integer.parseInt(MOPBorrow);
				 							logger.info("Borrow Points from Next Year in Method of Payments Section:" +MOPBorrow);
					 							logger.info("Estimated Points to Borrow: "+EstiBalanceOfPointsNeededINT);
										 							
					 								Assert.assertEquals(MOPBorrowINT, EstiBalanceOfPointsNeededINT);
					 									logger.info("Borrow from Next Year Matched the Estimation Points to Borrow: ASSERTION PASSED");
					 									
					 									
				
						//Total Points To Apply / Total Cost / Compared
			 			MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
					 		Assert.assertEquals(RAPoints, MOPTotalPointCost);
					 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
					 		
					 	//Total Cost for Borrowing Points
			 			 CostofPointsBorrowed = driver.findElement(By.xpath(".//*[@id='nPrePmtAmount']")).getAttribute("value"); //Cost of points borrowed
			 			 	logger.info("Total Cost for Borrowing Points: "+CostofPointsBorrowed);
					 						 	
		 			 	//Process		
						driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
							logger.info("Clicked on Process button");
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								
						BorrowedMessage = "Borrowed Points"; //Predefined Message
								
						//Wait till the confirmation message displays	
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the element is present 
								
						PointsBorrowed = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText(); //Get Confirmation Text
							logger.info(""+PointsBorrowed);
									
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
						
						//*********************************************************************************//
							//Add the Leadid into Cookie
							Cookie Borrow1 = new Cookie("CostofPointsBorrowed",CostofPointsBorrowed);
							Cookie Borrow2 = new Cookie("MOPBorrow",MOPBorrow);
							Cookie Borrow3 = new Cookie("BorrowedMessage",BorrowedMessage);
							
							driver.manage().addCookie(Borrow1);
							driver.manage().addCookie(Borrow2);
							driver.manage().addCookie(Borrow3);
								
						//*********************************************************************************//
				}
				
	
				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
				
				
			}
		
	

}
