package cclb_Reservations;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.testng.annotations.Test;


public class D_LeadInformation extends A_Common {

	Logger logger = Logger.getLogger("D_LeadInformation.class");
	String Lead_Area_Id;
	String Lead_Type;
	String Sub_Type;
	
	@Test
	public void LeadInfo_Reservations() throws InterruptedException {
		
		 logger.info("--------------------------------------------------------------------------Lead Information---------------------------------------------------------------------------------");
		 
		 logger.info("---------------------------------------------Primary Lead and Secondary Lead Information--------------------------------------------------------");
		 
		 if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) //no phone number	
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
			}
			
			else
			{
				logger.info("Alert window not found");
			}
			
			Thread.sleep(1000);
			
			if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) // Points
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
			}
			
			else
			{
				logger.info("Alert window not found");
			}
	
		//---------------------------------------------------------------------------------------------------------------------------------------------------//
		//Lead Area-Lead Id	
		Lead_Area_Id = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[1]/td[2]")).getText();
			logger.info("Lead Area-Lead Id: " +Lead_Area_Id);
			
		//Lead Type/ SubType
		Lead_Type = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[2]/td[2]/table/tbody/tr/td[1]")).getText();
			Sub_Type = driver.findElement(By.xpath(".//*[@id='lead_subtype_input']")).getAttribute("value");
				logger.info("Lead Type/Subtype: "+ Lead_Type +" "+Sub_Type);
					
				//******************************************************//
				//Add the Leadid into Cookie
				Cookie Leadid = new Cookie("Lead_Area_Id",Lead_Area_Id);
					driver.manage().addCookie(Leadid);
				//******************************************************//
		
		 
	}

}
