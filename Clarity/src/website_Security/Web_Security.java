package website_Security;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class Web_Security extends A_Common {
Logger logger = Logger.getLogger("B_DiamondResortsHotels");

	String Email = "Tester123@gmail.com";
	String Postal = "30071";
	
	public void A_DiamondResortsAndHotels()  {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** beta.diamondresortsandhotels.com **************************************");
		
		driver.get("https://beta.diamondresortsandhotels.com"); //Open the Site
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='nav-activities']/a"))); // Wait till the Activites link is enabled	
			
			driver.findElement(By.xpath(".//*[@id='nav-resorts']/a")).click();
				logger.info("Clicked on Resorts");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='locations']/div/div/div[1]/div[1]/ul/li[1]/a"))); // Wait till the Arizona link is enabled	
						String Place = driver.findElement(By.xpath(".//*[@id='locations']/div/div/div[1]/div[1]/ul/li[1]/a")).getText();
							logger.info("Place is: "+Place);
				
			
			driver.findElement(By.xpath(".//*[@id='nav-deals']/a")).click();
				logger.info("Clicked on Deals");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='main']/div[4]/div/h1"))); // Wait till the Deals Header link is enabled	
						String Header = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/h1")).getText();
							logger.info("Header is: "+Header);
							
			driver.findElement(By.xpath(".//*[@id='nav-events']/a")).click();
				logger.info("Clicked on Events Link");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='main']/div[4]/h1"))); // Wait till the Events Header link is enabled	
						String Hdr = driver.findElement(By.xpath(".//*[@id='main']/div[4]/h1")).getText();
							logger.info("Header is: "+Hdr);
							
							//Images src
							String Weddings = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[1]/div[2]/img")).getAttribute("src");
												logger.info(""+Weddings);
												
							String Conferences = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[2]/div[2]/img")).getAttribute("src");
												logger.info(""+Conferences);
							
							String Meetings = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[3]/div[2]/img")).getAttribute("src");
												logger.info(""+Meetings);
							
							String Events = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[4]/div[2]/img")).getAttribute("src");
												logger.info(""+Events);	

			driver.findElement(By.xpath(".//*[@id='nav-activities']/a")).click();
				logger.info("Clicked on Activities Link");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='main']/div[4]/div/div[3]/div[2]/a"))); // Wait till the Activites link is enabled
				
				//Images src
				String Beach = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[1]/div[2]/img")).getAttribute("src");
									logger.info(""+Beach);
									
				String SnowSki = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[2]/div[2]/img")).getAttribute("src");
									logger.info(""+SnowSki);
				
				String Golf = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[3]/div[2]/img")).getAttribute("src");
									logger.info(""+Golf);
				
				String Outdoors = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[4]/div[2]/img")).getAttribute("src");
									logger.info(""+Outdoors);	
		
			driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[3]/div[2]/a")).click();
				logger.info("Clicked on Golf Find Resorts");

			

		}

//**********************************************************************************************************************************************************************************************************//
	
	public void B_betalogin() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** betalogin.diamondresorts.com **************************************");
		driver.get("https://betalogin.diamondresorts.com"); //Open the Site
		
		
		String Register = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[1]/a")).getText();
			logger.info(""+Register);
			
			String Username = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[2]/a")).getText();
				logger.info(""+Username);

				String Password = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[3]/a")).getText();
					logger.info(""+Password);
					
		driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("un1918987");
  			logger.info("Entered Username");
			  
	  	driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("DRI.pass1");
	  		logger.info("Entered Password");
	  			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}
	
//**********************************************************************************************************************************************************************************************************//

	public void C_betamember() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** betamember.diamondresorts.com **************************************");
		
		driver.get("https://betamember.diamondresorts.com"); //Open the Site
		
		String Register = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[1]/a")).getText();
			logger.info(""+Register);
			
			String Username = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[2]/a")).getText();
				logger.info(""+Username);

				String Password = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[3]/a")).getText();
					logger.info(""+Password);
					
		driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("un1918987");
  			logger.info("Entered Username");
			  
	  	driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("DRI.pass1");
	  		logger.info("Entered Password");
	  			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			  	
	  	driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[3]/div[4]/input")).click();
	  		logger.info("Clicked on Submit");
	  		
	}
		
//**********************************************************************************************************************************************************************************************************//
	
	public void D_Sweeps() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** Sweeps **************************************");
		
		driver.get("https://beta.diamondresorts.com"); //Open the Site
		
		driver.findElement(By.xpath(".//*[@id='edit-email']")).sendKeys(Email);
			logger.info("Entered Email Address");
		
		driver.findElement(By.xpath(".//*[@id='edit-zip-code']")).sendKeys(Postal);
			logger.info("Entered Postal Code");
			
			driver.findElement(By.xpath(".//*[@id='edit-submit--2']")).click();
				logger.info("Clicked Enter");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='content']/article/div/div/div/div[2]/div[1]/p[2]/strong/a"))); // Wait till the Skip Quiz link is enabled
	
		driver.findElement(By.xpath(".//*[@id='content']/article/div/div/div/div[2]/div[1]/p[2]/strong/a")).click();
			logger.info("Clicked on Skip Quiz");
				
				driver.findElement(By.xpath(".//*[@id='edit-first-name']")).sendKeys("Tester");
					logger.info("Entered FirstName");
					
				driver.findElement(By.xpath(".//*[@id='edit-last-name']")).sendKeys("Tester");
					logger.info("Entered LastName");
				
				driver.findElement(By.xpath(".//*[@id='edit-contact-number']")).click();
				driver.findElement(By.xpath(".//*[@id='edit-contact-number']")).sendKeys("2022022222");
			
					logger.info("Entered Primary Contact Number");

				Select Marital = new Select(driver.findElement(By.xpath(".//*[@id='edit-marital-status']")));
					Marital.selectByValue("1");
					
				Select Gender = new Select(driver.findElement(By.xpath(".//*[@id='edit-gender']")));
					Gender.selectByValue("MAL");
			
				Select Vacation = new Select(driver.findElement(By.xpath(".//*[@id='edit-weeks-per-year']")));
					Vacation.selectByValue("2");
					
				Select Age = new Select(driver.findElement(By.xpath(".//*[@id='edit-age-range']")));
					Age.selectByValue("AG1");
			
				driver.findElement(By.xpath(".//*[@id='edit-street']")).sendKeys("10600 W Charleston Blvd");
					logger.info("Entered Street Address");
					
				driver.findElement(By.xpath(".//*[@id='edit-city']")).sendKeys("Las Vegas");	
					logger.info("Entered City");
					
				Select State = new Select(driver.findElement(By.xpath(".//*[@id='edit-state']")));
					State.selectByValue("NV");
					
				driver.findElement(By.xpath(".//*[@id='edit-member-status']")).click();	
					logger.info("Clicked On Member Status");
					
				driver.findElement(By.xpath(".//*[@id='edit-submit']")).click();
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='content']/article/div/div/div/h2"))); // Wait 
				
				String Sweeps = driver.findElement(By.xpath(".//*[@id='content']/article/div/div/div/h2")).getText();
					logger.info(""+Sweeps);

	}
	
	public void E_Clarity() {
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** clarity.diamondresorts.com **************************************");
		
		driver.get("http://clarity.diamondresorts.com"); //Open the Site
		
		driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
		logger.info("Entered Username");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan20");// Entering Password
					logger.info("Entered Password");
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
								logger.info("Clicked on Login Button");
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
										driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[2]")).click();
											logger.info("Logged Out");
											
									
											
	}
	
	public void F_ClubNavigo() {
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** http://clubnavigo.com/ **************************************");
		
		driver.get("http://clubnavigo.com/"); //Open the Site
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='content_container']/div/a")));
			driver.findElement(By.xpath(".//*[@id='content_container']/div/a"));
			logger.info("Clicked on click here link");
			String Title = driver.getTitle();
			logger.info(""+Title);
											
											
	}
					

}
