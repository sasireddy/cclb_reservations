package website_Security;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Web_Security_Live_Continue extends A_Common {
	
	Logger logger = Logger.getLogger("Web_Security_Live");
	
	public void K_LiveLogin(){

		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** login.diamondresorts.com **************************************");
		driver.get("https://login.diamondresorts.com"); //Open the Site
		
		
		String Register = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[1]/a")).getText();
			logger.info(""+Register);
			
			String Username = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[2]/a")).getText();
				logger.info(""+Username);

				String Password = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[3]/a")).getText();
					logger.info(""+Password);
					
		driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("un1918987");
  			logger.info("Entered Username");
			  
	  	driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("DRI.pass1");
	  		logger.info("Entered Password");
	  			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			  	
	  	driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[3]/div[4]/input")).click();
	  		logger.info("Clicked on Submit");
	  		
	}
	
	public void L_LiveLoginMember(){

		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** member.diamondresorts.com **************************************");
		driver.get("https://member.diamondresorts.com"); //Open the Site
		
		String Register = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[1]/a")).getText();
			logger.info(""+Register);
			
			String Username = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[2]/a")).getText();
				logger.info(""+Username);

				String Password = driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[4]/ul/li[3]/a")).getText();
					logger.info(""+Password);
					
		driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("un1918987");
  			logger.info("Entered Username");
			  
	  	driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("DRI.pass1");
	  		logger.info("Entered Password");
	  			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			  	
	  	driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[3]/div[4]/input")).click();
	  		logger.info("Clicked on Submit");
	  		
	}
	
	public void M_Partners() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** partners.diamondresorts.com **************************************");
		
		driver.get("http://partners.diamondresorts.com"); //Open the Site
		
		driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
		logger.info("Entered Username");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan20");// Entering Password
					logger.info("Entered Password");
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							driver.findElement(By.xpath(".//*[@id='submit']")).click();// Click on Login 
								logger.info("Clicked on Login Button");
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
										/*driver.findElement(By.xpath(".//*[@id='logOut']")).click();
											logger.info("Logged Out");
												driver.findElement(By.xpath(".//*[@id='confirm_button1']")).click();
											*/
											
	}
	
	public void N_PitchBook() {
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** pitchbook.diamondresorts.com **************************************");
		
		driver.get("http://pitchbook.diamondresorts.com"); //Open the Site
		
		driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
		logger.info("Entered Username");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan20");// Entering Password
					logger.info("Entered Password");
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
								logger.info("Clicked on Login Button");
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

											
	}

		public void O_PprForum() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** http://ppr-forum.com/ **************************************");
		driver.get("http://ppr-forum.com/");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")));
		
		driver.findElement(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")).click();
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtUserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtPassword']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_butLogon']")).click();
		logger.info("Clicked Login");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='_ctl2__ctl0_hypLogout']")));
	
		logger.info("Logout button enabled");
		
	}
		
	public void P_Prequal() {
			
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** prequal.diamondresorts.com **************************************");
		driver.get("https://prequal.diamondresorts.com");
		
		String Title = driver.getTitle();
			logger.info(""+Title);

		}
	
	public void Q_RmrForum() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** http://rmr-forum.com/ **************************************");
		driver.get("http://rmr-forum.com/");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")));
		
		driver.findElement(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")).click();
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtUserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtPassword']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_butLogon']")).click();
		logger.info("Clicked Login");
		
	}
	
	public void R_Clubselect() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** https://select.diamondresorts.com/ **************************************");
		driver.get("https://select.diamondresorts.com/");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ctl00_csLoginView_pnlLogin']/div[1]/p/a[1]")));
		
		String Title = driver.getTitle();
		logger.info(""+Title);
		
		driver.findElement(By.xpath(".//*[@id='ctl00_csLoginView_pnlLogin']/div[1]/p/a[1]")).click();
		
		driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='page-mobile']/div[3]/div[2]/div/form/div[3]/div[4]/input")).click();
		logger.info("Clicked Login");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='header']/div[1]/p[1]/a/strong")));
		
		driver.findElement(By.xpath(".//*[@id='header']/div[1]/p[1]/a/strong")).click();
		logger.info("Clicked Logout");
		
	}
	
	public void S_Ticketbranson() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** http://ticketbranson.com/ **************************************");
		driver.get("http://ticketbranson.com/");
		
		String Title = driver.getTitle();
		logger.info(""+Title);
		
		
	
	}
	
	public void T_TravelHawaii() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** http://travelhawaii.com/ **************************************");
		driver.get("http://travelhawaii.com/");
		
		String Title = driver.getTitle();
		logger.info(""+Title);
		
		
	}

}
