package website_Security;

import org.testng.annotations.Test;

public class Z_Execution {
	
	Web_Security WebTest = new Web_Security();
	Web_Security_Live WebLive = new Web_Security_Live();
	Web_Security_Live_Continue WebLive_Continue = new Web_Security_Live_Continue();
	
	@Test(enabled=false)
	public void WebTesting() {
		
		WebTest.A_DiamondResortsAndHotels();
		WebTest.B_betalogin();
		WebTest.C_betamember();
		WebTest.D_Sweeps();
		WebTest.E_Clarity();
		
		
	}

	@Test(enabled=true)
	public void WebLiveTesting() {
		
		WebLive.A_DiamondResorts();
		WebLive.B_DiamondResorts_Resorts();
		WebLive.C_DiamondResortsAndHotels();
		WebLive.D_DiamondReosrtsForums();
		WebLive.E_DiscoverDiamond();
		WebLive.F_DiscoverDiamondGreece();
		WebLive.G_Faq();
		WebLive.H_FloridaTicketStation();
		WebLive.I_Generalfaq();
		WebLive.J_Hacforum();
	}
	@Test(enabled=false)
	public void WebLiveTesting_Continue(){
		WebLive_Continue.K_LiveLogin();
		WebLive_Continue.M_Partners();
		WebLive_Continue.N_PitchBook();
		WebLive_Continue.O_PprForum();
		WebLive_Continue.P_Prequal();
		WebLive_Continue.Q_RmrForum();
		WebLive_Continue.R_Clubselect();
		WebLive_Continue.S_Ticketbranson();
		WebLive_Continue.T_TravelHawaii();
	}
}
