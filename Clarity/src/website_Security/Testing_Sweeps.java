package website_Security;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;


@Test
public class Testing_Sweeps extends A_Common {
	
	Logger logger = Logger.getLogger("Testing_Sweeps");
	
	String Postal = "89117";
	
	@Test
	public void Sweeps(){
	PropertyConfigurator.configure("Log4j.properties");
	
	logger.info("************************************** Testing.diamondresorts.com **************************************");
	
	driver.get("https://beta.diamondresorts.com"); //Open the Site
	
	driver.findElement(By.xpath(".//*[@id='edit-email']")).sendKeys("Tester1234@gmail.com");
		logger.info("Entered Email Address");
	
	driver.findElement(By.xpath(".//*[@id='edit-zip-code']")).sendKeys(Postal);
		logger.info("Entered Postal Code");
		
		driver.findElement(By.xpath(".//*[@id='edit-submit--2']")).click();
			logger.info("Clicked Enter");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='content']/article/div/div/div/div[2]/div[1]/p[2]/strong/a"))); // Wait till the Skip Quiz link is enabled

	driver.findElement(By.xpath(".//*[@id='content']/article/div/div/div/div[2]/div[1]/p[2]/strong/a")).click();
		logger.info("Clicked on Skip Quiz");
			
			driver.findElement(By.xpath(".//*[@id='edit-first-name']")).sendKeys("Tester");
				logger.info("Entered FirstName");
				
			driver.findElement(By.xpath(".//*[@id='edit-last-name']")).sendKeys("Tester");
				logger.info("Entered LastName");
			
			driver.findElement(By.xpath(".//*[@id='edit-contact-number']")).click();
			driver.findElement(By.xpath(".//*[@id='edit-contact-number']")).sendKeys("2022022222");
		
				logger.info("Entered Primary Contact Number");

			Select Marital = new Select(driver.findElement(By.xpath(".//*[@id='edit-marital-status']")));
				Marital.selectByValue("1");
				
			Select Gender = new Select(driver.findElement(By.xpath(".//*[@id='edit-gender']")));
				Gender.selectByValue("MAL");
		
			Select Vacation = new Select(driver.findElement(By.xpath(".//*[@id='edit-weeks-per-year']")));
				Vacation.selectByValue("2");
				
			Select Age = new Select(driver.findElement(By.xpath(".//*[@id='edit-age-range']")));
				Age.selectByValue("AG1");
		
			driver.findElement(By.xpath(".//*[@id='edit-street']")).sendKeys("10600 W Charleston Blvd");
				logger.info("Entered Street Address");
				
			driver.findElement(By.xpath(".//*[@id='edit-city']")).sendKeys("Las Vegas");	
				logger.info("Entered City");
				
			Select State = new Select(driver.findElement(By.xpath(".//*[@id='edit-state']")));
				State.selectByValue("NV");
				
			driver.findElement(By.xpath(".//*[@id='edit-member-status']")).click();	
				logger.info("Clicked On Member Status");
				
			driver.findElement(By.xpath(".//*[@id='edit-submit']")).click();
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='content']/article/div/div/div/h2"))); // Wait 
			
			String Sweeps = driver.findElement(By.xpath(".//*[@id='content']/article/div/div/div/h2")).getText();
				logger.info(""+Sweeps);
				
	}
}
