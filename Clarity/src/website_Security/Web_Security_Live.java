package website_Security;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Web_Security_Live extends A_Common {
	
	Logger logger = Logger.getLogger("Web_Security_Live");
	
	public void A_DiamondResorts() {
		
			PropertyConfigurator.configure("Log4j.properties");
		
			logger.info("************************************** www.diamondresorts.com **************************************");
			driver.get("https://www.diamondresorts.com");
			//-----------------------------------Click on Deals---------------------------------------------------------
			
			driver.findElement(By.xpath(".//*[@id='site-navigation-menu']/li[1]/a")).click();
			logger.info("Clicked on Deals");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='site-navigation-menu']/li[2]/a")));
			
			driver.findElement(By.xpath(".//*[@id='site-navigation-menu']/li[2]/a")).click();
			logger.info("Clicked on Why Vacations for Life");
			
	}
	
	public void B_DiamondResorts_Resorts() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** www.diamondresorts.com/resorts **************************************");
		driver.get("https://www.diamondresorts.com/resorts");
		
		String Header = driver.findElement(By.xpath(".//*[@id='white-page']/h3")).getText();
		logger.info("Clicked on Deals: "+Header);
		
		String Title = driver.getTitle();
		logger.info("Title is: "+Title);
	}

	public void C_DiamondResortsAndHotels()  {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** www.diamondresortsandhotels.com **************************************");
		
		driver.get("https://www.diamondresortsandhotels.com"); //Open the Site
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='nav-activities']/a"))); // Wait till the Activites link is enabled	
			
			driver.findElement(By.xpath(".//*[@id='nav-resorts']/a")).click();
				logger.info("Clicked on Resorts");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='locations']/div/div/div[1]/div[1]/ul/li[1]/a"))); // Wait till the Arizona link is enabled	
						String Place = driver.findElement(By.xpath(".//*[@id='locations']/div/div/div[1]/div[1]/ul/li[1]/a")).getText();
							logger.info("Place is: "+Place);
				
			
			driver.findElement(By.xpath(".//*[@id='nav-deals']/a")).click();
				logger.info("Clicked on Deals");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='main']/div[4]/div/h1"))); // Wait till the Deals Header link is enabled	
						String Header = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/h1")).getText();
							logger.info("Header is: "+Header);
							
			driver.findElement(By.xpath(".//*[@id='nav-events']/a")).click();
				logger.info("Clicked on Events Link");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='main']/div[4]/h1"))); // Wait till the Events Header link is enabled	
						String Hdr = driver.findElement(By.xpath(".//*[@id='main']/div[4]/h1")).getText();
							logger.info("Header is: "+Hdr);
							
							//Images src
							String Weddings = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[1]/div[2]/img")).getAttribute("src");
												logger.info(""+Weddings);
												
							String Conferences = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[2]/div[2]/img")).getAttribute("src");
												logger.info(""+Conferences);
							
							String Meetings = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[3]/div[2]/img")).getAttribute("src");
												logger.info(""+Meetings);
							
							String Events = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div[1]/div[4]/div[2]/img")).getAttribute("src");
												logger.info(""+Events);	

			driver.findElement(By.xpath(".//*[@id='nav-activities']/a")).click();
				logger.info("Clicked on Activities Link");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='main']/div[4]/div/div[3]/div[2]/a"))); // Wait till the Activites link is enabled
				
				//Images src
				String Beach = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[1]/div[2]/img")).getAttribute("src");
									logger.info(""+Beach);
									
				String SnowSki = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[2]/div[2]/img")).getAttribute("src");
									logger.info(""+SnowSki);
				
				String Golf = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[3]/div[2]/img")).getAttribute("src");
									logger.info(""+Golf);
				
				String Outdoors = driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[4]/div[2]/img")).getAttribute("src");
									logger.info(""+Outdoors);	
		
			driver.findElement(By.xpath(".//*[@id='main']/div[4]/div/div[3]/div[2]/a")).click();
				logger.info("Clicked on Golf Find Resorts");
			
		}
	
	public void D_DiamondReosrtsForums() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** www.diamondresortsforums.com **************************************");
		driver.get("https://www.diamondresortsforums.com");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")));
		
		driver.findElement(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")).click();
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtUserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtPassword']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_butLogon']")).click();
		logger.info("Clicked Login");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='_ctl2__ctl0_hypLogout']")));
		
		driver.findElement(By.xpath(".//*[@id='_ctl2__ctl0_hypLogout']")).click();
		logger.info("Clicked Logout");


	}
	
	public void E_DiscoverDiamond() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** www.discoverdiamond.net **************************************");
		driver.get("https://www.discoverdiamond.net");
		
		driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("not4you2");
		logger.info("Entered Passwored");
		
		driver.findElement(By.xpath(".//*[@id='login-form-bg']/form/input[4]")).click();
		logger.info("Clicked Login");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='logoff']/a")));
			
		String Title = driver.getTitle();
		logger.info(""+Title);
		
		driver.findElement(By.xpath(".//*[@id='logoff']/a")).click();

	}
	
	public void F_DiscoverDiamondGreece() {
		
		PropertyConfigurator.configure("Log4j.properties");
	
		logger.info("************************************** www.discoverdiamondgreece.net **************************************");
		driver.get("https://www.discoverdiamondgreece.net");
		
		String Title = driver.getTitle();
		logger.info(""+Title);		
	}
	
	public void G_Faq() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** faq.diamondresorts.com **************************************");
		driver.get("https://faq.diamondresorts.com");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ctl00_ctlContentPlaceHolder_ctl00_ctlHeader_ctlMainNavigation_butLogin']")));
		
		driver.findElement(By.xpath(".//*[@id='ctl00_ctlContentPlaceHolder_ctl00_ctlHeader_ctlMainNavigation_butLogin']")).click();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.findElement(By.xpath(".//*[@id='ctl00_ctlContentPlaceHolder_ctl00_ctlLoginControl_ctlLoginControls_txtUserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='ctl00_ctlContentPlaceHolder_ctl00_ctlLoginControl_ctlLoginControls_txtPassword']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='ctl00_ctlContentPlaceHolder_ctl00_ctlLoginControl_ctlLoginControls_butLogon']")).click();
		logger.info("Clicked Login");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ctl00_ctlContentPlaceHolder_ctl00_ctlHeader_ctlMainNavigation_hypLogout']")));
	
		logger.info("Logout button is enabled");
		
	}
	
	public void H_FloridaTicketStation() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** floridaticketstation.com **************************************");
		driver.get("https://floridaticketstation.com");
		
	}
	
	public void I_Generalfaq() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** generalfaq.diamondresorts.com **************************************");
		driver.get("https://generalfaq.diamondresorts.com");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ctlContentPlaceHolder_ctl00_ctlHeader_ctlMainNavigation_butLogin']")));
		
		driver.findElement(By.xpath(".//*[@id='ctlContentPlaceHolder_ctl00_ctlHeader_ctlMainNavigation_butLogin']")).click();
		
		driver.findElement(By.xpath(".//*[@id='ctlContentPlaceHolder_ctl00_ctlLoginControl_ctlLoginControls_txtUserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='ctlContentPlaceHolder_ctl00_ctlLoginControl_ctlLoginControls_txtPassword']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='ctlContentPlaceHolder_ctl00_ctlLoginControl_ctlLoginControls_butLogon']")).click();
		logger.info("Clicked Login");
		
		
	}
	
	public void J_Hacforum() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("************************************** hacforum.com **************************************");
		driver.get("https://hacforum.com");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")));
		
		driver.findElement(By.xpath(".//*[@id='_ctl2__ctl0_butLogin']")).click();
		
		String Title = driver.getTitle();
			logger.info(""+Title);
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtUserName']")).sendKeys("clubdri");
		logger.info("Entered Username as clubdri");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_txtPassword']")).sendKeys("not4you2");
		logger.info("Entered Password");
		
		driver.findElement(By.xpath(".//*[@id='_ctl2_ctlLogonControl_ctlLoginControls_butLogon']")).click();
		logger.info("Clicked Login");

		
	}
	
	
}