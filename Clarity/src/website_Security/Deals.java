  package website_Security;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

	public class Deals extends A_Common {
		Logger logger = Logger.getLogger("Deals");
		Calendar cal = Calendar.getInstance();
		String Day= new SimpleDateFormat("d").format(cal.getTime());
	

	@Test
	public void Vegas_Deal() {
					
	PropertyConfigurator.configure("Log4j.properties");
	
	logger.info("************************************** Deals **************************************");
	
	driver.get("https://beta.diamondresorts.com");
	
	PropertyConfigurator.configure("log4j.properties");
	
	//-----------------------------------Click on Deals---------------------------------------------------------
	
	driver.findElement(By.xpath(".//*[@id='site-navigation-menu']/li[1]/a")).click();

	String Destination = "Destination Deals";
		String Destinations1 = driver.findElement(By.cssSelector("h1[class='site-banner-title']")).getText();
			logger.info(""+Destinations1);
				Assert.assertEquals(Destinations1, Destination);
					logger.info("Actual String matches the expected String: Destination Deals");
						logger.info("Clicked on Deals Link");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							
	//---------------------------------------------Do You Have A Location in Mind?------------------------------------------------------------------
	
		Select Location = new Select(driver.findElement(By.xpath(".//*[@id='city_state']")));
			Location.selectByValue("Las Vegas, Nevada");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='content']/div[3]/div[1]/article"))); // Wait 

			
		if(driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[1]/article")).isDisplayed())
			{
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String Header = driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/div/div/header/h2")).getText();
						logger.info("The Stay is at: "+Header);
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String MidHeader = driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/div/div/header/div/div[1]")).getText();
						logger.info("The Mid Header: "+MidHeader);
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String Normally = driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/div/div/div/p[1]")).getText();
						logger.info(""+Normally);
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String Low = driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/div/div/div/p[2]")).getText();
						logger.info(""+Low);
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					String DestinationDeals = driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/footer/a[1]")).getText();
						driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/footer/a[1]")).click();
						logger.info("Clicked on "+DestinationDeals);
								String DestinationDeals1 = "Soak up the lights, the magic and the glamour of the Las Vegas experience";
									String DestinationDeals2 = driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/div/aside/div/ul[1]/li[1]")).getText();
										Assert.assertEquals(DestinationDeals2, DestinationDeals1);
											driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/footer/a[1]")).click();
												driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				
				//--------------------------------------------Chat on the phone------------------------------------------------
					driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/footer/p[1]/a")).click();
						logger.info("Clicked on chat on phone");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath("html/body/div[13]/div/div/a")).click();
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			 
				//-----------------------------------------Terms and Conditions--------------------------------------------------------
					driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/footer/p[2]/a")).click();
						logger.info("Clicked on Terms and Conditions");
						
							Object[] currentWindow1 =driver.getWindowHandles().toArray();
								String ParentWindow1=currentWindow1[0].toString();
									String ChildWindow1=currentWindow1[1].toString();
										driver.switchTo().window(ChildWindow1);
											driver.close();
												driver.switchTo().window(ParentWindow1);
													driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
													
				//---------------------------------------------Click on BOOK NOW---------------------------------------------------------
					driver.findElement(By.xpath(".//*[@id='content']/div[3]/div[3]/article/footer/a[2]")).click();
					logger.info("Clicked on More Information");
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			}
			
			else
			{
				logger.info("No Resorts are available for booking");
				driver.close();
			}
		
		logger.info("------------------------------------Select your travel date------------------------------------------");
		
		String TravelDate = driver.findElement(By.xpath(".//*[@id='cal-form']/div/div[2]/label/span[2]")).getText();
								logger.info(""+TravelDate);
								
		
		String TravelMonth = driver.findElement(By.xpath(".//*[@id='calendar_month']/option[8]")).getText();
								logger.info(""+TravelMonth);
		driver.findElement(By.xpath(".//*[@id='calendar_month']/option[8]")).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='calendar']/div[3]/div[1]/table"))); // Wait 
		
		if(driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[1]/table")).isDisplayed())
		{
			logger.info("Calendar Displayed");
			
			WebElement dateWidget = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div/div[4]/div[3]/div/table/tbody"));
			List<WebElement> columns=dateWidget.findElements(By.tagName("tr"));
					int rowsnum1 = columns.size();
						int colnum1=7;
							String xpath1 =null;
								String cellval1;
								
								Search:
										for(int i = 1;i<=rowsnum1;i++)
										{
											for(int j=1;j<=colnum1;j++)
											{
												xpath1= "/html/body/div/main/div[2]/div/div/form/div/div/div[4]/div[3]/div/table/tbody/tr[" + String.valueOf(i) + "]/td[" + String.valueOf(j) + "]/a";
												        
												cellval1 = driver.findElement(By.xpath(xpath1)).getText();
													driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
															if(Day.equals(cellval1))
															{
																	driver.findElement(By.xpath(xpath1)).click();
																	break Search;
															}
																					
															
											}
			
										}
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);						
						String SelectedDate = "May 16 � May 22, 2016";
							String SelectedDate1 = driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[2]/div[2]/div[2]")).getText();
											logger.info("SelectedDate: "+SelectedDate1);
												//Assert.assertEquals(SelectedDate1, SelectedDate);
													if (SelectedDate1.equals(SelectedDate))
													{
														logger.info("Date Selected - Matched");
														
													}
													
													else
													{
														logger.info("Selected Date didnt Match");
													}
														
											
					//--------------------------------------Click on Choose this travel date----------------------------------------//							
						driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[2]/div[2]/a")).click();	
							logger.info("Clicked on Choose this travel date");
							
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}						
					//------------------------------------------------Click CHANGE and Choose OPTION 2-----------------------------------------------------//
						driver.findElement(By.xpath(".//*[@id='cal-selection']/div/div[1]/div/div[2]/a")).click(); //CHANGE Button
							logger.info("Clicked on CHANGE Button");
								driver.findElement(By.xpath(".//*[@id='defer_date']")).click(); //Check box]


										driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											String LaterDate = "Pick a travel date later";
											//String LaterDate = "You�ve opted to pick a travel date later.";
												driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
													String LaterDate1 = driver.findElement(By.xpath(".//*[@id='cal-selection']/div/div[1]/div/div[2]/span[1]")).getText();
														//Assert.assertEquals(LaterDate1, LaterDate);
						
														if(LaterDate1.equals(LaterDate))
														{
															logger.info("Clicked on OPTION 2 :PASSED");
														}
														
														else
														{
															logger.info("Selecting OPTION2 Has Failed");
														}
					//---------------------------------Choose a Travel Date-------------------------------------------------------------------------------------//	
						driver.findElement(By.xpath(".//*[@id='cal-selection']/div/div[1]/div/div[2]/a")).click(); //CHANGE Button
							String TravelMonth1 = driver.findElement(By.xpath(".//*[@id='calendar_month']/option[5]")).getText();
								logger.info(""+TravelMonth1);
									driver.findElement(By.xpath(".//*[@id='calendar_month']/option[5]")).click();
						
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='calendar']/div[3]/div[1]/table"))); // Wait 
						
									if(driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[1]/table")).isDisplayed())
									{
										logger.info("Calendar Displayed");
										
										wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='calendar']/div[3]/div[1]/table/tbody/tr[3]/td[2]/a"))); // Wait
										try {
											Thread.sleep(1000);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
											driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[1]/table/tbody/tr[3]/td[2]/a")).click(); //Click on 15 date
											
											String SelectedDate2 = "February 15 � February 21, 2016";
											String SelectedDate3 = driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[2]/div[2]/div[2]")).getText();
															logger.info("SelectedDate: "+SelectedDate3);
																Assert.assertEquals(SelectedDate3, SelectedDate2);
																	if (SelectedDate3.equals(SelectedDate2))
																	{
																		logger.info("Date Selected Matched");
																		
																	}
																	
																	else
																	{
																		logger.info("Selected Date didnt Match");
																	}
																		
									
									}
									
									driver.findElement(By.xpath(".//*[@id='calendar']/div[3]/div[2]/div[2]/a")).click();	//Click on Choose this travel date
									logger.info("Clicked on Choose this travel date");
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
									
		}
		
		//---------------------------Click on Terms and Conditions------------------------------------------------------------------
		driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div[2]/div/div[1]/div/div[1]/p[3]/a")).click();
		logger.info("Clicked on Terms and Conditions");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			Object[] currentWindow2 =driver.getWindowHandles().toArray();
				String ParentWindow2=currentWindow2[0].toString();
					String ChildWindow2=currentWindow2[1].toString();
						driver.switchTo().window(ChildWindow2);
							driver.close();
								driver.switchTo().window(ParentWindow2);
									//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
									
		//-------------------------------------Click on Locations--------------------------------------------------------------------
		driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div[2]/div/div[1]/div/div[1]/section/div[2]/a")).click();
		logger.info("Clicked on Location icon");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			Object[] currentWindow3 =driver.getWindowHandles().toArray();
				String ParentWindow3 = currentWindow3[0].toString();
					String ChildWindow3 = currentWindow3[1].toString();
						driver.switchTo().window(ChildWindow3);
							driver.close();
								driver.switchTo().window(ParentWindow3);
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
									
		//---------------------------------------Click to see Eligibilty-------------------------------------------------------------
		driver.findElement(By.xpath(".//*[@id='cal-selection']/div/div[2]/a"));
		logger.info("Clicked on Click Here to See if I'm Eligible");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		logger.info("-----------------Sign-Up to Check Eligibility-------------------");
		
		driver.findElement(By.xpath(".//*[@id='edit-last-name']")).sendKeys("Test");
		logger.info("Entered Last Name");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-street-address']")).sendKeys("6415 Chestnut Glen Dr");
		logger.info("Entered Street Address");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-email']")).sendKeys("test11@test.com");
		logger.info("Entered Email Address");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-city']")).sendKeys("Norcross");
		logger.info("Entered City");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-phone']")).sendKeys("2033022222");
		logger.info("Entered Contact Number");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-state']/option[12]")).click();
		logger.info("Selected state from drop down");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-postal-code']")).sendKeys("30071-2271");
		logger.info("Entered Postal Code");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.id("edit-survey-10265-yes")).click();
		logger.info("Are you 25 years of age or older - Clicked YES");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.id("edit-survey-10285-yes")).click();
		logger.info("Is your annual household income at least $50000 - Clicked YES");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.id("edit-survey-10266-yes")).click();
		logger.info("Major CreditCards - Clicked YES");
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath(".//*[@id='edit-next']")).click();
		logger.info("Clicked on Click Here to See if I'm Eligible");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String Required = driver.findElement(By.xpath(".//*[@id='booking-form']/div[1]/div/div[1]/div[1]/div/div[1]/div/div/div[1]")).getText();
		logger.info("A Required Has been left blank: "+Required);
		
		driver.findElement(By.xpath(".//*[@id='edit-first-name']")).sendKeys("Diamond");
		logger.info("Entered First Name");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.findElement(By.xpath(".//*[@id='edit-skip']")).click();
			logger.info("Clicked on No thanks, just book my stay without the tour");
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
			
			String confirmation = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[1]/h2")).getText();
			logger.info("---------------------------------------------------"+confirmation+"------------------------------------------------------------------------");
			
			//---------------------------------------------------Contact Information----------------------------------------------------
			
			driver.findElement(By.xpath(".//*[@id='edit-contact']")).click();
			logger.info("Clicked on Contact Information Edit");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//----------------------------------------Getting information from sign-up page--------------------------------------------------------------------------------
			String Price = driver.findElement(By.className("deal-details-offer-more")).getText();
								logger.info(""+Price);
			String Price1 = Price.substring(2, 8);
								logger.info("Price with out tour: "+Price1);
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								
			String Firstname = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div/div/div/div/input")).getAttribute("value");
								logger.info("Firstname: "+Firstname);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
			String Lastname = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div/div/div[2]/div/input")).getAttribute("value");
								logger.info("Last Name: "+Lastname);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			String FullName = Firstname+ " "+Lastname;
								logger.info("FullName: "+FullName);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
									
			String Address = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div[2]/div/div/div/input")).getAttribute("value");
								logger.info("Address: "+Address);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			String Email = 	driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div/div/div[3]/div/input")).getAttribute("value");
								logger.info("Email Address: "+Email);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					
					
			String City = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div[2]/div/div[3]/div/input")).getAttribute("value");
							logger.info("City: "+City);
								driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			String State = "GA";		
					
			String Contact = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div/div/div[4]/div/input")).getAttribute("value");
								logger.info("Contact Number: "+Contact);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					
			String Postal = driver.findElement(By.xpath("/html/body/div/main/div[2]/div/div/form/div/div[3]/div/div/div/div[2]/div/div[5]/div/input")).getAttribute("value");
								logger.info("Postal Code: "+Postal);
									driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			
			driver.findElement(By.xpath(".//*[@id='edit-skip']")).click();
				logger.info("Clicked on No thanks, just book my stay without the tour");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
					
			//--------------------------------------Getting contact information from confirmation page-------------------------------------.
					
				String FullName1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[1]/div[2]/address/div[1]")).getText();
					Assert.assertEquals(FullName1, FullName);
						logger.info("FullName Matched");
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
				String Address1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[1]/div[2]/address/div[2]/span[1]")).getText();
					Assert.assertEquals(Address1, Address);
						logger.info("Address Matched");
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				String City1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[1]/div[2]/address/div[2]/span[2]")).getText();
					Assert.assertEquals(City1, City);
						logger.info("City Matched");
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				String State1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[1]/div[2]/address/div[2]/span[3]")).getText();
					Assert.assertEquals(State1, State);
						logger.info("State Matched");
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				String Postal1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[1]/div[2]/address/div[2]/span[4]")).getText();
					Assert.assertEquals(Postal1, Postal);
						logger.info("Postal Matched");
				
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				String Contact1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[1]/div[2]/address/div[3]/span")).getText();
					Assert.assertEquals(Contact1, Contact1);
						logger.info("Phone Number Matched");
						
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				logger.info("-----------------------------Travel--------------------------------------");
				
				String TravelDate1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[1]/div/div[2]/div[2]/p")).getText();
					logger.info("Travel Dates: "+TravelDate1);
				
				logger.info("---------------------------------Details-----------------------------------");
				
				String Details = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[2]/div/div[2]/table/tbody/tr/td[1]")).getText();
					logger.info("Details: "+Details);
				
				String Details1 = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[2]/div/div[1]/div[2]/p[1]")).getText();
					logger.info(""+Details1);
		
				logger.info("----------------------------Total Cost-------------------------------------");
				
				String Total = driver.findElement(By.xpath(".//*[@id='dri-deals-book-form']/div/div/div[2]/div[2]/div/p")).getText();
					logger.info(""+Total);
						String Total1 = Total.substring(7, 13);
							logger.info(""+Total1);
								String Total2 = Total1.replace(",","");
								driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				Assert.assertEquals(Price1, Total1);
					logger.info("The Total Cost Matched the Original Price (Price with out tour dates");
				
				logger.info("----------------Terms and Conditions----------------------------------");
					driver.findElement(By.xpath(".//*[@id='edit-qualified-terms']")).click();
						logger.info("Agreed to Terms and Conditions");
				
				driver.findElement(By.xpath(".//*[@id='edit-next']")).click();
					logger.info("Clicked on Next");
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				logger.info("--------------------------------------------------------Payment-------------------------------------------------------------------------------");
				
				logger.info("---------------------Order Details-----------------------");
					String OrderDetails = driver.findElement(By.xpath(".//*[@id='creditCardForm']/div/div[2]/div/div[2]/div/div[1]/div/table/tbody/tr/td[1]")).getText();
						Assert.assertEquals(Details, OrderDetails);
							logger.info("Order Details in payment page matched the Confirmation Page");
				
				String PaymentTotal = driver.findElement(By.xpath(".//*[@id='creditCardForm']/div/div[2]/div/div[2]/div/div[2]")).getText();
					logger.info(""+PaymentTotal);
						String PaymentTotal1 = PaymentTotal.substring(7, 12);
							Assert.assertEquals(Total2, PaymentTotal1);
						
								logger.info("Payment Total in payment page matched the Confirmation Page");
				
				driver.findElement(By.xpath(".//*[@id='cardNumber']")).sendKeys("4616222222222257");
					logger.info("Entered Card Number");
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				driver.findElement(By.xpath(".//*[@id='cardHolderFirstName']")).sendKeys("Tester");
					logger.info("Entered First Name");
				
				driver.findElement(By.xpath(".//*[@id='cardHolderLastName']")).sendKeys("Tester Tester");
					logger.info("Entered Last Name");
				
				driver.findElement(By.xpath(".//*[@id='expMonth']/option[2]")).click();
					logger.info("Selected Expiration Month");
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				driver.findElement(By.xpath(".//*[@id='expYear']/option[7]")).click();
					logger.info("Selected Expiration Year");
				
				driver.findElement(By.xpath(".//*[@id='cvv']")).sendKeys("333");
					logger.info("Entered Cvv Code");
					
	}	

}

