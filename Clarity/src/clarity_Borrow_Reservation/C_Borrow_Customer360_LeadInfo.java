package clarity_Borrow_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class C_Borrow_Customer360_LeadInfo extends A_Borrow_Common {
	
Logger logger = Logger.getLogger("C_Borrow_Customer360_LeadInfo.class");
	
	public String LILeadid; //Lead Information Tab
	
	public void Customer360() 
	{
		
		PropertyConfigurator.configure("Log4j.properties");
		
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 					
		/*String Leadid = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/select/option[10]")).getText(); // Get DBL Lead Area	
							driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/select/option[10]")).click(); // Click on the DBL Lead Area 
								logger.info("DBL Lead Area Selected as: "+Leadid);*/
								
								
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(Leadid1); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+Leadid1);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a Member");
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					 }
				 }
		}
		
		public void Lead_Id()
		{
					
			try {
		 		
				LILeadid = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[1]/td[2]")).getText();
				
			 	Properties properties = new Properties();
			 		properties.setProperty("LILeadid", LILeadid);
		
				File file = new File("Borrow_Res_Leadid.Properties");
				FileOutputStream fileOut = new FileOutputStream(file);
				properties.store(fileOut, "Lead Id");
				fileOut.close();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		public void Lead_Information() {
			
			 if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) 
				{
					logger.info("Clicked on Alert Ok window");
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
				}
				
				else
				{
					logger.info("Alert window not found");
				}
				
			 if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) 
				{
					logger.info("Cliked on Ok Button (Alert Window)");
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
				}
				
				else
				{
					logger.info("Alert window not found");
				}
			
			Lead_Id(); //Calling Lead Id Method 
			
			 logger.info("--------------------------------------------------------------------------Lead Information---------------------------------------------------------------------------------");
			 
			 logger.info("---------------------------------------------Primary Lead and Secondary Lead Information--------------------------------------------------------");

			//---------------------------------------------------------------------------------------------------------------------------------------------------//
			//Lead Area-Lead Id	
			String Lead_Area_Id = LILeadid;
				logger.info("Lead Area-Lead Id: " +Lead_Area_Id);
				
			String Lead_Type = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[2]/tbody/tr[2]/td[2]/table/tbody/tr/td[1]")).getText();
				String Sub_Type = driver.findElement(By.xpath(".//*[@id='lead_subtype_input']")).getAttribute("value");
					logger.info("Lead Type/Subtype: "+ Lead_Type +" "+Sub_Type);
			
				
		}

}



