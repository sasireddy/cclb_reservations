package clarity_Borrow_Reservation;

import org.testng.annotations.Test;

public class Z_Borrow_Execution {
	
	B_Borrow_Login Borrow_Login = new B_Borrow_Login();
	C_Borrow_Customer360_LeadInfo Borrow_Leadinfo = new C_Borrow_Customer360_LeadInfo();
	D_Borrow_Reservation Borrow_Reservation = new D_Borrow_Reservation();
	E_Borrow_ClubRes Borrow_ClubRes = new E_Borrow_ClubRes();
	F_Borrow_BookReservation Borrow_BookRes = new F_Borrow_BookReservation();
	G_Borrow_MOPPayments Borrow_MOP = new G_Borrow_MOPPayments();
	H_Borrow_ClubReservations Borrow_Reserve = new H_Borrow_ClubReservations();
	I_Borrow_FinalConfirmation Borrow_ResConfirm = new I_Borrow_FinalConfirmation();
	
	@Test
	public void Borrow_Reservation()
	{
		Borrow_Login.openBrowser();
		Borrow_Leadinfo.Customer360();
		Borrow_Leadinfo.Lead_Information();
		Borrow_Reservation.Reservations();
		Borrow_ClubRes.ClubRes();
		Borrow_BookRes.BookReservation();
		Borrow_BookRes.Resort_Availability();
		Borrow_BookRes.RoomUpgrade_No();
		Borrow_BookRes.Errata();
		Borrow_BookRes.Discount_No();
		Borrow_MOP.MethodofPaymentOptions();
		Borrow_Reserve.Reservation();
		Borrow_ResConfirm.Reservation_Confirmation();
		
	}

}


