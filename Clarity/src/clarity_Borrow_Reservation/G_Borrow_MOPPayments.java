package clarity_Borrow_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class G_Borrow_MOPPayments extends A_Borrow_Common {
	
	Logger logger =  Logger.getLogger("G_Borrow_MOPPayments.class");
	
	//MOP Options
	public String MOPTotResPointCost; // Total Reservation Point Cost
	public String MOPointsAvail; //Total Member Points Available
	public String MOPBalPointsNeeded; //Balance of Points Needed
	public String MOPUseMemPoints; //Use Membership Points
	public String MOPBorrowfrm; //Borrow 
	public String MOPTotalPointCost;// Total Points To Apply / Total Cost
	public String CostOfPntsBrwd; //Cost of Points Borrowed
	
	//Retrieve from Borrow_ResortAvailability.Properties
	public String RAPoints;
		
	//Retrieve from Borrow_Reservation.Properties
	public String PresentYrClubReservationpnt; //Get Present Year Points Replacing Comma
	
		
		public void ResProperties() {
			 
			 PropertyConfigurator.configure("Log4j.properties");	
				  try (FileReader reader = new FileReader("Borrow_ResortAvailability.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
			      		RAPoints = properties.getProperty("RAPnts");
			      		logger.info("Points from Resort Availability: " +RAPoints);
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				  
				  try (FileReader reader = new FileReader("Borrow_Reservation.Properties")) {
			      		Properties properties = new Properties();
				      		properties.load(reader);
				      		PresentYrClubReservationpnt = properties.getProperty("PresentYrClubReservationpnt");
				      	} catch (IOException e) {
				      		e.printStackTrace();
				      	}
				   
		 }
		

		//---------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------//
		
		public void MethodofPaymentOptions() {
							
			ResProperties(); //Call ResProperties Method
				
			logger.info("-------------------------------------------------------------------Method Of Payment Options------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mpwProcess']"))); // Wait till the element is present 
					
				if(driver.findElements(By.xpath(".//*[@id='MPWdiv']/div/table/tbody/tr/td")).size() !=0) 
				{
					
						 //Estimate and Confirm Reservation Points 
						 	
						 	//Total Point Cost / Compared
						 	MOPTotResPointCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getAttribute("value");
						 		Assert.assertEquals(RAPoints, MOPTotResPointCost);
						 			logger.info("Total Reservation Point Cost Matched The Points in Resort Availability: "+MOPTotResPointCost);
						 				int MOPTotResPointCostINT = Integer.parseInt(MOPTotResPointCost);
						 			
						 	//Total Member Points Available / Compared
						 	MOPointsAvail = driver.findElement(By.xpath(".//*[@id='nAvailOptions']")).getText();
						 		Assert.assertEquals(PresentYrClubReservationpnt, MOPointsAvail);
						 			logger.info("Total Member Points Available Matched The Available for Reservations in Club Reservations: "+MOPointsAvail);
						 				int MOPointsAvailINT = Integer.parseInt(MOPointsAvail);
						 			
						 	//Balance of Points Needed Estimation
					 		int EstiBalancePointsNeededINT = MOPTotResPointCostINT - MOPointsAvailINT; // Estimate
					 			int EstimatedBalPntsNeedINT = Math.abs(EstiBalancePointsNeededINT); //Make it a absolute value
					 				logger.info("Estimated Balance of Points Needed is: " +EstimatedBalPntsNeedINT);	
				 				
			 				String EstiBalancePointsNeeded = String.valueOf(EstimatedBalPntsNeedINT);
						 			
						 	//Balance of Points Needed // Compared 
				 			MOPBalPointsNeeded = driver.findElement(By.xpath(".//*[@id='PointsBal']")).getText();
				 				Assert.assertEquals(MOPBalPointsNeeded, EstiBalancePointsNeeded);
				 					logger.info("Balance of Points needed Matched the Estimation Assertion Passed");
								 	
		 					//Use Membership Points / Compared
					 		MOPUseMemPoints = driver.findElement(By.xpath(".//*[@id='UsePtsAmt']")).getAttribute("value");
					 			logger.info("Use Membership Points in Method of Payments Section: "+MOPUseMemPoints);
					 				logger.info("Available points: "+PresentYrClubReservationpnt);
					 					Assert.assertEquals(MOPUseMemPoints, PresentYrClubReservationpnt);
					 						logger.info("Use Membership Points Matched the Available points from USAGE INFORMATION: ASSERTION PASSED");
				
						 	//Borrow From Next Year / Compared
					 		MOPBorrowfrm = driver.findElement(By.xpath(".//*[@id='nBorPoints']")).getAttribute("value");	
		 						int MOPBorrowfrmNxYrINT = Integer.parseInt(MOPBorrowfrm);
		 							logger.info("Borrow Points from Next Year in Method of Payments Section:" +MOPBorrowfrmNxYrINT);
			 							logger.info("Estimated Points to Borrow: "+EstimatedBalPntsNeedINT);
								 							
			 								Assert.assertEquals(MOPBorrowfrmNxYrINT, EstimatedBalPntsNeedINT);
			 									logger.info("Borrow from Next Year Matched the Estimation Points to Borrow: ASSERTION PASSED");
			 									
	 									//Cost of the points borrowed
	 									try {
	 								 		 CostOfPntsBrwd = driver.findElement(By.xpath(".//*[@id='nPrePmtAmount']")).getAttribute("value"); //Cost of points borrowed
	 											logger.info("Cost for borrowed points: "+CostOfPntsBrwd);
	 									 	
	 											Properties properties = new Properties();
	 									 			properties.setProperty("CostOfPntsBrwd", CostOfPntsBrwd);
	 									 				properties.setProperty("MOPBorrowfrm", MOPBorrowfrm);
	 									 				
	 										File file = new File("Borrow_MOPPayments.Properties");
	 										FileOutputStream fileOut = new FileOutputStream(file);
	 										properties.store(fileOut, "Cost of Borrowed Points");
	 										fileOut.close();
	 									} catch (FileNotFoundException e) {
	 										e.printStackTrace();
	 									} catch (IOException e) {
	 										e.printStackTrace();
	 									}
			 									
							//Total Points To Apply / Total Cost
						 	MOPTotalPointCost = driver.findElement(By.xpath(".//*[@id='pvAmount']")).getAttribute("value");
						 		Assert.assertEquals(RAPoints, MOPTotalPointCost);
						 			logger.info("Total Points To Apply / Total Cost Matched The Points in Resort Availability: "+MOPTotalPointCost);
						 			
						 	//Process		
				 			driver.findElement(By.xpath(".//*[@id='mpwProcess']")).click();
								logger.info("Clicked on Process button");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								
							//Wait till the confirmation message displays	
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the element is present 
								
								String PointsBorrowed = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText(); //Get Confirmation Text
									logger.info(""+PointsBorrowed);
									
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
								
	 			}
									
				else
				{
					logger.info("Methods of Payemnt Options Window not found");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				}
		}

}
