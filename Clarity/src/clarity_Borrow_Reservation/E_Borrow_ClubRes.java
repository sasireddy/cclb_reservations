package clarity_Borrow_Reservation;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class E_Borrow_ClubRes extends A_Borrow_Common {

Logger logger = Logger.getLogger("E_Borrow_ClubRes.class");
	
	//-----------------------------------Guest----------------------------//
	public String ClubResLeadNum;//Lead Number
	public String Lead_Id; //Used inside try method to retrieve LILeadid from Borrow_Res_Leadid.Properties
	
	//-----------------------------------Membership Detail for:----------------------------//
	public String ClubResMemDetail; //Membership Detail for: details
	public String ClubResMemDetailfor; //Substring of ClubResMemDetail
	
	//-----------------------------------Usage Information----------------------------//
	//Usage Information
		public String PresentYearClubReservationspnt; //Present Year Available for Reservations
		public String PresentYrClubReservationpnt; // Replace PresentYearClubReservationspnt "," with "".
		public String ClubReservationspnt; //Get Next Year Points 
		public String ClubRespoints; //Replace ClubReservationspnt "," with "" from Next Year Points
		public String ClubResAllotment; // Next Year Allotment 
		public String ClubResReservations; //Next Year Points Available for Reservations
		public String ClubResMemBen; //Next Year Available for MemberBenefits
		public String ClubResSave; //Next Year Available to save to follwoing yr
		public String ClubResPntSvFlagHeader; //Next Year Available to Save to following year
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	
	public void ClubRes() {
		 
		 PropertyConfigurator.configure("Log4j.properties");
			
		 logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		 
		 //Get Leadid from Leadid.Properties
		 try (FileReader reader = new FileReader("Borrow_Res_Leadid.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		
	      		Lead_Id = properties.getProperty("LILeadid");
	      		logger.info(Lead_Id);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
			
		 	if(driver.findElements(By.xpath("/html/body/div[15]/div/div/div[3]")).size()!=0) 
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath("/html/body/div[15]/div/div/div[3]")).click();
				
			}
			
			else
			{
				logger.info("---Notice---Notice---Notice---Window not found");
			}

			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
	//-----------------------------------------------------------------------GUEST------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Guest------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='guestypeimg']"))); // Wait till the element LEAD TYPE FLAG is present to click
			
			//Lead Number / Compared
			ClubResLeadNum = driver.findElement(By.xpath(".//*[@id='pv1guest_lead']/a")).getText();
				logger.info("Lead Area-Lead Id: " +ClubResLeadNum);
					Assert.assertEquals(Lead_Id, ClubResLeadNum);
						logger.info("Lead Number in Lead Information Tab Matched with Club Reservations");
				
		//-----------------------------------------------------------------------Membership Detail------------------------------------------------------------------------------------------//
											
			logger.info("-------------------------------------------------------------------Membership Detail------------------------------------------------------------");
			
						
			//Details			
			ClubResMemDetail = driver.findElement(By.xpath(".//*[@id='tdMembership']")).getText(); //Membership Detail For:
				logger.info("Membership Details for: "+ClubResMemDetail);
				
		//-----------------------------------------------------------------------Usage Information------------------------------------------------------------------------------------------//
			
				logger.info("-------------------------------------------------------------------Usage Information------------------------------------------------------------");
				
				//--------------------------------------Points-------------------------------------//
					//Store Points in Borrow_Reservation.Properties file
					try {
						
						ClubResMemDetailfor = ClubResMemDetail.substring(0,6); //Membership Detail for subString
							logger.info("Membership Number: "+ClubResMemDetailfor);
						
						PresentYearClubReservationspnt = driver.findElement(By.xpath(".//*[@id='mbrRemBan']")).getText(); //Get Present Year Points 
						logger.info("Present Year Available Points: "+PresentYearClubReservationspnt);
						
						ClubReservationspnt = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[3]/td[3]")).getText(); //Get Next Year Points 
					 		
						
				 		int LengthPresentYear = PresentYearClubReservationspnt.length(); // Length
						 int LengthNextYear = ClubReservationspnt.length();
							String S = "1,000";
							int S1 = S.length(); // Length of S String
							
							if(LengthPresentYear == S1) //If Size is the same then take out "," and follows the rest of the process
							{
								PresentYrClubReservationpnt = PresentYearClubReservationspnt.replace(",",""); //Replace , with nothing
							}
							
							else
							{
								PresentYrClubReservationpnt = PresentYearClubReservationspnt;
							}
							
							if(LengthNextYear == S1) //If Size is the same then take out "," and follows the rest of the process
							{
								ClubRespoints = ClubReservationspnt.replace(",",""); //Replace , with nothing
							}
							
							else
							{
								ClubRespoints = ClubReservationspnt;
							}
		
			 
					 	Properties properties = new Properties();
					 	
					 	properties.setProperty("PresentYearClubReservationspnt", PresentYearClubReservationspnt); // No Comma
					 	properties.setProperty("PresentYrClubReservationpnt", PresentYrClubReservationpnt); //Replaced without Comma
					 		properties.setProperty("ClubReservationspnt", ClubReservationspnt);
					 			properties.setProperty("ClubRespoints",ClubRespoints);
			 						properties.setProperty("MembershipNumber", ClubResMemDetailfor); //Membership Detail for:
						

						File file = new File("Borrow_Reservation.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Available Points");
						fileOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				//Next Year points Allotment
				ClubResAllotment = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[2]/td[3]")).getText();
					logger.info("Allotment for Next Year: "+ClubResAllotment);
					
						//Next Year Avail for Reservations
						ClubResReservations = ClubReservationspnt;
								logger.info("Next Year Available for Reservations: "+ClubResReservations);
						
							//Next Year Avail for Member Benefits
							ClubResMemBen = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[4]/td[3]")).getText();
								logger.info("Next Year Available for Member Benefits: "+ClubResMemBen);
						
								
								//Next Year Avail to save to following yr 
								ClubResSave = driver.findElement(By.xpath(".//*[@id='MbrUsageGrid']/tbody/tr[5]/td[3]")).getText();
									logger.info("Next Year Available to Save to Following yr: "+ClubResSave);
									
								
				
	}
}	

