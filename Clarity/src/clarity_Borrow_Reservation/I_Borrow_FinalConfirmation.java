package clarity_Borrow_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class I_Borrow_FinalConfirmation extends A_Borrow_Common {
	
	Logger logger = Logger.getLogger(I_Borrow_FinalConfirmation.class);
	
	public String RCBegiPntsBal; //Beginning Point Balance
	public String RCResCost; //Reservation Cost
	public String RCResCt; //Replace "RCResCost" without ","
	public String RCBorrowedPnts; //Borrowed Points
	public String RCBorrowedPoints; //Replace Borrowed Points with ","
	public String RCTotalPntsPurchased; //Total Points Purchased
	public String RCTotalPointsPurchased; //Total Points Purchase without ","
	public String RCTotPntsReservation; //Total Points Used for Reservation
	public String RCTotalPointsReservation; //Replace "RCTotPnts" without ","
	public String RCEndPntBal; //Ending points balance
	public String ConfimationMes; //Confirmation Message with Confirmation Number
	
	//Retrieved from Borrow_Reservation.Properties
	public String ClubRespoints;
	public int ClubReservationPointsInt; //Conver ClubRespoints to Integer
	public String PresentYrClubReservationpnt;
	
	//Retrieved from Borrow_ResortAvailability.Properties
	public String RAPoints;
	
	//Retrieved from Borrow_MOPPayments.Properties
	public String MOPBorrowfrm;
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	public void ResProperties()
	 {
		
		 PropertyConfigurator.configure("Log4j.properties");	
		 
		 //Borrow_Reservation
		 try (FileReader reader = new FileReader("Borrow_Reservation.Properties")) {
     		Properties properties = new Properties();
	      		properties.load(reader);
	      		ClubRespoints = properties.getProperty("ClubRespoints");
	      		PresentYrClubReservationpnt = properties.getProperty("PresentYrClubReservationpnt");
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		 
		
		 //Borrow_ResortAvailability
		  try (FileReader reader = new FileReader("Borrow_ResortAvailability.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		RAPoints = properties.getProperty("RAPnts");
	      		logger.info("Points from Resort Availability: " +RAPoints);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
		  
		//Borrow_MOPPayments
		  try (FileReader reader = new FileReader("Borrow_MOPPayments.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		MOPBorrowfrm = properties.getProperty("MOPBorrowfrm");
	      		logger.info("Points Borrowed From Next Year: " +MOPBorrowfrm);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
	 }  
	
	public void Reservation_Confirmation()
	{
		PropertyConfigurator.configure("Log4j.properties");	
		
		ResProperties(); //Call the Method
					
		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					
		logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
								
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
			
			//Beginning Points Balance / Compared
			RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText();
			Assert.assertEquals(RCBegiPntsBal, RAPoints); //Comparision
					logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched: "+RCBegiPntsBal);
									
					//Reservation Cost
					RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost = RCResCost.length(); // Length of RCResCost
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCResCt = RCResCost.replace(",","");
						Assert.assertEquals(RCResCt, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt);
							
						//Borrowed Points
						RCBorrowedPnts = driver.findElement(By.xpath(".//*[@id='BorrowPoints']")).getText();
							RCBorrowedPoints = RCBorrowedPnts.replace(",", "");
								Assert.assertEquals(RCBorrowedPoints, MOPBorrowfrm); //Comparing 
									logger.info("Borrowed Points Displayed and Matched with Borrow Points from MOP Payments: "+RCBorrowedPnts);
						
						//Total Points Used for Reservation / Compared 
						RCTotPntsReservation = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							RCTotalPointsReservation = RCTotPntsReservation.replace(",","");
								Assert.assertEquals(RCTotalPointsReservation, RAPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Reservation Points: "+RCTotPntsReservation);
							
						//Estimating Ending Points
						int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
							int AvailPnts = Integer.parseInt(ClubRespoints); //Convert Next Year Available points to integer
								int EstEndPnts = AvailPnts - ResPnts;
									int PresentYearPoints = Integer.parseInt(PresentYrClubReservationpnt); //Convert Present Year Points to Integer
										int EstimatedEndPoints = EstEndPnts + PresentYearPoints;
											logger.info("Estimation of Ending Point Balance should be: "+EstimatedEndPoints);
										
						//Ending Points Balance	/ Compared
						String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal2']")).getText();
							int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
								logger.info("Ending Points Balance: "+RCEndPointsbal);
								
							Assert.assertEquals(RCEndPointsbal, EstimatedEndPoints);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
						
					}
					
					else		
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost);
				
						//Borrowed Points
						RCBorrowedPnts = driver.findElement(By.xpath(".//*[@id='BorrowPoints']")).getText();
								Assert.assertEquals(RCBorrowedPnts, MOPBorrowfrm); //Comparing 
								logger.info("Borrowed Points Displayed and Matched with Borrow Points from MOP Payments: "+RCBorrowedPnts);
						
						//Total Points Used for Reservation
						RCTotPntsReservation = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							Assert.assertEquals(RCTotPntsReservation, RAPoints); //Comparing 
								logger.info("Total Points Used for Reservation Displayed and Matched with Use Membership Points from MOP Screen: "+RCTotPntsReservation);
										
						//Estimating Ending Points
						int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
							int AvailPnts = Integer.parseInt(ClubRespoints); //Convert Next Year Available points to integer
								int EstEndPnts = AvailPnts - ResPnts;
									int PresentYearPoints = Integer.parseInt(PresentYrClubReservationpnt); //Convert Present Year Points to Integer
										int EstimatedEndPoints = EstEndPnts + PresentYearPoints;
											logger.info("Estimation of Ending Point Balance should be: "+EstimatedEndPoints);
											
						//Ending Points Balance	/ Compared
						String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal2']")).getText();
							int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
								logger.info("Ending Points Balance: "+RCEndPointsbal);
								
							Assert.assertEquals(RCEndPointsbal, EstimatedEndPoints);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							
					}
					
					//Confirmation Message
					ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
						logger.info(""+ConfimationMes);
						
						//Save to Confirmation.Properties File
						try{
						String ResConNum = ConfimationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
						logger.info("Reservation Confirmation number is: "+ResConNum);
						
						Properties properties = new Properties();
				 		properties.setProperty("ConfirmationNo", ResConNum);
				 		
				 		File file = new File("Borrow_Reservation_Confirmation.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Reservation Confirmation Number");
						fileOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
						
 			}				
}
