package clarity_RoomUpgrade_Reservation;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.testng.Assert;

public class G_RoomUpgrade_RoomUpgrade extends A_RoomUpgrade_Common {
	
	Logger logger = Logger.getLogger("G_RoomUpgrade_RoomUpgrade.class");
	
	//Room Upgrade
	public String RoomUpgrade; //RoomUpgrade Header
	public String RURoomType ; //Room Type
	public String RURoomType1; //Room Type
	public String RoomUpgCost; //Room Upgrade Cost.  Save it into a Cookie


	public void Room_Upgrade_Yes() {
		
		  PropertyConfigurator.configure("Log4j.properties");
		
		logger.info("-------------------------------------------------------------------Room Upgrade------------------------------------------------------------");
			
			if(driver.findElements(By.xpath(".//*[@id='upgrdRoomNo']")).size()!=0)
			{
				
				//Room Upgrade Header / Compared
				RoomUpgrade = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[1]/tbody/tr[1]/td")).getText();
					Assert.assertEquals(RoomUpgrade, RoomUpgrade1);
						logger.info("RoomUpgrade Header displayed and Passed Assertion: "+RoomUpgrade);
						
				//Click on RoomType for Upgrade
				driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[2]/tbody/tr[1]/td[1]/input")).click();	
				RURoomType = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[2]/tbody/tr[1]/td[2]")).getText();
					RURoomType1 = driver.findElement(By.xpath(".//*[@id='roomUpgradeDiv1']/table[2]/tbody/tr[1]/td[3]")).getText();	
						logger.info("Room Type: "+RURoomType+ " "+RURoomType1);
					
					logger.info("Clicked on Room Upgrade");
						
					RoomUpgCost = driver.findElement(By.xpath(".//*[@id='cst_upg']")).getText();
							logger.info("Room Upgrade Cost: "+RoomUpgCost);
							
							//*****************************************************************//
							Cookie RoomUpgrade = new Cookie("RoomUpgCost",RoomUpgCost);
							Cookie RoomUpgrade1 = new Cookie("RURoomType", RURoomType);
							
							driver.manage().addCookie(RoomUpgrade);
							driver.manage().addCookie(RoomUpgrade1);
							//*****************************************************************//
							
				driver.findElement(By.xpath(".//*[@id='upgrdRoomYes']")).click();
					logger.info("Clicked Yes for Room Upgrade");	
			}
				
			else
			{
				logger.info("Room Upgrade Screen Not Found");
					driver.quit();
			}
	 
	}
}
