package clarity_RoomUpgrade_Reservation;


import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class I_RoomUpgrade_ClubReservations extends A_RoomUpgrade_Common {
	
	Logger logger = Logger.getLogger("H_ClubReservations.class");
	
	//-------------------------------Reservation---------------------------//
	public String CRReservationHeader; //Reservation Header
	public String CRBookFunc; //Booking Function
	public String CRMarkeyCode; //Marketing Key Code
	public String CRMarKeyCodeFlagHeader; //Marketing Key Code Flag Header
	public String CResType; //Reservation Type
	public String CResStatus; //Reservation Status
	public String CRProperty; //Property
	public String CRPropFlagHeader; //Property Flag Header
	public String CRArrDate; //Arrival Date
	public String CRNights; //Nights
	public String CRNigFlagHeader; //Nights Flag Headr (Inventory)
	public String CRDepDate; //Departure Date
	public String CRGuaCode; //Guarantee Code
	public String CRUsagePeriod; //Usage Period
	public String CRMemshipNum; //Membership Number
	public String NoofGues; //Number of Guests
	public String CRRmType; //RoomType
	public String CRRoomType; //Substring for CRRmType
	public String CRRoomTypeFlagHeader; //RoomType Flag Header
		
	//-------------------------------Financial Summary--------------------------//
	public String CPResAdMsc; //Reservation and Misc Fees
	public String CPResAdMsc1; //Take off the $ or � sign of CPResAdMsc
	public String CPResAdMsc2; //Trim the String CPResAdMsc1
	public String CRResMisFeesHeader; //Reservation and Misc Fees Flag Header
		public String CRPaymentInforHeader; //Payment Information Header
		public String AccUpgradeFee; //Account Upgrade Fee
		public String TotalAmountDue; //Total Reservation and Misc Fees Total in Flag
		public String CCMainHeader; //Credit Card Maintenance Header	
		public String Leadid; //Leadid in Credit Card Maintenance Section
		
	//-------------------------------Financial Summary--------------------------//
	public String PaymentMessage;  //Payment Message 
	
	//Retrieve Values From Cookies
	public String MembershipNumber;
	public String RAResortName;
	public String RURoomType; //From RoomUpgrade Class
	public String RAArrivalDate;
	public String RADepartureDate;
	public String Lead_Area_Id;
	public String RoomUpgCost;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
		 
	public void Reservation() {
		
	 PropertyConfigurator.configure("Log4j.properties");
	 
	 //*******************************************************************************************//
		//Retrieve Values from cookies
		 MembershipNumber = driver.manage().getCookieNamed("MembershipNumber").getValue();
		 logger.info("Value Saved in Cookie - Membership Number: "+MembershipNumber);
		 
		 RAResortName =  driver.manage().getCookieNamed("RAResortName").getValue();
		 logger.info("Value Saved in Cookie - Resort Name: "+RAResortName);
		 
		 RURoomType =  driver.manage().getCookieNamed("RURoomType").getValue();
		 logger.info("Value Saved in Cookie - RoomType: "+RURoomType);
		 
		 RAArrivalDate = driver.manage().getCookieNamed("RAArrivalDate").getValue();
		 logger.info("Value Saved in Cookie - ArrivalDate: "+RAArrivalDate);
		 
		 RADepartureDate = driver.manage().getCookieNamed("RADepartureDate").getValue();
		 logger.info("Value Saved in Cookie - Departure Date: "+RADepartureDate);
		 
		 Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
		 logger.info("Value Saved in Cookie - LeadId: "+RADepartureDate);
		 
		 RoomUpgCost = driver.manage().getCookieNamed("RoomUpgCost").getValue();
		 logger.info("Value Saved in Cookie - Room Upgrade Cost: "+RoomUpgCost);
		 
	//*******************************************************************************************//
			
	    logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
		
		//-----------------------------------------------------------------------Reservation:------------------------------------------------------------------------------------------//
		
		logger.info("-------------------------------------------------------------------Reservation------------------------------------------------------------");
		
				
		//Booking Function / Compared
		CRBookFunc = driver.findElement(By.xpath(".//*[@id='ResSum_BookingPurpose']")).getText();
			Assert.assertEquals(CRBookFunc, BookTypeReserName);
				logger.info("Booking Fuction Displayed and Passed Assertion: "+CRBookFunc);
				
		//Marketing KeyCode / Compared
		CRMarkeyCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
			logger.info("Marketing Key Code: "+CRMarkeyCode);
				
				
		//ReservationType
		CResType = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[4]/td[2]")).getText();
			logger.info("Reservation Type is: "+CResType);
			
		//Reservation Status
		CResStatus = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]")).getText();
			logger.info("Reservation Status: "+CResStatus);
			
		//Property / Compared
		CRProperty = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText();
			Assert.assertEquals(CRProperty, RAResortName);
				logger.info("Property displayed and Matched From Resort Availabilty screen: "+CRProperty);
				
				//Property Flag / Compared
				driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[6]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on The flag image
					logger.info("Clicked on Property Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the Close button is present 
							CRPropFlagHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
								Assert.assertEquals(CRPropFlagHeader, ErrataHeader);
									logger.info("Property Flag Header displayed and Matched: "+CRPropFlagHeader);
										driver.findElement(By.id("REWbtnClose")).click(); //Click on Close button

		//ArrivalDate / Compared
		CRArrDate = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[7]/td[2]")).getText();
			Assert.assertEquals(CRArrDate, RAArrivalDate);
				logger.info("Arrival Date displayed and Matched From Resort Availabilty screen: "+CRArrDate);
		
		//Nights / Compared
		CRNights = driver.findElement(By.xpath(".//*[@id='ResSum_pnNights']")).getAttribute("value");
			Assert.assertEquals(SANights, CRNights);
				logger.info("Number of Nights Displayed and Matched Assertion: "+CRNights);
					
									
		//Departure Date / Compared
		CRDepDate = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value");
			Assert.assertEquals(CRDepDate, RADepartureDate);
				logger.info("Departure Date displayed and Matched from Resort Availability Screen: "+CRDepDate);		
				
		//Guarantee Code
		CRGuaCode = driver.findElement(By.xpath(".//*[@id='ResSum_pvGuarantee_input']")).getAttribute("value");
			logger.info("Guarantee Code: "+CRGuaCode);
			
		//Usage Period
		CRUsagePeriod = driver.findElement(By.xpath(".//*[@id='ResSum_UsagePeriod']")).getText();
			logger.info("Usage Period: "+CRUsagePeriod);
		
		//Membership Number / Compared
		CRMemshipNum = driver.findElement(By.xpath(".//*[@id='ResSum_MemberContrID']")).getText();
			logger.info("MemberShip Number: "+CRMemshipNum);
				Assert.assertEquals(CRMemshipNum, MembershipNumber);
					logger.info("Membership Number Matched from ClubRes (Membership Detail for:): "+CRMemshipNum);		
		
		//Number of Guests / Compared
		NoofGues = driver.findElement(By.xpath(".//*[@id='ResSum_Adults']")).getAttribute("value");	
			Assert.assertEquals(SAAdults, NoofGues);
				logger.info("Number of Adults Displayed and Matched in Search of Availability Screen: "+NoofGues);
		
		//Roomtype / Compared
		CRRmType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
			CRRoomType = CRRmType.substring(0, 3);
				logger.info("Room Type: " +CRRoomType);
					Assert.assertEquals(CRRoomType, RURoomType);
						logger.info("Room Type Displayed and Matched from RoomUpgrade Screen: "+CRRoomType);
		
		
			//Room Type Flag / Compared
			driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Roomtype Flag
				logger.info("Clicked on Room Type Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the Close button is present
						CRRoomTypeFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
							logger.info(""+CRRoomTypeFlagHeader);
								driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
								
	
	//-----------------------------------------------------------------------Travel Protection------------------------------------------------------------------------------------------//
	
		logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");			
			
		//Decline Rpp
		driver.findElement(By.xpath(".//*[@id='declineRPP']")).click();
			logger.info("Declined RPP");
					
	//----------------------------------------------------------------------Financial Summary------------------------------------------------------------------------------------------//				
			logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
									
			//Reservation And Misc Fees / Compared
			CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText();
				logger.info("Reservation And Misc Fees: "+CPResAdMsc);
				
				CPResAdMsc1 = CPResAdMsc.replace("�", ""); //Take of pound Sign
				 CPResAdMsc2 = CPResAdMsc1.trim();// Trim the string to compress blank space 
					
		
					//Reservation And Misc Fees Flag Header / Compared
					driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
						logger.info("Clicked on Reservation And Misc Fees Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
								
								//Reservation and Misc Fees Header / Compared
								CRResMisFeesHeader = driver.findElement(By.xpath(".//*[@id='RFIFTitle']")).getText();
									Assert.assertEquals(CRResMisFeesHeader, CRReserMiscFeesHeader);
										logger.info("Reservation And Misc Fees Flag Header Displayed and Passed Assertion: "+CRResMisFeesHeader);
										
								//Loyalty Accomodation Upgrade Fee Compare / Compareed
								logger.info("-------------------------------Loyalty Accomodation Upgrade Fee------------------------------------------------------------");
									AccUpgradeFee = driver.findElement(By.xpath(".//*[@id='FRRTRN2AMOUNT']")).getText();
										String AccountUpgradeFee = AccUpgradeFee.substring(0,2);
											logger.info("Loyality Accomodation Upgrade Fee: "+AccountUpgradeFee);
												Assert.assertEquals(AccountUpgradeFee, RoomUpgCost);
													logger.info("Loyalty Accomodation Upgrade Fee Matched with RoomUpgrade Cost: ASSERTION PASSED");
									
									/*//Payment Information Header / Compared
									CRPaymentInforHeader = driver.findElement(By.xpath(".//*[@id='RFIFContent']/div/div[1]/div/table/tbody/tr[9]/td")).getText();
										Assert.assertEquals(CRPaymentInforHeader, CRPayInfoHeader);
											logger.info("Payment Information Header Displayed and Passed Assertion: "+CRResMisFeesHeader);
												driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
*/									
									//Total Amount Due	/ Compared		
									TotalAmountDue = driver.findElement(By.xpath(".//*[@id='FRR_TOTAL_AMOUNT']")).getText();
										logger.info("Total Amount due is: "+TotalAmountDue);
											Assert.assertEquals(TotalAmountDue, CPResAdMsc2);
												logger.info("Total Amount due matched the Reservation and Misc Fees");
												
									//Add Credit Card
									driver.findElement(By.xpath(".//*[@id='pvpaycc_input']")).click();
										Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='pvpaycc_select']"))); 
											Payment.selectByValue("0");// Select Booking Type
											
											//Credit Card Maintenance Header and Section / Compared
											CCMainHeader = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[1]/tbody/tr/td/b")).getText();
												logger.info("Header is: "+CCMainHeader);
													Assert.assertEquals(CCMainHeader, CreditCardMainHeader);
														logger.info("Credit Card Maintenance Header Matched");
														
													//Lead ID / Compared
													Leadid = driver.findElement(By.xpath(".//*[@id='CCEntryInnerDiv']/form/table[2]/tbody/tr/td[4]/span/font")).getText();
														logger.info("Leadid in Credit Card Maintenance :"+Leadid);
															Assert.assertEquals(Leadid, Lead_Area_Id); 
																logger.info("Leadid from Credit Card Maintenance section matched");
																	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
																
													//-------Add Credit Card---------//
																
													//Credit Card Number
													driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
														driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
															driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
														
													/*//Credit Card Type
													String CCType1 = driver.findElement(By.xpath(".//*[@id='pvCCType_input']")).getAttribute("value");//Get Credit Card Type Value 
														logger.info(""+CCType1);
														*/
													//Card Holder Name
														driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
													driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
													
													//Exp mnth & Year
													driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
														Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
															ExpMth.selectByValue(ExpMonth);// Exp Mnth
													
													driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
														Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
															ExpYear.selectByValue(ExpYr);// Exp Year
														
													//Active
													driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
														Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
															Active.selectByValue("Y");// Active
																driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
													
													//DRI Credit Card Y/N
													driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
														Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
															Dricc.selectByValue("N");// DRI Credit Card	
													
													//Click on Save Button
													driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
														logger.info("Clicked on Save Button");
														
														wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Ok Button is present 
														
														String Success = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
															logger.info(""+Success); //Success MEssage
															
														driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Click on OK
										
														driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
											
				//Click on SAVE Button
				driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
						logger.info("Clicked on SAVE BUTTON");
						
						//Payment Mes
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the Close Button is present 
						
						PaymentMessage = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
							logger.info(""+PaymentMessage);
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click(); //Close						
		}
	
}	 
				

