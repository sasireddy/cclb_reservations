package clarity_RoomUpgrade_Reservation;



import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class E_RoomUpgrade_ClubRes extends A_RoomUpgrade_Common {

	Logger logger = Logger.getLogger("E_RoomUpgrade_ClubRes.class");
	
	//-----------------------------------Guest----------------------------//
	public String ClubResLeadNum;//Lead Number
	public String Lead_Area_Id; //Use to retrieve Lead id  from cookie
	
	//-----------------------------------Membership Detail for:----------------------------//
	public String ClubResMemDetail; //Membership Detail for: details
	public String ClubResMemDetailfor; //Substring of ClubResMemDetail
	
	//-----------------------------------Usage Information----------------------------//
	//Change in Future
	public String ClubResReservationsPoints; //Get Present Year Points 
	public String ClubRespoints; //Replace "," with "" from Present Year Points
	public String ClubResAllotment; // Present Year Allotment 
	public String ClubResMemBen; //Present Year Available for MemberBenefits
	public String ClubResSave; //Present Year Available to save to follwoing yr
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	
	public void ClubRes() {
		 
		 PropertyConfigurator.configure("Log4j.properties");
			
		 logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		 
			
		 	if(driver.findElements(By.xpath("/html/body/div[15]/div/div/div[3]")).size()!=0) 
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath("/html/body/div[15]/div/div/div[3]")).click();
				
			}
			
			else
			{
				logger.info("---Notice---Notice---Notice---Window not found");
			}

			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
	//-----------------------------------------------------------------------GUEST------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Guest------------------------------------------------------------");
			
			WebDriverWait wait = new WebDriverWait(driver, 120);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='guestypeimg']"))); // Wait till the element LEAD TYPE FLAG is present to click
			
			//***************************************************************************//
			//Retrieve Value from Cookie
			Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
			logger.info("Value Retrieved from cookie is "+Lead_Area_Id);
			//***************************************************************************//
			
			//Lead Number / Compared
			ClubResLeadNum = driver.findElement(By.xpath(".//*[@id='pv1guest_lead']/a")).getText();
				logger.info("Lead Area-Lead Id: " +ClubResLeadNum);
					Assert.assertEquals(ClubResLeadNum, Lead_Area_Id);
						logger.info("Lead Number in Lead Information Tab Matched with Club Reservations");
				
		//-----------------------------------------------------------------------Membership Detail------------------------------------------------------------------------------------------//
											
			logger.info("-------------------------------------------------------------------Membership Detail------------------------------------------------------------");
			
						
			//Details			
			ClubResMemDetail = driver.findElement(By.xpath(".//*[@id='tdMembership']")).getText(); //Membership Detail For:
				logger.info("Membership Details for: "+ClubResMemDetail);
			
				ClubResMemDetailfor = ClubResMemDetail.substring(0,6); //Membership Detail for subString
				logger.info("Membership Number: "+ClubResMemDetailfor);
				
		//-----------------------------------------------------------------------Usage Information------------------------------------------------------------------------------------------//
			
				logger.info("-------------------------------------------------------------------Usage Information------------------------------------------------------------");
					
				//Present Year points
				ClubResAllotment = driver.findElement(By.xpath(".//*[@id='pv1MbrAlt']")).getText();
					logger.info("Allotment for Present Year: "+ClubResAllotment);
					
						//Present Year Avail for Reservations
					ClubResReservationsPoints = driver.findElement(By.xpath(".//*[@id='mbrRemBan']")).getText(); //Get Present Year Points ;
							ClubRespoints = ClubResReservationsPoints.replace(",",""); //Replace , with nothing
								logger.info("Present Year Available for Reservations: "+ClubResReservationsPoints);
							
							//Present Year Avail for Member Benefits
							ClubResMemBen = driver.findElement(By.xpath(".//*[@id='mbrAvaBenefit']")).getText();
								logger.info("Present Year Available for Member Benefits: "+ClubResMemBen);
						
								
								//Present Year Avail to save to following yr 
								ClubResSave = driver.findElement(By.xpath(".//*[@id='mbrAvaSave']")).getText();
									logger.info("Present Year Available to Save to Following yr: "+ClubResSave);
									
								/*//Room Upgrades Used/ Remaining:
								String CRRoomUpgrade = driver.findElement(By.xpath(".//*[@id='mbrRoomUpd']")).getText();
									logger.info("Room Upgrades: "+CRRoomUpgrade);
										String RoomUpgrade = CRRoomUpgrade.substring(2);
											String RmUpgrade = CRRoomUpgrade.substring(0,1); 
											
											int CRRoomUpgrade1 = Integer.parseInt(RoomUpgrade);
												int CRRoomUpgrade2 = Integer.parseInt(RmUpgrade);
												
												if(CRRoomUpgrade2 < CRRoomUpgrade1)
												{
													logger.info("Room Upgrade is Available");
												}
												
												else
												{
													logger.info("Room Upgrade is not available");
													//driver.quit();
												}*/
												
							//Add Cookies
							Cookie ClubRes1 = new Cookie("ClubResReservationsPoints", ClubResReservationsPoints);
							Cookie ClubRes2 = new Cookie("ClubRespoints", ClubRespoints);
							Cookie ClubRes3 = new Cookie("MembershipNumber", ClubResMemDetailfor);

							driver.manage().addCookie(ClubRes1);
							driver.manage().addCookie(ClubRes2);
							driver.manage().addCookie(ClubRes3);

								
				
	
	 }
}
