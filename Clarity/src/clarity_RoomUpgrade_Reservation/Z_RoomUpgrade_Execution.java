package clarity_RoomUpgrade_Reservation;

import org.testng.annotations.Test;

public class Z_RoomUpgrade_Execution {

	B_RoomUpgrade_Login Login = new B_RoomUpgrade_Login();
	C_RoomUpgrade_Customer360_LeadInfo Leadinfo = new C_RoomUpgrade_Customer360_LeadInfo();
	D_RoomUpgrade_Reservation Reservation = new D_RoomUpgrade_Reservation();
	E_RoomUpgrade_ClubRes ClubRes = new E_RoomUpgrade_ClubRes();
	F_RoomUpgrade_BookReservation BookRes = new F_RoomUpgrade_BookReservation();
	G_RoomUpgrade_RoomUpgrade RoomUpg = new G_RoomUpgrade_RoomUpgrade();
	H_RoomUpgrade_MOPPayments MOP = new H_RoomUpgrade_MOPPayments();
	I_RoomUpgrade_ClubReservations Reserve = new I_RoomUpgrade_ClubReservations();
	J_RoomUpgrade_FinalConfirmation ResConfirm = new J_RoomUpgrade_FinalConfirmation();
	
	
	@Test
	public void RoomUpgrade_Reservation()
	{
		Login.openBrowser();
			Leadinfo.Customer360();
				Leadinfo.Lead_Information();
					Reservation.Reservations();
						ClubRes.ClubRes();
							BookRes.BookReservation();
								BookRes.Resort_Availability();
									RoomUpg.Room_Upgrade_Yes();
										BookRes.Errata();
											BookRes.Discount_No();
												MOP.MethodofPaymentOptions();
													Reserve.Reservation();
														ResConfirm.Reservation_Confirmation();
	}
	
	
			
}
