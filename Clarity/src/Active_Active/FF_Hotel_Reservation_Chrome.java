package Active_Active;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FF_Hotel_Reservation_Chrome {

WebDriver driver;
	
	String HotelResLead = "224688";
	String HotelResLeadArea = "9";
	String HotelReservationType = "REN";
	String HotelMKCode = "RENDRIWEB";
	
	String CCNumber = "4321000000001119";
	String CCType = "Visa";
	String CCName = "Diamond Tester";
	String ExpMonth = "01";
	String ExpYr = "20";
	Logger logger = Logger.getLogger("F_Hotel_Reservation.class");
	
	@BeforeTest
	public void setup() {
		System.setProperty("webdriver.chrome.driver","C:\\apache-jmeter-2.13\\chromedriver.exe");
		driver =  new ChromeDriver();
		
	}

	@Test(priority=1)
	public void openBrowser() throws InterruptedException  {
		
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		 
		  PropertyConfigurator.configure("Log4j.properties");
			//driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL STAGE
			  //driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL TEST
			 driver.get("https://clarityactive.diamondresorts.com/pls/clarityactive"); // URL ACTIVE ACTIVE
				driver.manage().window().maximize();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan22");// Entering Password
									logger.info("Entered Password");
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													Thread.sleep(2000);	
													
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")));
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 												
								
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(HotelResLead); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+HotelResLead);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a Member");
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div")));
															driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
															//Thread.sleep(1000);
															//driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
														
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					 }
				 }

			 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
			 Thread.sleep(1000);
			 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
			 		logger.info("clicked on Reservations Tab");
			 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			 			
			 			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='travelDiv']/table/tbody/tr/td[4]/div")));
			 			
						//Opens New Window
						driver.findElement(By.xpath(".//*[@id='travelDiv']/table/tbody/tr/td[4]/div")).click(); //Clicked on HotelRes
					 		logger.info("clicked on Hotel Res");
				 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

						// Switch to new window opened
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ren_buttonSearch']"))); //Wait till search button is clickable
		
		//Reservation Type
		driver.findElement(By.xpath(".//*[@id='rt_pvResvType_input']")).click();
			Select ReserType = new Select(driver.findElement(By.xpath(".//*[@id='rt_pvResvType_select']")));
				ReserType.selectByValue(HotelReservationType);
					logger.info("Reservation Type is REN - HOTEL Stay");
		
				//Marketing Key Code
				driver.findElement(By.xpath(".//*[@id='rt_pMARKETINGKEYCODE_input']")).click();
					Select Markeycode = new Select(driver.findElement(By.xpath(".//*[@id='rt_pMARKETINGKEYCODE_select']")));
						Markeycode.selectByValue(HotelMKCode);
							logger.info("Marketing Key Code is RENDRIWEB - General Reservation");
						
				//Property
				driver.findElement(By.xpath(".//*[@id='caw_property_input']")).click();
					driver.findElement(By.xpath(".//*[@id='caw_property_input']")).sendKeys("SST");
						logger.info("Selected Property as SST ");
					
				//Arrival Date
				driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).click();
					driver.findElement(By.xpath(".//*[@id='caw_arrival_date']")).sendKeys("t+10");
					
				//Nights	
				driver.findElement(By.xpath(".//*[@id='caw_nights']")).click();	
				Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='caw_nights']")).sendKeys("2");
						logger.info("Nights - 2");

				//Adults
				driver.findElement(By.xpath(".//*[@id='caw_num_adults']")).click();
					driver.findElement(By.xpath(".//*[@id='caw_num_adults']")).sendKeys("2");
						logger.info("Adults - 2");
						
						Thread.sleep(5000);
						
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
						logger.info("Clicked on Search");
						
						Thread.sleep(2000);
				
				//Search
				driver.findElement(By.xpath(".//*[@id='ren_buttonSearch']")).click();
					logger.info("Clicked on Search");
	
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='caw_DepRenContinue']"))); //Wait till the flag image is clickable
				
				driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody[1]/tr[1]/td[6]/select/option[2]")).click();	//Select Room as 1
					logger.info("Selected Room");
						Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody[1]/tr[1]/td[11]/span[1]")).click(); //Click the [+]
					logger.info("Clicked on [+]");
													
				driver.findElement(By.xpath(".//*[@id='ResAvailResults']/tbody[1]/tr[2]/td[5]/input")).click();
					logger.info("Selected the best Available Rate");
				
				driver.findElement(By.xpath(".//*[@id='caw_DepRenContinue']")).click();
					logger.info("Clicked on Continue");
					
				//Additional Resort Information
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); //Wait till the Close button is clickable
					driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();
						logger.info("Clicked on Additional Resort Information Close Button");
						
				//Hotel Reservations
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); //Wait till the save button is clickable
				Thread.sleep(1000);
						//Reservation Advance Deposit in Financial Summary
						driver.findElement(By.xpath(".//*[@id='folioImg']")).click();
							Thread.sleep(1000);
							//TransType
							driver.findElement(By.xpath(".//*[@id='pvTransType_input']")).click();	
								Select TransType = new Select(driver.findElement(By.xpath(".//*[@id='pvTransType_select']")));
									TransType.selectByValue("RP");
										logger.info("Selected TransType as Request/Payment");
											Thread.sleep(3000);
											
							//CreditCard
							driver.findElement(By.xpath(".//*[@id='pv_pay_cc_input']")).click();
								Select CreditCard = new Select(driver.findElement(By.xpath(".//*[@id='pv_pay_cc_select']")));
									CreditCard.selectByValue("0");
										wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='CCSaveBtn']"))); //Wait till the save button is clickable
										
										//-------Add Credit Card---------//
										
										//Credit Card Number
										driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
											driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
												driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
											
										//Card Holder Name
											driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
										driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
										
										//Exp mnth & Year
										driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
											Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
												ExpMth.selectByValue(ExpMonth);// Exp Mnth
										
										driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
											Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
												ExpYear.selectByValue(ExpYr);// Exp Year
											
										//Active
										driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
											Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
												Active.selectByValue("Y");// Active
													driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
										
										//DRI Credit Card Y/N
										driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
											Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
												Dricc.selectByValue("N");// DRI Credit Card	
										
										//Click on Save Button
										driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
											logger.info("Clicked on Save Button");
											
										//Successfully Added Message
											driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
												logger.info("Clicked on Ok");
								//Item Master
								driver.findElement(By.xpath(".//*[@id='pvItemMaster_input']")).click();
								Select ItemMaster = new Select(driver.findElement(By.xpath(".//*[@id='pvItemMaster_select']")));
								ItemMaster.selectByValue("10122");
								logger.info("Item Master as 10122 - Visa Payment");
								
								//Amount
								driver.findElement(By.xpath(".//*[@id='pv_Amount']")).sendKeys("171");
								logger.info("Amount Entered");
								
								//Click Save
								driver.findElement(By.xpath(".//*[@id='advDepositSaveButton']")).click();
								logger.info("Clicked on Save");
								
								//Click Save again in Form of Payment 
								driver.findElement(By.xpath(".//*[@id='FolCCSave']")).click();
									logger.info("Clicked on Save in Form of payment widget");
									
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div")));
									String PaymentApproved = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
										logger.info(""+PaymentApproved);
											driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
											logger.info("Clicked on Ok Button");
											
									driver.findElement(By.xpath(".//*[@id='BackBtnAdvDep']")).click();
										logger.info("Clicked on Close button in advance deposits screen");
										
								
								//Click on Form of Payment Credit Card Flag Image
								driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[11]/td[2]/span[2]/img")).click();
									logger.info("Clicked on Credit Card Flag Image");
									
										//Click on Save
										driver.findElement(By.xpath(".//*[@id='rentalFormOfPaymentWidgetSaveButton']")).click();	
											logger.info("Clicked on Save Button");
										
							//Click on Save to Book the reservation
							driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click();
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RENResvConf_close']"))); //Wait till the close button is clickable on reservation confirmation screen
								
							String ResConfirmation = driver.findElement(By.xpath(".//*[@id='resConfHeaderText']")).getText();
								logger.info(""+ResConfirmation);
								
								String ResConNum = ResConfirmation.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
								logger.info("Reservation Confirmation number for Hotel Reservations: "+ResConNum);
	}					

}

