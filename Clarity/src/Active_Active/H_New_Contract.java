package Active_Active;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class H_New_Contract extends A_Common {
	
Logger logger = Logger.getLogger("H_New_Contract.class");


	@Test(priority=1)
	public void openBrowser() throws InterruptedException  {
		
		  PropertyConfigurator.configure("Log4j.properties");
			//driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL STAGE
			  driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL TEST
			 //driver.get("http://clarityactive.diamondresorts.com/pls/clarityactive"); // URL ACTIVE ACTIVE
				driver.manage().window().maximize();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys(Username); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys(Password);// Entering Password
									logger.info("Entered Password");
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													Thread.sleep(2000);	
												
	}
	
	@Test(priority=2)
	public void Contracts() throws InterruptedException 
	{
		//Contract
		WebElement Contract = wait.until(ExpectedConditions.elementToBeClickable(By.id("120"))); // Wait till the Contracts is available to click
		Thread.sleep(1000);
		Contract.click(); // Click on Contracts
			logger.info("Clicked on Contracts"); //Click on Contracts
		
		//New Contract
		WebElement ContractEntry = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='1058']/span"))); // Wait till the ContractEntry is available to click
		ContractEntry.click(); // Click on New Contract	
			logger.info("Clicked on ContractEntry");
			
			logger.info("//-----------------------------------------------------------------Contract Entry----------------------------------------------------------------//");
		//-----------------------------------------------------------------Contract Entry----------------------------------------------------------------//
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadLookupButton']"))); //Lead Button is clickable
		Thread.sleep(1000);
		
		driver.findElement(By.id("leadLookupButton")).click(); //Lead Lookup button
			logger.info("Clicked on Lead Lookup button");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='newLeadSearchbtn']"))); //Search Button is clickable
			
			driver.findElement(By.xpath(".//*[@id='pvLeadArea_input']")).click(); //Lead Area
				Select LeadArea = new Select(driver.findElement(By.xpath(".//*[@id='pvLeadArea_select']")));
					LeadArea.selectByValue(ContractLeadArea);
						logger.info("Lead Area: "+ContractLeadArea);
						
			driver.findElement(By.xpath(".//*[@id='pvLeadNumber']")).click(); //Lead Number
				driver.findElement(By.xpath(".//*[@id='pvLeadNumber']")).sendKeys(ContractLeadnumber); //Lead Number
					logger.info("LeadNumber: "+ContractLeadnumber);
					
			driver.findElement(By.xpath(".//*[@id='newLeadSearchbtn']")).click();
				logger.info("Clicked on Search");
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='LSAWbtnSave']"))); //Save Button is clickable
			
			//Check box
			List<WebElement> Checkbox = driver.findElements(By.name("lead_radio"));
				int size = Checkbox.size();
					logger.info("Check boxes: "+size);
					for(int i=0;i<size;i++)
						{
							String value = Checkbox.get(i).getAttribute("name");
				
								if(value.equalsIgnoreCase("lead_radio"))
									{
										Checkbox.get(i).click();
											logger.info("Clicked on First Check box");
												break;
									}
						}
	
			driver.findElement(By.xpath(".//*[@id='LSAWbtnSave']")).click(); //Click on Save Button
				logger.info("Clicked on Save Button"); 
				Thread.sleep(1000);
			
			//Sales Center
			driver.findElement(By.xpath(".//*[@id='sales_center_input']")).click(); //Sales Center
				Select SalesCenter = new Select(driver.findElement(By.xpath(".//*[@id='sales_center_select']"))); //select sales center
					SalesCenter.selectByValue(ContractSalesCenter);
				
				driver.findElement(By.xpath(".//*[@id='contract_type_input']")).click(); //Contract Type
				
				ContractSalesCenterName = driver.findElement(By.xpath(".//*[@id='sales_center_input']")).getAttribute("value"); //Sales Center Name
					logger.info("Sales Center is: "+ContractSalesCenterName);
					
					driver.findElement(By.xpath(".//*[@id='searchButton']")).click(); //Search button
						logger.info("Clicked on Search Button");
					
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='addNewContract']"))); //Wait Till Add Contract is clickable
			
			driver.findElement(By.xpath(".//*[@id='addNewContract']")).click(); //Add Contract Button
				logger.info("Clicked on Add Contract Button");
				
	}

	@Test(priority=3)
	public void ContractEntry() throws InterruptedException {
		
	//--------------------------------------------------Contract Information----------------------------------------------------------------//
		
				logger.info("//--------------------------------------------------Contract Information----------------------------------------------------------------//");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='contract_type_input']"))); //Wait till Contract Type is clickable
				
				//Contract Type
				driver.findElement(By.xpath(".//*[@id='contract_type_input']")).click();	
					Select ContractEntry = new Select(driver.findElement(By.xpath(".//*[@id='contract_type_select']")));
						ContractEntry.selectByValue(ContractContractType);
							logger.info("Contract Type is  :"+ContractContractType);
							
				//Contract Number
				ContractNumber = driver.findElement(By.xpath(".//*[@id='user_contract_number']")).getAttribute("value");
					logger.info("ContractNumber is :"+ContractNumber);
					
				//Sales Center
				driver.findElement(By.xpath(".//*[@id='contract_sc_input']")).click(); //Sales Center
					Select SalesCenter = new Select(driver.findElement(By.xpath(".//*[@id='contract_sc_select']"))); //select sales center
						SalesCenter.selectByValue(ContractSalesCenter);
							logger.info("Sales Center is :"+ContractSalesCenterName);
							
				//Purchase Type
				driver.findElement(By.xpath(".//*[@id='purchase_type_input']")).click(); //Sales Center
					Select PurchaseType = new Select(driver.findElement(By.xpath(".//*[@id='purchase_type_select']"))); //select sales center
						PurchaseType.selectByValue(ContractPurchaseType);
							logger.info("Purchase Type :"+ContractSalesCenterName);
							
				//Purchase Date
				ContractPurchaseDate = driver.findElement(By.xpath(".//*[@id='purchase_date']")).getAttribute("value");
					logger.info("Purchase Date: "+ContractPurchaseDate);
					
					if(ContractPurchaseDate.equals(""))
					{
						logger.info("Tour has not been purchased with this lead");
						driver.quit();
					}
							
				//Vesting
				ContractVesting = driver.findElement(By.xpath(".//*[@id='vesting_input']")).getAttribute("value");
					logger.info("Vesting :"+ContractVesting);
			
		//---------------------------------------------------Lead Information------------------------------------------------------------------------//
					driver.findElement(By.xpath(".//*[@id='addlLeadInfo1']/div[2]")).click();	
						logger.info("Clicked on Edit Lead Info");
						
					Thread.sleep(1000);
					
					//First Name
					driver.findElement(By.xpath(".//*[@id='primary_first_name']")).click();
						driver.findElement(By.xpath(".//*[@id='primary_first_name']")).clear();
							driver.findElement(By.xpath(".//*[@id='primary_first_name']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractFirstName);
								logger.info("Firstname is :"+ContractFirstName);
					
						//Last Name
						driver.findElement(By.xpath(".//*[@id='primary_last_name']")).click();
							driver.findElement(By.xpath(".//*[@id='primary_last_name']")).clear();
								driver.findElement(By.xpath(".//*[@id='primary_last_name']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractLastName);
									logger.info("Lastname is :"+ContractLastName);
								
					//Fico Score
					driver.findElement(By.xpath(".//*[@id='primary_fico']")).sendKeys(ContractFicoScore);
						logger.info("Fico Score is :"+ContractFicoScore);
						
					//LegalTitle
					driver.findElement(By.xpath(".//*[@id='primary_legal_title1_input']")).click();
						Select LegalTitle = new Select(driver.findElement(By.xpath(".//*[@id='primary_legal_title1_select']")));
							LegalTitle.selectByValue(ContractLegalTitle);
								
						
					//Legal First Name
					driver.findElement(By.xpath(".//*[@id='primary_legal_first_name']")).click();
						driver.findElement(By.xpath(".//*[@id='primary_legal_first_name']")).clear();
							driver.findElement(By.xpath(".//*[@id='primary_legal_first_name']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractFirstName);
								logger.info("Legal First Name: "+ContractFirstName);
						
					//Legal Last Name
					driver.findElement(By.xpath(".//*[@id='primary_legal_last_name']")).click();
						driver.findElement(By.xpath(".//*[@id='primary_legal_last_name']")).clear();
							driver.findElement(By.xpath(".//*[@id='primary_legal_last_name']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractLastName);
								logger.info("Legal Last Name: "+ContractLastName);
						
					//Address
					driver.findElement(By.xpath(".//*[@id='address_addr1']")).click();
						driver.findElement(By.xpath(".//*[@id='address_addr1']")).clear();
							driver.findElement(By.xpath(".//*[@id='address_addr1']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractAddress);
								logger.info("Address is: "+ContractAddress);
						
					//Country
					driver.findElement(By.xpath(".//*[@id='address_country1_input']")).click();
						Select Country = new Select(driver.findElement(By.xpath(".//*[@id='address_country1_select']")));
							Country.selectByValue(ContractCountry);
								logger.info("Country is :"+ContractCountry);
						
					
					//Postal/Zip Code:
					driver.findElement(By.xpath(".//*[@id='address_postal1']")).click();
						driver.findElement(By.xpath(".//*[@id='address_postal1']")).clear();
							driver.findElement(By.xpath(".//*[@id='address_postal1']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractPostal);
								logger.info("Postal is: "+ContractPostal);
								
					driver.findElement(By.xpath(".//*[@id='address_county1_input']")).click();
						
					//Legal Type 
					ContractLegal_Title = driver.findElement(By.xpath(".//*[@id='primary_legal_title1_input']")).getAttribute("value");
						logger.info("Legal Title is :"+ContractLegal_Title);
						
					Thread.sleep(1000);
					
					//Same Mailing Address (Check Box)
						driver.findElement(By.xpath(".//*[@id='copyAddress1']")).click();
						logger.info("Clicked on Same Mailing Address Check Box");
						
					//City
					ContractCity = driver.findElement(By.xpath(".//*[@id='address_city1']")).getAttribute("value");
					logger.info("City is: "+ContractCity);
			
			//--------------------------------------------------------Inventory Selection--------------------------------------------------------------------//
					logger.info("//--------------------------------------------------------Inventory Selection--------------------------------------------------------------------//");
					
					//Project
					driver.findElement(By.xpath(".//*[@id='project_input']")).click(); //Project
						Select Project = new Select(driver.findElement(By.xpath(".//*[@id='project_select']"))); //Select Project
							Project.selectByValue(ContractProject);
								logger.info("Selected Project");
							
							Thread.sleep(1000);
								
					//Usage Sell Code
					driver.findElement(By.xpath(".//*[@id='usage_sell_input']")).click(); //Usage Sell Code
						Select UsagSellCode = new Select(driver.findElement(By.xpath(".//*[@id='usage_sell_select']"))); //Select Usage Sell Code
							UsagSellCode.selectByValue(ContractUsageSellCode);
								logger.info("Selected Usage Sell Code");
							Thread.sleep(1000);
				
					//Group
					driver.findElement(By.xpath(".//*[@id='group_input']")).click(); //Group
						Select Group = new Select(driver.findElement(By.xpath(".//*[@id='group_select']"))); //Select Group
							Group.selectByValue(ContractGroup);
								logger.info("Selected Group");
							
					//Points Package
					driver.findElement(By.xpath(".//*[@id='points_package']")).click();		
						driver.findElement(By.xpath(".//*[@id='points_package']")).sendKeys(ContractPointPackage);
						
						driver.findElement(By.xpath(".//*[@id='club_membership_input']")).click();
						
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); //Ok button

						String Message = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
							logger.info(Message);
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
									logger.info("Clicked on OK");
					
					Contract_Project = driver.findElement(By.xpath(".//*[@id='project_input']")).getAttribute("value");
					 logger.info("Project :"+Contract_Project);
					 
					 Contract_UsageSellCode = driver.findElement(By.xpath(".//*[@id='usage_sell_input']")).getAttribute("value");
					 	logger.info("Usage Sell Code :"+Contract_UsageSellCode);
					
				 	 Contract_Group = driver.findElement(By.xpath(".//*[@id='group_input']")).getAttribute("value");
						 	logger.info("Group :"+Contract_Group);
						 	
					

			//----------------------------------------------------Owner Services Setup-------------------------------------------------------------------------------//
					logger.info("//----------------------------------------------------Owner Services Setup-------------------------------------------------------------------------------//");
					
			String ContractClubMembership = "CS1";
			String ContractMainFees = "5";
			String ContractLinkthisContract = "-1";
			
					//Club Membership
					driver.findElement(By.xpath(".//*[@id='club_membership_input']")).click(); //Club Membership
						Select ClubMembership = new Select(driver.findElement(By.xpath(".//*[@id='club_membership_select']"))); //Select Club Membership
							ClubMembership.selectByValue(ContractClubMembership);
								logger.info("Club Membership: "+ContractClubMembership);
								
					//Maintenance Fees
					driver.findElement(By.xpath(".//*[@id='maint_fee']")).click();
						driver.findElement(By.xpath(".//*[@id='maint_fee']")).clear();
							driver.findElement(By.xpath(".//*[@id='maint_fee']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractMainFees);
								logger.info("Maintenance Fees :"+ContractMainFees);
								
					//Link This Contract To
					driver.findElement(By.xpath(".//*[@id='link_contract_to_input']")).click(); //Link This Contract To
						Select LinkContract = new Select(driver.findElement(By.xpath(".//*[@id='link_contract_to_select']"))); //Select Link This Contract To
							LinkContract.selectByValue(ContractLinkthisContract);
								logger.info("Linked Contract to Create New Membership");
								
			Thread.sleep(1000);
			
			//----------------------------------------------------Incentives and Fees--------------------------------------------------------------------------------------------//
			logger.info("//----------------------------------------------------Incentives and Fees--------------------------------------------------------------------------------------------//");
			
			driver.findElement(By.xpath("/html/body/div/div[2]/div[4]/div/div[11]/div/div/div/div[2]/div[2]/table/tbody/tr[1]/td[1]/input")).click();	
				logger.info("Clicked on the first option");
					
			//----------------------------------------------------Inventory Pricing--------------------------------------------------------------------------------------------//
			logger.info("//----------------------------------------------------Inventory Pricing--------------------------------------------------------------------------------------------//");		
			
			//Contract Currency
			ContractCurrency = driver.findElement(By.xpath(".//*[@id='contract_currency_input']")).getAttribute("value");
				logger.info("Contract Currency :"+ContractCurrency);
				
			//List Price
			ContractListPrice = driver.findElement(By.xpath(".//*[@id='base_list_price']")).getAttribute("value");
				logger.info("List Price :"+ContractListPrice);
				
			//Purchase Amount
			ContractPurchaseAmount = driver.findElement(By.xpath(".//*[@id='base_purchase_amount']")).getAttribute("value");
				logger.info("Purchase Amount :"+ContractPurchaseAmount);
				
			//Gross Purchase Price
			ContractGrossPurchasePrice = driver.findElement(By.xpath(".//*[@id='base_gross_price']")).getAttribute("value");
				logger.info("Gross Purchase Price :"+ContractGrossPurchasePrice);
				
			//Finance Type Selection
			driver.findElement(By.xpath(".//*[@id='finance_type_input']")).click(); //
				Select Finance = new Select(driver.findElement(By.xpath(".//*[@id='finance_type_select']")));
					Finance.selectByValue(FinanceTypeSelection);
						logger.info("Finance Type Selection :"+FinanceTypeSelection);
			
			//----------------------------------------------------Down Payment and Financing-------------------------------------------------------------------------------------------//
			logger.info("//-----------------------------------------------------Down Payment and Financing--------------------------------------------------------------------------------------------//");
			
			//Deposit Amoint Base Cur:
			driver.findElement(By.xpath(".//*[@id='base_deposit_amount']")).click();
				driver.findElement(By.xpath(".//*[@id='base_deposit_amount']")).clear();
					driver.findElement(By.xpath(".//*[@id='base_deposit_amount']")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ContractListPrice);
						logger.info("Base Cur: "+ContractListPrice);
						
							//Start Date
							driver.findElement(By.xpath(".//*[@id='deposit_date']")).sendKeys("t");
							
							//Payment Method
							Thread.sleep(1000);
								driver.findElement(By.xpath(".//*[@id='deposit_payment_input']")).click();	
									Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='deposit_payment_select']")));
										Payment.selectByValue(ContractPaymentMethod);
											logger.info("Payment Method :"+ContractPaymentMethod);
												Thread.sleep(1000);					
							//[+]
							driver.findElement(By.xpath(".//*[@id='addDeposit']")).click();
								logger.info("Clicked on [+] to add Deposit");
									
						
			//Term
			ContractTerm = driver.findElement(By.xpath(".//*[@id='term']")).getAttribute("value");
				logger.info("Term :"+ContractTerm);	
				
			//Frequency
			ContractFrequency = driver.findElement(By.xpath(".//*[@id='frequency']")).getAttribute("value");
				logger.info("Frequency :"+ContractFrequency);	
				
			//Interest
			ContractInterest = driver.findElement(By.xpath(".//*[@id='interest_rate']")).getAttribute("value");
				logger.info("Interest :"+ContractInterest);		
				
			//FirstPayment
			ContractFirstPayment = driver.findElement(By.xpath(".//*[@id='first_payment']")).getAttribute("value");
				logger.info("FirstPayment :"+ContractFirstPayment);		
				
			//LoanServiceFee
			ContractLoanServiceFee = driver.findElement(By.xpath(".//*[@id='base_loan_service_fee']")).getAttribute("value");
				logger.info("Loan Service Fee :"+ContractLoanServiceFee);
				
				Thread.sleep(1000);
				
		//PROCESS
			driver.findElement(By.xpath(".//*[@id='processContractButton']")).click();
				logger.info("Clicked on Process Button");
				
	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='closeScreenButton']"))); //Wait till search button is clickable
			
			driver.findElement(By.xpath(".//*[@id='closeScreenButton']")).click();
				logger.info("Clicked on Close button in clarity");
	}
			
	@Test(priority=4)
	public void DatabaseConfirm1() throws SQLException {
				 			  
				 //Initialize
					try {
				
						Class.forName("oracle.jdbc.driver.OracleDriver");
				
					} catch (ClassNotFoundException e) {
				
						logger.info("Oracle JDBC server not found");
						e.printStackTrace();
						return;
					}
					
					logger.info("Oracle JDBC Driver Registered!");
				
					//Connect to Database
					try {
				
						//connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=on)(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1551)))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "sreddy","bhushan22");
						connection = DriverManager.getConnection("jdbc:oracle:thin:@10.127.103.155:1541:TEST1", "sreddy","bhushan22"); //Test
				
					} catch (SQLException e) {
				
						logger.info("Connection Failed! Check output console");
						e.printStackTrace();
						return;
					}
				
					//Retrive data
					if (connection != null) {
						logger.info("Logged into DB Successfully");
						
						logger.info("//--------------------------------------------------------First Query-------------------------------------------------------------------------------------//");
						
						//Query
						prepared = connection.prepareStatement("SELECT PC.CONTR_NUM, PC.OWNER_GROUP_CODE, PC.FINANCE_TYPE_CODE, PC.CONTRACT_TYPE_CODE, PC.PURCH_TYPE_CODE, PC.VESTING_CODE, PC.CREATED_BY FROM P_CONTRACT PC WHERE PC.CONTR_NUM = ?");
						//Enter Confirmation Number in Query
						prepared.setString(1, ContractNumber);
						//Execute the query		
						prepared.executeUpdate();
						
						while (prepared.getResultSet().next()) {
							
							String CONTR_NUM = prepared.getResultSet().getString("CONTR_NUM"); 
							
							String OWNER_GROUP_CODE = prepared.getResultSet().getString("OWNER_GROUP_CODE"); 
							
							String FINANCE_TYPE_CODE = prepared.getResultSet().getString("FINANCE_TYPE_CODE");
							
							String CONTRACT_TYPE_CODE = prepared.getResultSet().getString("CONTRACT_TYPE_CODE");
							
							String PURCH_TYPE_CODE = prepared.getResultSet().getString("PURCH_TYPE_CODE");
							
							String VESTING_CODE = prepared.getResultSet().getString("VESTING_CODE");
							
							logger.info("------------------------------------------From Clarity------------------------------------------------");
								logger.info("\nContractNumber: " + ContractNumber + "\nSalesCenterCode: " +SalesCenterCode+ "\nFinanceTypeSelection: " +FinanceTypeSelection+ "\nContractContractType: " +ContractContractType+ "\nContractPurchaseType: " +ContractPurchaseType+ "\nContractVestingValue: " +ContractVestingValue);
							
							logger.info("------------------------------------------From TOAD------------------------------------------------");
								logger.info("\nCONTR_NUM: " + CONTR_NUM + "\nOWNER_GROUP_CODE: " + OWNER_GROUP_CODE +  "\nFINANCE_TYPE_CODE: " +FINANCE_TYPE_CODE+  "\nCONTRACT_TYPE_CODE: " +CONTRACT_TYPE_CODE+  "\nPURCH_TYPE_CODE: " +PURCH_TYPE_CODE+  "\nVESTING_CODE: " +VESTING_CODE);
							
							Assert.assertEquals(ContractNumber, CONTR_NUM);
							 	logger.info("Assertion Passed ContractNumber: "+CONTR_NUM);
							 
							 Assert.assertEquals(SalesCenterCode, OWNER_GROUP_CODE);
							 	logger.info("Assertion Passed SalesCenterCode: "+OWNER_GROUP_CODE);
							 	
						 	Assert.assertEquals(FinanceTypeSelection, FINANCE_TYPE_CODE);
							 	logger.info("Assertion Passed Finance Type Selection: "+ToursTourType);
							 	
						 	Assert.assertEquals(ContractContractType, CONTRACT_TYPE_CODE);
							 	logger.info("Assertion Passed Contract Type: "+CONTRACT_TYPE_CODE);
							 	
						 	Assert.assertEquals(ContractPurchaseType, PURCH_TYPE_CODE);
							 	logger.info("Assertion Passed Purchase Type: "+PURCH_TYPE_CODE);
							 	
						 	Assert.assertEquals(ContractVestingValue, VESTING_CODE);
							 	logger.info("Assertion Passed Vesting: "+VESTING_CODE);
					
							}
							connection.close();
						}
						
						else 
						{
						logger.info("Failed to make connection!");
						}
			}
			
	@Test(priority=5)
	public void DatabaseConfirm2() throws SQLException {
				 			  
		 //Initialize
			try {
		
				Class.forName("oracle.jdbc.driver.OracleDriver");
		
			} catch (ClassNotFoundException e) {
		
				logger.info("Oracle JDBC server not found");
				e.printStackTrace();
				return;
			}
			
			logger.info("Oracle JDBC Driver Registered!");
		
			//Connect to Database
			try {
		
				//connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=on)(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1551)))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "sreddy","bhushan22");
				connection = DriverManager.getConnection("jdbc:oracle:thin:@10.127.103.155:1541:TEST1", "sreddy","bhushan22"); //Test
			} catch (SQLException e) {
		
				logger.info("Connection Failed! Check output console");
				e.printStackTrace();
				return;
			}
		
		//Retrive data
		if (connection != null) {
				logger.info("Logged into DB Successfully");
	
				logger.info("//--------------------------------------------------------Second Query-------------------------------------------------------------------------------------//");
				
				//Query
				prepared = connection.prepareStatement("SELECT PCL.CONTR_NUM, PCL.LEAD_ID, PCL.LEGAL_LAST_NAME1, PCL.LEGAL_FIRST_NAME1, PCL.FICO_SCORE_1, PCL.PHYSICAL_ADDR1 FROM P_CONTRACT_LEAD PCL WHERE CONTR_NUM = ?");
				//Enter Confirmation Number in Query
				prepared.setString(1, ContractNumber);
				//Execute the query
				prepared.executeUpdate();
				
				while (prepared.getResultSet().next()) {
					
					String CONTR_NUM = prepared.getResultSet().getString("CONTR_NUM"); 
					
					String LEAD_ID	 = prepared.getResultSet().getString("LEAD_ID");
		
					String LEGAL_LAST_NAME1 = prepared.getResultSet().getString("LEGAL_LAST_NAME1"); 
					
					String LEGAL_FIRST_NAME1 = prepared.getResultSet().getString("LEGAL_FIRST_NAME1"); 
					
					String FICO_SCORE_1 = prepared.getResultSet().getString("FICO_SCORE_1");
					
					String PHYSICAL_ADDR1 = prepared.getResultSet().getString("PHYSICAL_ADDR1"); 		
					
					logger.info("------------------------------------------From Clarity------------------------------------------------");
					logger.info("\nContractNumber: " + ContractNumber + "\nContractLeadnumber: " + ContractLeadnumber +  "\nContractLastName: " +ContractLastName+ "\nContractFirstName: " + ContractFirstName + "\nContractFicoScore: " + ContractFicoScore +  "\nContractAddress: " +ContractAddress);
					
					logger.info("------------------------------------------From TOAD------------------------------------------------");
					logger.info("\nCONTR_NUM: " + CONTR_NUM + "\nLEAD_ID: " + LEAD_ID +  "\nLEGAL_LAST_NAME1: " +LEGAL_LAST_NAME1+ "\nLEGAL_FIRST_NAME1: " + LEGAL_FIRST_NAME1 + "\nFICO_SCORE_1: " + FICO_SCORE_1 +  "\nPHYSICAL_ADDR1: " +PHYSICAL_ADDR1);
					
					Assert.assertEquals(ContractNumber, CONTR_NUM);
				 		logger.info("Assertion Passed ContractNumber: "+CONTR_NUM);
				 		
			 		Assert.assertEquals(ContractLeadnumber, LEAD_ID);
				 		logger.info("Assertion Passed Leadnumber: "+LEAD_ID);
				 		
			 		Assert.assertEquals(ContractLastName, LEGAL_LAST_NAME1);
				 		logger.info("Assertion Passed LastName: "+LEGAL_LAST_NAME1);
				 		
			 		Assert.assertEquals(ContractFirstName, LEGAL_FIRST_NAME1);
				 		logger.info("Assertion Passed FirstName: "+LEGAL_FIRST_NAME1);
				 		
			 		Assert.assertEquals(ContractFicoScore, FICO_SCORE_1);
				 		logger.info("Assertion Passed FicoScore: "+FICO_SCORE_1);
				 		
			 		Assert.assertEquals(ContractAddress, PHYSICAL_ADDR1);
			 			logger.info("Assertion Passed Address: "+PHYSICAL_ADDR1);

				}
			}
			else 
			{
			logger.info("Failed to make connection!");
			}
	}
			
	@Test(priority=6)
	public void DatabaseConfirm3() throws SQLException {
				 			  
		 //Initialize
			try {
		
				Class.forName("oracle.jdbc.driver.OracleDriver");
		
			} catch (ClassNotFoundException e) {
		
				logger.info("Oracle JDBC server not found");
				e.printStackTrace();
				return;
			}
			
			logger.info("Oracle JDBC Driver Registered!");
		
			//Connect to Database
			try {
		
				//connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=on)(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1551)))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "sreddy","bhushan22");
				connection = DriverManager.getConnection("jdbc:oracle:thin:@10.127.103.155:1541:TEST1", "sreddy","bhushan22"); //Test			
			} catch (SQLException e) {
		
				logger.info("Connection Failed! Check output console");
				e.printStackTrace();
				return;
			}
		
		//Retrive data
		if (connection != null) {
				logger.info("Logged into DB Successfully");
	
				logger.info("//--------------------------------------------------------Third Query-------------------------------------------------------------------------------------//");
				
				//Query
				prepared = connection.prepareStatement("SELECT  PCP.CONTR_NUM,  PCP.PROJECT_ID,  PCP.USAGE_SELL_CODE, PCP.BASE_PURCHASE_AMT,  PCP.BASE_LIST_PRICE, PCP.FOREIGN_PURCHASE_AMT, PCP.FOREIGN_LIST_PRICE, PCP.POINTS_PURCHASED,  PCP.CLUB_MBR_TYPE,  PCP.PURCHASE_AMT, PCP.NET_PURCHASE_AMT FROM p_contract_purchase PCP WHERE PCP.CONTR_NUM = ?");
				//Enter Confirmation Number in Query
				prepared.setString(1, ContractNumber);
				//Execute the query
				prepared.executeUpdate();
				
				while (prepared.getResultSet().next()) {
					
					String CONTR_NUM = prepared.getResultSet().getString("CONTR_NUM"); 
					
					String PROJECT_ID	 = prepared.getResultSet().getString("PROJECT_ID");
		
					String USAGE_SELL_CODE = prepared.getResultSet().getString("USAGE_SELL_CODE"); 

					String POINTS_PURCHASED = prepared.getResultSet().getString("POINTS_PURCHASED"); 	
					
					String CLUB_MBR_TYPE = prepared.getResultSet().getString("CLUB_MBR_TYPE"); 	
					
					String PURCHASE_AMT = prepared.getResultSet().getString("PURCHASE_AMT"); 	
						DecimalFormat decimal = new DecimalFormat("0.00"); //Decimal Format. Decimal Format is used to match the retrieved values from databse to match with values retrieved from clarity.																			 //Example: Clarity value = 0.00, Oracle value is 0. To match both values the oracle value will be changed to 0.00.
							String Purchase_Amount = decimal.format(PURCHASE_AMT);
			 		
					
					logger.info("------------------------------------------From Clarity------------------------------------------------");
					logger.info("\nContractNumber: " + ContractNumber + "\nContractProject: " + ContractProject +  "\nContractUsageSellCode: " +ContractUsageSellCode+ "\nContractPointPackage: " + ContractPointPackage + "\nContractClubMembership: " + ContractClubMembership +  "\nContractPurchaseAmount: " +ContractPurchaseAmount);
					
					logger.info("------------------------------------------From TOAD------------------------------------------------");
					logger.info("\nCONTR_NUM: " + CONTR_NUM + "\nPROJECT_ID: " + PROJECT_ID +  "\nUSAGE_SELL_CODE: " +USAGE_SELL_CODE+ "\nPOINTS_PURCHASED: " + POINTS_PURCHASED + "\nCLUB_MBR_TYPE: " + CLUB_MBR_TYPE +  "\nPURCHASE_AMT: " +PURCHASE_AMT);
					
					Assert.assertEquals(ContractNumber, CONTR_NUM);
				 		logger.info("Assertion Passed Number: "+CONTR_NUM);
				 		
			 		Assert.assertEquals(ContractProject, PROJECT_ID);
				 		logger.info("Assertion Passed Project: "+PROJECT_ID);
				 		
			 		Assert.assertEquals(ContractUsageSellCode, USAGE_SELL_CODE);
				 		logger.info("Assertion Passed UsageSellCode: "+USAGE_SELL_CODE);
				 		
			 		Assert.assertEquals(ContractPointPackage, POINTS_PURCHASED);
				 		logger.info("Assertion Passed PointPackage: "+POINTS_PURCHASED);
				 		
			 		Assert.assertEquals(ContractClubMembership, CLUB_MBR_TYPE);
				 		logger.info("Assertion Passed ClubMembership: "+CLUB_MBR_TYPE);
				 		
			 		Assert.assertEquals(ContractPurchaseAmount, Purchase_Amount);
			 			logger.info("Assertion Passed PurchaseAmount: "+Purchase_Amount);

				}
			}
			else 
			{
			logger.info("Failed to make connection!");
			}

	}
}

	

