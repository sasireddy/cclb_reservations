package Active_Active;


import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class G_Owner_Reservation extends A_Common {
	
Logger logger = Logger.getLogger("G_Owner_Reservation.class");
	
	public String Wkday;
	
	@Test(priority=1)
	public void openBrowser() throws InterruptedException  {
		  
		  PropertyConfigurator.configure("Log4j.properties");
			//driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL STAGE
			  //driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL TEST
			 driver.get("https://clarityactive.diamondresorts.com/pls/clarityactive"); // URL ACTIVE ACTIVE
				driver.manage().window().maximize();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys(Username); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys(Password);// Entering Password
									logger.info("Entered Password");
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													Thread.sleep(2000);	
												
	}
	
	@Test(priority=2)
	public void Customer360() throws InterruptedException 
	{
		  PropertyConfigurator.configure("Log4j.properties");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 												
								
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(OwnerResLead); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+OwnerResLead);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "OWN";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a OWNER");
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div")));
															driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
															
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					 }
				 }
		}
	
	@Test(priority=3)
	public void Reservations() {
		
		PropertyConfigurator.configure("Log4j.properties");

			 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
			 
			 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
			 		logger.info("clicked on Reservations Tab");
			 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			 			
			 			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='travelDiv']/table/tbody/tr/td[4]/div")));
			 			
						//Opens New Window
						driver.findElement(By.xpath(".//*[@id='travelDiv']/table/tbody/tr/td[6]/div")).click(); //Clicked on HotelRes
					 		logger.info("clicked on OWN Res");
				 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

						// Switch to new window opened
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
		}
	
	@Test(priority=4)
	public void Owner_Reservation() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='sp_tdButtonBar']/table/tbody/tr/td[4]/div"))); //Wait till the Book Reservation button is clickable

		
		driver.findElement(By.xpath(".//*[@id='tblOwnerUseHist']/tbody/tr[1]/td[1]/input")).click();
			logger.info("Clicked on 2016 radio box");
			
		driver.findElement(By.xpath(".//*[@id='sp_tdButtonBar']/table/tbody/tr/td[4]/div")).click();
			logger.info("Click on Book Reservation");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='rtm_continueBtn']"))); //Wait till Continue button is clickable
			
			driver.findElement(By.xpath(".//*[@id='rtmBookPurp_input']")).click(); //Booking Purpose
				Select BookPurpose = new Select(driver.findElement(By.xpath(".//*[@id='rtmBookPurp_select']")));
					BookPurpose.selectByValue(BookingPurpose);
			
			driver.findElement(By.xpath(".//*[@id='rtm_continueBtn']")).click(); //Clicked on Continue button
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ow_buttonSearch']"))); //Wait till Search button is clickable
			
			driver.findElement(By.xpath(".//*[@id='sp_pvArrivalDate']")).click(); //Click on Arrival Date
				driver.findElement(By.xpath(".//*[@id='sp_pvArrivalDate']")).sendKeys(SelectFriday);	
					logger.info("Selected Arrival Date");
					
			
			driver.findElement(By.xpath(".//*[@id='sp_pvNumAdults']")).click();
			driver.findElement(By.xpath(".//*[@id='sp_pvNumAdults']")).sendKeys("2");
				logger.info("Entered Adults as 2");
				
				driver.findElement(By.xpath(".//*[@id='ow_buttonSearch']")).click();
					logger.info("Clicked on Search");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ow_buttonContinue']"))); //wait till continue button is clickable
				
				driver.findElement(By.xpath(".//*[@id='book_it_1']")).click();
					logger.info("Clicked on First Available Resort");
					
				driver.findElement(By.xpath(".//*[@id='ow_buttonContinue']")).click();
					logger.info("Clicked on Continue");
							
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); //wait till close button is clickable in Additional Resort Information(Errata)
					
				driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();
					logger.info("Clicked on close button in Additional Resort Information(Errata)");
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div[3]/div[4]/table/tbody/tr/td/table/tbody/tr/td[2]/div"))); //Save button is clickable in owner reservations section
				
				driver.findElement(By.xpath("/html/body/div/div[2]/div[3]/div[4]/table/tbody/tr/td/table/tbody/tr/td[2]/div")).click();
					logger.info("Clicked on Save Button to confirm reservation" );
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RENResvConf_close']"))); //Wait till close button is clickable in reservation confirmation screen
				
					String ResConfirmation = driver.findElement(By.xpath(".//*[@id='resConfHeaderText']")).getText();
					logger.info(""+ResConfirmation);
					
					String ResConNum = ResConfirmation.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
					logger.info("Reservation Confirmation number for OWNER Reservations: "+ResConNum);
					
					driver.findElement(By.xpath(".//*[@id='RENResvConf_close']")).click();
						logger.info("Clicked on Close button");
						
			
		 }

}
