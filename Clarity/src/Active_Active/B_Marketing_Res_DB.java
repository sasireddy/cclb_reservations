package Active_Active;



import java.sql.DriverManager;

import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class B_Marketing_Res_DB extends A_Common {
	
	 Logger logger = Logger.getLogger("B_Marketing_Res_DB.class");
	
	static String ConfirmationNo = "639005045"; 
	
	
	public void DatabaseConfirm() throws SQLException {
		
		 
		 PropertyConfigurator.configure("Log4j.properties");
		  
	 //Initialize
		try {
	
			Class.forName("oracle.jdbc.driver.OracleDriver");
	
		} catch (ClassNotFoundException e) {
	
			logger.info("Oracle JDBC server not found");
			e.printStackTrace();
			return;
		}
		
		logger.info("Oracle JDBC Driver Registered!");
	
		//Connect to Database
		try {
	
			//connection = DriverManager.getConnection("jdbc:oracle:thin:@dc1-ora-ddba26.global.ldap.wan:1541:STAGE1", "sreddy","bhushan22"); //Stage
			
			connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1550))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "LLUZ","p@ss0105");

			//connection = DriverManager.getConnection("jdbc:oracle:thin:@10.127.103.155:1541:TEST1", "sreddy","bhushan20"); //Test
	
		} catch (SQLException e) {
	
			logger.info("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	
		//Retrive data
		if (connection != null) {
			logger.info("Logged into STAGE DB Successfully");
			
			//Query
			prepared = connection.prepareStatement("select pr.resv_num, pr.resv_status_code, pr.resv_type_code, pr.property_id, pr.arrival_date, pr.departure_date, pt.tour_id, pt.office_code, pt.wave_code, pt.tour_type_code, pt.tour_date, pru.resv_num, pru.num_adults, pru.num_children, resv_eff_date, resv_end_date FROM p_reservation pr INNER JOIN p_tour pt ON pr.resv_num = pt.resv_num INNER JOIN p_resv_unit pru ON pr.resv_num = pru.resv_num WHERE pr.resv_num = ?");
			//Enter Confirmation Number in Query
			prepared.setString(1, ConfirmationNo);
			//Execute the query
	
			prepared.executeUpdate();
			
			while (prepared.getResultSet().next()) {
				
				//DecimalFormat decimal = new DecimalFormat("0.00"); //Decimal Format.  Decimal Format is used to match the retrieved values from databse to match with values retrieved from clarity.																			 //Example: Clarity value = 0.00, Oracle value is 0. To match both values the oracle value will be changed to 0.00.
				
				String Date = prepared.getResultSet().getString("ARRIVAL_DATE"); //Get RESV_EFF_DATE
				
				String DepartureDate = prepared.getResultSet().getString("DEPARTURE_DATE");

				String PTS_AMT = prepared.getResultSet().getString("RESV_END_DATE"); //Get PTS_AMT
				
				System.out.println("ArrivalDate:" + Date + "DEPARTURE_DATE" + DepartureDate +  "End Date:" +PTS_AMT);
				System.out.println();
			}
		}
		else 
		{
		logger.info("Failed to make connection!");
		}
	}

}

