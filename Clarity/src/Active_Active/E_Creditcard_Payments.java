package Active_Active;

import java.awt.AWTException;
import java.awt.Robot;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class E_Creditcard_Payments extends A_Common {
	
	Logger logger = Logger.getLogger("E_Creditcard_Payments.class");
	
	public String ClubNumber;
	public String PaymentsTotal;
	public String ClubAuthCode;
	
	public String Clubgroupcode = "DEF";
	public String DBUsername = "SREDDY";
	
	
	@Test(priority=1)
	public void openBrowser() throws InterruptedException  {
		  
		  PropertyConfigurator.configure("Log4j.properties");
			//driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL STAGE
			  //driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL TEST
			 driver.get("https://clarityactive.diamondresorts.com/pls/clarityactive"); // URL ACTIVE ACTIVE
				driver.manage().window().maximize();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys(Username); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys(Password);// Entering Password
									logger.info("Entered Password");
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													Thread.sleep(2000);	
												
	}
	
	@Test(priority=2)
	public void Customer360() throws InterruptedException 
	{
		  PropertyConfigurator.configure("Log4j.properties");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 												
								
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(PaymentsLead); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+PaymentsLead);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div")));
															//driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
															Thread.sleep(1000);
															driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
														
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
					 }
				 }
				
				
		}
	
	@Test(priority=3)
	public void Financials() throws InterruptedException, AWTException 
	{
		driver.findElement(By.xpath(".//*[@id='financialsTabClick']/div")).click();
			logger.info("Clicked on Financials");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='fin_backbutton']"))); //Back to Search button
			
			//CLUB
			ClubNumber = driver.findElement(By.xpath(".//*[@id='tbl_MembershipList_1']/tbody/tr/td[2]/a")).getText();
				logger.info("CLub Member Number is: "+ClubNumber);
			
			driver.findElement(By.xpath(".//*[@id='tbl_MembershipList_1']/tbody/tr/td[4]/a")).click();
				logger.info("Clicked on The Club Due");
	
				
			//-------------------------------------------------------Payment---------------------------------------------------------------//
			driver.findElement(By.xpath(".//*[@id='paymentRowsSelector']")).click();
				logger.info("Clicked on ALL");
				
			//Club
			driver.findElement(By.xpath(".//*[@id='PaymentAmountTbl']/tbody/tr[1]/td[6]/input")).click();	
			driver.findElement(By.xpath(".//*[@id='PaymentAmountTbl']/tbody/tr[1]/td[6]/input")).clear(); //Clear the value 
				driver.findElement(By.xpath(".//*[@id='PaymentAmountTbl']/tbody/tr[1]/td[6]/input")).sendKeys(Keys.chord(Keys.CONTROL,"a"),ClubPayment);
					logger.info("Club Dues Payment Amount: "+ClubPayment);
					
			//TOTAL
			PaymentsTotal = driver.findElement(By.xpath(".//*[@id='PaymentTotals']")).getText();
				logger.info("Total Payment is: "+PaymentsTotal);
			
			//Payment Type
			driver.findElement(By.xpath(".//*[@id='payment_method_input']")).click();
				Select PaymentType = new Select(driver.findElement(By.xpath(".//*[@id='payment_method_select']")));
					PaymentType.selectByValue(FinanPayType);
						logger.info("Selected PaymentMethod as Creditcard");
						
						Thread.sleep(1000);
			//Add Credit Card
			driver.findElement(By.xpath(".//*[@id='pvpaycc_input']")).click();
				Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='pvpaycc_select']"))); 
					Payment.selectByValue("0");
										
							//-------Add Credit Card---------//
										
							//Credit Card Number
							driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
								driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
									driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								
							//Card Holder Name
								driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
							driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
							
							//Exp mnth & Year
							driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
								Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
									ExpMth.selectByValue(ExpMonth);// Exp Mnth
							
							driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
								Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
									ExpYear.selectByValue(ExpYr);// Exp Year
								
							//Active
							driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
								Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
									Active.selectByValue("Y");// Active
										driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
							
							//DRI Credit Card Y/N
							driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
								Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
									Dricc.selectByValue("N");// DRI Credit Card	
							
							//Click on Save Button
							driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
								logger.info("Clicked on Save Button");
								
								   Robot r = new Robot();
									r.keyPress(java.awt.event.KeyEvent.VK_ESCAPE); //Press Escape
									r.keyRelease(java.awt.event.KeyEvent.VK_ESCAPE);
									r.keyPress(java.awt.event.KeyEvent.VK_ENTER); //Press Enter
									r.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
									
					//Process
					driver.findElement(By.xpath(".//*[@id='btnProcess']")).click();
						logger.info("Clicked on Process Button");
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='PaymentResultsWidget']/button")));
						
						//Club Auth Code
						String CCClubAuthCode = driver.findElement(By.xpath(".//*[@id='PaymentResultsWidget']/table/tbody/tr[3]/td/table/tbody/tr[1]/td[4]")).getText();
							logger.info("Club "+CCClubAuthCode);
								ClubAuthCode = CCClubAuthCode.substring(CCClubAuthCode.lastIndexOf(':') + 2);
									logger.info("Club: "+ClubAuthCode);
														
						driver.findElement(By.xpath(".//*[@id='PaymentResultsWidget']/button")).click();
							logger.info("Clicked on Continue button");
						
				
	}
	
	@Test(priority=4)
	public void DatabaseConfirm() throws SQLException {
		 			  
		 //Initialize
		 try {
				
				Class.forName("oracle.jdbc.driver.OracleDriver");
		
			} catch (ClassNotFoundException e) {
		
				System.out.println("Oracle JDBC server not found");
				e.printStackTrace();
				return;
			}
			
			System.out.println("Oracle JDBC Driver Registered!");
		
			//Connect to Database
			try {
		
				connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1550))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "LLUZ","p@ss0105");
		
			} catch (SQLException e) {
		
				System.out.println("Connection Failed! Check output console");
				e.printStackTrace();
				return;
			}
		
			//Retrive data
			if (connection != null) {
				System.out.println("Logged into Active Active DB Successfully");
				
				//--------------------------------------------------------First Query-------------------------------------------------------------------------------------//
				
				//Query
				prepared = connection.prepareStatement("SELECT ROWID, C.TRANS_DATE,  C.TRANS_SOURCE, C.SETTLE_TRANS_STATUS, C.AUTHORIZATION_CODE, C.AMOUNT, C.MERCHANT_REFERENCE_NUMBER, C.REF_TRANS_NUMBER, C.DB_USER_NAME, C.CC_ID, C.AUTH_TRANS_ID, C.AR_CLUB_GROUP_CODE FROM CCI.CCIT_SETTLE_TRANS C where C.AUTHORIZATION_CODE = ? and C.AMOUNT = ?");
				//Enter Confirmation Number in Query
				prepared.setString(1, ClubAuthCode);
				prepared.setString(2, ClubPayment);
				//Execute the query		
				prepared.executeUpdate();
				
				while (prepared.getResultSet().next()) {
					
					String TRANS_SOURCE = prepared.getResultSet().getString("TRANS_SOURCE"); 
					
					String SETTLE_TRANS_STATUS = prepared.getResultSet().getString("SETTLE_TRANS_STATUS");
					
					String AUTHORIZATION_CODE = prepared.getResultSet().getString("AUTHORIZATION_CODE"); 
					
					String AMOUNT = prepared.getResultSet().getString("AMOUNT");
					
					String MERCHANT_REFERENCE_NUMBER = prepared.getResultSet().getString("MERCHANT_REFERENCE_NUMBER");
					
					String DB_USER_NAME = prepared.getResultSet().getString("DB_USER_NAME");
					
					String CC_ID = prepared.getResultSet().getString("CC_ID");
					
					String AUTH_TRANS_ID = prepared.getResultSet().getString("AUTH_TRANS_ID");
					
					String AR_CLUB_GROUP_CODE = prepared.getResultSet().getString("AR_CLUB_GROUP_CODE");
					
					logger.info("------------------------------------------From Clarity------------------------------------------------");
					logger.info("\nMember Number: " + ClubNumber + "\nAmount: " + ClubPayment +  "\nAuthorization Code: " +ClubAuthCode+  "\nDBUsername(Declared): " +DBUsername+  "\nClubgroupcode(Declared): " +Clubgroupcode);
					
					logger.info("------------------------------------------From TOAD------------------------------------------------");
					logger.info("\nTRANS_SOURCE: " + TRANS_SOURCE + "\nSETTLE_TRANS_STATUS: " + SETTLE_TRANS_STATUS +  "\nAUTHORIZATION_CODE: " +AUTHORIZATION_CODE+ "\nAMOUNT: " + AMOUNT + "\nMERCHANT_REFERENCE_NUMBER: " + MERCHANT_REFERENCE_NUMBER +  "\nDB_USER_NAME: " +DB_USER_NAME+ "\nCC_ID: " + CC_ID +  "\nAUTH_TRANS_ID: " +AUTH_TRANS_ID+  "\nAR_CLUB_GROUP_CODE: " +AR_CLUB_GROUP_CODE);
					
					Assert.assertEquals(ClubNumber, MERCHANT_REFERENCE_NUMBER);
			 			logger.info("Assertion Passed for Merchant Reference Number: "+MERCHANT_REFERENCE_NUMBER);
			 		
			 		Assert.assertEquals(ClubPayment, AMOUNT);
				 		logger.info("Assertion Passed for the amount payed: "+AMOUNT);
				 		
			 		Assert.assertEquals(ClubAuthCode, AUTHORIZATION_CODE);
				 		logger.info("Assertion Passed for Authorization Code: "+AUTHORIZATION_CODE);
				 		
			 		Assert.assertEquals(DBUsername, DB_USER_NAME);
				 		logger.info("Assertion Passed for the Database Username: "+DB_USER_NAME);
				 		
			 		Assert.assertEquals(Clubgroupcode, AR_CLUB_GROUP_CODE);
				 		logger.info("Assertion Passed for the group code: "+AR_CLUB_GROUP_CODE);
				
					
					}
					connection.close();
				}
				
				else 
				{
				System.out.println("Failed to make connection!");
				}
	}
	
	
	

}
