package Active_Active;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class I_Create_LeadID extends A_Common {
	
	Logger logger = Logger.getLogger("I_Create_LeadID.class");
	


	@Test(priority=1)
	public void openBrowser() throws InterruptedException  {
		  
		  PropertyConfigurator.configure("Log4j.properties");
			//driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL STAGE
			  //driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL TEST
			 driver.get("http://claritytest.diamondresorts.com/pls/clarityactive/sign_in"); // URL ACTIVE ACTIVE
				driver.manage().window().maximize();
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys(Username); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys(Password);// Entering Password
									logger.info("Entered Password");
											driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
												logger.info("Clicked on Login Button");
													Thread.sleep(2000);	
												
	}
	
	@Test(priority=2)
	public void Customer360() 
	{
		  PropertyConfigurator.configure("Log4j.properties");
		 //Customer 360
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 												
			//Lead Area/ID					
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(NewLeadid); //Enter Leadid
				logger.info("Lead ID is: "+MarketingLead);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
					
					//Wait
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='searchAddButton']"))); //Wait till Add new button is clickable
					
					//Add New
					driver.findElement(By.xpath(".//*[@id='searchAddButton']")).click();	
						logger.info("Clicked on Add New Button");
					//Wait	
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='quickaddFrmSaveBtn']"))); //Wait till Save button is clickable
					
					
					
	}
}
