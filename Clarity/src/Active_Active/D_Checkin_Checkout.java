package Active_Active;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;


public class D_Checkin_Checkout extends A_Common  {
	
Logger logger = Logger.getLogger("D_Checkin_Checkout.class");
	
	String ResConNum;
	String UnitNo1;
	
	public void Utilities() throws InterruptedException 
	{
		
				//----------------------------------------------Change HotelDate-----------------------------------------------------------------------------------------------//
			 	WebElement Utilities = wait.until(ExpectedConditions.elementToBeClickable(By.id("552"))); // Wait till the Utilities is available to click
			 	Utilities.click(); // Click on utilities
			 			logger.info("Clicked on Utilities");
			 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			 				
			 				WebElement ChangeHtlDate = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='554']"))); // Wait till the Change Hotel Date is available to click
			 				ChangeHtlDate.click(); // Click on ChangeHtlDate
			 			 			logger.info("Clicked on ChangeHotelDate");		 			 		
											Thread.sleep(1000);
											
							WebElement HotelDate = driver.findElement(By.xpath(".//*[@id='change_hd']/tbody"));
							List<WebElement> HotelDatecolumns = HotelDate.findElements(By.tagName("tr"));
								logger.info("No.of rows" +HotelDatecolumns.size());
									int HotelDaterowsnum = HotelDatecolumns.size();
											String HotelDatexpath =null;
												String HotelDatecellval;
												
									Search:
										for(int i = 1;i<=HotelDaterowsnum;i++)
										{
											HotelDatexpath = ".//*[@id='change_hd']/tbody/tr[" + String.valueOf(i) + "]/td[2]";
												HotelDatecellval = driver.findElement(By.xpath(HotelDatexpath)).getText();
													String HotelDateCellval1 = HotelDatecellval.substring(0,3);
														logger.info(""+HotelDatecellval);
															
													if(PropertyType.equals(HotelDateCellval1))
														{
															driver.findElement(By.xpath(".//*[@id='change_hd']/tbody/tr[" + String.valueOf(i) + "]/td[4]/div/span/input")).click();
															Thread.sleep(1000);

															
															driver.findElement(By.xpath(".//*[@id='change_hd']/tbody/tr[" + String.valueOf(i) + "]/td[4]/div/span/input")).sendKeys(Keys.chord(Keys.CONTROL,"a"),"t");
															Thread.sleep(1000);
															driver.findElement(By.xpath(".//*[@id='change_hd']/tbody/tr[" + String.valueOf(i) + "]/td[4]/div/span/img")).click(); 
															break Search;
														}
		    		 
										}
																		
					logger.info("Clicked on " +PropertyType+ " Date");
													
							driver.findElement(By.xpath(".//*[@id='change_hd_process_button']")).click(); // Process button
								driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
									String Message = driver.findElement(By.xpath(".//*[@id='alert']/div")).getText();
										logger.info(""+Message);
									
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); //Wait till ok button is clickable
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
									logger.info("Clicked on OK button");
								
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]"))); //Wait till menu button is clickable
								driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]")).click();
									logger.info("Clicked on Menu button");
																			
	}

	public void FrontDesk() throws InterruptedException 
	{
		
		PropertyConfigurator.configure("Log4j.properties");
		
				//-----------------------------FrontDesk--------------------------------------------------------------------------------------------//		
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='100']"))); //Wait till menu button is clickable
						driver.findElement(By.xpath(".//*[@id='100']")).click();
							logger.info("Clicked on Front Desk");
								
				//-----------------------------Master Property--------------------------------------------------------------------------------------------//
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='searchSearchButton']"))); //Wait till the search button is clickable
						driver.findElement(By.xpath(".//*[@id='pvSearchResv_masterprop_input']")).click();
							Select MasterProp = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_masterprop_select']")));
								MasterProp.selectByValue("");
									logger.info("Selected MasterProperty as none");
				//-----------------------------Property--------------------------------------------------------------------------------------------//											
					driver.findElement(By.xpath(".//*[@id='pvSearchResv_property_input']")).click();
						Select Property = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_property_select']")));
							Property.selectByValue(PropertyType);
								logger.info("Selected PropertyType as: "+PropertyType);
							
				//----------------------------- Reservation Type--------------------------------------------------------------------------------------------//		
					driver.findElement(By.xpath(".//*[@id='pvSearchResv_type_input']")).click();
						Select Reservation  = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_type_select']")));
							Reservation.selectByValue(ReservationType);
								logger.info("Selected ReservationType as: "+ReservationType);
							
				//-----------------------------Arrival Date--------------------------------------------------------------------------------------------//
					driver.findElement(By.xpath(".//*[@id='pvSearchResv_arrivalfrom']")).click();
						driver.findElement(By.xpath(".//*[@id='pvSearchResv_arrivalfrom']")).sendKeys("t");
							logger.info("Selected Arrival Date to Today");
								driver.findElement(By.xpath(".//*[@id='pvSearchResv_arrivalto']")).click();
								driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();
									logger.info("Clicked on Search");
								
					
		//-----------------------------Search and Store Unit Number from FrontDesk------------------------------------------------------------------------------//
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//*[@id='searchResultsData']/tbody")));
		
		WebElement tablebody= driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody")); // Use By.tagName() to get all elements of desired tag underneath the above static element
		List<WebElement> rows  = tablebody.findElements(By.tagName("tr")); // Get the size of weblist to get number of rows    
			logger.info("No.of rows" +rows.size());
				int rowsnum1= rows.size();
					String xpath1 =null;
						String cellval1 = null;
							UnitNo1 = null;
						Search:
							for(int i = 1;i<=rowsnum1;)
								{
									xpath1 = ".//*[@id='searchResultsData']/tbody/tr[" + String.valueOf(i) + "]/td[10]" ;
										cellval1 = driver.findElement(By.xpath(xpath1)).getText();
											logger.info(cellval1);
									
									if(cellval1.equals(""))
									{
										logger.info("The Unit Number is Null");
										
										driver.findElement(By.xpath(xpath1)).click();
										
										Boolean iselementpresent = driver.findElements(By.xpath(".//*[@id='SmartPCloseBtn']")).size()!= 0; 
										
										if (iselementpresent == true) 
										{ 
											
											driver.findElement(By.xpath(".//*[@id='SmartPCloseBtn']")).click();
											
										} 
	
										//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='SmartPCloseBtn']"))); //Wait until CLose button to be clickable

										//HomePhone
										driver.findElement(By.xpath(".//*[@id='pvHomephone_idialcd_input']")).click();
											Select HomePhone = new Select(driver.findElement(By.xpath(".//*[@id='pvHomephone_idialcd_select']")));
												HomePhone.selectByValue(Phone);
											
										//WorkPhone
										driver.findElement(By.xpath(".//*[@id='pvWorkphone_idialcd_input']")).click();
											Select WorkPhone = new Select(driver.findElement(By.xpath(".//*[@id='pvWorkphone_idialcd_select']")));
											WorkPhone.selectByValue(Phone);
										
										//Mobile
										driver.findElement(By.xpath(".//*[@id='pvMobilephone_idialcd_input']")).click();
											Select Mobile = new Select(driver.findElement(By.xpath(".//*[@id='pvMobilephone_idialcd_select']")));
												Mobile.selectByValue(Phone);
												
										//Click on Unit Number
											driver.findElement(By.xpath(".//*[@id='pvunit_num']")).click(); //Unit Number
												logger.info("Clicked on Unit Number");
												
								
												Thread.sleep(1000);
												
												wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='unitSelection']/tbody/tr[2]/td[1]/div"))); //Wait until save button to be clickable
												
												UnitNo1 =  driver.findElement(By.xpath(".//*[@id='unitSelection']/tbody/tr[2]/td[1]/div")).getText(); //Get Unit Number
													logger.info("The Unit Number is :"+UnitNo1);
													
												driver.findElement(By.xpath(".//*[@id='unitSelection']/tbody/tr[2]/td[1]/div")).click(); //Click on the first available one
													logger.info("Clicked on First Available Resort");
													
													driver.findElement(By.xpath(".//*[@id='uplUnitSaveButton']")).click(); //Click on Save
														logger.info("Clicked on Save button");
														
														Thread.sleep(1000);
													
														driver.findElement(By.xpath(".//*[@id='summarySaveButton']")).click();
															logger.info("Clicked on Save Button");
															
														wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); //Wait until save button to be clickable
														
														String StayUpdate = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
															logger.info(""+StayUpdate);
															
														driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
															logger.info("Clicked on OK");
															break Search;
																
									}
									else  
									{
										String UnitNo = driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody/tr[" + String.valueOf(i) + "]/td[10]")).getText();										
											logger.info("The Unitno is " +UnitNo);
												logger.info("The Property is: "+PropertyType);
													UnitNo1 = UnitNo;	
														break Search;
									}
									
								}	
								
	}
	

	
	
	public void HouseKeeping() throws InterruptedException 
	{
		
		//-----------------------------Click on Menu--------------------------------------------------------------------------------------------//	
		driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]")).click(); //Menu
			logger.info("Clicked on Menu");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='111']"))); ////Wait until element to be clickable
		
		Thread.sleep(2000);
		
		   //-----------------------------HouseKeeping--------------------------------------------------------------------------------------------//
			driver.findElement(By.xpath(".//*[@id='111']")).click(); //Housekeeping
				logger.info("Clicked on House Keeping");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='644']"))); //Wait until element to be clickable
		  //----------------------------- Manage HouseKeeping--------------------------------------------------------------------------------------------//
			
			driver.findElement(By.xpath(".//*[@id='644']")).click(); //Manage HouseKeeping
				logger.info("Clicked on Manage HouseKeeping");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='SearchBtn']"))); //Wait until Search button to be clickable
				
		 //-----------------------------HouseKeeping Property--------------------------------------------------------------------------------------------//				
			driver.findElement(By.xpath(".//*[@id='pv_property_id_input']")).click();
				Select HouseKeepingProperty = new Select(driver.findElement(By.xpath(".//*[@id='pv_property_id_select']")));
					HouseKeepingProperty.selectByValue(PropertyType);
						logger.info("Selected PropertyType as: "+PropertyType);
						
		//-----------------------------HouseKeeping Unit Number--------------------------------------------------------------------------------------------//	
			driver.findElement(By.xpath(".//*[@id='pv_unit_nbr']")).click();
			
			driver.findElement(By.xpath(".//*[@id='pv_unit_nbr']")).sendKeys(UnitNo1);
				logger.info("Entered UnitNo as: "+UnitNo1);
		//-----------------------------Search--------------------------------------------------------------------------------------------//	
				driver.findElement(By.xpath(".//*[@id='SearchBtn']")).click(); //search
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='manag_hsk_ChngStatus_button']"))); //Wait until ChngUnitStatus button to be clickable		
					
					
					WebElement tablebody2= driver.findElement(By.xpath(".//*[@id='prp_unit_table']/tbody")); // Use By.tagName() to get all elements of desired tag underneath the above static element
					List<WebElement> rows2  = tablebody2.findElements(By.tagName("tr")); // Get the size of weblist to get number of rows    
						System.out.println("No.of rows" +rows2.size());
							int rowsnum2= rows2.size();
								String xpath2 =null;
									String cellval2 = null;
									
									Search:
										for(int i = 1;i<=rowsnum2;i++)
											{
												xpath2 = ".//*[@id='prp_unit_table']/tbody/tr[" + String.valueOf(i) + "]/td[2]" ;
												
												cellval2 = driver.findElement(By.xpath(xpath2)).getText();
												
												System.out.println(cellval2);
												
											
												if(UnitNo1.equals(cellval2) && UnitNo1 != null) 
												{
													driver.findElement(By.xpath(".//*[@id='prp_unit_table']/tbody/tr[" + String.valueOf(i) + "]/td[2]/a")).click();
													break Search;
												}	
												
											}
				
				//-----------------------------Change unit Status------------------------------------------------------------//	
				//RoomCondition
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='SaveBtn']"))); //Wait until save button to be clickable
				
					driver.findElement(By.id("pv_rcu_input")).click();
						Select RoomCondition = new Select(driver.findElement(By.xpath(".//*[@id='pv_rcu_select']")));
							RoomCondition.selectByValue(RmConditon);
								logger.info("Front Desk Status: "+RmConditon);

					
				//HouseKeeping Occupancy
					driver.findElement(By.xpath(".//*[@id='pv_hsk_occp_input']")).click();
						Select HouseKeepingOccupancy = new Select(driver.findElement(By.xpath(".//*[@id='pv_hsk_occp_select']")));
							HouseKeepingOccupancy.selectByValue(HsOccupancy);
								logger.info("HouseKeeping Occupancy: "+HsOccupancy);

				//Front Desk Occupancy
					driver.findElement(By.xpath(".//*[@id='pv_fd_occp_input']")).click();
						Select FrontDeskOccupancy = new Select(driver.findElement(By.xpath(".//*[@id='pv_fd_occp_select']")));
							FrontDeskOccupancy.selectByValue(FDOccupancy);
								logger.info("Front Desk Occupancy: "+FDOccupancy);
								
								Thread.sleep(2000);
				//----------------------------Save--------------------------------------------------------------------//			
					driver.findElement(By.xpath(".//*[@id='SaveBtn']")).click();
						logger.info("Clicked on Save");
						
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]"))); //Wait until save button to be clickable
							driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]")).click();
		
								FrontDesk(); //Run Front Desk Status Method again
									
	}

	public void Checkin() throws InterruptedException
	{
		//-----------------------------Search and Click on the unit number------------------------------------------------------------------------------//
				wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//*[@id='searchResultsData']/tbody")));
				
				WebElement tablebody = driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody")); // Use By.tagName() to get all elements of desired tag underneath the above static element
				List<WebElement> rows  = tablebody.findElements(By.tagName("tr")); // Get the size of weblist to get number of rows    
					logger.info("No.of rows" +rows.size());
						int rowsnum1= rows.size();
							String xpath1 =null;
								String cellval1 = null;
								Search:
									for(int i = 1;i<=rowsnum1;i++)
										{
											xpath1 = ".//*[@id='searchResultsData']/tbody/tr[" + String.valueOf(i) + "]/td[10]" ;
												cellval1 = driver.findElement(By.xpath(xpath1)).getText();
													logger.info(cellval1);
											
											if(cellval1.equals(UnitNo1))
											{
												driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody/tr[" + String.valueOf(i) + "]/td[10]")).click();
												logger.info("Unit number found and Clicked");
												break Search;
											}
											else  
											{
												logger.info("Unit Number Not Found");
											}
											
										}
								
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='SmartPCloseBtn']"))); //Wait until CLose button to be clickable
								
								driver.findElement(By.xpath(".//*[@id='SmartPCloseBtn']")).click();
								
								//HomePhone
								driver.findElement(By.xpath(".//*[@id='pvHomephone_idialcd_input']")).click();
									Select HomePhone = new Select(driver.findElement(By.xpath(".//*[@id='pvHomephone_idialcd_select']")));
										HomePhone.selectByValue(Phone);
									
								//WorkPhone
								driver.findElement(By.xpath(".//*[@id='pvWorkphone_idialcd_input']")).click();
									Select WorkPhone = new Select(driver.findElement(By.xpath(".//*[@id='pvWorkphone_idialcd_select']")));
									WorkPhone.selectByValue(Phone);
								
								//Mobile
								driver.findElement(By.xpath(".//*[@id='pvMobilephone_idialcd_input']")).click();
									Select Mobile = new Select(driver.findElement(By.xpath(".//*[@id='pvMobilephone_idialcd_select']")));
										Mobile.selectByValue(Phone);
								
								driver.findElement(By.xpath(".//*[@id='summarySaveButton']")).click();
									logger.info("Clicked on Save Button");
								
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div")));
									String Message = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
										logger.info(""+Message);
											driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
								
								//------------------------------Unit------------------------------------------//
								String UnitNumber = driver.findElement(By.xpath(".//*[@id='pvunit_num']")).getAttribute("value");
									logger.info("Unit Number is: "+UnitNumber);
									Assert.assertEquals(UnitNumber, UnitNo1);
										logger.info("Assertion Passed: "+UnitNumber);
									
								driver.findElement(By.xpath(".//*[@id='pvunit_houseocc_input']")).click();
									Select HousekeepingEmpty = new Select(driver.findElement(By.xpath(".//*[@id='pvunit_houseocc_select']")));
									HousekeepingEmpty.selectByValue(HouseKeepingEmp);
										
									
								driver.findElement(By.xpath(".//*[@id='pvunit_housestatus_input']")).click();
									Select HousekeepingClean = new Select(driver.findElement(By.xpath(".//*[@id='pvunit_housestatus_select']")));
										HousekeepingClean.selectByValue(HouseKeepingCln);
										 
									logger.info("Changed the Housekeeping Status to Empty and Clean");
					
							driver.findElement(By.xpath(".//*[@id='summaryCheckInButton']")).click();
								logger.info("Clicked on Check in");
								
						
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); //Wait till No button is clickable
							
								String Confirmation = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
									System.out.println("The " +Confirmation);
									
								ResConNum = Confirmation.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
									logger.info("Reservation Confirmation number is: "+ResConNum);
								
									driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();		
										logger.info("Clicked on Ok");
								
	}
	

	public void Checkout() throws InterruptedException
	{
			//-----------------------------Click on Menu--------------------------------------------------------------------------------------------//	
			driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]")).click(); //Menu
				logger.info("Clicked on Menu");
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='100']"))); //Frontdesk Button is clickable
				
			//-----------------------------FrontDesk--------------------------------------------------------------------------------------------//		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='100']"))); //Wait till menu button is clickable
				driver.findElement(By.xpath(".//*[@id='100']")).click();
					logger.info("Clicked on Front Desk");
			
		
			//-----------------------------Master Property--------------------------------------------------------------------------------------------//
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='searchSearchButton']"))); //Wait till the search button is clickable
					driver.findElement(By.xpath(".//*[@id='pvSearchResv_masterprop_input']")).click();
						Select MasterProp = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_masterprop_select']")));
							MasterProp.selectByValue("");
								logger.info("Selected MasterProperty as none");
			//-----------------------------Property--------------------------------------------------------------------------------------------//											
				driver.findElement(By.xpath(".//*[@id='pvSearchResv_property_input']")).click();
					Select Property = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_property_select']")));
						Property.selectByValue(PropertyType);
							logger.info("Selected PropertyType as: "+PropertyType);
						
			//----------------------------- Reservation Type--------------------------------------------------------------------------------------------//		
				driver.findElement(By.xpath(".//*[@id='pvSearchResv_type_input']")).click();
					Select Reservation  = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_type_select']")));
						Reservation.selectByValue(ReservationType);
							logger.info("Selected ReservationType as: "+ReservationType);
							
			//----------------------------- Reservation Status--------------------------------------------------------------------------------------------//		
			driver.findElement(By.xpath(".//*[@id='pvSearchResv_status_input']")).click();
				Select ReservationStatus  = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchResv_status_select']")));
					ReservationStatus.selectByValue(ReserStatus);
						logger.info("Selected Reservation Status as: "+ReserStatus);
						
			//-----------------------------Arrival Date--------------------------------------------------------------------------------------------//
				driver.findElement(By.xpath(".//*[@id='pvSearchResv_arrivalfrom']")).click();
					driver.findElement(By.xpath(".//*[@id='pvSearchResv_arrivalfrom']")).sendKeys("t");
						logger.info("Selected Arrival Date to Today");
							driver.findElement(By.xpath(".//*[@id='pvSearchResv_arrivalto']")).click();
							driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();
								logger.info("Clicked on Search");
						
		
						
						wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//*[@id='searchResultsData']/tbody"))); //Wait till the table is visible
						
						WebElement tablebody = driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody")); // Use By.tagName() to get all elements of desired tag underneath the above static element
						List<WebElement> rows  = tablebody.findElements(By.tagName("tr")); // Get the size of weblist to get number of rows    
							logger.info("No.of rows" +rows.size());
								int rowsnum1= rows.size();
									String xpath1 =null;
										String cellval1 = null;
										Search:
											for(int i = 1;i<=rowsnum1;i++)
												{
													xpath1 = ".//*[@id='searchResultsData']/tbody/tr[" + String.valueOf(i) + "]/td[3]" ;
														cellval1 = driver.findElement(By.xpath(xpath1)).getText();
															logger.info(cellval1);
													
													if(cellval1.equals(ResConNum))
													{
														driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody/tr[" + String.valueOf(i) + "]/td[10]")).click();
														logger.info("Reservation Confirmation Matches");
														break Search;
													}
													else  
													{
														logger.info("Reservation Number not found");
													}
													
												}
										
										wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='summarySaveButton']"))); //Wait until CLose button to be clickable
										
				driver.findElement(By.xpath(".//*[@id='folioTabClick']")).click();
					logger.info("Clicked on Folio");
					
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='FolioCheckoutButton']"))); //Wait until checkout button to be clickable
				
				String FolioBalance = driver.findElement(By.xpath(".//*[@id='pvFolioBal']")).getText();
				
					if(FolioBalance.equals(Folio))
					{
						logger.info("Folio Balance is Equal to Zero: "+FolioBalance);
						
						driver.findElement(By.xpath(".//*[@id='FolioCheckoutButton']")).click();
							logger.info("Clicked on Checkout Button");
							
						String Checkout = driver.findElement(By.xpath(".//*[@id='confirm']/div/div[2]/pre")).getText();
							logger.info(""+Checkout);
							
						driver.findElement(By.xpath(".//*[@id='confirm_button1']")).click();
							logger.info("Clicked on Ok button");
							
							
						String CheckoutFolio = driver.findElement(By.xpath(".//*[@id='confirm']/div/div[2]/pre")).getText();
							logger.info(""+CheckoutFolio);
							
						String CheckoutFolioConfNum = CheckoutFolio.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
							logger.info("Checkout Folio: "+CheckoutFolioConfNum);
							
						driver.findElement(By.xpath(".//*[@id='confirm_button1']")).click();	
							logger.info("clicked on ok to checkout");
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); //Wait until ok button to be clickable
							
						String SuccessfulCheckout = driver.findElement(By.xpath(".//*[@id='alert']/div/div[2]/pre")).getText();
							logger.info(""+SuccessfulCheckout);
							
							driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
							
							//driver.close();
					}
										
	}


	
}
