package Active_Active;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class Upgrade {
	
	 static Logger logger = Logger.getLogger("Upgrade.class");
	 static Connection connection = null;
		static PreparedStatement prepared = null;
	

	public static void main(String args[]) throws SQLException {
		
		 
		 PropertyConfigurator.configure("Log4j.properties");
		  
	 //Initialize
		try {
	
			Class.forName("oracle.jdbc.driver.OracleDriver");
	
		} catch (ClassNotFoundException e) {
	
			logger.info("Oracle JDBC server not found");
			e.printStackTrace();
			return;
		}
		
		logger.info("Oracle JDBC Driver Registered!");
	
		//Connect to Database
		try {
	
			
			
			connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=upgradescan.global.ldap.wan)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=upgrade)))", "sreddy","bhushan22");

	
		} catch (SQLException e) {
	
			logger.info("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	
		//Retrive data
		if (connection != null) {
			logger.info("Logged into Upgrade DB Successfully");
			
			prepared = connection.prepareStatement("update LED_ADDRESS SET ADDR1 = ?, CITY = ?, STATE_CODE = ?, FMT_POSTAL_CODE = ? "
					+ "where LEAD_ID in (174546281, 175297526)");
					
			prepared.setString(1, "245 N 4th Avenue");
			prepared.setString(2, "Atlanta");
			prepared.setString(3, "GA");
			prepared.setString(4, "30071");
			prepared.executeUpdate();
			logger.info("Successfully Updated Address, city, state, postal code: DONE");
			
		} else {
			logger.info("Failed to make connection!");
		}
		
		if (connection != null) {
			logger.info("Update P_LEAD");
		
			//String update = "update LED_ADDRESS SET" + "ADDR1 = ? where LEAD_ID = ?";
			
			prepared = connection.prepareStatement("update P_LEAD SET P_LEAD.FMT_HOME_PHONE = ?, EMAIL_ADDRESS= ? "
					+ "where LEAD_ID in (174546281, 175297526)");
					
			prepared.setString(1, "7026042236");
			prepared.setString(2, "Tester@diamondresorts.com");
			prepared.executeUpdate();
			logger.info("Updated Phone Number and Email Address Successfully: DONE");
			logger.info("Finished");
			
		} else {
			logger.info("Failed to make connection!");
		}
		
	}

}