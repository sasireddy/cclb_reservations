package Active_Active;

import java.io.IOException;
import java.sql.SQLException;

import org.testng.annotations.Test;

public class Z_Execution {

	AA_OpenBrowser OpenBrowser = new AA_OpenBrowser();
	B_Marketing_Res MarketingRes = new B_Marketing_Res();
	BB_Marketing_Res_Rep MarketingResRep = new BB_Marketing_Res_Rep();
	C_Create_Tours CreateTours = new C_Create_Tours();
	D_Checkin_Checkout Checkinout = new D_Checkin_Checkout();
	J_Tours_Checkin CheckinTour = new J_Tours_Checkin();
	
	
	/*@Test(priority=1)
	public void MarketingReservation() throws InterruptedException, IOException
	{
		OpenBrowser.openBrowser();
		MarketingRes.Customer360();
		MarketingRes.Reservations();
		MarketingRes.Marketing_Res();
		MarketingRes.Stay_Parameters();
		MarketingRes.Reservation_Processing();

	}*/
	
	/*@Test(priority=2)
	public void MarketingReservationREP() throws InterruptedException, IOException
	{
		OpenBrowser.openBrowser();
		MarketingRes.Customer360();
		MarketingResRep.Reservations();
	}*/
	
	
	/*@Test
	public void CreateTours() throws InterruptedException, IOException, SQLException{
		OpenBrowser.openBrowser();
		CreateTours.Tours();
		CreateTours.TourBooking();
		CreateTours.Customer360();
		CreateTours.DatabaseConfirm();
		CreateTours.DatabaseConfirm_Activity();

	}*/
	
	@Test
	public void Checkin_Tour() throws InterruptedException, IOException, SQLException{
		OpenBrowser.openBrowser();
		CheckinTour.Tours();
		CheckinTour.TourBooking();

	}
	
	
	
	/*@Test
	public void CreateToursRep() throws InterruptedException, IOException, SQLException
	{
		OpenBrowser.openBrowser();
		CreateTours.Create_Tours_Rep();
		CreateTours.Customer360();
		CreateTours.DatabaseConfirm();
		CreateTours.DatabaseConfirm_Activity();
	} */
	
	/*@Test
	public void Checkin_Checkout() throws InterruptedException, IOException, SQLException
	{
		OpenBrowser.openBrowser();
		Checkinout.Utilities();
		Checkinout.FrontDesk();
		Checkinout.HouseKeeping();
		Checkinout.Checkin();
		Checkinout.Checkout();
		
	}*/
	
}
