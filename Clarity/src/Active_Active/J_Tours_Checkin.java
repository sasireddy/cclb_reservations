package Active_Active;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class J_Tours_Checkin extends A_Common {

	Logger logger = Logger.getLogger("C_Create_Tours.class");
	
	public void Tours() throws InterruptedException 
	{
		 PropertyConfigurator.configure("Log4j.properties");
		 	WebElement Tours = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div/div/table/tbody/tr[2]/td[2]/div"))); // Wait till the Tours is available to click
		 	Tours.click(); // Click on Tours
		 			logger.info("Clicked on Tours");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				WebElement ToursAvail = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='444']"))); // Wait till the ToursAvailability is available to click
		 				ToursAvail.click(); // Click on Tours
		 			 			logger.info("Clicked on ToursAvailability");
		 			 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

										Thread.sleep(1000);
						
						//Sales center
		 			 	driver.findElement(By.xpath(".//*[@id='selectSalesCenter']")).click();
		 			 		driver.findElement(By.xpath(".//*[@id='selectSalesCenter']")).sendKeys("949");
		 			 			logger.info("Clicked on Sales Center and entered 51");
		 			 		
		 			 	//Tour Date
	 			 		driver.findElement(By.xpath(".//*[@id='tourDate']")).click();
			 			 	driver.findElement(By.xpath(".//*[@id='tourDate']")).sendKeys("t");
			 			 		driver.findElement(By.xpath(".//*[@id='selectSalesOffice']")).click();
			 			 			logger.info("Select Tour Date");
		 			 		
		 			 	
		 			 	//Search
		 			 	driver.findElement(By.xpath(".//*[@id='tourAvailabilitySearchButton']")).click();
		 			 		logger.info("Clicked on Search button");
		 			 		
		 			 		Thread.sleep(2000);
		 			 		
		 			 	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/span/span[2]")).click();
		 			 		logger.info("Click on the [+] sedona summit - sedona, AZ");

								Thread.sleep(1000);

 			 			driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/div[5]/span[2]")).click();
		 			 		logger.info("[+] Summit Inhouse");
		 			 		
		 			 		WebElement dateWidget = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/div[5]/div/table/tbody"));
							List<WebElement> columns=dateWidget.findElements(By.tagName("tr"));
									int rowsnum1 = columns.size();
										int colnum1=21;
											String xpath1 =null;
												
												Search:
														for(int i = 3;i<=rowsnum1;i++)
														{
															for(int j=7;j<=colnum1; j+=4)
															{
																xpath1= "/html/body/div[1]/div[2]/div[1]/div/div/div[5]/div/table/tbody/tr[" + String.valueOf(i) + "]/td[" + String.valueOf(j) + "]";
																
																String olddate = driver.findElement(By.xpath(xpath1)).getAttribute("olddate");
																
																	System.out.println("Value is "+olddate);
																
																if(olddate.equals(Olddate_defined))
																{
																
																	String Available = driver.findElement(By.xpath(xpath1)).getText();
																	
																	Thread.sleep(1000);
																
																	logger.info("Available Count is: "+Available);
																
																	int Availablecount = Integer.parseInt(Available);
																
																	if(Availablecount > 0)
																	{
																		driver.findElement(By.xpath(xpath1)).click();
																			logger.info("Clicked on Available Count");
																				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
																				  break Search;
																																							
																	}
																}
																
																else
																{
																	System.out.println("Olddate Value is Yes: "+olddate);
																}
																																																		
															}
							
														}	
			 			
	}
	
	public void TourBooking() throws InterruptedException, IOException 
	{
			 
		//Opens New Window
		 driver.findElement(By.xpath(".//*[@id='confirm_button1']")).click(); //Clicked on yes
			logger.info("Clicked on Yes for continue booking a tour");
	 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			// Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='tour_Srch_Lead']")));
			
		//Lead Id/Area
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_input']")).click();
				Select LeadArea = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_select']")));
					LeadArea.selectByValue(ToursLeadArea);
						logger.info("Selected Lead Area");
						
						driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).click();
							driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(ToursLead);
								logger.info("Entered Lead Id");
				
								driver.findElement(By.xpath(".//*[@id='tour_Srch_Lead']")).click();	
									logger.info("Clicked on Search");
										Thread.sleep(10000);
			
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='tour_Book']")));
			
			//---------------------------------------Tour Information--------------------------------------------------------//
		
			//Sales Center
			SalesCenter = driver.findElement(By.xpath(".//*[@id='pvsales_center_input']")).getAttribute("value");
				logger.info("SalesCenter: "+SalesCenter);
				
			//TourOffice
			TourOffice = driver.findElement(By.xpath(".//*[@id='pvtour_sales_office_input']")).getAttribute("value");
				logger.info("TourOffice: "+TourOffice);
				
			//TourWave 
			TourWave = driver.findElement(By.xpath(".//*[@id='pvtour_wave_input']")).getAttribute("value");
				logger.info("TourWave: "+TourWave);
				
				Thread.sleep(1000);
				
			//Tour Type
			driver.findElement(By.xpath(".//*[@id='pvtour_type_input']")).click();
				driver.findElement(By.xpath(".//*[@id='pvtour_type_select']/option[49]")).click();
				
			//TourDate
			TourDate = driver.findElement(By.xpath(".//*[@id='pdtour_date']")).getAttribute("value");
				logger.info("TourDate: "+TourDate);
					
						
			//---------------------------------------Booking Agent------------------------------------------------------------//
			
			//TourCreatedBy
			TourCreatedBy = driver.findElement(By.xpath(".//*[@id='pvTorCreatedBy']")).getAttribute("value");
				logger.info("Tour Created By: "+TourCreatedBy);
			
			//TourCreatedBy
			TourCreatedOn = driver.findElement(By.xpath(".//*[@id='pvHTorCreatedOn']")).getAttribute("value");
				logger.info("Tour Created on: "+TourCreatedOn);
				
			//---------------------------------------Booking Agent------------------------------------------------------------//
			
			//Sales Marketing Personal
			SalesMarkPersonal = driver.findElement(By.xpath(".//*[@id='pvAgent_select_to_display']")).getAttribute("value");
				logger.info("OPC/TeleMarketer: "+SalesMarkPersonal);
				
			//---------------------------------------Tour Marketing Information-----------------------------------------------//
			driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).click();
				Select MKCCode = new Select(driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_select']")));
					MKCCode.selectByValue(TourMKCCode);
						logger.info("Selected Marketing Key Code as: "+TourMKCCode);
							Thread.sleep(1000);
							
			//----------------------------------------Book Tour-------------------------------------------------------------//
			driver.findElement(By.xpath(".//*[@id='tour_Book']")).click();
				logger.info("Clicked on Book Tour");
				
				if(driver.findElements(By.xpath("/html/body/div[4]/div/div[3]/div")).size()!= 0){
					
					driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/div")).click();
						logger.info("Clicked on Ok");
						
						Thread.sleep(2000);
						
						driver.findElement(By.xpath(".//*[@id='tour_Book']")).click();
							logger.info("Clicked on Book Tour");
				}
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='NavCloseBtn']")));
			
			String Confirmation = driver.findElement(By.xpath(".//*[@id='NavPopup']/div[1]")).getText();
				logger.info(""+Confirmation);
				
			ResConNum = Confirmation.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
				logger.info("Reservation Confirmation number is: "+ResConNum);
				
			driver.findElement(By.xpath(".//*[@id='NavCustomer360Btn']")).click();
				logger.info("Clicked on Customer360 Button");
				
				Thread.sleep(5000);
				
				if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) //no phone number	
				{
					logger.info("Cliked on Notice---Notice--Notice--- Close Button");
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
				}
				
				else
				{
					logger.info("Alert window not found");
				}
				
				Thread.sleep(1000);
				
				if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) //Points saved information
				{
					logger.info("Cliked on Notice---Notice--Notice--- Close Button");
						driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
				}
				
				else
				{
					logger.info("Alert window not found");
				}
				
			
				driver.findElement(By.xpath("/html/body/div/div[2]/div[3]/div/div")).click();	
					logger.info("Clicked on Marketing Tab");
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='marketingBackButton']"))); //Wait till back to search button is visible
							
							/*// Store the current window handle
							 String winHandleBefore = driver.getWindowHandle();*/
							
			//-----------------------------------------------------------------------------------Tour History------------------------------------------------------------------------//
							WebElement TourHistory = driver.findElement(By.xpath(".//*[@id='mkt_tourhistorytable']/tbody")); //Table Tbody
							List<WebElement> columns = TourHistory.findElements(By.tagName("tr"));
								//logger.info("No.of rows: " +columns.size());
									int rowsnum = columns.size();
											String xpath =null;
												String ResNum;
													
														for(int i = 1;i<=rowsnum;i++)
														{
															xpath= ".//*[@id='mkt_tourhistorytable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
																ResNum = driver.findElement(By.xpath(xpath)).getText();
																		logger.info("Substring is: "+ResNum);
																	
																driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
																
																if(ResConNum.equals(ResNum))
																{
																	logger.info("Confirmation Number Found and Matched");
																		driver.findElement(By.xpath(".//*[@id='mkt_tourhistorytable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
																			logger.info("Clicked on the Flag image of the Confirmation Number");
																			break;
																}
														}
						
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
					    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='tour_Save']"))); //Wait till Save button is clickable
					    
				 //-------------------------------------------------------Tour Information, Booking Agentm Sales and Marketing Personnel------------------------------------------------------//
					   
				    String TourSalesCenter = driver.findElement(By.xpath(".//*[@id='pvsales_center_input']")).getAttribute("value");
					  	Assert.assertEquals(TourSalesCenter, SalesCenter);
					  		logger.info("Assertion Passed: "+TourSalesCenter);
					  		
			  		String TourTourOffice = driver.findElement(By.xpath(".//*[@id='pvtour_sales_office_input']")).getAttribute("value");
					  	Assert.assertEquals(TourTourOffice, TourOffice);
					  		logger.info("Assertion Passed: "+TourTourOffice);
					  		
			  		String TourTourWave = driver.findElement(By.xpath(".//*[@id='pvtour_wave_input']")).getAttribute("value");
					  	Assert.assertEquals(TourTourWave, TourWave);
					  		logger.info("Assertion Passed: "+TourTourWave);

					String TourTourDate = driver.findElement(By.xpath(".//*[@id='pdtour_date']")).getAttribute("value");
					SoftAssert softAssert = new SoftAssert();
					softAssert.assertEquals(TourTourDate, TourDate);
							logger.info("Soft Assertion Passed: "+TourTourDate);
							
					String TourTourCreatedBy = driver.findElement(By.xpath(".//*[@id='pvTorCreatedBy']")).getAttribute("value");
						softAssert.assertEquals(TourTourCreatedBy, TourCreatedBy);
					  		logger.info("Soft Assertion Passed: "+TourTourCreatedBy);
		
					String TourSalesMarkPersonal = driver.findElement(By.xpath(".//*[@id='pvAgent_select_to_display']")).getAttribute("value");
						softAssert.assertEquals(TourSalesMarkPersonal, SalesMarkPersonal);
							logger.info("Soft Assertion Passed: "+TourSalesMarkPersonal);
						
					String TourMKCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
						Assert.assertEquals(TourMKCode, TourMKeyCode);
							logger.info("Assertion Passed: "+TourMKCode);
							
					driver.findElement(By.xpath(".//*[@id='imgSalMktInfo']")).click();
						logger.info("Clicked on Sales and Marketing Personnel flag");
						
						 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='pvSAL_input']"))); 
						 
						 driver.findElement(By.xpath(".//*[@id='pvSAL_input']")).click(); //Click on Sales Person
						 	Select Sales = new Select(driver.findElement(By.xpath(".//*[@id='pvSAL_select']"))); 
						 		Sales.selectByValue(SalesPerson);// Select Sales Person	
						 			logger.info("Selected Sales Person");
						
						 			driver.findElement(By.xpath(".//*[@id='pvSMR_input']")).click(); //Click on Sales Manager
								 		Select Manager = new Select(driver.findElement(By.xpath(".//*[@id='pvSMR_select']"))); 
								 			Manager.selectByValue(SalesManager);// Select Sales Manager	
								 				logger.info("Selected Sales Manager");
								 				
								 				driver.findElement(By.xpath(".//*[@id='personnelSave']")).click();
								 					logger.info("clicked on Apply");
								 				
						Thread.sleep(1000);
						
						driver.findElement(By.xpath(".//*[@id='ChckInChckOut']/table/tbody/tr[2]/td[2]/a/img")).click();
							logger.info("Clicked on checked in Time");
								 				
					
							driver.findElement(By.xpath(".//*[@id='tour_Save']")).click();
								logger.info("Clicked on Save");
								
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='confirm_button1']"))); //Wait till ok button is clickable
									String UpdatedMessage = driver.findElement(By.xpath(".//*[@id='confirm']/div/div[2]/pre")).getText();
										logger.info(""+UpdatedMessage);
							
	}
	
}