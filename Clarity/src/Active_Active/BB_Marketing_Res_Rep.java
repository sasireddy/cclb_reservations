package Active_Active;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;


public class BB_Marketing_Res_Rep extends A_Common {
	
	Logger logger = Logger.getLogger("BB_Marketing_Res_Rep.class");

		public void Reservations() {
			
			PropertyConfigurator.configure("Log4j.properties");
			
			//------------------------------------------------------------------------------------//
			//Reservation Number from Marketing_Reservation.Properties
			try (FileReader reader = new FileReader("Marketing_Reservation.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		ReservationNumber = properties.getProperty("ReservationNumber");
		      		ArrivalDate = properties.getProperty("ArrivalDate");
		      		ResType = properties.getProperty("ResType");
		      		DepartureDate = properties.getProperty("DepartureDate");
		      		MarketingKeyCode = properties.getProperty("MarketingKeyCode");
		      		Property = properties.getProperty("Property");
		      		TourId = properties.getProperty("TourId");
		      		
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			//------------------------------------------------------------------------------------//
		
		
	
				 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

				 			
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[5]/div"))); //wait for MKT Res
							
			//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//					
							WebElement dateWidget = driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody")); //Table Tbody
									List<WebElement> columns = dateWidget.findElements(By.tagName("tr"));
										//logger.info("No.of rows: " +columns.size());
											int rowsnum = columns.size();
													String xpath =null;
														String ResNum;
															
																for(int i = 1;i<=rowsnum;i++)
																{
																	xpath= ".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
																		ResNum = driver.findElement(By.xpath(xpath)).getText();
																				logger.info("Substring is: "+ResNum);
																			
																		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
																		
																		if(ReservationNumber.equals(ResNum))
																		{
																			logger.info("Confirmation Number Found and Matcehd from Marketing Reservaton");
																			
																				driver.findElement(By.xpath(".//*[@id='tvl_resvHistTable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
																					logger.info("Clicked on the Flag image of the Confirmation Number");
																					break;
																		}
																}	
																
								wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RMS_btnSave']"))); //wait for Save Button is Clickable
				 			
								MarketingKeyCode_Rep = driver.findElement(By.xpath(".//*[@id='ResSum_MktKeyCodeDesc']")).getText().trim();
									Assert.assertEquals(MarketingKeyCode, MarketingKeyCode_Rep);
										logger.info("Assertion Passed: "+MarketingKeyCode_Rep);
								
								ResType_Rep = driver.findElement(By.xpath(".//*[@id='summaryDiv']/div[2]/div[1]/table[1]/tbody/tr[3]/td[2]")).getText().trim();
									Assert.assertEquals(ResType, ResType_Rep);
										logger.info("Assertion Passed: "+ResType_Rep);	
										
								Property_Rep = driver.findElement(By.xpath(".//*[@id='ResSum_pvProperty_input']")).getAttribute("value").trim();
									Assert.assertEquals(Property, Property_Rep);
										logger.info("Assertion Passed: "+Property_Rep);
								
								ArrivalDate_Rep = driver.findElement(By.xpath(".//*[@id='ResSum_pdArrivalDate']")).getAttribute("value").trim();
									Assert.assertEquals(ArrivalDate, ArrivalDate_Rep);
										logger.info("Assertion Passed: "+ArrivalDate_Rep);		
										
								DepartureDate_Rep = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value").trim();
									Assert.assertEquals(DepartureDate, DepartureDate_Rep);
										logger.info("Assertion Passed: "+DepartureDate_Rep);	
										
								TourId_Rep = driver.findElement(By.xpath(".//*[@id='resv_tours_Tbl']/tbody/tr[3]/td[1]")).getText().trim();
									Assert.assertEquals(TourId, TourId_Rep);
										logger.info("Assertion Passed: "+TourId_Rep);	
								
		}
		
	}