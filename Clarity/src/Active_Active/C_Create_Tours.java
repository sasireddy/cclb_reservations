package Active_Active;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class C_Create_Tours extends A_Common {

	Logger logger = Logger.getLogger("C_Create_Tours.class");
	
	public void Tours() throws InterruptedException 
	{
		 PropertyConfigurator.configure("Log4j.properties");
		 	WebElement Tours = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div/div/table/tbody/tr[2]/td[2]/div"))); // Wait till the Tours is available to click
		 	Tours.click(); // Click on Tours
		 			logger.info("Clicked on Tours");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				WebElement ToursAvail = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='444']"))); // Wait till the ToursAvailability is available to click
		 				ToursAvail.click(); // Click on Tours
		 			 			logger.info("Clicked on ToursAvailability");
		 			 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

										Thread.sleep(1000);
						
						//Sales center
		 			 	driver.findElement(By.xpath(".//*[@id='selectSalesCenter']")).click();
		 			 		driver.findElement(By.xpath(".//*[@id='selectSalesCenter']")).sendKeys("949");
		 			 			logger.info("Clicked on Sales Center and entered 51");
		 			 		
		 			 	//Tour Date
	 			 		driver.findElement(By.xpath(".//*[@id='tourDate']")).click();
			 			 	driver.findElement(By.xpath(".//*[@id='tourDate']")).sendKeys("t");
			 			 		driver.findElement(By.xpath(".//*[@id='selectSalesOffice']")).click();
			 			 			logger.info("Select Tour Date");
		 			 		
		 			 	
		 			 	//Search
		 			 	driver.findElement(By.xpath(".//*[@id='tourAvailabilitySearchButton']")).click();
		 			 		logger.info("Clicked on Search button");
		 			 		
		 			 		Thread.sleep(2000);
		 			 		
		 			 	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/span/span[2]")).click();
		 			 		logger.info("Click on the [+] sedona summit - sedona, AZ");

								Thread.sleep(1000);

 			 			driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/div[5]/span[2]")).click();
		 			 		logger.info("[+] Summit Inhouse");
		 			 		
		 			 		WebElement dateWidget = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/div[5]/div/table/tbody"));
							List<WebElement> columns=dateWidget.findElements(By.tagName("tr"));
									int rowsnum1 = columns.size();
										int colnum1=21;
											String xpath1 =null;
												
												Search:
														for(int i = 3;i<=rowsnum1;i++)
														{
															for(int j=7;j<=colnum1; j+=4)
															{
																xpath1= "/html/body/div[1]/div[2]/div[1]/div/div/div[5]/div/table/tbody/tr[" + String.valueOf(i) + "]/td[" + String.valueOf(j) + "]";
																
																String olddate = driver.findElement(By.xpath(xpath1)).getAttribute("olddate");
																
																	System.out.println("Value is "+olddate);
																
																if(olddate.equals(Olddate_defined))
																{
																
																	String Available = driver.findElement(By.xpath(xpath1)).getText();
																	
																	Thread.sleep(1000);
																
																	logger.info("Available Count is: "+Available);
																
																	int Availablecount = Integer.parseInt(Available);
																
																	if(Availablecount > 0)
																	{
																		driver.findElement(By.xpath(xpath1)).click();
																			logger.info("Clicked on Available Count");
																				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
																				  break Search;
																																							
																	}
																}
																
																else
																{
																	System.out.println("Olddate Value is Yes: "+olddate);
																}
																																																		
															}
							
														}	
			 			
	}
	
	public void TourBooking() throws InterruptedException, IOException 
	{
		
		// Store the current window handle
		 String winHandleBefore = driver.getWindowHandle();
		 
		//Opens New Window
		 driver.findElement(By.xpath(".//*[@id='confirm_button1']")).click(); //Clicked on yes
			logger.info("Clicked on Yes for continue booking a tour");
	 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			// Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
	
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='tour_Srch_Lead']")));
			
		//Lead Id/Area
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_input']")).click();
				Select LeadArea = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_select']")));
					LeadArea.selectByValue(ToursLeadArea);
						logger.info("Selected Lead Area");
						
						driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).click();
							driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(ToursLead);
								logger.info("Entered Lead Id");
				
								driver.findElement(By.xpath(".//*[@id='tour_Srch_Lead']")).click();	
									logger.info("Clicked on Search");
										Thread.sleep(10000);
			
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='tour_Book']")));
			
			//---------------------------------------Tour Information--------------------------------------------------------//
		
			//Sales Center
			SalesCenter = driver.findElement(By.xpath(".//*[@id='pvsales_center_input']")).getAttribute("value");
				logger.info("SalesCenter: "+SalesCenter);
				
			//TourOffice
			TourOffice = driver.findElement(By.xpath(".//*[@id='pvtour_sales_office_input']")).getAttribute("value");
				logger.info("TourOffice: "+TourOffice);
				
			//TourWave 
			TourWave = driver.findElement(By.xpath(".//*[@id='pvtour_wave_input']")).getAttribute("value");
				logger.info("TourWave: "+TourWave);
				
				Thread.sleep(1000);
				
			//Tour Type
			driver.findElement(By.xpath(".//*[@id='pvtour_type_input']")).click();
				driver.findElement(By.xpath(".//*[@id='pvtour_type_select']/option[49]")).click();
				
			//TourDate
			TourDate = driver.findElement(By.xpath(".//*[@id='pdtour_date']")).getAttribute("value");
				logger.info("TourDate: "+TourDate);
					
						
			//---------------------------------------Booking Agent------------------------------------------------------------//
			
			//TourCreatedBy
			TourCreatedBy = driver.findElement(By.xpath(".//*[@id='pvTorCreatedBy']")).getAttribute("value");
				logger.info("Tour Created By: "+TourCreatedBy);
			
			//TourCreatedBy
			TourCreatedOn = driver.findElement(By.xpath(".//*[@id='pvHTorCreatedOn']")).getAttribute("value");
				logger.info("Tour Created on: "+TourCreatedOn);
				
			//---------------------------------------Booking Agent------------------------------------------------------------//
			
			//Sales Marketing Personal
			SalesMarkPersonal = driver.findElement(By.xpath(".//*[@id='pvAgent_select_to_display']")).getAttribute("value");
				logger.info("OPC/TeleMarketer: "+SalesMarkPersonal);
				
			//---------------------------------------Tour Marketing Information-----------------------------------------------//
			driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).click();
				Select MKCCode = new Select(driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_select']")));
					MKCCode.selectByValue(TourMKCCode);
						logger.info("Selected Marketing Key Code as: "+TourMKCCode);
							Thread.sleep(1000);
							
			//----------------------------------------Book Tour-------------------------------------------------------------//
			driver.findElement(By.xpath(".//*[@id='tour_Book']")).click();
				logger.info("Clicked on Book Tour");
				
				if(driver.findElements(By.xpath("/html/body/div[4]/div/div[3]/div")).size()!= 0){
					
					driver.findElement(By.xpath("/html/body/div[4]/div/div[3]/div")).click();
						logger.info("Clicked on Ok");
						
						Thread.sleep(2000);
						
						driver.findElement(By.xpath(".//*[@id='tour_Book']")).click();
							logger.info("Clicked on Book Tour");
				}
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='NavCloseBtn']")));
			
			String Confirmation = driver.findElement(By.xpath(".//*[@id='NavPopup']/div[1]")).getText();
				logger.info(""+Confirmation);
				
			ResConNum = Confirmation.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
				logger.info("Reservation Confirmation number is: "+ResConNum);
				
			driver.findElement(By.xpath(".//*[@id='NavCloseBtn']")).click();
				logger.info("Clicked on Close Button");
				
			driver.switchTo().window(winHandleBefore);
			
			driver.findElement(By.xpath("html/body/div[1]/div[1]/div[3]/span/span[2]/a[1]")).click();
				logger.info("Clicked on Menu Button");
	
				//----------------------------------------------------------------------------//
				try {
						Properties properties = new Properties();
					 		properties.setProperty("ResConNum",ResConNum);
			 					properties.setProperty("SalesCenter", SalesCenter);
			 						properties.setProperty("TourOffice",TourOffice);
			 							properties.setProperty("TourWave", TourWave);
			 								properties.setProperty("TourDate", TourDate);
			 									properties.setProperty("TourCreatedBy", TourCreatedBy);
			 										properties.setProperty("TourCreatedOn", TourCreatedOn);
			 											properties.setProperty("SalesMarkPersonal", SalesMarkPersonal);
			 										
							
							File file = new File("Create_Tours.Properties");
							FileOutputStream fileOut = new FileOutputStream(file);
							properties.store(fileOut, "Create Tours Information");
							fileOut.close();
					}

					catch (FileNotFoundException e) {
						e.printStackTrace();
						}
				//----------------------------------------------------------------------------//
				
			
	}
	
	public void Create_Tours_Rep() {
		//Reservation Number from Create_Tours.Properties
				try (FileReader reader = new FileReader("Create_Tours.Properties")) {
					 Properties properties = new Properties();
			      		properties.load(reader);
								
						ResConNum = properties.getProperty("ResConNum");
							SalesCenter = properties.getProperty("SalesCenter");
								TourOffice = properties.getProperty("TourOffice");
									TourWave = properties.getProperty("TourWave");
										TourDate = properties.getProperty("TourDate");
											TourCreatedBy = properties.getProperty("TourCreatedBy");
												TourCreatedOn = properties.getProperty("TourCreatedOn");
													SalesMarkPersonal = properties.getProperty("SalesMarkPersonal");
			      		
			      	} catch (IOException e) {
			      		e.printStackTrace();
			      	}
				
				
			
	}
	
	public void Customer360() throws InterruptedException 
	{
	 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
	 		customer360.click(); // Click on Customer 360
	 			logger.info("Clicked on Customer 360");
	 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
											
		//LeadArea/Lead Id	
		driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_input']")).click();
		Select LeadArea = new Select(driver.findElement(By.xpath(".//*[@id='pvSearchLead_area_select']")));
			LeadArea.selectByValue(ToursLeadArea);
				logger.info("Selected Lead Area");
				
				driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).click();
					driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(ToursLead);
						logger.info("Entered Lead Id: "+ToursLead);
		
						driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();	
							logger.info("Clicked on Search");
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='searchResultsData']/tbody/tr/td[1]")));
							
						driver.findElement(By.xpath(".//*[@id='searchResultsData']/tbody/tr/td[1]")).click();
							logger.info("Clicked on the first result");
						
							if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) //no phone number	
							{
								logger.info("Cliked on Notice---Notice--Notice--- Close Button");
									driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
							}
							
							else
							{
								logger.info("Alert window not found");
							}
							
						
						driver.findElement(By.xpath("/html/body/div/div[2]/div[3]/div/div")).click();	
							logger.info("Clicked on Marketing Tab");
							
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='marketingBackButton']"))); //Wait till back to search button is visible
							
							/*// Store the current window handle
							 String winHandleBefore = driver.getWindowHandle();*/
							
			//-----------------------------------------------------------------------------------Tour History------------------------------------------------------------------------//
							WebElement TourHistory = driver.findElement(By.xpath(".//*[@id='mkt_tourhistorytable']/tbody")); //Table Tbody
							List<WebElement> columns = TourHistory.findElements(By.tagName("tr"));
								//logger.info("No.of rows: " +columns.size());
									int rowsnum = columns.size();
											String xpath =null;
												String ResNum;
													
														for(int i = 1;i<=rowsnum;i++)
														{
															xpath= ".//*[@id='mkt_tourhistorytable']/tbody/tr[" +String.valueOf(i)+ "]/td[2]";
																ResNum = driver.findElement(By.xpath(xpath)).getText();
																		logger.info("Substring is: "+ResNum);
																	
																driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
																
																if(ResConNum.equals(ResNum))
																{
																	logger.info("Confirmation Number Found and Matched");
																		driver.findElement(By.xpath(".//*[@id='mkt_tourhistorytable']/tbody/tr[" +String.valueOf(i)+ "]/td[1]/img")).click();
																			logger.info("Clicked on the Flag image of the Confirmation Number");
																			break;
																}
														}
						
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
					    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='tour_Save']"))); //Wait till Save button is clickable
					    
				 //-------------------------------------------------------Tour Information, Booking Agentm Sales and Marketing Personnel------------------------------------------------------//
					   
				    String TourSalesCenter = driver.findElement(By.xpath(".//*[@id='pvsales_center_input']")).getAttribute("value");
					  	Assert.assertEquals(TourSalesCenter, SalesCenter);
					  		logger.info("Assertion Passed: "+TourSalesCenter);
					  		
			  		String TourTourOffice = driver.findElement(By.xpath(".//*[@id='pvtour_sales_office_input']")).getAttribute("value");
					  	Assert.assertEquals(TourTourOffice, TourOffice);
					  		logger.info("Assertion Passed: "+TourTourOffice);
					  		
			  		String TourTourWave = driver.findElement(By.xpath(".//*[@id='pvtour_wave_input']")).getAttribute("value");
					  	Assert.assertEquals(TourTourWave, TourWave);
					  		logger.info("Assertion Passed: "+TourTourWave);

					String TourTourDate = driver.findElement(By.xpath(".//*[@id='pdtour_date']")).getAttribute("value");
					SoftAssert softAssert = new SoftAssert();
					softAssert.assertEquals(TourTourDate, TourDate);
							logger.info("Soft Assertion Passed: "+TourTourDate);
							
					String TourTourCreatedBy = driver.findElement(By.xpath(".//*[@id='pvTorCreatedBy']")).getAttribute("value");
						softAssert.assertEquals(TourTourCreatedBy, TourCreatedBy);
					  		logger.info("Soft Assertion Passed: "+TourTourCreatedBy);
		
					String TourSalesMarkPersonal = driver.findElement(By.xpath(".//*[@id='pvAgent_select_to_display']")).getAttribute("value");
						softAssert.assertEquals(TourSalesMarkPersonal, SalesMarkPersonal);
							logger.info("Soft Assertion Passed: "+TourSalesMarkPersonal);
						
					String TourMKCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
						Assert.assertEquals(TourMKCode, TourMKeyCode);
							logger.info("Assertion Passed: "+TourMKCode);
							
					driver.findElement(By.xpath(".//*[@id='imgMktInfo']")).click();
						logger.info("Clicked on MKC Flag");
						
					 wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mktSaveButton']"))); //Wait till Save button is clickable
					 
				 //-----------------------------------------Marketing Information------------------------------------------------------------------//
					 Mktinfo = driver.findElement(By.xpath(".//*[@id='KEYCODE']")).getAttribute("value");
					 	logger.info("Marketing Key Code: "+Mktinfo);
					 	
				 	 MktProduct = driver.findElement(By.xpath(".//*[@id='PRODUCTCODE']")).getAttribute("value");
					 	logger.info("Product: "+MktProduct);
					 		
				 	 MktPromotion = driver.findElement(By.xpath(".//*[@id='PROMO']")).getAttribute("value");
					 	logger.info("Promotion: "+MktPromotion);
					 	
				 	 MktSource = driver.findElement(By.xpath(".//*[@id='SOURCE']")).getAttribute("value");
					 	logger.info("Marketing Source: "+MktSource);
					 	
				 	 MktSummary = driver.findElement(By.xpath(".//*[@id='SUMMARY']")).getAttribute("value");
					 	logger.info("Marketing Summary: "+MktSummary);
					 	
				 	 MktProgram = driver.findElement(By.xpath(".//*[@id='PROGRAM']")).getAttribute("value");
					 	logger.info("Marketing Program: "+MktProgram);
					 		
				 	 MktCompany = driver.findElement(By.xpath(".//*[@id='COMPANY']")).getAttribute("value");
					 	logger.info("Marketing Company: "+MktCompany);
					 	
				 	 MktBoxEvent = driver.findElement(By.xpath(".//*[@id='BOX']")).getAttribute("value");
					 	logger.info("Box/Event: "+MktBoxEvent);
					 	
				 	 MktCallCenter = driver.findElement(By.xpath(".//*[@id='CALLCENTER']")).getAttribute("value");
					 	logger.info("Call Center: "+MktCallCenter);
					 	
				 	 MktChannel = driver.findElement(By.xpath(".//*[@id='CHANNEL']")).getAttribute("value");
					 	logger.info("Channel: "+MktChannel);
						
					 	
					 	
	}
	 
	
	public void DatabaseConfirm() throws SQLException {
		 			  
		 //Initialize
			try {
		
				Class.forName("oracle.jdbc.driver.OracleDriver");
		
			} catch (ClassNotFoundException e) {
		
				logger.info("Oracle JDBC server not found");
				e.printStackTrace();
				return;
			}
			
			logger.info("Oracle JDBC Driver Registered!");
		
			//Connect to Database
			try {
		
				connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=on)(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1550)))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "sreddy","bhushan22");
		
			} catch (SQLException e) {
		
				logger.info("Connection Failed! Check output console");
				e.printStackTrace();
				return;
			}
		
			//Retrive data
			if (connection != null) {
				logger.info("Logged into Clarity Active DB Successfully");
				
				//--------------------------------------------------------First Query-------------------------------------------------------------------------------------//
				
				//Query
				prepared = connection.prepareStatement("Select P.OFFICE_CODE, P.MKT_ACTIVITY_ID, P.TOUR_TYPE_CODE FROM PREMIER.P_TOUR P where tour_id = ?");
				//Enter Confirmation Number in Query
				prepared.setString(1, ResConNum);
				//Execute the query		
				prepared.executeUpdate();
				
				while (prepared.getResultSet().next()) {
					
					String OFFICE_CODE = prepared.getResultSet().getString("OFFICE_CODE"); 
					
					MKT_ACTIVITY_ID = prepared.getResultSet().getString("MKT_ACTIVITY_ID"); 
					
					String TOUR_TYPE_CODE = prepared.getResultSet().getString("TOUR_TYPE_CODE");
					
					logger.info("------------------------------------------From Clarity------------------------------------------------");
						logger.info("\nSalesOffice: " + SalesOffice + "\nToursTourType: " +ToursTourType);
					
					logger.info("------------------------------------------From TOAD------------------------------------------------");
						logger.info("\nOFFICE_CODE: " + OFFICE_CODE + "\nMKT_ACTIVITY_ID: " + MKT_ACTIVITY_ID +  "\nTOUR_TYPE_CODE: " +TOUR_TYPE_CODE);
					
					Assert.assertEquals(OFFICE_CODE, SalesOffice);
					 	logger.info("Assertion Passed: "+SalesOffice);
					 
					 Assert.assertEquals(TOUR_TYPE_CODE, ToursTourType);
					 	logger.info("Assertion Passed: "+ToursTourType);
			
					}
					connection.close();
				}
				
				else 
				{
				logger.info("Failed to make connection!");
				}
	}
	
	public void DatabaseConfirm_Activity() throws SQLException {
				 			  
		 //Initialize
			try {
		
				Class.forName("oracle.jdbc.driver.OracleDriver");
		
			} catch (ClassNotFoundException e) {
		
				logger.info("Oracle JDBC server not found");
				e.printStackTrace();
				return;
			}
			
			logger.info("Oracle JDBC Driver Registered!");
		
			//Connect to Database
			try {
		
				connection = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=on)(ADDRESS_LIST=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=dc1-ora-pdba25.global.ldap.wan)(PORT=1550)))(CONNECT_DATA=(SERVICE_NAME=atlas_gds.atlas_pool.oradbcloud)))", "sreddy","bhushan22");
		
			} catch (SQLException e) {
		
				logger.info("Connection Failed! Check output console");
				e.printStackTrace();
				return;
			}
		
		//Retrive data
		if (connection != null) {
				logger.info("Logged into STAGE DB Successfully");
	
				logger.info("-------------------------------------------------------Activity id----------------------------------------------------------------------");
				
				//Query
				prepared = connection.prepareStatement("SELECT ROWID, P.PRODUCT_CODE, P.SOURCE_CODE, P.MKT_PROGRAM_CODE,  P.MKT_KEY_CODE, P.MKT_COMPANY_ID, P.BOX_CODE, P.CALL_CENTER_CODE,P.CHANNEL_CODE FROM PREMIER.P_MKT_ACTIVITY_HISTORY P where mkt_activity_id = ?");
				//Enter Confirmation Number in Query
				prepared.setString(1, MKT_ACTIVITY_ID);
				//Execute the query
				prepared.executeUpdate();
				
				while (prepared.getResultSet().next()) {
					
					String PRODUCT_CODE = prepared.getResultSet().getString("PRODUCT_CODE"); 
					
					String SOURCE_CODE	 = prepared.getResultSet().getString("SOURCE_CODE");
		
					String MKT_PROGRAM_CODE = prepared.getResultSet().getString("MKT_PROGRAM_CODE"); 
					
					String MKT_KEY_CODE = prepared.getResultSet().getString("MKT_KEY_CODE"); 
					
					String MKT_COMPANY_ID = prepared.getResultSet().getString("MKT_COMPANY_ID");
					
					String BOX_CODE = prepared.getResultSet().getString("BOX_CODE"); 		
					
					String CALL_CENTER_CODE = prepared.getResultSet().getString("CALL_CENTER_CODE");
					
					String CHANNEL_CODE = prepared.getResultSet().getString("CHANNEL_CODE");
					
					logger.info("------------------------------------------From Clarity------------------------------------------------");
					logger.info("\nProduct: " + MktProduct + "\nMarketing Source: " + MktSource +  "\nMarketing Program: " +MktProgram+ "\nMarketing Key Code: " + Mktinfo + "\nMarketing Company: " + MktCompany +  "\nBox/Event: " +MktBoxEvent+ "\nCall Center: " + MktCallCenter +  "\nChannel: " +MktChannel);
					
					logger.info("------------------------------------------From TOAD------------------------------------------------");
					logger.info("\nPRODUCT_CODE: " + PRODUCT_CODE + "\nSOURCE_CODE: " + SOURCE_CODE +  "\nMKT_PROGRAM_CODE: " +MKT_PROGRAM_CODE+ "\nMKT_KEY_CODE: " + MKT_KEY_CODE + "\nMKT_COMPANY_ID: " + MKT_COMPANY_ID +  "\nBOX_CODE: " +BOX_CODE+ "\nCALL_CENTER_CODE: " + CALL_CENTER_CODE +  "\nCHANNEL_CODE: " +CHANNEL_CODE);
					
					Assert.assertEquals(PRODUCT_CODE, MktProduct);
				 		logger.info("Assertion Passed: "+MktProduct);
				 		
			 		Assert.assertEquals(SOURCE_CODE, MktSource);
				 		logger.info("Assertion Passed: "+MktSource);
				 		
			 		Assert.assertEquals(MKT_PROGRAM_CODE, MktProgram);
				 		logger.info("Assertion Passed: "+MktProgram);
				 		
			 		Assert.assertEquals(MKT_KEY_CODE, Mktinfo);
				 		logger.info("Assertion Passed: "+Mktinfo);
				 		
			 		Assert.assertEquals(MKT_COMPANY_ID, MktCompany);
				 		logger.info("Assertion Passed: "+MktCompany);
				 		
			 		Assert.assertEquals(BOX_CODE, MktBoxEvent);
				 		logger.info("Assertion Passed: "+MktBoxEvent);
				 		
			 		Assert.assertEquals(CALL_CENTER_CODE, MktCallCenter);
				 		logger.info("Assertion Passed: "+MktCallCenter);
				 		
			 		/*Assert.assertEquals(CHANNEL_CODE, MktChannel);
				 		logger.info("Assertion Passed: "+MktChannel);*/
		
				}
			}
			else 
			{
			logger.info("Failed to make connection!");
			}
		}
	
	
}