package Active_Active;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class B_Marketing_Res extends A_Common {
	
	Logger logger = Logger.getLogger("B_Marketing_Res.class");
	
	public void Customer360() 
	{
		  PropertyConfigurator.configure("Log4j.properties");
		 WebDriverWait wait = new WebDriverWait(driver, 30);
		 	WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
		 		customer360.click(); // Click on Customer 360
		 			logger.info("Clicked on Customer 360");
		 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 				
		 				driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span/input")).click(); //Click on the drop down
		 					driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		 												
								
			driver.findElement(By.xpath(".//*[@id='pvSearchLead_id']")).sendKeys(MarketingLead); //Enter DBL LEAD Id
				logger.info("Lead ID is: "+MarketingLead);	 
					driver.findElement(By.xpath(".//*[@id='searchSearchButton']")).click();// click on search
		
				String LeadType = "MBR";
		
				Search:
				 for (int i=1; i<=10;i++)
				 {
					 for(int j=2;j<=2;j++)
					 {
						 String LeadType1 = null;
							String xpath  = null;
								xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
									LeadType1 = driver.findElement(By.xpath(xpath)).getText();
											driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
										if(LeadType.equals(LeadType1))
										{
													driver.findElement(By.xpath(xpath)).click();
														logger.info("Lead is a Member");
														
														if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) //no phone number	
														{
															logger.info("Cliked on Notice---Notice--Notice--- Close Button");
																driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
														}
														
														else
														{
															logger.info("Alert window not found");
														}
														
														if(driver.findElements(By.xpath(".//*[@id='alert']/div/div[3]/div")).size()!=0) // Points
														{
															logger.info("Cliked on Notice---Notice--Notice--- Close Button");
																driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
														}
														
														else
														{
															logger.info("Alert window not found");
														}
															//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div")));
															//driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
														
															wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")));
																String LeadName = driver.findElement(By.xpath(".//*[@id='leadinfoDiv']/div[1]/div/table[1]/tbody/tr/td[2]/span[1]")).getText();
																	logger.info("LeadName: " +LeadName);
																		break Search;				
										}
										driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
										
										     
										
					 }
				 }
		}
	
	
	public void Reservations() {
		
		PropertyConfigurator.configure("Log4j.properties");
		
		 winHandlebefore = driver.getWindowHandle();
		   
		

			 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
			 
			 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
			 		logger.info("clicked on Reservations Tab");
			 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			 			
			 			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[5]/div"))); //wait for MKT Res
			 					
						//Opens New Window
						driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[5]/div")).click(); //Clicked on MKT Res
					 		logger.info("clicked on Marketing Res");

						// Switch to new window opened
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
		}
	
	public void Marketing_Res() {
		
		//Select Reservation Type
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='rt_pvResvType_input']"))); //wait till Reservation Type is Clickable
		driver.findElement(By.xpath(".//*[@id='rt_pvResvType_input']")).click(); //Click on Reservation Type
			Select ResType = new Select(driver.findElement(By.id("rt_pvResvType_select")));
				ResType.selectByValue(Reservation_Type); //Select by Value
					logger.info("Reservation Type: "+Reservation_Type);
		
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='MARKETINGKEYCODE_input']")));
						
						//Select Marketing key code
						driver.findElement(By.xpath(".//*[@id='MARKETINGKEYCODE_input']")).click(); //Click on Marketing key code
							Select MKCode = new Select(driver.findElement(By.id("MARKETINGKEYCODE_select")));
							MKCode.selectByValue(MKeyCode); //Select by Value
									logger.info("Marketing Key Code: "+MKeyCode);
										
										try {
											Thread.sleep(2000);
										} catch (InterruptedException e) {							
											e.printStackTrace();
										}
											
	}
	
	public void Stay_Parameters() {
		
		//Select Property
				driver.findElement(By.xpath(".//*[@id='sp_pvProperty_input']")).click(); //Click on Reservation Type
					driver.findElement(By.xpath(".//*[@id='sp_pvProperty_select']/option[3]")).click();
						logger.info("Property Selected as Arizona");
								driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
								
								driver.findElement(By.xpath(".//*[@id='sp_pvNumAdults']")).sendKeys(Guests);
								
								//Arrival
								Search:
								for(int n=20;n<=100;n++)
								{
									int r = n+1;
									String u = String.valueOf(r);
									
									String t = "t";
									
									driver.findElement(By.xpath(".//*[@id='sp_pvArrivalDate']")).click(); //Arrival Date
									
									driver.findElement(By.xpath(".//*[@id='sp_pvArrivalDate']")).clear();
									
									driver.findElement(By.xpath(".//*[@id='sp_pvArrivalDate']")).sendKeys(t+u); //Arrival Date
									
									driver.findElement(By.xpath(".//*[@id='sp_pvNumNights']")).clear();
									
									driver.findElement(By.xpath(".//*[@id='sp_pvNumNights']")).sendKeys(Nights);
									
									driver.findElement(By.xpath(".//*[@id='sp_pvOffsetNights']")).click(); //Flexible days
									
									driver.findElement(By.xpath(".//*[@id='sp_buttonProcess']")).click(); //Search
									
									Boolean iselementpresent = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div/table/tbody/tr/td/input")).size()!= 0; 
									
									if (iselementpresent == true) 
									{ 
										
										driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/table/tbody/tr[1]/td[1]/input")).click();
										logger.info("Clicked on the first resort available");
										break Search;
									} 
									
									else 
									{ 
										String Text = driver.findElement(By.xpath(".//*[@id='AvailScrollContent']/table[2]/tbody/tr[1]/td")).getText();
										logger.info(Text); 
												
									} 

								}
								
								List<WebElement> Checkbox = driver.findElements(By.name("TourAvailRadio"));
								
								int size = Checkbox.size();
								
								logger.info(size);
								
								for(int i=0;i<size;i++)
								{
									String value = Checkbox.get(i).getAttribute("name");
									
									if(value.equalsIgnoreCase("TourAvailRadio"))
									{
										Checkbox.get(i).click();
										logger.info("Clicked on First Check box");
										break;
									}
								}
								
								driver.findElement(By.xpath(".//*[@id='ASWBtnProcess']")).click();
									logger.info("Clicked on Continue");
											
								driver.findElement(By.xpath(".//*[@id='REWbtnClose']")).click();
									logger.info("Additional Resort Information Pop Up Closed");
	}
	
		public void Reservation_Processing() throws IOException {
									
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='MTPF_OPC_OP3_DESC']")));
			
			Reservation = driver.findElement(By.xpath(".//*[@id='SubFormDIV2']/div[1]/div[1]/table[1]/tbody/tr[1]/td")).getText(); //Reservation
				logger.info("Reservation Number is: "+Reservation);
				
			ReservationNumber = Reservation.substring(13, 22); //Reservation Number
				logger.info(""+ReservationNumber);
				
			MarketingKeyCode = driver.findElement(By.xpath(".//*[@id='ResSum_MktKeyCodeDesc']")).getText(); //Marketing Key Code
				logger.info("Marketing Key Code: "+MarketingKeyCode);
				
			ResType = driver.findElement(By.xpath(".//*[@id='SubFormDIV2']/div[1]/div[1]/table[1]/tbody/tr[3]/td[2]")).getText(); //Reservation Type
				logger.info("Reservation Type is: "+ResType);
				
			Property  = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText(); //Property
				logger.info("Property is: "+Property);
				
			ArrivalDate = driver.findElement(By.xpath(".//*[@id='SubFormDIV2']/div[1]/div[1]/table[1]/tbody/tr[6]/td[2]")).getText(); //Arrival Date
				logger.info("Arrival Date is: "+ArrivalDate);
					
			DepartureDate = driver.findElement(By.xpath(".//*[@id='SubFormDIV2']/div[1]/div[1]/table[1]/tbody/tr[8]/td[2]")).getText(); //Departure Date
				logger.info("Departure Date is: "+DepartureDate);
				
			TourId = driver.findElement(By.xpath(".//*[@id='resv_tours_Tbl']/tbody/tr[3]/td[1]")).getText(); //Tour id
				logger.info("Tour Id: "+TourId);
				
			//Marketing Personnel
				
			driver.findElement(By.xpath(".//*[@id='MTPF_OPC_OP3_DESC']")).click();
				logger.info("Clicked on operator OPC3/Activator");
				
			driver.findElement(By.id("selectbox_search")).sendKeys(Operator);
				logger.info("Entered Name in Search box");
				
			driver.findElement(By.xpath(".//*[@id='selectboxSearch']")).click();
				logger.info("Clicked on Search");
				
			driver.findElement(By.xpath(".//*[@id='clarityselectboxtable']/tbody/tr[2]/td[1]")).click();
				logger.info("Clicked on Avail Operator");
				
			//Payment information
				
				//Add Credit Card
				driver.findElement(By.xpath(".//*[@id='pvpaycc_input']")).click();
					Select Payment = new Select(driver.findElement(By.xpath(".//*[@id='pvpaycc_select']"))); 
						Payment.selectByValue("0");// Select Booking Type
				
				//Credit Card Number
				driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).click();
					driver.findElement(By.xpath(".//*[@id='pvCCNumber']")).sendKeys(CCNumber); //Credit Card Number
						driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					
				/*//Credit Card Type
				String CCType1 = driver.findElement(By.xpath(".//*[@id='pvCCType_input']")).getAttribute("value");//Get Credit Card Type Value 
					logger.info(""+CCType1);
					*/
				//Card Holder Name
					driver.findElement(By.xpath(".//*[@id='pvChName']")).click();
				driver.findElement(By.xpath(".//*[@id='pvChName']")).sendKeys(CCName); //Card Holder Name
				
				//Exp mnth & Year
				driver.findElement(By.xpath(".//*[@id='pvExpMonth_input']")).click();
					Select ExpMth = new Select(driver.findElement(By.xpath(".//*[@id='pvExpMonth_select']"))); 
						ExpMth.selectByValue(ExpMonth);// Exp Mnth
				
				driver.findElement(By.xpath(".//*[@id='pvExpYear_input']")).click();
					Select ExpYear = new Select(driver.findElement(By.xpath(".//*[@id='pvExpYear_select']"))); 
						ExpYear.selectByValue(ExpYr);// Exp Year
					
				//Active
				driver.findElement(By.xpath(".//*[@id='pvActive_input']")).click();
					Select Active = new Select(driver.findElement(By.xpath(".//*[@id='pvActive_select']"))); 
						Active.selectByValue("Y");// Active
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				
				//DRI Credit Card Y/N
				driver.findElement(By.xpath(".//*[@id='pvDRICC_input']")).click();
					Select Dricc = new Select(driver.findElement(By.xpath(".//*[@id='pvDRICC_select']"))); 
						Dricc.selectByValue("N");// DRI Credit Card	
				
				//Click on Save Button
				driver.findElement(By.xpath(".//*[@id='CCSaveBtn']")).click();
					logger.info("Clicked on Save Button");
				
				//Credit Card 
				driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();	
				logger.info("Clicked on OK button");
				
				//Process
				driver.findElement(By.xpath(".//*[@id='SubFormDIV2']/div[3]/div/table/tbody/tr/td[3]/div")).click();
					logger.info("Clicked on Process button");
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='CompleteMsgBox']/div[3]/div")));
					
					//----------------------------------------------------------------------------//
					try {
							Properties properties = new Properties();
						 		properties.setProperty("ReservationNumber",ReservationNumber);
				 					properties.setProperty("MarketingKeyCode", MarketingKeyCode);
				 						properties.setProperty("ResType",ResType);
				 							properties.setProperty("Property", Property);
				 								properties.setProperty("ArrivalDate", ArrivalDate);
				 									properties.setProperty("DepartureDate", DepartureDate);
				 										properties.setProperty("TourId", TourId);
				 										
								
								File file = new File("Marketing_Reservation.Properties");
								FileOutputStream fileOut = new FileOutputStream(file);
								properties.store(fileOut, "Marketing Reservation Information");
								fileOut.close();
						}

						catch (FileNotFoundException e) {
							e.printStackTrace();
							}
					//----------------------------------------------------------------------------//
					
				//Reservation Confirmation
				String ResConfirm = driver.findElement(By.xpath(".//*[@id='CompleteMsgBoxMsgBoxContent']")).getText();
					logger.info("Reservation Confirmation: "+ResConfirm);
					
					int len = ResConfirm.length();
						logger.info("Lengths is: "+len);
					
				String ResConfirm1 = ResConfirm.substring(74,83);
					logger.info("Reservation Number: "+ResConfirm1);
						Assert.assertEquals(ReservationNumber, ResConfirm1);
							logger.info("Reservation Number Matches");
							
							driver.switchTo().window(winHandlebefore);

				//Close
				//driver.findElement(By.xpath(".//*[@id='CompleteMsgBox']/div[3]/div")).click();
					//logger.info("Clicked on close button");
							
							

				
			
	}
		
}

