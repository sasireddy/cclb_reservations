package Active_Active;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;

public class AA_OpenBrowser extends A_Common{
	
	Logger logger = Logger.getLogger("B_Marketing_Res_DB.class");
	
	public void openBrowser() throws InterruptedException  {
		  
		  PropertyConfigurator.configure("Log4j.properties");
		driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL STAGE
			// driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL TEST
			// driver.get("http://ausdev.diamondresorts.com/pls/claritydev/sign_in"); // URL Dev
				driver.manage().window().maximize();
					Thread.sleep(2000);
					
					driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys(Username); //Entering Username
						logger.info("Entered Username");
							driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
								
							driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys(Password);// Entering Password
								logger.info("Entered Password");
									driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
										logger.info("Clicked on Login Button");
											Thread.sleep(2000);	
												
	}

}
