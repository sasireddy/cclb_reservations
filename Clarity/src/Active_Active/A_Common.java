package Active_Active;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A_Common {

	static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 80);
	
	//DB
	Connection connection = null;
	PreparedStatement prepared = null;

	String Username = "sreddy";
	String Password = "Bhushan23";
	//-----------------------------------------------------------------Marketing Reservation------------------------------------------------------------//
	String MarketingLead = "313050";  //Lead	
	String Reservation_Type = "MKT__MKT";	//Reservation Type	
	String MKeyCode = "DARNT"; //Marketing Key Cide
	String Guests = "2";
	String Nights = "3";
	String NotAvail = "No reservation availability was found for the specified criteria. Please modify your search results and try again.";
	String Operator = "Derrick";
	String CCNumber = "4321000000001119";
	String CCType = "Visa";
	String CCName = "Diamond Tester";
	String ExpMonth = "01";
	String ExpYr = "20";
	
	//-----------------B_Marketing_Res
	String Reservation; //Save Reservation Number
	String ReservationNumber; //Shortlist from Reservation
	String MarketingKeyCode; //Save MarketingKeyCode
	String ResType; //Reservation Type
	String Property; //Save Property
	String ArrivalDate; //Arrival Date
	String DepartureDate; //Departure Date
	String TourId; // Tour id
	String winHandlebefore;
	String winHandleAfter;
	//---------------------B_Marketing_Res_Rep
	String MarketingKeyCode_Rep; //Save MarketingKeyCode
	String ResType_Rep; //Reservation Type
	String Property_Rep; //Save Property
	String ArrivalDate_Rep; //Arrival Date
	String DepartureDate_Rep; //Departure Date
	String TourId_Rep; // Tour id
	//-----------------------------------------------------------------Create Tours------------------------------------------------------------//
	String Olddate_defined = "N";
	String ToursLeadArea = "9";
	String ToursLead = "185815518";
	String SalesOffice = "SST";
	String ToursTourType = "LIN";
	String TourType1 = "Regular Line";
	String TourMKCCode = "CLUB";
	String TourMKeyCode = "CLUB - CLUB";
	
	//--------Tour Information
	String SalesCenter; //SalesCenter
	String TourOffice; //TourOffice
	String TourWave; //TourWave
	String TourDate; //TourDate
	
	//--------Booking Agent
	String TourCreatedBy; //TourCreatedBy
	String TourCreatedOn; //TourCreatedOn
	
	//--------Sales and Marketing Personnel
	String SalesMarkPersonal; //Sales Marketing Personal
	
	//-----------Marketing Information
	String Mktinfo; //Marketing Key Code  
 	String MktProduct; //Product
 	String MktPromotion; //Promotion
 	String MktSource; //Source
 	String MktSummary; //Marketing Summary 
 	String MktProgram; //Marketing Program
 	String MktCompany; //Marketing Company
 	String MktBoxEvent; //Box/Event
 	String MktCallCenter; //Call Center
 	String MktChannel; //Channel
	String MKT_ACTIVITY_ID; //Marketing Activity id from Toad
	
	//-----------Reservation Confirmation Number
	String ResConNum; //Confirmation 
	
	//-----------Sales Person and Sales Manager
	String SalesPerson = "945979";
	String SalesManager = "910600";
	//-----------------------------------------------------------------Check- In/Check out------------------------------------------------------------//
	String PropertyType = "PPR";
	String ReservationType = "CLB";
	String FrontDesk = "OPN";
	String RmConditon = "CLN";
	String HsOccupancy = "N";
	String FDOccupancy = "N";
	String HouseKeepingEmp = "N";
	String HouseKeepingCln = "CLN";
	String Phone = "";
	String ReserStatus = "CHI";
	String Folio = "$ 0.00";
	
	//-----------------------------------------------------------------Credit Card Payments------------------------------------------------------------//
	String PaymentsLead = "112196";
	String PaymentsLeadArea = "21";
	String ClubPayment = "12.58";
	String FinanPayType = "cc";
	
	//-----------------------------------------------------------------Hotel Reservation------------------------------------------------------------//
	String HotelResLead = "224688";
	String HotelResLeadArea = "9";
	String HotelReservationType = "REN";
	String HotelMKCode = "RENDRIWEB";
	
	//-----------------------------------------------------------------Owner Reservation------------------------------------------------------------//
	String OwnerResLead = "1172229800";
	String OwnerResLeadArea = "51";
	String BookingPurpose = "CLU"; //Clarity Use
	String SelectFriday = "t+1"; //Select Friday and keep changing the number
	
	//-----------------------------------------------------------------Contract Entry------------------------------------------------------------//
	
	//Contract Information
	String ContractLeadArea = "9"; //Lead Area
	String ContractLeadnumber = "475800"; //Lead Number 
	String ContractSalesCenter = "593469"; //Sales Center Select Value
	String ContractSalesCenterName; //Sales Center Name
	String SalesCenterCode = "CAN"; //Sales Center Value
	String ContractNumber; //Contract Number
	String ContractContractType = "NEW"; //Contract Type
	String ContractPurchaseType = "FUL"; //Purchase Type
	String ContractPurchaseDate; //Purchase Date
	String ContractVesting; //Vesting
	String ContractVestingValue = "NVR"; //Contract Select Value
	
	//Lead Record
	String ContractFirstName = "Diamond"; //First NAme
	String ContractLastName = "Tester"; //Last Name
	String ContractFicoScore = "500"; //Fico Score
	String ContractLegalTitle = "2"; //Husband and Wife
	String ContractLegal_Title; //Legal Title
	String ContractAddress = "10600 W Charleston Blvd"; //Address
	String ContractCountry = "USA"; //Country
	String ContractCity; //City
	String ContractPostal = "89135"; //Postal
	
	//Inventory Selection
	String ContractProject = "CSV";  //Project
	String ContractUsageSellCode = "PPTS"; //Usage Sell code
	String ContractGroup = "476"; //Group Code
	String ContractPointPackage = "5000"; //Point Package
	String Contract_Project; //Project Print Text
	String Contract_UsageSellCode; //Print Usage Sell Code F
	String Contract_Group; //Print Group
	
	//Owner Services Setup
	String ContractClubMembership = "CS1"; //Club Membership
	String ContractMainFees = "5"; //Fees
	String ContractLinkthisContract = "-1"; //Link this contract to 

	//Inventory Pricing
	String ContractCurrency; //Currency
	String ContractListPrice; //List Price
	String ContractPurchaseAmount; //Purchase Amount
	String ContractGrossPurchasePrice; //Gross Purchase Price
	String FinanceTypeSelection = "CSH"; // Finance Type Selection
	String ContractTerm; //Term
	String ContractFrequency; //Frequency
	String ContractInterest; //Interest
	String ContractFirstPayment; //First Payment
	String ContractLoanServiceFee; //Loan Service Fee
	String ContractPaymentMethod = "CRD"; //Payment Method
	
	//----------------------------------------------------Create New LeadID----------------------------------------------
	String NewLeadid = "200222653"; //Lead id to create new
	String NewFirstName = "Automation";
	String NewLastName = "Testers";
	String NewSecondaryFirstName = "Selenium";
	String NewSecondaryLastName = "Java";
	String NewCountryCode = "USA";
	String NewHomePhone = "7026048596";
	String NewWorkPhone = "7026058597";
	String NewMobilePhone = "7026068598";
	String NewFaxNumber = "7026078599";
	String EmailAddress = "AutomationTester@Testing.com";
	String NewAddressLine1 = "4801 Harrison Drive";
	String NewCountry = "USA";
	String NewPostalCode = "89121";
	
	
	
}

