package clarity_Standard_Reservation;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class H_Standard_ClubReservations extends A_Standard_Common {
	
	Logger logger = Logger.getLogger("H_ClubReservations.class");
	
	//-------------------------------Reservation---------------------------//
	public String CRReservationHeader; //Reservation Header
	public String CRBookFunc; //Booking Function
	public String CRMarkeyCode; //Marketing Key Code
	public String CRMarKeyCodeFlagHeader; //Marketing Key Code Flag Header
	public String CResType; //Reservation Type
	public String CResStatus; //Reservation Status
	public String CRProperty; //Property
	public String CRPropFlagHeader; //Property Flag Header
	public String CRArrDate; //Arrival Date
	public String CRNights; //Nights
	public String CRNigFlagHeader; //Nights Flag Headr (Inventory)
	public String CRDepDate; //Departure Date
	public String CRGuaCode; //Guarantee Code
	public String CRUsagePeriod; //Usage Period
	public String CRMemshipNum; //Membership Number
	public String NoofGues; //Number of Guests
	public String CRRmType; //RoomType
	public String CRRoomType; //Substring for CRRmType
	public String CRRoomTypeFlagHeader; //RoomType Flag Header
	
	//-------------------------------Special Requests---------------------------//
	public String CRSpecRequestHeader; //Special Requests Header
	
	//-------------------------------Disability and Access Requirements---------------------------//
	public String CRDisAccRequirementsHeader; //Disability and Access Requirements Header
	
	//-------------------------------Guests--------------------------//
	public String CRGuesHeader; //Guests Header
	
	//-------------------------------Cancellation Policy --------------------------//
	public String CRCancelPolicyHeader; //Cancellation Policy Header
	
	//-------------------------------Travel Protection--------------------------//
	public String CRTravelProtectHeader;
	public String CRTravProInfoFlag_Header;
	
	//-------------------------------Additional Products Offer--------------------------//
	public String CRAddProOffHeader; //Additional Products Offer Header
	
	//-------------------------------Financial Summary--------------------------//
	public String CRFinancialSummHeader; //Financial Summary Header
	public String CRNigRatInfoHeader; //Rate Amount(Curr/Pts) Flag Header (Night Rate Information)
	public String CRExtraRstCharHeader; //Extra Resort Charges Flag Header (Extra Resort Charges)
	public String CRAdvanceDepHeader; //Reservation Advance Deposit (Advance Deposits)
		public String CRRoomRateInfoHeader; //Room Rate Information Header
		public String AddDepositTranHeader; //Add Deposit Transaction
		public String CurAdDepositTranHeader; //Current Advance Deposit Transactions	
	public String CPResAdMsc; //	Reservation and Misc Fees value
		public String CRResMisFeesHeader; //Reservation and Misc Fees Flag Header
		public String CRPaymentInforHeader; //Payment Information Header
	
	//Retrieve Values From Cookies
	public String MembershipNumber;
	public String RAResortName;
	public String RARoomType;
	public String RAArrivalDate;
	public String RADepartureDate;

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	
	
	public void Reservation() {
	
	 PropertyConfigurator.configure("Log4j.properties");
	 
	 //**************************************************************************************//
	 //Retrieve Values from cookies
	 MembershipNumber = driver.manage().getCookieNamed("MembershipNumber").getValue();
	 logger.info(""+MembershipNumber);
	 
	 RAResortName =  driver.manage().getCookieNamed("RAResortName").getValue();
	 logger.info(""+RAResortName);
	 
	 RARoomType =  driver.manage().getCookieNamed("RARoomType").getValue();
	 logger.info(""+RARoomType);
	 
	 RAArrivalDate = driver.manage().getCookieNamed("RAArrivalDate").getValue();
	 logger.info(""+RAArrivalDate);
	 
	 RADepartureDate = driver.manage().getCookieNamed("RADepartureDate").getValue();
	 logger.info(""+RADepartureDate);
	//**************************************************************************************//
			
	    logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='cr_buttonSAV']"))); // Wait till the SAVE button is present 
		
		//-----------------------------------------------------------------------Reservation:------------------------------------------------------------------------------------------//
		
		logger.info("-------------------------------------------------------------------Reservation------------------------------------------------------------");
		
		//Reservation Header / Compared
		String CRReservationHeader = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[1]/td")).getText();
			Assert.assertEquals(CRReservationHeader, CRReserHeader);
				logger.info("Reservation Header Displayed and Passed Assertion: "+CRReservationHeader);
				
		//Booking Function / Compared
		CRBookFunc = driver.findElement(By.xpath(".//*[@id='ResSum_BookingPurpose']")).getText();
			Assert.assertEquals(CRBookFunc, BookTypeReserName);
				logger.info("Booking Fuction Displayed and Passed Assertion: "+CRBookFunc);
				
		//Marketing KeyCode
		CRMarkeyCode = driver.findElement(By.xpath(".//*[@id='pv1MKTKEYCODE_input']")).getAttribute("value");
			logger.info("Marketing Key Code: "+CRMarkeyCode);
				
		//Marketing KeyCode Flag / Compared
				driver.findElement(By.xpath(".//*[@id='imgMktInfo']")).click(); //Click on Marketing Keycode Img Flag
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='mktSaveButton']"))); // Wait till the SAVE button is present 
						CRMarKeyCodeFlagHeader = driver.findElement(By.xpath(".//*[@id='RMIWHeader']")).getText();
							Assert.assertEquals(CRMarKeyCodeFlagHeader, CRMrKyFlagHeader);
								logger.info("Marketing Information (In Flag) Header Displayed and Passed Assertion: "+CRMarKeyCodeFlagHeader);
									driver.findElement(By.xpath(".//*[@id='RMIWbtnClose']")).click(); //Click on Close button
				
		//ReservationType
		CResType = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[4]/td[2]")).getText();
			logger.info("Reservation Type is: "+CResType);
			
		//Reservation Status
		CResStatus = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[5]/td[2]")).getText();
			logger.info("Reservation Status: "+CResStatus);
			
		//Property / Compared
		CRProperty = driver.findElement(By.xpath(".//*[@id='ResSum_tdProperty']")).getText();
			Assert.assertEquals(CRProperty, RAResortName);
				logger.info("Property displayed and Matched From Resort Availabilty screen: "+CRProperty);
				
				//Property Flag / Compared
				driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[6]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on The flag image
					logger.info("Clicked on Property Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='REWbtnClose']"))); // Wait till the Close button is present 
							CRPropFlagHeader = driver.findElement(By.xpath(".//*[@id='REWHeader']")).getText();
								Assert.assertEquals(CRPropFlagHeader, ErrataHeader);
									logger.info("Property Flag Header displayed and Matched: "+CRPropFlagHeader);
										driver.findElement(By.id("REWbtnClose")).click(); //Click on Close button

		//ArrivalDate / Compared
		CRArrDate = driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[7]/td[2]")).getText();
			Assert.assertEquals(CRArrDate, RAArrivalDate);
				logger.info("Arrival Date displayed and Matched From Resort Availabilty screen: "+CRArrDate);
		
		//Nights / Compared
		CRNights = driver.findElement(By.xpath(".//*[@id='ResSum_pnNights']")).getAttribute("value");
			Assert.assertEquals(SANights, CRNights);
				logger.info("Number of Nights Displayed and Matched Assertion: "+CRNights);
					
					//Nights Flag / Compared
					driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[8]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Nights Flag
					logger.info("Clicked on Nights Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RNWbtnClose']"))); // Wait till the Close button is present 
							CRNigFlagHeader = driver.findElement(By.xpath(".//*[@id='RNWHeader']")).getText();
								Assert.assertEquals(CRNigFlagHeader, CRNightsFlagHeader);
									logger.info("Nights Flag Header Displayed: "+CRNigFlagHeader);
										logger.info("Nights Flag Header displayed and Matched: "+CRPropFlagHeader);
											driver.findElement(By.xpath(".//*[@id='RNWbtnClose']")).click();
									
		//Departure Date / Compared
		CRDepDate = driver.findElement(By.xpath(".//*[@id='ResSum_pdDepartureDate']")).getAttribute("value");
			Assert.assertEquals(CRDepDate, RADepartureDate);
				logger.info("Departure Date displayed and Matched from Resort Availability Screen: "+CRDepDate);		
				
		//Guarantee Code
		CRGuaCode = driver.findElement(By.xpath(".//*[@id='ResSum_pvGuarantee_input']")).getAttribute("value");
			logger.info("Guarantee Code: "+CRGuaCode);
			
		//Usage Period
		CRUsagePeriod = driver.findElement(By.xpath(".//*[@id='ResSum_UsagePeriod']")).getText();
			logger.info("Usage Period: "+CRUsagePeriod);
		
		//Membership Number / Compared
		CRMemshipNum = driver.findElement(By.xpath(".//*[@id='ResSum_MemberContrID']")).getText();
			logger.info("MemberShip Number: "+CRMemshipNum);
				Assert.assertEquals(CRMemshipNum, MembershipNumber);
					logger.info("Membership Number Matched from ClubRes (Membership Detail for:): "+CRMemshipNum);		
		
		//Number of Guests / Compared
		NoofGues = driver.findElement(By.xpath(".//*[@id='ResSum_Adults']")).getAttribute("value");	
			Assert.assertEquals(SAAdults, NoofGues);
				logger.info("Number of Adults Displayed and Matched in Search of Availability Screen: "+NoofGues);
		
		//Roomtype / Compared
		CRRmType = driver.findElement(By.xpath(".//*[@id='ResSum_UnitType_input']")).getAttribute("value");
			CRRoomType = CRRmType.substring(0, 3);
				logger.info("Room Type: " +CRRoomType);
					Assert.assertEquals(CRRoomType, RARoomType);
						logger.info("Room Type Displayed and Matched from Reservation Availability Screen: "+CRRoomType);
		
		
			//Room Type Flag / Compared
			driver.findElement(By.xpath(".//*[@id='sectionResvDetails']/div/table[1]/tbody/tr[15]/td[2]/table/tbody/tr/td[2]/img")).click(); //Click on Roomtype Flag
				logger.info("Clicked on Room Type Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RUTAWbtnClose']"))); // Wait till the Close button is present
						CRRoomTypeFlagHeader = driver.findElement(By.xpath(".//*[@id='RUTAWHeader']")).getText();
							logger.info(""+CRRoomTypeFlagHeader);
								driver.findElement(By.xpath(".//*[@id='RUTAWbtnClose']")).click();
								
	//-----------------------------------------------------------------------Special Requests------------------------------------------------------------------------------------------//

		logger.info("-------------------------------------------------------------------Special Requests------------------------------------------------------------");
		
		/*//Special Request Header / Compared 
		CRSpecRequestHeader = driver.findElement(By.xpath(".//*[@id='specialRequests']/table/tbody/tr[1]/td")).getText();
			Assert.assertEquals(CRSpecRequestHeader, CRSpecReqHeader);
				logger.info("Special Requests Header displayed and Passed Assertion: "+CRSpecRequestHeader);*/
		
	//-----------------------------------------------------------------------Disability and Access Requirements------------------------------------------------------------------------------------------//
				
		logger.info("-------------------------------------------------------------------Disability and Access Requirements------------------------------------------------------------");
		
		//Disability and Access Requirements / Compared
		CRDisAccRequirementsHeader = driver.findElement(By.xpath(".//*[@id='ADAwidget']/table[1]/tbody/tr[1]/td")).getText();
			Assert.assertEquals(CRDisAccRequirementsHeader, CRDisAccReqHeader);
				logger.info("Disability and Access Requirement Header displayed and Passed Assertion: "+CRDisAccRequirementsHeader);
				
	//-----------------------------------------------------------------------Guests------------------------------------------------------------------------------------------//
				
		logger.info("-------------------------------------------------------------------Guests------------------------------------------------------------");		
		
		//Guests / Compared
		CRGuesHeader = driver.findElement(By.xpath(".//*[@id='addGuestDiv']/form/table[1]/tbody/tr/td")).getText();
			Assert.assertEquals(CRGuesHeader, CRGuestsHeader);
				logger.info("Guests Header displayed and Passed Assertion: "+CRGuesHeader);	
				
	//-----------------------------------------------------------------------Cancellation Policy------------------------------------------------------------------------------------------//
		
		logger.info("-------------------------------------------------------------------Cancellation Policy------------------------------------------------------------");
		
		//Cancellation Policy / Compared
		CRCancelPolicyHeader = driver.findElement(By.xpath(".//*[@id='cancelation_header']/tbody/tr[1]/td")).getText();
			Assert.assertEquals(CRCancelPolicyHeader, CRCanPolHeader);
				logger.info("Cancellation Policy Header displayed and Passed Assertion: "+CRCancelPolicyHeader);	
				
			
	//-----------------------------------------------------------------------Travel Protection------------------------------------------------------------------------------------------//
	
		logger.info("-------------------------------------------------------------------Travel Protection------------------------------------------------------------");			
				
		//Travel Protection
		CRTravelProtectHeader = driver.findElement(By.xpath(".//*[@id='travelProtection']/tbody/tr[1]/td")).getText();
			Assert.assertEquals(CRTravelProtectHeader, CRTravelProtectionHeader);
				logger.info("Travel Policy Header displayed and Passed Assertion: "+CRTravelProtectHeader);
				
				//Travel Policy Flag	
				driver.findElement(By.xpath(".//*[@id='trvlprotImage']")).click();
					logger.info("Clicked on Travel Policy Flag");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='trvlproinfCancelButton']"))); // Wait till the Close Button is present 
							CRTravProInfoFlag_Header = driver.findElement(By.xpath(".//*[@id='travelProtectionInf_header']/tbody/tr/td")).getText();
								Assert.assertEquals(CRTravProInfoFlag_Header, CRTraProtecFlagHeader);
									logger.info("Travel Protection Information Flag Header Displayed and Passed Assertion: "+CRTravProInfoFlag_Header);
										driver.findElement(By.xpath(".//*[@id='trvlproinfCancelButton']")).click();	
			
					driver.findElement(By.xpath(".//*[@id='declineRPP']")).click();
						logger.info("Declined RPP");
						
	//-----------------------------------------------------------------------Additional Products Offer------------------------------------------------------------------------------------------//
	
			logger.info("-------------------------------------------------------------------Additional Products Offer------------------------------------------------------------");
	
			//Additional Products Offer / Compared
			CRAddProOffHeader = driver.findElement(By.xpath(".//*[@id='OFFRDATATABLE']/tbody/tr[1]/td")).getText();
				Assert.assertEquals(CRAddProOffHeader, CRAddProductsOffHeader);
					logger.info("Additional Products Offer Header Displayed and Passed Assertion: "+CRAddProOffHeader);
					
	//----------------------------------------------------------------------Financial Summary------------------------------------------------------------------------------------------//				
			logger.info("------------------------------------------------------------------- Financial Summary------------------------------------------------------------");
			
			//Financial Summary Header / Compared
			CRFinancialSummHeader = driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[1]/td[1]")).getText();
				Assert.assertEquals(CRFinancialSummHeader, CRFinSummHeader);
					logger.info("Financial Summary Header displayed and Passed Assertion: "+CRFinancialSummHeader);
						
			//Rate Amount (Curr/Pts) Flag Header / Compared
			driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[3]/td[2]/div/span[2]/img")).click(); //Click on Flag icon	
				logger.info("Clicked on Rate Amount Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]"))); // Wait till the Close Button is present 
						CRNigRatInfoHeader = driver.findElement(By.xpath(".//*[@id='RATEINFO']/tbody/tr[1]/td")).getText();
							Assert.assertEquals(CRNigRatInfoHeader, CRNgtRateInformHeader);
								logger.info("Night Rate Information Flag Header Displayed and Passed Assertion: "+CRNigRatInfoHeader);
									driver.findElement(By.xpath(".//*[@id='RATE']/div/table/tbody/tr/td[2]")).click(); //Click on Close button
			
					
			//Extra Resort Charges Flag / Compared
			driver.findElement(By.xpath(".//*[@id='folio_form']/table[1]/tbody/tr[8]/td[2]/div/span[2]/img")).click(); //Click on Flag icon	
				logger.info("Extra Resort Charges Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='exchdiv']/div/table/tbody/tr/td[1]"))); // Wait till the Close Button is present 
						CRExtraRstCharHeader = driver.findElement(By.xpath(".//*[@id='EXTRACHARGESINFO']/tbody/tr[1]/td")).getText();
							Assert.assertEquals(CRExtraRstCharHeader, CRExtrResCharHeader);
								logger.info("Extra Resort Charges Flag Header Displayed and Passed Assertion: "+CRExtraRstCharHeader);
									driver.findElement(By.xpath(".//*[@id='exchdiv']/div/table/tbody/tr/td[1]")).click(); //Click on Close button
					

			//Reservation Advance Deposit Flag / Compared
			driver.findElement(By.xpath(".//*[@id='folioImg']")).click();
				logger.info("Clicked on Reservation Advance Deposit Flag");
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='BackBtnAdvDep']"))); // Wait till the Close Button is present 
						
						//Advance Deposits / Compared
						CRAdvanceDepHeader = driver.findElement(By.xpath(".//*[@id='ADVDEPOSITSINFO1']/tbody/tr[1]/td")).getText();
							Assert.assertEquals(CRAdvanceDepHeader, CRAdvDepositsHeader);
								logger.info("Advance Deposits Flag Header Displayed and Passed Assertion: "+CRAdvanceDepHeader);
										
									//Room Rate Information / Compared
									 CRRoomRateInfoHeader = driver.findElement(By.xpath(".//*[@id='ADVDEPOSITSINFO1']/tbody/tr[3]/td")).getText();
										Assert.assertEquals(CRRoomRateInfoHeader, CRRoomRtInfoHeader);
											logger.info("Room Rate Information Header Displayed and Passed Assertion: "+CRRoomRateInfoHeader);
											
											//Add Deposit Transaction / Compared
											AddDepositTranHeader = driver.findElement(By.xpath(".//*[@id='ADVDEPOSITSINFO3']/tbody/tr/td")).getText();
												Assert.assertEquals(AddDepositTranHeader, AddDpstTranHeader);
													logger.info("Add Deposit Transaction Header Displayed and Passed Assertion: "+AddDepositTranHeader);
													
													//Current Advance Deposit Transactions / Compared
													CurAdDepositTranHeader = driver.findElement(By.xpath(".//*[@id='ADVDEPOSITSINFO5']/tbody/tr[1]/td")).getText();
														Assert.assertEquals(CurAdDepositTranHeader, CurAdDeTraHeader);
															logger.info("Current Advance Deposit Transactions Header Displayed and Passed Assertion: "+CurAdDepositTranHeader);
																driver.findElement(By.xpath(".//*[@id='BackBtnAdvDep']")).click(); //Click on Close button	
												
									
			//Reservation And Misc Fees / Compared
			CPResAdMsc = driver.findElement(By.xpath(".//*[@id='pvfolio_balance']")).getText();
				logger.info("Reservation And Misc Fees: "+CPResAdMsc);
					Assert.assertEquals(CPResAdMsc, CRResAndMiscFees);
						logger.info("Reservation and Misc Fees Matched and Assertion Passed");
					
		
					//Reservation And Misc Fees Flag Header / Compared
					driver.findElement(By.xpath(".//*[@id='get_resv_fee_details']")).click();
						logger.info("Clicked on Reservation And Misc Fees Flag");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='RFIFbtnClose']"))); // Wait till the Close Button is present 
								
								//Reservation and Misc Fees Header / Compared
								CRResMisFeesHeader = driver.findElement(By.xpath(".//*[@id='RFIFTitle']")).getText();
									Assert.assertEquals(CRResMisFeesHeader, CRReserMiscFeesHeader);
										logger.info("Reservation And Misc Fees Flag Header Displayed and Passed Assertion: "+CRResMisFeesHeader);
											
											//Payment Information Header / Compared
											CRPaymentInforHeader = driver.findElement(By.xpath(".//*[@id='RFIFContent']/div/div[1]/div/table/tbody/tr[9]/td")).getText();
												Assert.assertEquals(CRPaymentInforHeader, CRPayInfoHeader);
													logger.info("Payment Information Header Displayed and Passed Assertion: "+CRResMisFeesHeader);
														driver.findElement(By.xpath(".//*[@id='RFIFbtnClose']")).click(); //Click on Close button	
											
				//Click on SAVE Button
				driver.findElement(By.xpath(".//*[@id='cr_buttonSAV']")).click(); 
						logger.info("Clicked on SAVE BUTTON");
						
		}
	
}	 
				

