package clarity_Standard_Reservation;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.testng.Assert;


public class D_Standard_Reservation extends A_Standard_Common {
	
	Logger logger = Logger.getLogger("D_Reservation.class");
	
	public String winHandleBefore; //Store Current Window Handle
	public String Lead_Area_Id; //Use to retrieve Lead id  from cookie
	public String ResCustomer; // Stores Customer Name
	public String ResHistory; //Reservation History Header
	public String ResLeadId; //Reservation LeadId


//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
		public void window()
		{
			// Store the current window handle
			 winHandleBefore = driver.getWindowHandle();
		
		}
		
			
		public void Reservations() {
			
			PropertyConfigurator.configure("Log4j.properties");
			
			//****************************************************************************//
			//Retrieve Value from Cookie
			Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
			logger.info("Value Retrieved from cookie is "+Lead_Area_Id);
			//****************************************************************************//
			
			logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 			
				 			//Customer Name
				 			 ResCustomer = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[2]/span")).getText();
				 										logger.info("Customer Name: "+ResCustomer);
				 			//Reservation History							
							String ResHistory = driver.findElement(By.xpath(".//*[@id='tvl_resvHistDiv']/table/tbody/tr/td")).getText();
													logger.info("Header: "+ResHistory);
														Assert.assertEquals(ResHistHeader, ResHistory);
															logger.info("Reservations History Header Displayed and Assertion Passed");
																driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
							//Capture Leadid
								String ResLeadId = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
															logger.info("Lead Number in Reservations Tab: "+ResLeadId);
																logger.info("From Lead Information TAB "+Lead_Area_Id);
																	Assert.assertEquals(ResLeadId, Lead_Area_Id);
																		logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");

							//Opens New Window
							driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
						 		logger.info("clicked on Club Res");
					 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

							// Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}
			}
		
			
}
