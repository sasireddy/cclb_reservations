package clarity_Standard_Reservation;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class E_Standard_ClubRes extends A_Standard_Common {


	Logger logger = Logger.getLogger("E_ClubRes.class");
	
	//-----------------------------------Guest----------------------------//
	public String ClubResGuestHeader; //Guest Header
	public String ClubResGuestName; //Guest Name
	public String ClubResLeadNum;//Lead Number
	public String Lead_Area_Id; //Use to retrieve Lead id  from cookie
	public String ClubResLead_Type; // LeadType
	public String ClubResMemNum; // Membership Number
	public String ClubResHomePhone; //Home Phone
	public String ClubResWorkPhone; //Work Phone
	public String ClubResMobilePhone; //Mobile Phone
	public String ClubResFaxNumber; //FaxNumber
	public String ClubResEmail; //Email
	public String ClubResAddress; //Address
	public String ClubResPostal; //Postal Code
	public String ClubResLeadFlags; //Lead Flags Header
	public String ClubResCusProFlag; //Customer Profile Flag Header
	public String ClubResLeadInfoFlag; //View Full Lead Information Flag Header
	
	//-----------------------------------Membership Information----------------------------//
	public String ClubResMembershipInfo; //Membership Information Header
	public String ClubResLeadDesig; //Lead Designation
	public String ClubResMemType; //Membership Type
	public String ClubResMemStatus; //Membership Status
	public String ClubResMemDate; //Membership Date
	public String ClubResMemEffec; //Membership Effective
	public String ClubResExcAff; //Exchange Affiliation
	
	//-----------------------------------Membership Detail for:----------------------------//
	public String ClubResMemershipDetailfor; //Membership Detail for: Header
	public String ClubResMemDetail; //Membership Detail for: details
	public String ClubResMemDetailfor; //Substring of ClubResMemDetail
	
	//-----------------------------------Club Programs:----------------------------//
	public String ClubPrograms; //Club Programs Header
	
	//-----------------------------------Usage Information----------------------------//
	public String ClubResUsageInfoHeader; //Usage Information Header
	public String ClubResRecogLev; //Recognition Level
	public String ClubResLevEffDate; //Level Effective Date
	public String ClubResClubSelect; //Club Select
	public String ClubResClubCom; //Club Combination
	public String ClubResIntevals; // # of Intervals
	public String ClubResAnnualPoints; //Annual Points
	
	//Present Year Usage Information
	public String ClubResReservationsPoints; //Get Present Year Points 
	public String ClubRespoints; //Replace "," with "" from Present Year Points
	public String ClubResAllotment; // Present Year Allotment 
	public String ClubResMemBen; //Present Year Available for MemberBenefits
	public String ClubResMemBenfFlagHeader; //Avilable for Member Benefits Flag Header
	public String ClubResSave; //Present Year Available to save to follwoing yr
	public String ClubResPntSvFlagHeader; //Present Year Available to Save to following year
	
	//-----------------------------------Financial Information----------------------------//
	public String ClubResFinancialHeader; //Financial Information Header
	
	//-----------------------------------Contract Details----------------------------//
	public String ClubResContractHeader; //Contract Details Header

	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	
	public void ClubRes() {
		 
		 PropertyConfigurator.configure("Log4j.properties");
			
		 logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		 
		
			
		 	if(driver.findElements(By.xpath("/html/body/div[15]/div/div/div[3]")).size()!=0) 
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath("/html/body/div[15]/div/div/div[3]")).click();
				
			}
			
			else
			{
				logger.info("---Notice---Notice---Notice---Window not found");
			}

			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
	//-----------------------------------------------------------------------GUEST------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Guest------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='guestypeimg']"))); // Wait till the element LEAD TYPE FLAG is present to click
			
			//Guest Header / Compared
			ClubResGuestHeader = driver.findElement(By.xpath(".//*[@id='GUESTTABLE']/tbody/tr[1]/td[1]/span[1]")).getText();
				logger.info("Guest Header Displayed: "+ClubResGuestHeader);
					Assert.assertEquals(ClubResGuestHeader, CRGuest);
						logger.info("Guest Header Matched");
				
			//Guest Name
			ClubResGuestName = driver.findElement(By.xpath(".//*[@id='pv1guest_name']")).getText();
				logger.info("Guest Name: "+ClubResGuestName);
				
				//***********************************************************************//
				//Retrieve Value from Cookie
				Lead_Area_Id = driver.manage().getCookieNamed("Lead_Area_Id").getValue();
				logger.info("Value Retrieved from cookie is "+Lead_Area_Id);
				//***********************************************************************//
				
			//Lead Number / Compared
			ClubResLeadNum = driver.findElement(By.xpath(".//*[@id='pv1guest_lead']/a")).getText();
				logger.info("Lead Area-Lead Id: " +ClubResLeadNum);
					Assert.assertEquals(ClubResLeadNum, Lead_Area_Id);
						logger.info("Lead Number in Lead Information Tab Matched with Club Reservations");
				
					//Lead Type
				 	ClubResLead_Type = driver.findElement(By.xpath(".//*[@id='pv1GuestType']/b")).getText();
							logger.info("Lead Type/Subtype: "+ ClubResLead_Type);
					
					//LeadType Flag
					driver.findElement(By.xpath(".//*[@id='guestypeimg']")).click();
						logger.info("Clicked on Lead Type Flag icon");	
							Actions action = new Actions(driver);
								WebElement LTFlag = driver.findElement(By.xpath(".//*[@id='Closeguesttooltip']/font"));
									action.moveToElement(LTFlag).perform();
											      
			/*//Membership Number
			ClubResMemNum = driver.findElement(By.xpath(".//*[@id='pv1IIMemberNum']")).getText();
				logger.info("Membership Number: "+ClubResMemNum);*/
										
			//HomePhone / Compared
			ClubResHomePhone = driver.findElement(By.xpath(".//*[@id='pv1HomePhNum']")).getAttribute("value");
		        	logger.info("Club Reservations Homephone: " +ClubResHomePhone);
		        		Assert.assertEquals(ClubResHomePhone, HomePhone);
		        			logger.info("HomePhone Number in Club Reservations Matched with HomePhone Number in LeadInformation Page");

    		//WorkPhone / Compared
			ClubResWorkPhone = driver.findElement(By.xpath(".//*[@id='pv1WorkPhNum']")).getAttribute("value");
		        	logger.info("Club Reservations WorkPhone: " +ClubResWorkPhone);
		        		Assert.assertEquals(ClubResWorkPhone, WorkPhone);
		        			logger.info("WorkPhone Number in Club Reservations Matched with WorkPhone Number in LeadInformation Page");
				
			//MobilePhone / Compared
			ClubResMobilePhone = driver.findElement(By.xpath(".//*[@id='pv1MobilePhNum']")).getAttribute("value");
		        	logger.info("Club Reservations MobilePhone: " +ClubResMobilePhone);
		        		Assert.assertEquals(ClubResMobilePhone, MobilePhone);
		        			logger.info("MobilePhone Number in Club Reservations Matched with MobilePhone Number in LeadInformation Page");

			//FaxNumber / Compared
			ClubResFaxNumber = driver.findElement(By.xpath(".//*[@id='pv1FaxNum']")).getAttribute("value");
		        	logger.info("Club Reservations FaxNumber: " +ClubResFaxNumber);
		        		Assert.assertEquals(FaxNumber, ClubResFaxNumber);
		        			logger.info("FaxNumber in Club Reservations Matched with FaxNumber in LeadInformation Page");

			//Email Address / Compared
		    ClubResEmail = driver.findElement(By.xpath(".//*[@id='pv1Email']")).getAttribute("value");
		    	logger.info("Club Reservations Email Address: "+ClubResEmail);
		    		Assert.assertEquals(PriEmail, ClubResEmail);
		    			logger.info("Email Address in Club Reservations Matched with Email Address in LeadInformation Page");

			//Address / Compared
		    ClubResAddress = driver.findElement(By.xpath(".//*[@id='pv1Addr']")).getAttribute("value");
		    	logger.info("Club Reservations Address: "+ClubResAddress);
		    		Assert.assertEquals(Address, ClubResAddress);
		    			logger.info("Address in Club Reservations Matched with Address in LeadInformation Page");
				    			
			//Postal / Compared
		    ClubResPostal = driver.findElement(By.xpath(".//*[@id='pv1Postal']")).getAttribute("value");
		    	logger.info("Club Reservations Postal Code: "+ClubResPostal);
		    		Assert.assertEquals(Postal, ClubResPostal);
		    			logger.info("Postal Code in Club Reservations Matched with Postal Code in LeadInformation Page");
						
												    					
			//LeadFlags Flag / Compared
				driver.findElement(By.xpath(".//*[@id='1leadflagimage']")).click();
					logger.info("Clicked on Lead Flags icon");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='addLeadCancelButton']"))); // Wait till the element is present to click
						
							 ClubResLeadFlags = driver.findElement(By.xpath(".//*[@id='leadFlagsHeaderTable']/tbody/tr/td")).getText();
								logger.info("LeadFlags displayed:"+ClubResLeadFlags);
									Assert.assertEquals(ClubResLeadFlags, CRLeadFlag);
										logger.info("LeadFlags header Passed (Flag)");
											driver.findElement(By.xpath(".//*[@id='addLeadCancelButton']")).click();	
												logger.info("Clicked on Close Button (Flag)");
										
				//Customer Profile Flag /Compared
				driver.findElement(By.xpath(".//*[@id='1custProfileImage']")).click();
					logger.info("Clicked on CustomerProfile Flag icon");
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='CPWDCloseBtn']"))); // Wait till the element is present to click
						
							 ClubResCusProFlag = driver.findElement(By.xpath(".//*[@id='CPWDTitle']")).getText();
								logger.info("Customer Profile Flag displayed: "+ClubResCusProFlag);
									Assert.assertEquals(ClubResCusProFlag, CRCusProfile);
										logger.info("Customer Profile header Passed (Flag)");
											driver.findElement(By.xpath(".//*[@id='CPWDCloseBtn']")).click();	
												logger.info("Clicked on Close Button (Flag)");
									
					//View Full Lead Information Flag
					driver.findElement(By.xpath(".//*[@id='pv1FullLeadInfoIcon']")).click();
						logger.info("Clicked on Full Lead Information Flag icon");
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='LEAD_FRM_CLOSE_BUTTON']"))); // Wait till the element is present to click
							
								 ClubResLeadInfoFlag = driver.findElement(By.xpath(".//*[@id='Lead_FRM_DragBar']")).getText();
									logger.info("Full Lead Information Flag displayed: "+ClubResLeadInfoFlag);
										Assert.assertEquals(ClubResLeadInfoFlag, CRFullLeadInfo);
											logger.info("Lead Information header Passed (Flag)");
												driver.findElement(By.xpath(".//*[@id='LEAD_FRM_CLOSE_BUTTON']")).click();	
													logger.info("Clicked on Close Button (Flag)");								

		//-----------------------------------------------------------------------Membership Information------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Membership Information------------------------------------------------------------");		
			
				//Membership Information Header / Compared
				ClubResMembershipInfo = driver.findElement(By.xpath(".//*[@id='divMemberInfo']/table[1]/tbody/tr/td")).getText();
					logger.info("Membership Information Header Displayed: " +ClubResMembershipInfo);
						Assert.assertEquals(ClubResMembershipInfo, CRMemInfo);
							logger.info("Membership Information Matched");
				
				//Lead Designation
				ClubResLeadDesig = driver.findElement(By.xpath(".//*[@id='tdMbrLeadDesgn']")).getText();
					logger.info("Lead Designation: "+ClubResLeadDesig);
				
				//Membership Type
				ClubResMemType = driver.findElement(By.xpath(".//*[@id='tdMbrtype']")).getText();
					logger.info("Membership Type: "+ClubResMemType);
				
				//Membership Status
				ClubResMemStatus = driver.findElement(By.xpath(".//*[@id='mbrStatus']")).getText();
					logger.info("Membership Status: "+ClubResMemStatus);
	
				//Membership Date
				ClubResMemDate = driver.findElement(By.xpath(".//*[@id='tdMbrDate']")).getText();
					logger.info("Membership Date: "+ClubResMemDate);
		
				//Membership Effective
				ClubResMemEffec = driver.findElement(By.xpath(".//*[@id='tdMbrEff']")).getText();
					logger.info("Membership Effective: "+ClubResMemEffec);
	
				/*//Exchange Affiliation
				ClubResExcAff = driver.findElement(By.xpath(".//*[@id='tdMbrXchAff']")).getText();
					logger.info("Exchange Affiliation: "+ClubResExcAff);*/
		/*								
				// Store the current window handle
					String winClubRes = driver.getWindowHandle();
						
					//Associate Members Flag
					driver.findElement(By.xpath(".//*[@id='AtlasMembInfoIcon']")).click();
						logger.info("Clicked on Associate Members Flag icon");
			 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

					// Switch to new window opened
					//for(String winAssociate : driver.getWindowHandles()){
					   // driver.switchTo().window(winAssociate);
					
						driver.switchTo().frame(driver.findElement(By.id("folioFrameSet")));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/form/table[2]/tbody/tr/td"))); // Wait till the element is present to click
								String AssocMembers = driver.findElement(By.xpath("html/body/form/table[2]/tbody/tr/td")).getText();
									logger.info("Associate Member Header displayed: "+AssocMembers);
										Assert.assertEquals(AssocMembers, CRAssocMembers);
											logger.info("Associate Members header Passed (Flag)");
												driver.findElement(By.xpath("html/body/form/div[1]/table/tbody/tr/td[1]/a/img")).click();	
													logger.info("Clicked on Back Button (Flag)");	
														driver.switchTo().window(winClubRes);//Change to Parent Window
	  				*/
		/*//----------------------------------------------------------------------Club Programs-----------------------------------------------------------------------------------------//
		
			logger.info("-------------------------------------------------------------------Club Programs------------------------------------------------------------");	
			
			ClubPrograms = driver.findElement(By.xpath(".//*[@id='SubFormDIV']/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[1]")).getText();
				logger.info("Header is: "+ClubPrograms);
					Assert.assertEquals(ClubPrograms, ClubProgramsHeader);
						logger.info("Club Programs: Matched and Passed Assertion");

		*/
		//-----------------------------------------------------------------------Membership Detail------------------------------------------------------------------------------------------//
											
			logger.info("-------------------------------------------------------------------Membership Detail------------------------------------------------------------");
			
			//Membership Detail for: Header / Compared
			ClubResMemershipDetailfor = driver.findElement(By.xpath(".//*[@id='MbrSelector']/tbody/tr[1]/td[1]/span[1]")).getText();
				logger.info("Header is: "+ClubResMemershipDetailfor);
					Assert.assertEquals(ClubResMemershipDetailfor, CRMemDetailHeader);
						logger.info("Membership Detail for: Matched");
						
			//Details			
			ClubResMemDetail = driver.findElement(By.xpath(".//*[@id='tdMembership']")).getText(); //Membership Detail For:
				logger.info("Membership Details for: "+ClubResMemDetail);
				
			ClubResMemDetailfor = ClubResMemDetail.substring(0,6); //Membership Detail for subString
				logger.info("Membership Number: "+ClubResMemDetailfor);
				
		//-----------------------------------------------------------------------Usage Information------------------------------------------------------------------------------------------//
			
				logger.info("-------------------------------------------------------------------Usage Information------------------------------------------------------------");
				
				//Usage Information Header / Compared
				ClubResUsageInfoHeader = driver.findElement(By.xpath(".//*[@id='MembershipUsageTABLE']/table[1]/tbody/tr[1]/td[1]/span[1]")).getText();
					logger.info("Header is: "+ClubResUsageInfoHeader);
						Assert.assertEquals(ClubResUsageInfoHeader, CRUsageInfo);
							logger.info("Usage Information Header Matched");
							
				/*//Recognition Level
				ClubResRecogLev = driver.findElement(By.xpath(".//*[@id='Recognitionlevel_input']")).getText();
					logger.info("Recognition Level: "+ClubResRecogLev);*/
						
				//Level Effective Date
				ClubResLevEffDate = driver.findElement(By.xpath(".//*[@id='pvMbrEFD']")).getText();
					logger.info("Level Effective Date: "+ClubResLevEffDate);
					
				//Club Select
				ClubResClubSelect = driver.findElement(By.xpath(".//*[@id='pvMbrClub']")).getText();
					logger.info("Club Select: "+ClubResClubSelect);
						
				//Club Combination
				ClubResClubCom = driver.findElement(By.xpath(".//*[@id='pvMbrClubCombo']")).getText();
					logger.info("Club Combination: "+ClubResClubCom);
		
				//# of Intervals
				ClubResIntevals = driver.findElement(By.xpath(".//*[@id='pnMbrInterval']")).getText();
					logger.info("# of Intervals: "+ClubResIntevals);
								
				//Annual Points
				ClubResAnnualPoints = driver.findElement(By.xpath(".//*[@id='pnMbrAP']")).getText();
					logger.info("Annual Points: "+ClubResAnnualPoints);
					
				//--------------------------------------Points-------------------------------------//
					
				//Allotment
				ClubResAllotment = driver.findElement(By.xpath(".//*[@id='pv1MbrAlt']")).getText();
					logger.info("Allotment for Present Year: "+ClubResAllotment);
					
						//Present Year Avail for Reservations
						ClubResReservationsPoints = driver.findElement(By.xpath(".//*[@id='mbrRemBan']")).getText(); //Get Present Year Points ;
							ClubRespoints = ClubResReservationsPoints.replace(",",""); //Replace , with nothing
								logger.info("Present Year Available for Reservations: "+ClubResReservationsPoints);
							
							//Present Year Avail for Member Benefits
							ClubResMemBen = driver.findElement(By.xpath(".//*[@id='mbrAvaBenefit']")).getText();
								logger.info("Present Year Available for Member Benefits: "+ClubResMemBen);
						
							//Available for Member Benefits Flag / Compared
							driver.findElement(By.xpath(".//*[@id='mbrbenefitimg']")).click(); //Clicked on the Flag 
								logger.info("Clicked on Available for Member Benefits Flag icon");
									wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='MBTCloseBtn']"))); // Wait till the element is present to click
									ClubResMemBenfFlagHeader = driver.findElement(By.xpath(".//*[@id='MBTTitle']")).getText(); // Flag Header
											Assert.assertEquals(ClubResMemBenfFlagHeader, CRMemBenHeader); //Compare
												logger.info("Member Benefits Header display and  Passed Assertion (Flag): "+ClubResMemBenfFlagHeader); // Assertion Passed
													driver.findElement(By.xpath(".//*[@id='MBTCloseBtn']")).click(); // Click on Close button
														logger.info("Clicked on Close Button (Flag)");		
								
								//Present Year Avail to save to following yr 
								ClubResSave = driver.findElement(By.xpath(".//*[@id='mbrAvaSave']")).getText();
									logger.info("Present Year Available to Save to Following yr: "+ClubResSave);
									
								//Available to Save to following yr Flag / Compared
								driver.findElement(By.xpath(".//*[@id='mbrsavepointimg']")).click(); // Click on the Flag
									logger.info("Clicked on Available to Save to following yr Flag icon");
										wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='MSPICloseBtn']"))); // Wait till the element is present to click
											 ClubResPntSvFlagHeader = driver.findElement(By.xpath(".//*[@id='MSPITitle']")).getText();
												Assert.assertEquals(ClubResPntSvFlagHeader, CRPoiSavDdLinesHeader);
													logger.info("Point Saving Deadlines Header display and  Passed Assertion (Flag): "+ClubResPntSvFlagHeader);
														driver.findElement(By.xpath(".//*[@id='MSPICloseBtn']")).click();	
															logger.info("Clicked on Close Button (Flag)");		
								
										
				//-----------------------------------------------------------------------------------Financial Information----------------------------------------------------------------//
				
					logger.info("-------------------------------------------------------------------Financial Information------------------------------------------------------------");
				
				//Financial Information Header / Compared
				ClubResFinancialHeader = driver.findElement(By.xpath(".//*[@id='membershipAcctTitle']")).getText();
					Assert.assertEquals(ClubResFinancialHeader, CRFinancialInforHeader);
						logger.info("Financial Information Header displayed and Passed Assertion- "+ClubResFinancialHeader);
						
				//-----------------------------------------------------------------------------------Contract Details----------------------------------------------------------------//
	
					logger.info("-------------------------------------------------------------------Contract Details------------------------------------------------------------");
					
				//Contract Details Header / Compared
				ClubResContractHeader = driver.findElement(By.xpath(".//*[@id='tblContractInfo']/thead/tr[1]/td")).getText();
				Assert.assertEquals(ClubResContractHeader, CRContractDetHeader);
					logger.info("Contract Details Header displayed and Passed Assertion- "+ClubResContractHeader);	
					
					//*****************************************************************************************//
					//Add Cookies
					Cookie ClubRes1 = new Cookie("ClubResReservationsPoints", ClubResReservationsPoints);
					Cookie ClubRes2 = new Cookie("ClubRespoints", ClubRespoints);
					Cookie ClubRes3 = new Cookie("MembershipNumber", ClubResMemDetailfor);

					driver.manage().addCookie(ClubRes1);
					driver.manage().addCookie(ClubRes2);
					driver.manage().addCookie(ClubRes3);
					//*****************************************************************************************//					
					
	
	 }
}
