package clarity_Standard_Reservation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A_Standard_Common {
	

	static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 1000);
	
	String Leadid1 = "1973112"; //CS1 - STA
	String LeadType = "MBR";
	
	//Lead Information
	String PLH = "Primary Lead:";
	String SLH = "Secondary Lead:";
	
	String LeadFlag = "Owner Snapshot";
	
	String LeadEmailFlag = "Email Type";
	String RegularMailHeader = "Regular Mail";
	String PriEmailAddHeader = "Primary Email Address";
	String SecEmailAddHeader = "Secondary Email Address";
	
	String LeadSocialNetworks = "Social Network Account Information";
	String LeadIDLookup = "ID Lookup";
	String LeadCC = "Credit Card Maintenance";
	String AddressHeader = "Address Information:";
	String DemographHeader = "Demographics";
	String Com_AlertsHeader = "Comments and Alerts:";
	
	String HomePhone = "2022022222";
	String WorkPhone = "3033033333";
	String MobilePhone = "4044044444";
	String FaxNumber = "5055055555";
	
	String HomePhone1 = "6066066666";
	String WorkPhone1 = "7077077777";
	String MobilePhone1 = "8088088888";
	String FaxNumber1 = "9099099999";
	
	String PriEmail = "Primary@dri.com";
	String SecEmail = "Secondary@dri.com";
	String Address = "10175 W Charleston Blvd";
	String Country = "USA - United States";
	String Postal = "89135-1260";
	String City = "Las Vegas";
	String State = "NV - Nevada";
	
	//Reservations Tab
	String ResHistHeader = "Reservations History";
	
	//-----------------------------------------------------------------Club Res------------------------------------------------------------//
	//Guest
	String CRGuest = "Guest:";
	String CRLeadFlag = "Lead Flags";
	String CRCusProfile = "Customer Profile";
	String CRFullLeadInfo = "Lead Information";
	
	//Membership Information
	String CRMemInfo = "Membership Information:";
	String CRAssocMembers = "Associate Member";
	
	//Membership Detail for:
	String CRMemDetailHeader = "Membership Detail for:";
	
	//Club Programs:
	String  ClubProgramsHeader = "Club Programs:";
	
	//Usage Information:
	String CRUsageInfo = "Usage Information:"; // Header
	String CRMemBenHeader = "Member Benefits"; //Available for Member Benefits Flag
	String CRPoiSavDdLinesHeader = "Point Saving Deadlines"; //Available to Save to Following Yr Flag
	
	//Financial Information
	String CRFinancialInforHeader = "Financial Information";
	
	//Contract Details
	String CRContractDetHeader = "Contract Details";
	
	//-----------------------------------------------------------------Book New Reservation------------------------------------------------------------//
	//Book New Reservation
	String BookNewResHeader = "Book New Reservation";
	String BookingTypeReservation = "10194"; //Home Standard Reservation Value
	String BookTypeReserName = "HOME-STANDARD RESERVATION";
	
	//-----------------------------------------------------------------Search for Availability------------------------------------------------------------//
	//Search for Availability
	String SASearchforAvaHeader = "Search for Availability"; //Search for Availability Header
	String SADestinationHeader = "Choose a Destination"; //Choose a destination Header
	String SATravelDatesHeader = "Choose Travel Dates:"; //Choose Travel Dates Header
	String SASearParaHeader = "Additional Search Parameters:"; //Choose Additional Search Parameters
	String SARegion = "AZ"; //Region as Arizona 
	String SAProperty = "BRI"; //Property as Bell Rock Inn
	String SAArrivalDateMethod = "FlexDays"; //Arrival Date Method as Flexible Days
	String SAArrivalDate = "t+30"; //Arrival Date 
	String SANights = "2";
	String SAAdults = "2";
	
	//-----------------------------------------------------------------Resort Availability - CLUB------------------------------------------------------------//
	//Resort Availability - CLUB
	String ResortAvaCLUBHeader = "Resort Availability - CLUB"; //Resort Availbility - CLUB Header
	String ResortType = "EXX"; //Resort Type SubString
	
	//-----------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------//
	//Additional Resort Information (Errata)
	String ErrataHeader = "Additional Resort Information (Errata)"; //
	
	//-----------------------------------------------------------------Room Upgrade------------------------------------------------------------//
	//Room Upgrade
	String RoomUpgrade1 = "Room Upgrade";
	
	//-----------------------------------------------------------------Discount------------------------------------------------------------//
	//Discount
	String DiscountHeader = "Discount";
	String DisMesAvail = "Member has sufficient points to book this reservation.";
	String DisMesNotAvail = "Member does not have sufficient points to book this reservation.";
	String DiscountPercentage = "25";
	String DiscountYesMessage = "Selected Yes For Discount";
	
	//-----------------------------------------------------------------Method of Payments------------------------------------------------------------//
	//Method of Payments
	String MethodofPaymentsHeader = "Methods of Payment Options"; 
	String MOPBalPointsNd = "0"; //Balance of Points needed
	
	//-----------------------------------------------------------------Club Reservations------------------------------------------------------------//
	
	//Reservation:
	String CRReserHeader = "Reservation:";
	String CRMrKyFlagHeader = "Marketing Information";
	String CRNightsFlagHeader  = "Inventory";
	
	//Special Requests
	String CRSpecReqHeader = "Special Request";
	
	//Disability and Access Requirements
	String CRDisAccReqHeader = "Disability and Access Requirements";
	
	//Guests
	String CRGuestsHeader = "Guests:";
	
	//Cancellation Policy
	String CRCanPolHeader = "Cancellation Policy";
	
	//Travel Protection
	String CRTravelProtectionHeader = "Travel Protection";
	String CRTraProtecFlagHeader = "Travel Protection Information";
	
	//Additional Products Offer
	String CRAddProductsOffHeader = "Additional Products Offer";
	
	//Financial Summary
	String CRFinSummHeader = "Financial Summary:"; //Financial Summary Header
	String CRNgtRateInformHeader = "Night Rate Information:"; //Rate Amount Flag Header
	String CRExtrResCharHeader = "Extra Resort Charges"; //Extra Resort Charges Flag Header
	String CRAdvDepositsHeader = "Advance Deposits"; //Reservation Advance Deposit Flag Header
		
		//Inside Reservation Advance Deposit Flag
		String CRRoomRtInfoHeader = "Room Rate Information"; //Room Rate Information Header
		String AddDpstTranHeader = "Add Deposit Transaction"; //Add Deposit Transaction
		String CurAdDeTraHeader = "Current Advance Deposit Transactions"; //Current Advance Deposit Transactions
		
	//Reservation and Misc Fees
	String CRResAndMiscFees = "$ 0.00";
	String CRReserMiscFeesHeader = "Reservation and Misc Fees"; //Reservation and Misc Fees Flag Header
		String CRPayInfoHeader = "Payment Information";

	//-----------------------------------------------------------------Reservation Confirmation------------------------------------------------------------//
		
	String ResConfirmationHeader = "Reservation Confirmation"; //Reservation Confirmation Header
	String RCPnSumHeader = "Points Summary";
	String RCConfirmLetterHeader = "Confirmation Letter";

}
