package clarity_Standard_Reservation;

import org.testng.annotations.Test;

public class Z_Standard_Execution {

	B_Standard_Login Login = new B_Standard_Login();
	C_Standard_Customer360_LeadInfo Leadinfo = new C_Standard_Customer360_LeadInfo();
	D_Standard_Reservation Reservation = new D_Standard_Reservation();
	E_Standard_ClubRes ClubRes = new E_Standard_ClubRes();
	F_Standard_BookReservation BookRes = new F_Standard_BookReservation();
	G_Standard_MOPPayments MOP = new G_Standard_MOPPayments();
	H_Standard_ClubReservations Reserve = new H_Standard_ClubReservations();
	I_Standard_FinalConfirmation ResConfirm = new I_Standard_FinalConfirmation();
	
	
	@Test
	public void StandardReservation()
	{
		Login.openBrowser();
			Leadinfo.Customer360();
				Leadinfo.Lead_Information();
					Reservation.Reservations();
						ClubRes.ClubRes();
							BookRes.BookReservation();
								BookRes.Resort_Availability();
									BookRes.Errata();
										BookRes.Discount_No();
											MOP.MethodofPaymentOptions();
												Reserve.Reservation();
													ResConfirm.Reservation_Confirmation();
	}
			
}
