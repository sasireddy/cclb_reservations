package clarity_Standard_Reservation;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class I_Standard_FinalConfirmation extends A_Standard_Common {
	
	Logger logger = Logger.getLogger("I_FinalConfirmation.class");
	
	public String ResConfirmHeader; //Reservation Confirmation Header
	public String RCPointsSumHeader; //Points Summary
	public String RCBegiPntsBal; //Beginning Point Balance
	public String RCResCost; //Reservation Cost
	public String RCResCt; //Replace "RCResCost" without ","
	public String RCTotPnts; //Total Points Used for Reservation
	public String RCTotalPoints; //Replace "RCTotPnts" without ","
	public String RCEndPntBal; //Ending points balance
	public String ConfimationMes; //Confirmation Message with Confirmation Number
	
	//Retrieve from Cookies
		public String RAPoints;
		public String ClubRespoints;
		
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	
	public void Reservation_Confirmation()
	{
		PropertyConfigurator.configure("Log4j.properties");	
		
		//***************************************************************************//
		//Get Values from Cookies
		RAPoints = driver.manage().getCookieNamed("RAPoints").getValue();
		logger.info("Reservation Points from Resort Availability: "+RAPoints);
		
		ClubRespoints =	driver.manage().getCookieNamed("ClubRespoints").getValue();
		logger.info("Reservation available from Club Reservations: "+ClubRespoints);
		//***************************************************************************//
		
		//-----------------------------------------------------------------------Reservation Confirmation------------------------------------------------------------------------------------------//
					
		logger.info("------------------------------------------------------------------------Reservation Confirmation---------------------------------------------------------------------");
								
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ClbResvConf_close']"))); // Wait till the element is present 
					
			//Reservation Confimation Header / Compared
			ResConfirmHeader = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[1]/td")).getText();
				Assert.assertEquals(ResConfirmHeader, ResConfirmationHeader);
					logger.info("Reservation Confirmation Header Displayed and Passed Assertion: "+ResConfirmHeader);
			
			//Points Summary Header / Compared
			RCPointsSumHeader = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[3]/th")).getText();
				Assert.assertEquals(RCPointsSumHeader, RCPnSumHeader);
					logger.info("Points Summary Header Displayed and Passed Assertion: "+RCPointsSumHeader); 	
			
			//Beginning Points Balance / Compared
			RCBegiPntsBal = driver.findElement(By.xpath(".//*[@id='mbrRemBan1']")).getText(); //Present Year points
				Assert.assertEquals(RCBegiPntsBal, ClubRespoints);
					logger.info("Beginning Points Balance Displayed and Passed Assertion.  Reservation Confirmation Beginning Points Matched With Club Res Available Points For Reservations Present Year: "+RCBegiPntsBal);
						logger.info("Beginning point balance: " +RCBegiPntsBal);
							logger.info("Club Reservations: "+ClubRespoints);
									
					//Reservation Cost
					RCResCost = driver.findElement(By.xpath(".//*[@id='ResvCost']")).getText();
					int LengthofRCResCost = RCResCost.length(); // Length of RCResCost
					String S = "1,000";
					int S1 = S.length(); // Length of S String
					
					if(LengthofRCResCost == S1) //If Size is the same then take out "," and follows the rest of the process
					{
						//Reservation Cost / Compared 
						RCResCt = RCResCost.replace(",","");
						Assert.assertEquals(RCResCt, RAPoints); //Comparing 
							logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCt);
									
						//Total Points Used for Reservation / Compared 
						RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							RCTotalPoints = RCTotPnts.replace(",","");
								Assert.assertEquals(RCTotalPoints, RAPoints); //Comparing 
									logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCTotalPoints);
				
									
						//Estimating Ending Points
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	/ Compared
							String RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
									logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");
							
					}
					
					else		
					{
						Assert.assertEquals(RCResCost, RAPoints); //Comparing 
						logger.info("Reservation Cost Points Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCResCost);
							logger.info("Reservation Confirmation: "+RCResCost);
								logger.info("Club Reservations: "+RAPoints);
								
						//Total Points Used for Reservation
						RCTotPnts = driver.findElement(By.xpath(".//*[@id='TotResvPoints']")).getText();
							Assert.assertEquals(RCTotPnts, RAPoints); //Comparing 
								logger.info("Total Points Used for Reservation Displayed and Matched with Cost of Points from Resort Availability Screen: "+RCTotPnts);
									//logger.info("RCCCC: "+RcTotPnts);
										//logger.info("From Resort AVAIl: "+RAPoints);
										
						//Estimating Ending Points
							int AvailPoints = Integer.parseInt(ClubRespoints);//Convert String to int
								int ResPnts = Integer.parseInt(RAPoints); //Conver String to Int
									int EstEndPnts = AvailPoints - ResPnts;
										logger.info("Estimation of Ending Point Balance should be: "+EstEndPnts);
										
							//Ending Points Balance	
							RCEndPntBal = driver.findElement(By.xpath(".//*[@id='EndPointsBal1']")).getText();
								int RCEndPointsbal = Integer.parseInt(RCEndPntBal); //Convert From String to int
									logger.info("Ending Points Balance: "+RCEndPointsbal);
									
								Assert.assertEquals(RCEndPointsbal, EstEndPnts);
								logger.info("ESTIMATED ENDING POINTS BALANCE MATCHED WITH ACTUAL ENDING BALANCE POINTS");			
							
					}
				
					//Confirmation Message
					ConfimationMes = driver.findElement(By.xpath(".//*[@id='ClbResvConf']/div[1]/table[1]/tbody/tr[2]/td")).getText();
						logger.info(""+ConfimationMes);
							logger.info("lenght of the string is:" +ConfimationMes.length());
						
							//Save to Cookie
							String ResConNum = ConfimationMes.replaceAll("[^0-9]",""); //Stores only Numbers from the confirmation message
							logger.info("Reservation Confirmation number is: "+ResConNum);
							
							Cookie FinalConfirmation = new Cookie("ResConNum",ResConNum);
							driver.manage().addCookie(FinalConfirmation);
						
 			}				
		
	}
	


