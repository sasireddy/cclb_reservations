package clarity_UpdateLead;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Update_Leads_Excel {

	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 40);
	Logger logger = Logger.getLogger("Login.class");
	Random rand = new Random();
  	String num1 = "5102";
  	String PriEmail = "Primary@dri.com";
	String SecEmail = "Secondary@dri.com";
	String Address = "10600 W Charleston Blvd";
	String Country = "USA - United States";
	String Postal = "89135-1260";
	String City = "Las Vegas";
	String State = "NV - Nevada";

	@BeforeTest
	public void before() throws InterruptedException
	{
		PropertyConfigurator.configure("Log4j.properties");
		  driver.get("http://claritystage.diamondresorts.com/pls/claritystage/sign_in"); // URL
		//  driver.get("http://claritytest.diamondresorts.com/pls/claritytest/sign_in"); // URL
			driver.manage().window().maximize();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='loginButton']"))); // Wait till the element is present to click
			Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='pvUsername']")).sendKeys("sreddy"); //Entering Username
					logger.info("Entered Username");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							driver.findElement(By.xpath(".//*[@id='pvPassword']")).sendKeys("Bhushan23");// Entering Password
								logger.info("Entered Password");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										driver.findElement(By.xpath(".//*[@id='loginButton']")).click();// Click on Login 
											logger.info("Clicked on Login Button");
												
											Thread.sleep(2000);
												
			WebElement customer360 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='101']"))); // Wait till the Customer360 is available to click
	 		customer360.click(); // Click on Customer 360
	 			logger.info("Clicked on Customer 360");
	 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										 				
	}
		
	@SuppressWarnings("resource")
	@Test
	public void maintest() throws InterruptedException  
	{
		try {
				
				FileInputStream fileInputStream = new FileInputStream("H://Leadids.xlsx");
				XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
				//XSSFSheet worksheet = workbook.getSheet("Corbin1");
				XSSFSheet worksheet = workbook.getSheet("Sasi");
				int rowCount = worksheet.getLastRowNum()- worksheet.getFirstRowNum();
				logger.info(""+worksheet.getLastRowNum());
				logger.info(""+worksheet.getFirstRowNum());
				logger.info("Row count is" +rowCount);

				for (int k = 0; k <= rowCount; k++) {	 
			        
						Row row = worksheet.getRow(k);
				        String Leadid = new DataFormatter().formatCellValue(row.getCell(0));
		        		logger.info(Leadid);
	
		        		driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/table/tbody/tr/td/div/form/table/tbody/tr/td[2]/span[2]/input")).sendKeys(Leadid); //Enter DBL LEAD Id
							logger.info("Lead ID is: "+Leadid);	 
								driver.findElement(By.xpath("/html/body/div/div[2]/div/table[2]/tbody/tr/td/table/tbody/tr/td[3]/div")).click();// click on search
				
						String LeadType = "MBR";
						String LeadType2 = "OWN";
			
						Search:
						 for (int i=1; i<=10;i++)
						 {
							 for(int j=2;j<=2;j++)
							 {
								 String LeadType1 = null;
									String xpath  = null;
										xpath = "/html/body/div/div[2]/div[2]/div/div/div/table/tbody/tr["+ i +"]/td["+ j +"]";
											LeadType1 = driver.findElement(By.xpath(xpath)).getText();
													driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
											
												if(LeadType1.equals(LeadType) || LeadType1.equals(LeadType2))
												{
															driver.findElement(By.xpath(xpath)).click();
																logger.info("Lead is a Member");
																	break Search;				
												}
												driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
												
							 }
						 }
						
						if(driver.findElements(By.xpath(".//*[@id='alert']/div")).size()!=0) 
						{
							logger.info("Member has limited mobility");
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
							
						}
						
						else
						{
							logger.info("Member has limited mobility------Not Found for this lead");
						}
						
						Thread.sleep(5000);
				
						if(driver.findElements(By.xpath(".//*[@id='alert']/div")).size()!=0) 
						{
							logger.info("Member has limited mobility");
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
							
						}
						
						else
							
						{
							logger.info("Member has limited mobility------Not Found for this lead");
						}
						
						
						if(driver.findElements(By.xpath(".//*[@id='alert']/div")).size()!=0) 
						{
							logger.info("There is currently no phone number for this member. Please add one if available.");
								driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
							
						}
						
						else
						{
							logger.info("There is currently no phone number for this member. Please add one if available--------------Window not found for this lead");
						}
		
					
						
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='homephone_country_primary_input']")));
								
								
						//---------------------------------------------------------------------------------------------------------------------------------------------//
								
						//HomePhone Primary Lead
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							driver.findElement(By.xpath(".//*[@id='homephone_country_primary_input']")).click();	
								driver.findElement(By.xpath(".//*[@id='homephone_country_primary_select']/option[2]")).click();
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										driver.findElement(By.xpath(".//*[@id='homephone_formatted_primary_input']")).clear();
										
										int num5 = rand.nextInt(5) + 4; //Y = X+Y-1 //Generates from 4 to 8
										int num6 = rand.nextInt(3) + 5; 
										int num7 = rand.nextInt(4) + 6;
										int num8 = rand.nextInt(3) + 6;
										int num9 = rand.nextInt(2) + 5;
										int num10 = rand.nextInt(3)+ 5;
										
								        String PLHomePhone = num1+num5+num6+num7+num8+num9+num10;
								        
								        System.out.println("Primary Lead Home Phone: "+PLHomePhone);
											driver.findElement(By.xpath(".//*[@id='homephone_formatted_primary_input']")).sendKeys(PLHomePhone);
												logger.info("Entered PrimaryLead Home Phone");
												
						//WorkPhone	Primary Lead				
						driver.findElement(By.xpath(".//*[@id='workphone_country_primary_input']")).click();	
							driver.findElement(By.xpath(".//*[@id='workphone_country_primary_select']/option[1]")).click();
								
										
						//Mobile Phone Primary Lead
						driver.findElement(By.xpath(".//*[@id='mobilephone_country_primary_input']")).click();	
							driver.findElement(By.xpath(".//*[@id='mobilephone_country_primary_select']/option[1]")).click();
								
						//Fax Number Primary Lead
						driver.findElement(By.xpath(".//*[@id='fax_number_country_primary_input']")).click();	
							driver.findElement(By.xpath(".//*[@id='fax_number_country_primary_select']/option[1]")).click();
								
						//---------------------------------------------------------------------------------------------------------------------------------------------//
									
							//HomePhone Secondary Lead
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								driver.findElement(By.xpath(".//*[@id='homephone_country_secondary_input']")).click();	
									driver.findElement(By.xpath(".//*[@id='homephone_country_secondary_select']/option[1]")).click();	
									
							//WorkPhone	Secondary Lead				
							driver.findElement(By.xpath(".//*[@id='workphone_country_secondary_input']")).click();	
								driver.findElement(By.xpath(".//*[@id='workphone_country_secondary_select']/option[1]")).click();
									
							//MobilePhone Secondary Lead
							driver.findElement(By.xpath(".//*[@id='mobilephone_country_secondary_input']")).click();	
								driver.findElement(By.xpath(".//*[@id='mobilephone_country_secondary_select']/option[1]")).click();
			
							//FaxNumber Secondary Lead
							driver.findElement(By.xpath(".//*[@id='fax_number_country_secondary_input']")).click();	
								driver.findElement(By.xpath(".//*[@id='fax_number_country_secondary_select']/option[1]")).click();
			
						//---------------------------------------------------------------------------------------------------------------------------------------------//
									
						//Primary Email Address
						driver.findElement(By.xpath(".//*[@id='emailaddress_primary_input']")).clear();
							driver.findElement(By.xpath(".//*[@id='emailaddress_primary_input']")).sendKeys(PriEmail);
								logger.info("Entered Primary Email Address");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
								
						//Secondary Email Address
						driver.findElement(By.xpath(".//*[@id='emailaddress_secondary_input']")).clear();
							driver.findElement(By.xpath(".//*[@id='emailaddress_secondary_input']")).sendKeys(SecEmail);
								logger.info("Entered Secondary Email Address");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
									
						//Enter Address
						driver.findElement(By.xpath(".//*[@id='addressLine1Input']")).clear();
							driver.findElement(By.xpath(".//*[@id='addressLine1Input']")).sendKeys(Address);
								logger.info("Entered Address");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
							
								
						//Enter Postal Code.
						driver.findElement(By.xpath(".//*[@id='pvguest_postal_code']")).clear();
							driver.findElement(By.xpath(".//*[@id='pvguest_postal_code']")).sendKeys(Postal);
								logger.info("Entered Postal Code");
									driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
										driver.findElement(By.xpath(".//*[@id='pvguest_city']")).click(); 
								
						Thread.sleep(2000);
						
						//Click on Save 
						driver.findElement(By.xpath(".//*[@id='leadSaveButton']")).click();
							logger.info("Clicked on Save Button");
							
								/*//Verify Your Address Details
								if(driver.findElements(By.xpath(".//*[@id='address_closebtn']")).size()!=0) 
								{
									logger.info("Verify Your Address Details");
										driver.findElement(By.xpath(".//*[@id='address_closebtn']")).click();
									
								}
								
								else
								{
									logger.info("Verify Your Address Details--------------Window not found for this lead");
								}*/
					
						WebElement Updated = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='alert']/div/div[3]/div"))); // Wait till the element is present to click
							driver.findElement(By.xpath(".//*[@id='alert']/div/div[3]/div")).click();
								logger.info("Lead Updated Successfully" +Updated);
							
						driver.findElement(By.xpath(".//*[@id='leadBackButton']")).click();
							logger.info("Clicked on Back to Search");
								
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='searchSearchButton']")));
						 	logger.info("Updated LEAD" +Leadid);
							 	logger.info("--------------------------------------------------------------------------------------------------------------------------------------------");
								 
						 Cell cell = null; // declare a Cell object
							cell = worksheet.getRow(k).getCell(1);   // Access the second cell in second row to update the value   
								cell.setCellValue("Lead Updated");  // Get current cell value value and overwrite the value
									XSSFCellStyle style = workbook.createCellStyle();
										style.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 0))); //Highlights Yellow
										 	style.setFillPattern(CellStyle.SOLID_FOREGROUND);
										 		cell.setCellStyle(style);
										 			fileInputStream.close(); //Close the InputStream
										 				FileOutputStream output_file =new FileOutputStream(new File("H://Leadids.xlsx"));  //Open FileOutputStream to write updates			                  
										 					workbook.write(output_file); //write changes 
										 						output_file.close();  //close the stream   
  
				}
				
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
			}
		}
	}


