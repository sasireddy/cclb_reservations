package clarity_UpdateLead;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Atlas_SingleLead_AddPoints {

	Logger logger = Logger.getLogger(Atlas_Add_Points.class);
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 40);
	
	String allotment = "10000";
	String Leadid = "1230140";
	
	@BeforeTest
	public void before()
	{
		PropertyConfigurator.configure("Log4j.properties");
		
		//Sign in
		driver.get("http://ausstage.diamondresorts.com/stage/sec_pkg.sign_in");
		
		//driver.get("http://austest.diamondresorts.com/test/sec_pkg.sign_in");
		
		
		//Wait till Login Button displays 
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[7]/td/a/img")));
		
		//Username
		driver.findElement(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[5]/td[2]/input")).sendKeys("sreddy");
			logger.info("Entered Username");
		
		//Password
		driver.findElement(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[6]/td[2]/input")).sendKeys("Bhushan22");
			logger.info("Entered Password");
		
		//Click on Login Button
		driver.findElement(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[7]/td/a/img")).click();
			logger.info("Clicked on Login button");
			
		//Wait till all the buttons appears	
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='v0']/img")));
										 				
	}
	
	@Test
	public void AddPoints() {
		
		PropertyConfigurator.configure("Log4j.properties");

		//Club Button		
		driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/a/img")).click();
			logger.info("Clicked on Club Button");
			
		//Club Utilities Button			
		driver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td[2]/a/img")).click();
			logger.info("Clicked on Club utilities");
		
		//Point Adjustments	
		driver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td[2]/a/img")).click();
			logger.info("Clicked on Point Adjustments");
		
		//Wait till the search button is clickable
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td/a[2]/img"))); //wait till search button is visible
		
		//Enter Lead id
		driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td[2]/input")).sendKeys(Leadid);
			logger.info("Entered Lead id: " +Leadid);
		
		//Click on Search Button
		driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td/a[2]/img")).click();
		logger.info("Clicked on Search button");
						
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/table/tbody/tr[4]/td/table/tbody/tr[2]/td[1]/a"))); //wait till Search results
		
		//Click on the Search Results			
		driver.findElement(By.xpath("html/body/table/tbody/tr[4]/td/table/tbody/tr[2]/td[1]/a")).click();
		
		//Wait till Process button is clickable
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr/td[4]/a/img"))); 
		
		//Click on Adjustment amount text box
		driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input")).click();
		
		//Clear the Amount 
		driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input")).clear();
		
		//Enter the points 
		driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input")).sendKeys(allotment);
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//Select Transaction Type
		Select TransactionType = new Select(driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[4]/select"))); 
		TransactionType.selectByValue("AJD"); 
		logger.info("TransactionType Selected");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//Parent Mbr Id/User Contr Selected
		driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[2]/td/table/tbody/tr[5]/td[4]/select/option[2]")).click();
		logger.info("Parent Mbr Id/User Contr Selected");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//Adjustment Reason
		Select AdjustmentReason = new Select(driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[2]/td/table/tbody/tr[6]/td[4]/select"))); 
		AdjustmentReason.selectByValue("LSI"); 
		logger.info("Adjustment Reason Selected");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//Adjustment Remark
		driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[7]/td[4]/input")).sendKeys("Tester Tester");
		logger.info("Entered Adjustment Remark");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//Click on Process Button
		driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr/td[4]/a/img")).click();
		logger.info("Clicked on Process button");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.switchTo().alert().accept();
		
		
		//Wait till the search button appears in next screen
		if(driver.findElement(By.xpath(".//*[@id='headernavlist']/li[5]/a")).isDisplayed())
		{
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='headernavlist']/li[5]/a"))); //Wait till Search button is visible
		
		logger.info("Process Completed and successfully added 10000 points to: "+Leadid);
		logger.info("--------------------------------------------------------------------------------------------------------------------------------------------");
		}
		else
		{
			driver.switchTo().alert().accept();
		}
		
}
}
