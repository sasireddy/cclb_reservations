package clarity_UpdateLead;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Atlas_Add_Points {
	
	Logger logger = Logger.getLogger(Atlas_Add_Points.class);
	WebDriver driver = new FirefoxDriver();
	WebDriverWait wait = new WebDriverWait(driver, 40);
	
	String allotment = "10000";
	
	@BeforeTest
	public void before()
	{
		PropertyConfigurator.configure("Log4j.properties");
		
		//Sign in
		driver.get("http://ausstage.diamondresorts.com/stage/sec_pkg.sign_in");
		//driver.get("http://austest.diamondresorts.com/test/sec_pkg.sign_in");
		
		//Wait till Login Button displays 
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[7]/td/a/img")));
		
		//Username
		driver.findElement(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[5]/td[2]/input")).sendKeys("sreddy");
			logger.info("Entered Username");
		
		//Password
		driver.findElement(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[6]/td[2]/input")).sendKeys("Bhushan22");
			logger.info("Entered Password");
		
		//Click on Login Button
		driver.findElement(By.xpath("html/body/div[2]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[7]/td/a/img")).click();
			logger.info("Clicked on Login button");
			
		//Wait till all the buttons appears	
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='v0']/img")));
										 				
	}
	
	@SuppressWarnings("resource")
	@Test
	public void AddPoints() {
		
		PropertyConfigurator.configure("Log4j.properties");
				
			try {
					
					FileInputStream fileInputStream = new FileInputStream("H://AddPoints.xlsx");
					XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
					XSSFSheet worksheet = workbook.getSheet("AddPoints");
					
					int rowCount = worksheet.getLastRowNum()- worksheet.getFirstRowNum();
					
					logger.info(""+worksheet.getLastRowNum());
					
					logger.info(""+worksheet.getFirstRowNum());
					
					logger.info("Row count is" +rowCount);
					
					
					
					for (int k = 0; k <= rowCount; k++) {
						 
				        Row row = worksheet.getRow(k);
				        
				        String Leadid = new DataFormatter().formatCellValue(row.getCell(0));
				        		
				        		logger.info(Leadid);
				
						//Club Button		
						driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/a/img")).click();
							logger.info("Clicked on Club Button");
							
						//Club Utilities Button			
						driver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td[2]/a/img")).click();
							logger.info("Clicked on Club utilities");
						
						//Point Adjustments	
						driver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td[2]/a/img")).click();
							logger.info("Clicked on Point Adjustments");
						
						//Wait till the search button is clickable
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td/a[2]/img"))); //wait till search button is visible
						
						//Enter Lead id
						driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td[2]/input")).sendKeys(Leadid);
							logger.info("Entered Lead id: " +Leadid);
						
						//Click on Search Button
						driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td/a[2]/img")).click();
						logger.info("Clicked on Search button");
										
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/table/tbody/tr[4]/td/table/tbody/tr[2]/td[1]/a"))); //wait till Search results
						
						//Click on the Search Results			
						driver.findElement(By.xpath("html/body/table/tbody/tr[4]/td/table/tbody/tr[2]/td[1]/a")).click();
						
						//Wait till Process button is clickable
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr/td[4]/a/img"))); 
						
						//Click on Adjustment amount text box
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input")).click();
						
						//Clear the Amount 
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input")).clear();
						
						//Enter the points 
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input")).sendKeys(allotment);
						
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
						//Select Transaction Type
						Select TransactionType = new Select(driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td[4]/select"))); 
						TransactionType.selectByValue("AJD"); 
						logger.info("TransactionType Selected");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
						//Parent Mbr Id/User Contr Selected
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[2]/td/table/tbody/tr[5]/td[4]/select/option[2]")).click();
						logger.info("Parent Mbr Id/User Contr Selected");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
						//Adjustment Reason
						Select AdjustmentReason = new Select(driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[2]/td/table/tbody/tr[6]/td[4]/select"))); 
						AdjustmentReason.selectByValue("LSI"); 
						logger.info("Adjustment Reason Selected");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
						//Adjustment Remark
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[2]/td/table/tbody/tr[7]/td[4]/input")).sendKeys("Tester Tester");
						logger.info("Entered Adjustment Remark");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
						//Click on Process Button
						driver.findElement(By.xpath("html/body/table/tbody/tr/td/form/div/div/table/tbody/tr/td/div/table/tbody/tr[3]/td/div/table/tbody/tr/td[4]/a/img")).click();
						logger.info("Clicked on Process button");
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
						
						//Wait till the search button appears in next screen
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='headernavlist']/li[5]/a"))); //Wait till Search button is visible
						
						logger.info("Process Completed and successfully added 10000 points to: "+Leadid);
						logger.info("--------------------------------------------------------------------------------------------------------------------------------------------");
						
						//Click on Home Button
						driver.findElement(By.xpath(".//*[@id='headernavlist']/li[5]/a")).click();
				 
						Cell cell = null; // declare a Cell object

				        cell = worksheet.getRow(k).getCell(1);   // Access the second cell in second row to update the value
				          
				        cell.setCellValue("Lead Updated with 10000 points");  // Get current cell value value and overwrite the value
				        
				        XSSFCellStyle style = workbook.createCellStyle();
				        style.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 0))); //Highlights Yellow
				        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				        cell.setCellStyle(style);
				
				        fileInputStream.close(); //Close the InputStream
				         
				        FileOutputStream output_file =new FileOutputStream(new File("H://AddPoints.xlsx"));  //Open FileOutputStream to write updates
				          
				        workbook.write(output_file); //write changes
				          
				        output_file.close();  //close the stream   
			
			    
				}

			} catch (FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
}
}
}