package clarity_Search_Reservation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A_Search_Common {

	static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 100);
	
	String Leadid1 = "1866329";  // CS1 & GLD //Add Points to present year

	//-----------------------------------------------------------------Book New Reservation------------------------------------------------------------//
	//Book New Reservation
	String BookingTypeReservation = "10194"; //Home Standard Reservation Value
	String BookTypeReserName = "HOME-STANDARD RESERVATION";
	
	//-----------------------------------------------------------------Search for Availability------------------------------------------------------------//
	//Search for Availability
	String SAProperty = "AAB"; //Property as A C C at Anantara Bangkok Sathorn
	String SAArrivalDateMethod = "FlexDays"; //Arrival Date Method as Flexible Days
	String SAArrivalDate = "t+100"; //Arrival Date 
	String SANights = "2";
	String SAAdults = "2";
	//-----------------------------------------------------------------Availability Search Request----------------------------------------------------------//
	String AvailabilityResultsHeader = "Availability Results";
	String AvailabilitySearchReqHeader = "Availability Search Request";
	String EarliestArrivalDate = "t+60"; //Availability Search Request Earilest Arrival Date
	String LatestArrivalDate = "t+70"; //Availability Search Request Latest Arrival Date
	String AvailSeaReqChoicesAvail = "Availability Search Request - Choices Available"; //Availability Search Request - Choices Available Header
	String CurSeaReqHeaader = "Current Search Requests";
	String PastSeaReqHeader = "Past Search Requests";

}

