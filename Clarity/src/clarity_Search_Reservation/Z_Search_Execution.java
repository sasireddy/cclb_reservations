package clarity_Search_Reservation;


import org.testng.annotations.Test;



public class Z_Search_Execution {
	
	//ClubReservation Reservations = new ClubReservation();

	B_Search_Login Search_Login = new B_Search_Login();
	C_Search_Customer360_LeadInfo Search_Leadinfo = new C_Search_Customer360_LeadInfo();
	D_Search_Reservation Search_Reservation = new D_Search_Reservation();
	E_Search_ClubRes Search_ClubRes = new E_Search_ClubRes();
	F_Search_BookReservation Search_BookRes = new F_Search_BookReservation();

	@Test
	public void Search_Reservation()
	{
		Search_Login.openBrowser();
		Search_Leadinfo.Customer360();
		Search_Leadinfo.Lead_Information();
		Search_Reservation.Reservations();
		Search_ClubRes.ClubRes();
		Search_BookRes.BookReservation();
		Search_BookRes.Availability_Search_Request_ChoicesAvailable();
		
		
	}

}

