package clarity_AlternateGuest_Reservation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class E_AlternateGuest_ClubRes extends A_AlternateGuest_Common {

Logger logger = Logger.getLogger("E_AlternateGuest_ClubRes.class");
	
	//-----------------------------------Guest----------------------------//
	public String ClubResLeadNum;//Lead Number
	public String Lead_Id; //Used inside try method to retrieve LILeadid from Leadid.Properties
	
	//-----------------------------------Membership Detail for:----------------------------//
	public String ClubResMemDetail; //Membership Detail for: details
	public String ClubResMemDetailfor; //Substring of ClubResMemDetail
	
	//-----------------------------------Usage Information----------------------------//
	//Usage Information
		public String ClubReservationspnt; //Get Present Year Points 
		public String ClubRespoints; //Replace "," with "" from Present Year Points
		public String ClubResAllotment; // Present Year Allotment 
		public String ClubResReservations; //Present Year Points Available for Reservations
		public String ClubResMemBen; //Present Year Available for MemberBenefits
		public String ClubResMemBenfFlagHeader; //Avilable for Member Benefits Flag Header
		public String ClubResSave; //Present Year Available to save to follwoing yr
		public String ClubResPntSvFlagHeader; //Present Year Available to Save to following year
	
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
	
	public void ClubRes() {
		 
		 PropertyConfigurator.configure("Log4j.properties");
			
		 logger.info("--------------------------------------------------------------------------Club Reservation---------------------------------------------------------------------------------");
		 
		 //Get Leadid from Alternate_Res_Leadid.Properties
		 try (FileReader reader = new FileReader("Alternate_Res_Leadid.Properties")) {
			 Properties properties = new Properties();
	      		properties.load(reader);
	      		
	      		Lead_Id = properties.getProperty("LILeadid");
	      		logger.info(Lead_Id);
	      	} catch (IOException e) {
	      		e.printStackTrace();
	      	}
			
		 	if(driver.findElements(By.xpath("/html/body/div[9]/div/div/div[3]")).size()!=0) 
			{
				logger.info("Cliked on Notice---Notice--Notice--- Close Button");
					driver.findElement(By.xpath("/html/body/div[9]/div/div/div[3]")).click();
				
			}
			
			else
			{
				logger.info("---Notice---Notice---Notice---Window not found");
			}

			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
	//-----------------------------------------------------------------------GUEST------------------------------------------------------------------------------------------//
			
			logger.info("-------------------------------------------------------------------Guest------------------------------------------------------------");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='guestypeimg']"))); // Wait till the element LEAD TYPE FLAG is present to click
			
			//Lead Number / Compared
			ClubResLeadNum = driver.findElement(By.xpath(".//*[@id='pv1guest_lead']/a")).getText();
				logger.info("Lead Area-Lead Id: " +ClubResLeadNum);
					Assert.assertEquals(Lead_Id, ClubResLeadNum);
						logger.info("Lead Number in Lead Information Tab Matched with Club Reservations");
				
		//-----------------------------------------------------------------------Membership Detail------------------------------------------------------------------------------------------//
											
			logger.info("-------------------------------------------------------------------Membership Detail------------------------------------------------------------");
			
						
			//Details			
			ClubResMemDetail = driver.findElement(By.xpath(".//*[@id='tdMembership']")).getText(); //Membership Detail For:
				logger.info("Membership Details for: "+ClubResMemDetail);
				
		//-----------------------------------------------------------------------Usage Information------------------------------------------------------------------------------------------//
			
				logger.info("-------------------------------------------------------------------Usage Information------------------------------------------------------------");
				
				//--------------------------------------Points-------------------------------------//
					//Store Points in Alternate_Reservation.Properties file
					try {
						
						ClubResMemDetailfor = ClubResMemDetail.substring(0,6); //Membership Detail for subString
							logger.info("Membership Number: "+ClubResMemDetailfor);
							
				 		 ClubReservationspnt = driver.findElement(By.xpath(".//*[@id='mbrRemBan']")).getText(); //Get Present Year Points 
							ClubRespoints = ClubReservationspnt.replace(",",""); //Replace , with nothing
						 
					 	Properties properties = new Properties();
					 		properties.setProperty("ClubReservationspnt", ClubReservationspnt);
					 			properties.setProperty("ClubRespoints",ClubRespoints);
			 						properties.setProperty("MembershipNumber", ClubResMemDetailfor); //Membership Detail for:
						

						File file = new File("Alternate_Reservation.Properties");
						FileOutputStream fileOut = new FileOutputStream(file);
						properties.store(fileOut, "Available Points");
						fileOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				//Present Year points
				ClubResAllotment = driver.findElement(By.xpath(".//*[@id='pv1MbrAlt']")).getText();
					logger.info("Allotment for Present Year: "+ClubResAllotment);
					
						//Present Year Avail for Reservations
						ClubResReservations = ClubReservationspnt;
								logger.info("Present Year Available for Reservations: "+ClubResReservations);
						
							//Present Year Avail for Member Benefits
							ClubResMemBen = driver.findElement(By.xpath(".//*[@id='mbrAvaBenefit']")).getText();
								logger.info("Present Year Available for Member Benefits: "+ClubResMemBen);
						
								
								//Present Year Avail to save to following yr 
								ClubResSave = driver.findElement(By.xpath(".//*[@id='mbrAvaSave']")).getText();
									logger.info("Present Year Available to Save to Following yr: "+ClubResSave);
									
								
								//Room Upgrades Used/ Remaining:
								String CRRoomUpgrade = driver.findElement(By.xpath(".//*[@id='mbrRoomUpd']")).getText();
									logger.info("Room Upgrades: "+CRRoomUpgrade);
										String RoomUpgrade = CRRoomUpgrade.substring(2);
											String RmUpgrade = CRRoomUpgrade.substring(0,1);
											
											int CRRoomUpgrade1 = Integer.parseInt(RoomUpgrade);
												int CRRoomUpgrade2 = Integer.parseInt(RmUpgrade);
												
												if(CRRoomUpgrade2 < CRRoomUpgrade1)
												{
													logger.info("Room Upgrade is Available");
												}
												
												else
												{
													logger.info("Room Upgrade is not available");
													
												}
								

	 }
}