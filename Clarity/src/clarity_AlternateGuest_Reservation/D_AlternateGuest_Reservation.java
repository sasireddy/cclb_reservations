package clarity_AlternateGuest_Reservation;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.testng.Assert;

public class D_AlternateGuest_Reservation extends A_AlternateGuest_Common {
	
Logger logger = Logger.getLogger("D_AlternateGuest_Reservation.class");
	
	public String winHandleBefore; //Store Current Window Handle
	public String Lead_Id; //Used inside try method to retrieve LILeadid from Alternate_Res_Leadid.Properties
	public String ResLeadId; //Reservation LeadId
	
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//	
		public void window()
		{
			// Store the current window handle
			 winHandleBefore = driver.getWindowHandle();
		
		}
		
			
		public void Reservations() {
			
			PropertyConfigurator.configure("Log4j.properties");
		
			 try (FileReader reader = new FileReader("Alternate_Res_Leadid.Properties")) {
				 Properties properties = new Properties();
		      		properties.load(reader);
		      		
		      		Lead_Id = properties.getProperty("LILeadid");
		      		logger.info(Lead_Id);
		      	} catch (IOException e) {
		      		e.printStackTrace();
		      	}
			 
				 
				 
				 logger.info("---------------------------------------------Reservations---------------------------------------------------------------------------------");
				 
				 driver.findElement(By.xpath("/html/body/div/div[2]/div[6]/div/div")).click(); // Click on reservations tab
				 		logger.info("clicked on Reservations Tab");
				 			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				 						
							//Capture Leadid
								String ResLeadId = driver.findElement(By.xpath(".//*[@id='travelDiv']/div[1]/table/tbody/tr/td[4]/span")).getText();
															logger.info("Lead Number in Reservations Tab: "+ResLeadId);
																logger.info("From Lead Information TAB "+Lead_Id);
																	Assert.assertEquals(ResLeadId, Lead_Id);
																		logger.info("Lead Number in Lead Information Tab Matched in Reservations Tab");

							//Opens New Window
							driver.findElement(By.xpath("/html/body/div/div[2]/div[9]/div[5]/table/tbody/tr/td[3]/div")).click(); //Clicked on Club Res
						 		logger.info("clicked on Club Res");
					 				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

							// Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}
			}

}
