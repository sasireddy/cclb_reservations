package clarity_AlternateGuest_Reservation;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A_AlternateGuest_Common {

	protected static WebDriver driver = new FirefoxDriver();
	Calendar cal = Calendar.getInstance();
	String Date = new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
	String Day= new SimpleDateFormat("d").format(cal.getTime());	
	WebDriverWait wait = new WebDriverWait(driver, 100);
	
	String Leadid1 = "4572092";  // PVC - Add Guest Certificate Fee Row from Atlas
	
	String AlternateLeadArea = "9";
	String AlternateLead = "475800"; //Alternate Lead
	

	//-----------------------------------------------------------------Book New Reservation------------------------------------------------------------//
	//Book New Reservation
	String BookingTypeReservation = "10194"; //Home Standard Reservation Value
	String BookTypeReserName = "HOME-STANDARD RESERVATION";
	
	//-----------------------------------------------------------------Search for Availability------------------------------------------------------------//
	//Search for Availability
	String SARegion = "AZ"; //Region as Arizona 
	String SAProperty = "BRI"; //Property as Bell Rock Inn
	String SAArrivalDateMethod = "FlexDays"; //Arrival Date Method as Flexible Days 
	String SAArrivalDate = "t+2"; //Arrival Date 
	String SANights = "2";
	String SAAdults = "2";
	
	//-----------------------------------------------------------------Resort Availability - CLUB------------------------------------------------------------//
	//Resort Availability - CLUB
	String ResortType = "1XX"; //Resort Type SubString
	
	//-----------------------------------------------------------------Additional Resort Information (Errata)------------------------------------------------------------//
	//Additional Resort Information (Errata)
	String ErrataHeader = "Additional Resort Information (Errata)"; //
	
	//-----------------------------------------------------------------Room Upgrade------------------------------------------------------------//
	//Room Upgrade
	String RoomUpgrade1 = "Room Upgrade";
	
	//-----------------------------------------------------------------Discount------------------------------------------------------------//
	//Discount
	String DiscountHeader = "Discount";
	String DisMesAvail = "Member has sufficient points to book this reservation.";
	String DisMesNotAvail = "Member does not have sufficient points to book this reservation.";
	String DiscountPercentage = "25";
	
	//-----------------------------------------------------------------Method of Payments------------------------------------------------------------//
	//Method of Payments
	String MOPBalPointsNd = "0"; //Balance of Points needed
	
	//-----------------------------------------------------------------Club Reservations------------------------------------------------------------//
	//Guests
	String AlternateGuest = "ALT";
	
	//Reservation and Misc Fees
	String CRReserMiscFeesHeader = "Reservation and Misc Fees"; //Reservation and Misc Fees Flag Header
		String CRPayInfoHeader = "Payment Information";
			String CreditCardMainHeader = "Credit Card Maintenance";
				String CCNumber = "4321000000001119";
					String CCType = "Visa";
						String CCName = "Diamond Tester";
							String ExpMonth = "01";
								String ExpYr = "20";

	//-----------------------------------------------------------------Reservation Confirmation------------------------------------------------------------//
		
	String ResConfirmationHeader = "Reservation Confirmation"; //Reservation Confirmation Header
	String RCPnSumHeader = "Points Summary";
	String RCConfirmLetterHeader = "Confirmation Letter";

}
