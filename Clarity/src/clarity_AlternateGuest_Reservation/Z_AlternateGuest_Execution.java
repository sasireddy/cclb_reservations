
package clarity_AlternateGuest_Reservation;

import org.testng.annotations.Test;


public class Z_AlternateGuest_Execution {

	B_AlternateGuest_Login AlternateGuest_Login = new B_AlternateGuest_Login();
	C_AlternateGuest_Customer360_LeadInfo AlternateGuest_Leadinfo = new C_AlternateGuest_Customer360_LeadInfo();
	D_AlternateGuest_Reservation AlternateGuest_Reservation = new D_AlternateGuest_Reservation();
	E_AlternateGuest_ClubRes AlternateGuest_ClubRes = new E_AlternateGuest_ClubRes();
	F_AlternateGuest_BookReservation AlternateGuest_BookRes = new F_AlternateGuest_BookReservation();
	G_AlternateGuest_MOPPayments AlternateGuest_MOP = new G_AlternateGuest_MOPPayments();
	H_AlternateGuest_ClubReservations AlternateGuest_Reserve = new H_AlternateGuest_ClubReservations();
	I_AlternateGuest_FinalConfirmation AlternateGuest_ResConfirm = new I_AlternateGuest_FinalConfirmation();
	
	@Test
	public void AlternateGuest_Reservation()
	{
		AlternateGuest_Login.openBrowser();
		AlternateGuest_Leadinfo.Customer360();
		AlternateGuest_Leadinfo.Lead_Information();
		AlternateGuest_Reservation.Reservations();
		AlternateGuest_ClubRes.ClubRes();
		AlternateGuest_BookRes.BookReservation();
		AlternateGuest_BookRes.Resort_Availability();
		AlternateGuest_BookRes.RoomUpgrade_No();
		AlternateGuest_BookRes.Errata();
		AlternateGuest_BookRes.Discount_No();
		AlternateGuest_MOP.MethodofPaymentOptions();
		AlternateGuest_Reserve.Reservation();
		AlternateGuest_ResConfirm.Reservation_Confirmation();
		
	}

}
